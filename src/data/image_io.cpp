#include "magmadnn/data/image_io.h"
#include "tensor/tensor.h"

#include <stdexcept>
#include <string>

namespace magmadnn {
namespace data {

#if defined(MAGMADNN_HAVE_OPENCV)
cv::Mat cv_read_image(const std::string &filename, const int height, const int width, const bool is_color) {
    cv::Mat cv_img;
    // IMREAD_COLOR: Always convert image to the 3 channel BGR color image.
    // IMREAD_GRAYSCALE: Always convert image to the single channel
    // grayscale image.
    int cv_read_flag = (is_color ? cv::IMREAD_COLOR : cv::IMREAD_COLOR); //TODO maybe a bug
    cv::Mat cv_img_origin = cv::imread(filename, cv_read_flag);
    if (!cv_img_origin.data) {
        // throw std::runtime_error(
        // "Could NOT open image file: " + filename);
        return cv_img_origin;
    }
    if (height > 0 && width > 0) {
        cv::resize(cv_img_origin, cv_img, cv::Size(width, height));
    } else {
        cv_img = cv_img_origin;
    }
    return cv_img;
}
#endif

// Read image whose path is set in `filename` and add the
// corresponding to the image pixels to the 4D tensor `images_tensor` at
// `data_idx` index. The output image size can be specified with
// `height` and `width` parameters. The output will be resized according
// to the 'height' and 'width' parameters.
template <typename T>
void add_image_to_tensor(const std::string &filename, const int height, const int width, const bool is_color,
                         magmadnn::Tensor<T> *images_tensor, unsigned int idx) {
#if defined(MAGMADNN_HAVE_OPENCV)

    // auto read_image_sa = std::chrono::high_resolution_clock::now();
    cv::Mat cv_img = cv_read_image(filename, height, width, is_color);
    // auto read_image_en = std::chrono::high_resolution_clock::now();
    // double t_read_image = std::chrono::duration<double>(
    // read_image_en-read_image_sa).count();
    // std::cout << "Read image time (s) = " << t_read_image << std::endl;

    if (cv_img.data) {
        unsigned int nchannels = cv_img.channels();
        unsigned int nrows = cv_img.rows;
        unsigned int ncols = cv_img.cols;

        // std::cout << "[add_image_to_tensor] nrows = " << nrows << ", ncols = " << ncols << "nchannels = " <<
        // nchannels << std::endl;

        // auto set_tensor_sa = std::chrono::high_resolution_clock::now();
        for (unsigned int row = 0; row < nrows; ++row) {
            const uchar *ptr = cv_img.ptr<uchar>(row);
            int img_index = 0;
            for (unsigned int col = 0; col < ncols; ++col) {
                for (unsigned int ch = 0; ch < nchannels; ++ch) {
                    T val = static_cast<T>(ptr[img_index]);
                    val = val / 255.0f;

                    images_tensor->set({idx, ch, row, col}, val);
                    img_index++;
                }
            }
        }
    }

#else
    throw std::runtime_error("OpenCV must be enabled to read images");
#endif
}

template void add_image_to_tensor<float>(const std::string &filename, const int height, const int width,
                                         const bool is_color, magmadnn::Tensor<float> *images_tensor,
                                         unsigned int image_idx);

// Read trimap whose path is set in `filename` and add the
// corresponding to the image pixels to the 3D tensor `images_tensor` at
// `data_idx` index. The output image size can be specified with
// `height` and `width` parameters. The output will be resized according
// to the 'height' and 'width' parameters.
template <typename T>
void add_trimap_to_tensor(const std::string &filename, const int height, const int width, const bool is_color,
                          magmadnn::Tensor<T> *images_tensor, unsigned int idx) {
#if defined(MAGMADNN_HAVE_OPENCV)

    // auto read_image_sa = std::chrono::high_resolution_clock::now();
    cv::Mat cv_img = cv_read_image(filename, height, width, is_color);
    // auto read_image_en = std::chrono::high_resolution_clock::now();
    // double t_read_image = std::chrono::duration<double>(
    // read_image_en-read_image_sa).count();
    // std::cout << "Read image time (s) = " << t_read_image << std::endl;

    if (cv_img.data) {
        unsigned int nchannels = cv_img.channels();
        unsigned int nrows = cv_img.rows;
        unsigned int ncols = cv_img.cols;

        // std::cout << "[add_image_to_tensor] nrows = " << nrows << ", ncols = " << ncols << "nchannels = " <<
        // nchannels << std::endl;

        // auto set_tensor_sa = std::chrono::high_resolution_clock::now();
        for (unsigned int row = 0; row < nrows; ++row) {
            const uchar *ptr = cv_img.ptr<uchar>(row);
            int img_index = 0;
            for (unsigned int col = 0; col < ncols; ++col) {
                for (unsigned int ch = 0; ch < nchannels; ++ch) {
                    T val = static_cast<T>(ptr[img_index]);
                    if (ch == 0) {
                        images_tensor->set({idx, unsigned(0), row, col}, val);
                    }
                    img_index++;
                }
            }
        }
    }

#else
    throw std::runtime_error("OpenCV must be enabled to read images");
#endif
}
template void add_trimap_to_tensor<float>(const std::string &filename, const int height, const int width,
                                          const bool is_color, magmadnn::Tensor<float> *images_tensor,
                                          unsigned int image_idx);

// http://www.64lines.com/jpeg-width-height
// Gets the JPEG size from the array of data passed to the function,
// file reference: http://www.obrador.com/essentialjpeg/headerinfo.htm
bool get_jpeg_size(const uint8_t *data, uint32_t data_size, int64_t *width, int64_t *height) {
    // Check for valid JPEG image
    uint32_t i = 0;  // Keeps track of the position within the file
    if (data[i] == 0xFF && data[i + 1] == 0xD8 && data[i + 2] == 0xFF && data[i + 3] == 0xE0) {
        i += 4;
        // Check for valid JPEG header (null terminated JFIF)
        if (data[i + 2] == 'J' && data[i + 3] == 'F' && data[i + 4] == 'I' && data[i + 5] == 'F' &&
            data[i + 6] == 0x00) {
            // Retrieve the block length of the first block since
            // the first block will not contain the size of file
            uint16_t block_length = data[i] * 256 + data[i + 1];
            while (i < data_size) {
                i += block_length;                  // Increase the file index to get to the next block
                if (i >= data_size) return false;   // Check to protect against segmentation faults
                if (data[i] != 0xFF) return false;  // Check that we are truly at the start of another block
                uint8_t m = data[i + 1];
                if (m == 0xC0 || (m >= 0xC1 && m <= 0xCF && m != 0xC4 && m != 0xC8 && m != 0xCC)) {
                    // 0xFFC0 is the "Start of frame" marker which contains the file size
                    // The structure of the 0xFFC0 block is quite simple
                    // [0xFFC0][ushort length][uchar precision][ushort x][ushort y]
                    *height = data[i + 5] * 256 + data[i + 6];
                    *width = data[i + 7] * 256 + data[i + 8];
                    return true;
                } else {
                    i += 2;                                      // Skip the block marker
                    block_length = data[i] * 256 + data[i + 1];  // Go to the next block
                }
            }
            return false;  // If this point is reached then no size was found
        } else {
            return false;  // Not a valid JFIF string
        }
    } else {
        return false;  // Not a valid SOI header
    }
}

// Output 3D 'image_tensor' at 'data_idx' index with 'height' and 'width'
// to the path that is provided in 'filename'. Specifically made for trimap
template <typename T>
void tensor_to_image_3d(const std::string &filename, const int height, const int width, const bool is_color,
                        magmadnn::Tensor<T> *images_tensor, unsigned int idx) {
#if defined(MAGMADNN_HAVE_OPENCV)
    cv::Mat output_img(height, width, CV_32FC3);

    auto images_tensor_CPU = new Tensor<T>({images_tensor->get_shape()[0], images_tensor->get_shape()[1],
                                            images_tensor->get_shape()[2], images_tensor->get_shape()[3]},
                                           {ZERO, {}}, HOST);
    images_tensor_CPU->copy_from(*images_tensor);
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            output_img.at<cv::Vec<float, 3>>(i, j)[0] = (images_tensor_CPU->get({int(idx), 0, i, j}));
            output_img.at<cv::Vec<float, 3>>(i, j)[1] = (images_tensor_CPU->get({int(idx), 0, i, j}));
            output_img.at<cv::Vec<float, 3>>(i, j)[2] = (images_tensor_CPU->get({int(idx), 0, i, j}));
        }
    }
    cv::imwrite(filename, output_img * 255);
    std::cout << "Image output success" << std::endl;
#else
    throw std::runtime_error("OpenCV must be enabled to read images");
#endif
}

template void tensor_to_image_3d<float>(const std::string &filename, const int height, const int width,
                                        const bool is_color, magmadnn::Tensor<float> *images_tensor,
                                        unsigned int image_idx);
// Output 3D 'image_tensor' at 'data_idx' index with 'height' and 'width'
// to the path that is provided in 'filename'.
template <typename T>
void tensor_to_image_4d(const std::string &filename, const int height, const int width, const bool is_color,
                        magmadnn::Tensor<T> *images_tensor, unsigned int idx) {
#if defined(MAGMADNN_HAVE_OPENCV)
    cv::Mat output_img(height, width, CV_32FC3);
    auto images_tensor_CPU = new Tensor<T>({images_tensor->get_shape()[0], images_tensor->get_shape()[1],
                                            images_tensor->get_shape()[2], images_tensor->get_shape()[3]},
                                           {ZERO, {}}, HOST);
    images_tensor_CPU->copy_from(*images_tensor);
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            output_img.at<cv::Vec<float, 3>>(i, j)[0] = (images_tensor_CPU->get({int(idx), 0, i, j}));
            output_img.at<cv::Vec<float, 3>>(i, j)[1] = (images_tensor_CPU->get({int(idx), 1, i, j}));
            output_img.at<cv::Vec<float, 3>>(i, j)[2] = (images_tensor_CPU->get({int(idx), 2, i, j}));
        }
    }
    cv::imwrite(filename, output_img * 255);
    std::cout << "Image output success" << std::endl;
#else
    throw std::runtime_error("OpenCV must be enabled to read images");
#endif
}

template void tensor_to_image_4d<float>(const std::string &filename, const int height, const int width,
                                        const bool is_color, magmadnn::Tensor<float> *images_tensor,
                                        unsigned int image_idx);
}  // namespace data
}  // namespace magmadnn

// End of namespace magmadnn::data
