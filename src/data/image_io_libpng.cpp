/**
 * @file image_io_libpng.cpp
 * @author Eric Zhao
 * @author Stephen Qiu
 * @version 1.4
 * @date 2022-12-21
 *
 * @copyright Copyright (c) 2019
 */

#include "magmadnn/data/image_io_libpng.h"
#include "tensor/tensor.h"

#include <string>
#include <stdexcept>

namespace magmadnn {
namespace data {

png_infop read_png(const char *filename) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        throw std::runtime_error("Cannot open file\n");
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop info_ptr = png_create_info_struct(png_ptr);
    png_init_io(png_ptr, fp);
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
    png_destroy_read_struct(&png_ptr, NULL, NULL);
    fclose(fp);

    return info_ptr;
}

void write_png(const char *filename, int width, int height, float *buffer, bool is_trimap) {

    FILE *fp = NULL;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    png_bytep row = NULL;

    // Open file for writing (binary mode)
    fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        goto finalise;
    }

    // Initialize write structure
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fprintf(stderr, "Could not allocate write struct\n");
        goto finalise;
    }

    // Initialize info structure
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fprintf(stderr, "Could not allocate info struct\n");
        goto finalise;
    }

    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during png creation\n");
        goto finalise;
    }

    png_init_io(png_ptr, fp);
    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
                 PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    row = (png_bytep) malloc(3 * width * sizeof(png_byte));
    // Write image data
    int x, y;
    if (is_trimap) {
        for (y = 0; y < height; y++) {
            for (x = 0; x < width; x++) {
                row[x * 3] = buffer[y * width + x] * 255;
                row[x * 3 + 1] = buffer[y * width + x] * 255;
                row[x * 3 + 2] = buffer[y * width + x] * 255;
            }
            png_write_row(png_ptr, row);
        }
    } else {
        for (y = 0; y < height; y++) {
            for (x = 0; x < width; x++) {
                row[x * 3 + 2] = buffer[y * width + x] * 255;
                row[x * 3 + 1] = buffer[height * width + y * width + x] * 255;
                row[x * 3] = buffer[height * width * 2 + y * width + x] * 255;
            }
            png_write_row(png_ptr, row);
        }
    }

    // End write
    png_write_end(png_ptr, NULL);

finalise:
    if (fp != NULL) fclose(fp);
    if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp) NULL);
    if (row != NULL) free(row);
}

// Read image whose path is set in `filename` and add the
// corresponding to the image pixels to the tensor `images_tensor` at
// `data_idx` index. the height and width are predetermined by the size of the 
// image tensor. 'trimap' determines the number of channels, with true only
// storing the values to one channel and does not divide the values by 255, mainly
// for segmentation masks
// there is not functionality for image resizing currently
template <typename T>
void add_png_to_tensor(const char *filename, const bool trimap, magmadnn::Tensor<T> *images_tensor, unsigned int idx) {
    
    // readpng struct
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    // the image
    png_infop info_ptr = read_png(filename);


    unsigned int nchannels = png_get_channels(png_ptr, info_ptr);
    unsigned int nrows = png_get_image_height(png_ptr, info_ptr);
    unsigned int ncols = png_get_image_width(png_ptr, info_ptr);

printf("rows %d, cols, %d, chan %d\n", nrows, ncols, nchannels);

    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);

    for (unsigned int row = 0; row < nrows; ++row) {
        const auto *ptr = row_pointers[row];

        int img_index = 0;
        for (unsigned int col = 0; col < ncols; ++col) {
            for (unsigned int ch = 0; ch < nchannels; ++ch) {
                T val = static_cast<T>(ptr[img_index]);
                if (!trimap) val = val / 255.0f;

                if (!trimap)
                    images_tensor->set({idx, ch, row, col}, val);
                else
                    images_tensor->set({idx, unsigned(0), row, col}, val);
                img_index++;
            }
        }
    }
    // free
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
}

template void add_png_to_tensor<float>(const char *filename, const bool trimap, magmadnn::Tensor<float> *images_tensor,
                                       unsigned int image_idx);

// stores the values in the tensor into a png file with name 'filename'
// 'image_idx' determines which image to store based off the height and width of the tensor values.
// 'trimap' determines how the values are output and should be the same as how the values were read in. mainly for segmentation masks.
// tensor has to be in HOST for the code to be able to run
 
template <typename T>
void tensor_to_png(const char *filename, const int height, const int width, magmadnn::Tensor<T> *images_tensor,
                   unsigned int idx, bool is_trimap) {
    write_png(filename, width, height, images_tensor->get_ptr()+idx*height*width, is_trimap);
}

template void tensor_to_png<float>(const char *filename, const int height, const int width,
                                   magmadnn::Tensor<float> *images_tensor, unsigned int image_idx, bool is_trimap);
}  // namespace data
}  // namespace magmadnn