/**
 * @file image_io_libjpeg.cpp
 * @author Eric Zhao
 * @author Stephen Qiu
 * @version 1.4
 * @date 2022-12-21
 *
 * @copyright Copyright (c) 2019
 */

#include "magmadnn/data/image_io_libjpeg.h"
#include "tensor/tensor.h"

#include <stdexcept>
#include <string>

namespace magmadnn {
namespace data {

struct imgRawImage *read_jpeg(const char *lpFilename) {
    struct jpeg_decompress_struct info;
    struct jpeg_error_mgr err;

    struct imgRawImage *lpNewImage;

    unsigned long int imgWidth, imgHeight;
    int numComponents;

    unsigned long int dwBufferBytes;
    unsigned char *lpData;

    unsigned char *lpRowBuffer[1];

    FILE *fHandle;

    fHandle = fopen(lpFilename, "rb");
    if (fHandle == NULL) {
#ifdef DEBUG
        fprintf(stderr, "%s:%u: Failed to read file %s\n", __FILE__, __LINE__, lpFilename);
#endif
        return NULL; /* ToDo */
    }

    info.err = jpeg_std_error(&err);
    jpeg_create_decompress(&info);

    jpeg_stdio_src(&info, fHandle);
    jpeg_read_header(&info, TRUE);

    jpeg_start_decompress(&info);
    imgWidth = info.output_width;
    imgHeight = info.output_height;
    numComponents = info.num_components;

#ifdef DEBUG
    fprintf(stderr, "%s:%u: Reading JPEG with dimensions %lu x %lu and %u components\n", __FILE__, __LINE__, imgWidth,
            imgHeight, numComponents);
#endif

    dwBufferBytes = imgWidth * imgHeight * 3; /* We only read RGB, not A */
    lpData = (unsigned char *) malloc(sizeof(unsigned char) * dwBufferBytes);

    lpNewImage = (struct imgRawImage *) malloc(sizeof(struct imgRawImage));
    lpNewImage->numComponents = numComponents;
    lpNewImage->width = imgWidth;
    lpNewImage->height = imgHeight;
    lpNewImage->lpData = lpData;

    /* Read scanline by scanline */
    while (info.output_scanline < info.output_height) {
        lpRowBuffer[0] = (unsigned char *) (&lpData[3 * info.output_width * info.output_scanline]);
        jpeg_read_scanlines(&info, lpRowBuffer, 1);
    }

    jpeg_finish_decompress(&info);
    jpeg_destroy_decompress(&info);
    fclose(fHandle);

    return lpNewImage;
}

void write_jpeg(const char *lpFilename, unsigned int width, unsigned int height, float *buffer, bool is_trimap) {
    struct jpeg_compress_struct info;
    struct jpeg_error_mgr err;

    unsigned char *lpRowBuffer[1];

    FILE *fHandle;

    fHandle = fopen(lpFilename, "wb");
    if (fHandle == NULL) {
#ifdef DEBUG
        fprintf(stderr, "%s:%u Failed to open output file %s\n", __FILE__, __LINE__, lpFilename);
#endif
        return;
    }

    info.err = jpeg_std_error(&err);
    jpeg_create_compress(&info);

    jpeg_stdio_dest(&info, fHandle);

    info.image_width = width;
    info.image_height = height;
    info.input_components = 3;
    info.in_color_space = JCS_RGB;

    jpeg_set_defaults(&info);
    jpeg_set_quality(&info, 100, TRUE);

    jpeg_start_compress(&info, TRUE);

    /* Write every scanline ... */
    for (unsigned int i = 0; i < width * height * 3; i++) {
        buffer[i] *= 255;
    }
    unsigned char lpData[width * height * 3];

    for (unsigned int i = 0; i < height * width; i++) {
        for (int j = 0; j < 3; j++) {
            int tmp = buffer[j * height * width + i];
            lpData[i * 3 + j] = tmp;
        }
    }

    while (info.next_scanline < info.image_height) {
        lpRowBuffer[0] = &(lpData[info.next_scanline * (width * 3)]);
        jpeg_write_scanlines(&info, lpRowBuffer, 1);
    }

    jpeg_finish_compress(&info);
    fclose(fHandle);

    jpeg_destroy_compress(&info);
    return;
}

template <typename T>
void add_jpeg_to_tensor(const char *filename, const bool trimap, magmadnn::Tensor<T> *images_tensor, unsigned int idx) {
    // readjpeg struct
    struct imgRawImage *input;
    // the image
    input = read_jpeg(filename);

    unsigned int nchannels = input->numComponents;
    unsigned int nrows = input->height;
    unsigned int ncols = input->width;

    for (unsigned int row = 0; row < nrows; ++row) {
        int img_index = 0;
        for (unsigned int col = 0; col < ncols; ++col) {
            for (unsigned int ch = 0; ch < nchannels; ++ch) {
                T val = input->lpData[row * ncols * nchannels + col * nchannels + ch];

                if (!trimap) {
                    val = val / 255.0f;
                    images_tensor->set({idx, ch, row, col}, val);
                } else {
                    images_tensor->set({idx, unsigned(0), row, col}, val);
                }
                img_index++;
            }
        }
    }
}

template void add_jpeg_to_tensor<float>(const char *filename, const bool trimap, magmadnn::Tensor<float> *images_tensor,
                                        unsigned int image_idx);

template <typename T>
void tensor_to_jpeg(const char *filename, const int height, const int width, magmadnn::Tensor<T> *images_tensor,
                    unsigned int idx, bool is_trimap) {
    write_jpeg(filename, width, height, images_tensor->get_ptr() + idx * height * width, is_trimap);
}

template void tensor_to_jpeg<float>(const char *filename, const int height, const int width,
                                    magmadnn::Tensor<float> *images_tensor, unsigned int image_idx, bool is_trimap);
}  // namespace data
}  // namespace magmadnn
