/**
 * @file tensormatmul.cpp
 * @author Stephen Qiu
 * @version 0.1
 * @date 2021-07-14
 *
 * @copyright Copyright (c) 2021
 */
#include "compute/tensormatmul/tensormatmulop.h"
#include "compute/tensormatmul/dgetm_internal.h"

#if defined(MAGMADNN_HAVE_CUDA)
#include "cublas_v2.h"
#endif

namespace magmadnn {
namespace op {

template <typename T>
TensorMatmulOp<T>::TensorMatmulOp(T alpha, Operation<T> *a, Operation<T> *b, T beta, Operation<T> *c, bool copy, bool needs_grad)
    : Operation<T>::Operation({a, b, c}, needs_grad), a(a), b(b), c(c), alpha(alpha), beta(beta), copy(copy) {
    unsigned int M, N, K, Q;

    // must have same memory types
    assert(a->get_memory_type() == b->get_memory_type());
    assert(b->get_memory_type() == c->get_memory_type());

    // tensors must be matrices
    assert(a->get_output_shape().size() == 3);
    assert(b->get_output_shape().size() == 2);
    assert(c->get_output_shape().size() == 3);

    // A: QxMxK  B: KxN  C: QxMxN
    M = a->get_output_shape(1);
    K = a->get_output_shape(2);
    N = b->get_output_shape(1);
    Q = a->get_output_shape(0);
    
    // valid shapes
    assert(b->get_output_shape(0) == K);
    assert(c->get_output_shape(1) == M);
    assert(c->get_output_shape(2) == N);
    assert(c->get_output_shape(0) == Q);

    this->output_shape = {Q, M, N};
    this->mem_type = a->get_memory_type();

    /* avoid allocating memory in eval */
    if (copy) {
        this->output_tensor = new Tensor<T>(this->output_shape, this->mem_type);
    }

    /* init gradient tensors to NULL */
    this->_grad_cache[(uintptr_t) a] = NULL;
    this->_grad_cache[(uintptr_t) b] = NULL;
    this->_grad_cache[(uintptr_t) c] = NULL;
}

template <typename T>
Tensor<T> *TensorMatmulOp<T>::_eval(bool recompute) {
    a_tensor = a->eval(recompute);  // MxK
    b_tensor = b->eval(recompute);  // KxN
    c_tensor = c->eval(recompute);

    if (copy) {
        this->output_tensor->copy_from(*c_tensor);
    } else {
        this->output_tensor = c_tensor;
    }

    internal::dgetm_full(alpha, beta, false, false, a_tensor, b_tensor, this->output_tensor);

    return this->output_tensor;
}

template <typename T>
Tensor<T> *TensorMatmulOp<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) { //TODO : NOT UPDATED YET
    /* wrt a: dot(grad, B^T)  |  wrt b: dot(a^T, grad) */
    Tensor<T> *out = this->_grad_cache[(uintptr_t) var];

    if (var == a) {
        b_tensor = b->eval(false); /* don't recalculate if necessary */

        /* init grad tensor */
        if (out == NULL) {
            if (T_IS_MATRIX(b_tensor)) {
                /* grad.B^T has shape row(grad) x row(B) */
                out = new Tensor<T>({grad->get_shape(0), b_tensor->get_shape(0)}, {NONE, {}}, this->mem_type);
            } else if (T_IS_VECTOR(a_tensor)) {
                /* grad.B^T has shape row(B) */
                out = new Tensor<T>({b_tensor->get_shape(0)}, {NONE, {}}, this->mem_type);
            } else {
                /* grad.B^T has shape of B^T */
                out = new Tensor<T>({b_tensor->get_shape(1), b_tensor->get_shape(0)}, {NONE, {}}, this->mem_type);
            }

#if defined(MAGMADNN_HAVE_CUDA)
            out->set_custream(this->get_custream());
            out->set_cublas_handle(this->get_cublas_handle());
#endif

            this->_grad_cache[(uintptr_t) a] = out;
        }

        math::dot((T) 1, false, grad, true, b_tensor, (T) 0, out);

        // return dot(grad, transpose(b, true, false), true, false);
    } else {
        a_tensor = a->eval(false);

        /* need to create grad out for this */
        if (out == NULL) {
            if (T_IS_MATRIX(grad)) {
                /* if grad is a matrix, then a^T grad is the gradient */
                out = new Tensor<T>({a_tensor->get_shape(1), grad->get_shape(1)}, {NONE, {}}, this->mem_type);
            } else if (T_IS_VECTOR(grad)) {
                /* if grad is a vector, then a^T grad has the same shape as col(a) */
                out = new Tensor<T>({a_tensor->get_shape(1)}, {NONE, {}}, this->mem_type);
            } else {
                /* if grad is a scalar?, then a^T grad has the same shape as A^T */
                out = new Tensor<T>({a_tensor->get_shape(1), a_tensor->get_shape(0)}, {NONE, {}}, this->mem_type);
            }

#if defined(MAGMADNN_HAVE_CUDA)
            out->set_custream(this->get_custream());
            out->set_cublas_handle(this->get_cublas_handle());
#endif

            this->_grad_cache[(uintptr_t) b] = out;
        }

        math::dot((T) 1, true, a_tensor, false, grad, (T) 0, out);

        // return dot(transpose(a, true, false), grad, true, false);
    }
    return out;
}
template class MatmulOp<int>;
template class MatmulOp<float>;
template class MatmulOp<double>;



template <typename T>
TensorMatmulOp<T> *tensormatmul(T alpha, Operation<T> *a, Operation<T> *b, T beta, Operation<T> *c, bool copy, bool needs_grad) {
    return new TensorMatmulOp<T>(alpha, a, b, beta, c, copy);
}
template TensorMatmulOp<int> *tensormatmul(int alpha, Operation<int> *a, Operation<int> *b, int beta, Operation<int> *c, bool copy,
                               bool needs_grad);
template TensorMatmulOp<float> *tensormatmul(float alpha, Operation<float> *a, Operation<float> *b, float beta, Operation<float> *c,
                                 bool copy, bool needs_grad);
template TensorMatmulOp<double> *tensormatmul(double alpha, Operation<double> *a, Operation<double> *b, double beta,
                                  Operation<double> *c, bool copy, bool needs_grad);

}  // namespace op
}  // namespace magmadnn
