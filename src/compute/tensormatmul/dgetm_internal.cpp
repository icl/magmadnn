/**
 * @file dgetm_internal.cpp
 * @author Stephen Qiu
 * @version 1.0
 * @date 2021-07-15
 *
 * @copyright Copyright (c) 2021
 */
#include "compute/tensormatmul/dgetm_internal.h"

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "math/wrappers.h"

namespace magmadnn {
namespace internal {

template <>
void dgetm_full(int alpha, int beta, bool trans_A, bool trans_B, Tensor<int> *A, Tensor<int> *B, Tensor<int> *C){}

template <>
void dgetm_full(float alpha, float beta, bool trans_A, bool trans_B, Tensor<float> *A, Tensor<float> *B, Tensor<float> *C/*, unsigned int c, unsigned int M,
                    unsigned int N, unsigned int K*/) {
    // unsigned int ldda, lddb, lddc;
    unsigned int M, N, K, Q, ldda, lddb, lddc;

    /* op(A) : QxMxK ; op(B) : KxN ; C : QxMxN */

    Q = C->get_shape(0); 
    M = C->get_shape(1);                 /* rows of C and op(A) */
    N = C->get_shape(2);                 /* columns of C and op(B) */
    K = A->get_shape((trans_A) ? 1 : 2); /* columns of op(A) and rows of op(B) */
    ldda = (trans_A) ? M : K;            /* leading dimension of op(A) */
    lddb = (trans_B) ? K : N;            /* leading dimension of op(B) */
    lddc = N;

    // A: MxK  B: KxN  C: MxN
    // (MxR)(RxN) + (MxN) = (MxN) + (MxN) = (MxN)
   
    if (A->get_memory_type() == HOST) {

        // Assuming row-major storage
        magmadnn::math::operation a_trans = (trans_A) ? magmadnn::math::OP_T : magmadnn::math::OP_N;
        magmadnn::math::operation b_trans = (trans_B) ? magmadnn::math::OP_T : magmadnn::math::OP_N;

        for(unsigned int i = 0; i < Q; i++){

        gemm(b_trans, a_trans, N, M, K, alpha, B->get_ptr(), lddb, A->get_ptr() + i * M * N, ldda, beta, C->get_ptr() + i * M * N, lddc);
        
        }
    }
}

template <>
void dgetm_full(double alpha, double beta, bool trans_A, bool trans_B, Tensor<double> *A, Tensor<double> *B, Tensor<double> *C)
 {
    // unsigned int ldda, lddb, lddc;
    unsigned int M, N, K, Q, ldda, lddb, lddc;

    /* op(A) : QxMxK ; op(B) : KxN ; C : QxMxN */

    Q = C->get_shape(0); 
    M = C->get_shape(1);                 /* rows of C and op(A) */
    N = C->get_shape(2);                 /* columns of C and op(B) */
    K = A->get_shape((trans_A) ? 1 : 2); /* columns of op(A) and rows of op(B) */
    ldda = (trans_A) ? M : K;            /* leading dimension of op(A) */
    lddb = (trans_B) ? K : N;            /* leading dimension of op(B) */
    lddc = N;

    // A: MxK  B: KxN  C: MxN
    // (MxR)(RxN) + (MxN) = (MxN) + (MxN) = (MxN)
   
    if (A->get_memory_type() == HOST) {

        // Assuming row-major storage
        magmadnn::math::operation a_trans = (trans_A) ? magmadnn::math::OP_T : magmadnn::math::OP_N;
        magmadnn::math::operation b_trans = (trans_B) ? magmadnn::math::OP_T : magmadnn::math::OP_N;

        for(unsigned int i = 0; i < Q; i++){

        gemm(b_trans, a_trans, N, M, K, alpha, B->get_ptr(), lddb, A->get_ptr() + i * M * N, ldda, beta, C->get_ptr() + i * M * N, lddc);
        
        }
    }
}

} //namespace internal
} //namespace magmadnn