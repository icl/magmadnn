/**
 * @file slice_internal.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */

#include "compute/slice/slice_internal.h"

namespace magmadnn {
namespace internal {

template <typename T>
void slice_cpu(Tensor<T>* input, unsigned int axis, unsigned int index, Tensor<T>* output) {
    unsigned int d0, d1;
    std::vector<unsigned int> myshape = input->get_shape();
    if (axis == 0) {
        d0 = myshape[1];
        d1 = myshape[2];
    } else if (axis == 1) {
        d0 = myshape[0];
        d1 = myshape[2];
    } else if (axis == 2) {
        d0 = myshape[0];
        d1 = myshape[1];
    } else {
        throw std::invalid_argument("slice_cpu only works for 3 dimensional tensor\n");
    }

    //	Tensor<T> * output = new Tensor({d0,d1}, {NONE, {}}, input->get_memory_type());
    for (unsigned int i = 0; i < d0; i++) {
        for (unsigned int j = 0; j < d1; j++) {
            if (axis == 0) output->set({i, j}, input->get({index, i, j}));
            if (axis == 1) output->set({i, j}, input->get({i, index, j}));
            if (axis == 2) output->set({i, j}, input->get({i, j, index}));
        }
    }

    //	return input;
}

template void slice_cpu(Tensor<int>* input, unsigned int axis, unsigned int index, Tensor<int>* output);
template void slice_cpu(Tensor<float>* input, unsigned int axis, unsigned int index, Tensor<float>* output);
template void slice_cpu(Tensor<double>* input, unsigned int axis, unsigned int index, Tensor<double>* output);

}  // namespace internal
}  // namespace magmadnn
