/**
 * @file sliceop.cpp
 * @author Pierluigi Cambie-Fabris
 * @autor Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */
#include "compute/slice/sliceop.h"
namespace magmadnn {
namespace op {

template<typename T>
SliceOp<T>::SliceOp(op::Operation<T> *input, unsigned axis, unsigned index, bool copy,
	bool needs_grad) : Operation<T>({input}, needs_grad), input(input),
	axis(axis), index(index), copy(copy) {

	init();
}

template<typename T>
void SliceOp<T>::init() {
	
	//TODO implements for non 3D tensors
	if (this->input->get_output_shape().size() != 3) {
		throw std::invalid_argument("\nSliceOp Operation has been implemented only for three-dimensional inputs; but it recieved another dimensioned input.");
	} 
	
	unsigned d0, d1;

	if (this->axis == 0) {
		d0 = this->input->get_output_shape()[1];
		d1 = this->input->get_output_shape()[2];
	} 
	else if (this->axis == 1) {	
		d0 = this->input->get_output_shape()[0];
		d1 = this->input->get_output_shape()[2];
	}
	//for axis == 2
	else {	
		d0 = this->input->get_output_shape()[0];
		d1 = this->input->get_output_shape()[1];
	}

	this->mem_type = this->input->get_memory_type();
	//allocate space for the output
	this->output_tensor = new Tensor<T>({d0,d1}, {NONE, {}}, this->mem_type);
	this->output_shape = {d0, d1};
	return;
}

template<typename T>
Tensor<T> *SliceOp<T>::_eval(bool recompute) {
	this->input->eval(recompute);
	//TODO implement copy = false
	if (copy) {
	}
	else {
		throw std::invalid_argument(
			"SliceOp with copy = false has not yet been implemented.");
	}

	//helper for sliceop

	if (this->mem_type == HOST){
	
		internal::slice_cpu(this->input->get_output_tensor(), this->axis,
			this->index, this->output_tensor);
	}

#if defined(MAGMADNN_HAVE_CUDA)
       	if(this->mem_type == DEVICE) {
		internal::slice_gpu(this->get_custream(), this->input->get_output_tensor(), this->axis,
			this->index, this->output_tensor);
		if (!this->get_async()) cudaStreamSynchronize(this->get_custream());

        }
#endif


	return this->output_tensor;
}

template<typename T>
Tensor<T> *SliceOp<T>::_grad(Operation<T> *consumer, Operation<T> *var,
	Tensor<T> *grad) {
	//check cache	
	Tensor<T> *out = this->_grad_cache[(uintptr_t) var];
	if (out == NULL) {
		//create new tensor and add to cache
		out = new Tensor<T>(this->input->get_output_shape(),
			{CONSTANT, {(T)0}},this->mem_type);
#if defined(MAGMADNN_HAVE_CUDA)
		out->set_custream(this->get_custream());
		out->set_cublas_handle(this->get_cublas_handle());
#endif

		this->_grad_cache[(uintptr_t) var] = out;
	}
	//set values in output tensor
	if (out->get_memory_type() == HOST) {
		for (unsigned i = 0; i < this->output_tensor->get_shape(0); ++i) {
			for (unsigned j = 0; j < this->output_tensor->get_shape(1); ++j){
				T fac = grad->get({i,j});	
				if (this->axis == 0) {
					out->set({this->index, i, j},fac);
				}
				else if (this->axis == 1){
					out->set({i, this->index, j}, fac);
				}
				else if (this->axis == 2) {
					out->set({i, j, this->index}, fac);
				}
			}
		}
	}

#if defined(MAGMADNN_HAVE_CUDA)
	else {
		magmadnn::internal::slice_grad_gpu(this->get_custream(), grad, this->axis, this->index, out);
		if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
	}
#endif


	return out; 
}

template class SliceOp<int>;
template class SliceOp<float>;
template class SliceOp<double>;

//return a  sliceop operation
template<typename T>
SliceOp<T> *slice(op::Operation<T> *input, unsigned axis, unsigned index, bool copy,
	bool needs_grad) {
	return new SliceOp<T>(input, axis, index, copy, needs_grad);
}

template SliceOp<int> *slice(op::Operation<int> *input, unsigned axis, unsigned index,
	bool copy, bool needs_grad);
template SliceOp<float> *slice(op::Operation<float> *input, unsigned axis,
	unsigned index, bool copy, bool needs_grad);
template SliceOp<double> *slice(op::Operation<double> *input, unsigned axis,
	unsigned index, bool copy, bool needs_grad);
}
}
