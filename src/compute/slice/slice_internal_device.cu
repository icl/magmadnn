/**
 * @file slice_internal_device.cu
 * @author Pierluigi Cambie-Fabris
 * @version 1.0
 * @date 2019-02-22
 */
#include "magmadnn/math.h"
#include "tensor/tensor.h"

#define BLK_SIZE 1024

namespace magmadnn {
namespace internal {

//kernel function for slice_gpu
template <typename T>
__global__ 
void kernel_slice_gpu(T * A, T * B, unsigned int ind, unsigned int size, unsigned int ten_size, unsigned int Z, unsigned int width) {
	unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
        unsigned int stride = blockDim.x * gridDim.x;
        for (unsigned int i = idx; i < size; i += stride) {
		B[i] = A[ind * width + i % width + (i/width) * ten_size];
	}

}

template <typename T>
void slice_gpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output){
	unsigned int size = output->get_size();
	const auto grid_dim = ceildiv(size, BLK_SIZE);
	if (axis != 1){
                throw std::invalid_argument("SliceOp gpu has only been implemented for axis = 1");
        }
	
	kernel_slice_gpu<<<grid_dim, BLK_SIZE>>>(input->get_ptr(), output->get_ptr(), index, output->get_size(),
			input->get_shape(1) * input->get_shape(2), input->get_shape(0),input->get_shape(2));
}


template void slice_gpu(Tensor<int> * input, unsigned int axis, unsigned int index, Tensor<int> *output);
template void slice_gpu(Tensor<float> * input, unsigned int axis, unsigned int index, Tensor<float> *output);
template void slice_gpu(Tensor<double> * input, unsigned int axis, unsigned int index, Tensor<double> *output);

template <typename T>
void slice_gpu(cudaStream_t custream, Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output){
	unsigned int size = output->get_size();
	const auto grid_dim = ceildiv(size, BLK_SIZE);
	if (axis != 1){
                throw std::invalid_argument("SliceOp gpu has only been implemented for axis = 1");
        }
	
	kernel_slice_gpu<<<grid_dim, BLK_SIZE, 0, custream>>>(input->get_ptr(), output->get_ptr(), index, output->get_size(),
			input->get_shape(1) * input->get_shape(2), input->get_shape(0),input->get_shape(2));
}


template void slice_gpu(cudaStream_t custream, Tensor<int> * input, unsigned int axis, unsigned int index, Tensor<int> *output);
template void slice_gpu(cudaStream_t custream, Tensor<float> * input, unsigned int axis, unsigned int index, Tensor<float> *output);
template void slice_gpu(cudaStream_t custream, Tensor<double> * input, unsigned int axis, unsigned int index, Tensor<double> *output);







template <typename T>
__global__
void kernel_slice_grad_gpu(T * A, T * B, unsigned int ind, unsigned int size, unsigned int ten_size, unsigned int width) {
        unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
        unsigned int stride = blockDim.x * gridDim.x;
        for (unsigned int i = idx; i < size; i += stride) {
		B[ind * width + i % width + (i/width) * ten_size] = A[i];
	}


}


template <typename T>
void slice_grad_gpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> * output){
	unsigned int size = output->get_size();
	const auto grid_dim = ceildiv(size, BLK_SIZE);
	if (axis != 1){
                throw std::invalid_argument("SliceOp gpu has only been implemented for axis = 1");
        }

	kernel_slice_grad_gpu<<<grid_dim, BLK_SIZE>>>(input->get_ptr(), output->get_ptr(), index, input->get_size(),
			output->get_shape(1) * output->get_shape(2),output->get_shape(2));


	
}

template void slice_grad_gpu(Tensor<int> * input, unsigned int axis, unsigned int index, Tensor<int> * output);
template void slice_grad_gpu(Tensor<float> * input, unsigned int axis, unsigned int index, Tensor<float> * output);
template void slice_grad_gpu(Tensor<double> * input, unsigned int axis, unsigned int index, Tensor<double> * output);


template <typename T>
void slice_grad_gpu(cudaStream_t custream, Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> * output){
	unsigned int size = output->get_size();
	const auto grid_dim = ceildiv(size, BLK_SIZE);
	if (axis != 1){
                throw std::invalid_argument("SliceOp gpu has only been implemented for axis = 1");
        }

	kernel_slice_grad_gpu<<<grid_dim, BLK_SIZE, 0, custream>>>(input->get_ptr(), output->get_ptr(), index, input->get_size(),
			output->get_shape(1) * output->get_shape(2),output->get_shape(2));


}


template void slice_grad_gpu(cudaStream_t custream, Tensor<int> * input, unsigned int axis, 
		unsigned int index, Tensor<int> * output);
template void slice_grad_gpu(cudaStream_t custream, Tensor<float> * input, unsigned int axis, 
		unsigned int index, Tensor<float> * output);
template void slice_grad_gpu(cudaStream_t custream, Tensor<double> * input, unsigned int axis, 
		unsigned int index, Tensor<double> * output);

} //namespace internal
} //namespace magmadnn
