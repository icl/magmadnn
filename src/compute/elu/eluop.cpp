#include "compute/elu/eluop.h"

namespace magmadnn {
namespace op {

template <typename T>
EluOp<T>::EluOp(Operation<T> *x, double a, bool copy, bool needs_grad)
    : Operation<T>::Operation({x}, needs_grad), x(x), copy(copy) {
    this->output_shape = x->get_output_shape();
    this->mem_type = x->get_memory_type();
    this->name = "ELU";
    this->a = a;

    if (copy) {
        this->output_tensor = new Tensor<T>(this->output_shape, this->mem_type);
    } else {
        fprintf(stderr, "inplace elu not defined\n");
    }

#if defined(MAGMADNN_HAVE_CUDA)
    cudnnErrchk(cudnnCreateActivationDescriptor(&cudnn_settings.descriptor));
    cudnnErrchk(
        cudnnSetActivationDescriptor(cudnn_settings.descriptor, CUDNN_ACTIVATION_ELU, CUDNN_NOT_PROPAGATE_NAN, this->a));
#endif
}

template <typename T>
EluOp<T>::~EluOp() {
#if defined(MAGMADNN_HAVE_CUDA)
    cudnnErrchk(cudnnDestroyActivationDescriptor(cudnn_settings.descriptor));
#endif
}

template <typename T>
Tensor<T> *EluOp<T>::_eval(bool recompute) {
    x_tensor = x->eval(recompute);

    this->output_tensor = x_tensor;

    // internal::relu_full(x_tensor, this->output_tensor);
    if (this->mem_type == HOST) {
        math::elu(x_tensor, this->a, this->output_tensor);
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        this->cudnn_settings.handle = this->get_cudnn_handle();
        math::elu_device(x_tensor, this->output_tensor, this->cudnn_settings);
        if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
    }
#endif

    return this->output_tensor;
}

template <typename T>
Tensor<T> *EluOp<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
    Tensor<T> *out = this->_grad_cache[(uintptr_t) var];

    if (out == NULL) {
        out = new Tensor<T>(this->output_shape, {NONE, {}}, this->mem_type);
#if defined(MAGMADNN_HAVE_CUDA)
        out->set_custream(this->get_custream());
        out->set_cublas_handle(this->get_cublas_handle());
#endif
        this->_grad_cache[(uintptr_t) var] = out;
    }

    x_tensor = x->eval(false);
    if (this->mem_type == HOST) {
        math::elu_grad(this->x_tensor, this->output_tensor, this->a, grad, out);
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        this->cudnn_settings.handle = this->get_cudnn_handle();
        math::elu_grad_device(x_tensor, this->output_tensor, grad, out, this->cudnn_settings);
        if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
    }
#endif

    return out;
}

template class EluOp<int>;
template class EluOp<float>;
template class EluOp<double>;

template <typename T>
EluOp<T> *elu(Operation<T> *x, double a, bool copy, bool needs_grad) {
    return new EluOp<T>(x, a, copy, needs_grad);
}
template EluOp<int> *elu(Operation<int> *x, double a, bool copy, bool needs_grad);
template EluOp<float> *elu(Operation<float> *x, double a, bool copy, bool needs_grad);
template EluOp<double> *elu(Operation<double> *x, double a, bool copy, bool needs_grad);

}  // namespace op
}  // namespace magmadnn
