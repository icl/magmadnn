/**
 * @file concat.cu
 * @author Edward Karak
 * @author Nichole Chow
 * @version 1.0
 * @date 2022-07-08
 *
 * @copyright Copyright (c) 2022
 */

#include <stdio.h>
#include "magmadnn.h"
#include "./layer/layer_utilities.h"
#include "tensor/tensor.h"
#include "../include/memory/memory_internal_device.h"
#include "../include/compute/concat4d/concat.cuh"

using namespace magmadnn;

namespace magmadnn {
namespace internal {

template <typename T>
/** Concatenate NCHW tensors `a` and `b` on the device along the axis channel
 * @tparam T
 * @param a pointer to tensor memory of first input tensor
 * @param chw_a C*H*W of a
 * @param b pointer to tensor memory of second input tensor
 * @param chw_b C*H*W of b
 * @param c pointer to destination tensor memory. Assumes space has been allocated for it
 * @param nchw_c N*C*H*W of c (i.e., number of elements in c)
 * @param chw_c C*H*W of c
 */
__global__ void kernel_concat4D(const T *a, unsigned chw_a, const T *b, unsigned chw_b, 
				T *c, unsigned nchw_c, unsigned chw_c)
{
	assert(a != nullptr); assert(b != nullptr); assert(c != nullptr); 
	assert(chw_a > 0); assert(chw_b > 0); assert(nchw_c > 0); assert(chw_c > 0);

	unsigned i = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;

	for (; i < nchw_c; i += stride) {
		unsigned batchIdx = i/chw_c;
		int batchOff = i - batchIdx*chw_c;
		c[i] = (batchOff < chw_a) ? 
				a[batchOff + batchIdx*chw_a] : b[(batchOff - chw_a) + batchIdx*chw_b];
	}
}

// taken from https://forums.developer.nvidia.com/t/concatenate-using-cudnn/63553
template <typename T>
void dev_concat4D_C(Tensor<T> *a, Tensor<T> *b, Tensor<T> *out)
{
	constexpr unsigned NBLOCKS = 1;
	constexpr unsigned NTHREADS_PER_BLOCK = 1024;

	std::vector<unsigned> shape = out->get_shape();
	assert(shape.size() == 4);

	kernel_concat4D<<<NBLOCKS, NTHREADS_PER_BLOCK>>>
	(a->get_ptr(), a->get_size()/shape[0], b->get_ptr(), b->get_size()/shape[0], out->get_ptr(), out->get_size(), shape[1]*shape[2]*shape[3]);
}
template void dev_concat4D_C(Tensor<int> *a, Tensor<int> *b, Tensor<int> *out);
template void dev_concat4D_C(Tensor<float> *a, Tensor<float> *b, Tensor<float> *out);
template void dev_concat4D_C(Tensor<double> *a, Tensor<double> *b, Tensor<double> *out);
}
}
