/**
 * @file concat4dop.cpp
 * @author Edward Karak
 * @author Nichole Chow
 * @version 1.0
 * @date 2022-07-20
 *
 * @copyright Copyright (c) 2022
 */
#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "../include/math/concat.h"
#include "compute/concat4d/concat.cuh"
#include "compute/concat4d/concat4dop.h"

namespace magmadnn {
namespace op {

template <typename T>
Concat4dOp<T>::Concat4dOp(Operation<T> *a, Operation<T> *b, unsigned axis, bool copy, bool needs_grad)
    : Operation<T>::Operation({a, b}, needs_grad), a(a), b(b), axis(axis), copy(copy) {
    assert(a->get_memory_type() == b->get_memory_type());
    auto a_shape = a->get_output_shape();
    a_shape[axis] = a_shape[axis] + b->get_output_shape()[axis];
    this->output_shape = a_shape;
    this->mem_type = b->get_memory_type();
    this->output_tensor = new Tensor<T>(this->output_shape, {NONE, {}}, this->mem_type);
}

template <typename T>
Tensor<T> *Concat4dOp<T>::_eval(bool recompute) {
    this->a_tensor = a->eval(recompute);
    this->b_tensor = b->eval(recompute);
#if defined(MAGMADNN_HAVE_CUDA)
    this->output_tensor->set_custream(this->get_custream());
    this->output_tensor->set_cublas_handle(this->get_cublas_handle());
#endif
    if (this->output_tensor->get_memory_type() == HOST) {
        math::concat(a_tensor, b_tensor, this->output_tensor, axis);
        // math::concat(a_tensor, b_tensor, this->output_tensor, axis);
        // fprintf(stderr, "%s:%d: Did concat on the CPU\n", __FILE__, __LINE__);
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else if (this->output_tensor->get_memory_type() == DEVICE) {
        internal::dev_concat4D_C(a_tensor, b_tensor, this->output_tensor);
        // fprintf(stderr, "%s:%d: Did concat on the GPU\n", __FILE__, __LINE__);
    }
#endif
    return this->output_tensor;
}

template <typename T>
Tensor<T> *Concat4dOp<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
    /* return gradient in here ... */
    Tensor<T> *out = this->_grad_cache[(uintptr_t) var];
    if (out == NULL) {
        out = new Tensor<T>(a->get_output_shape(), {NONE, {}}, this->mem_type);
        this->_grad_cache[(uintptr_t) var] = out;
    }

    Tensor<T> *host_grad = new Tensor<T>(grad->get_shape(), {CONSTANT, {1}}, HOST);
    host_grad->copy_from(*grad);
    if (var == this->a) {
        Tensor<T> *host_out = new Tensor<T>(a->get_output_shape(), {CONSTANT, {1}}, HOST);

        for (unsigned batch = 0; batch < a->get_output_shape()[0]; batch++) {
            for (unsigned channel = 0; channel < a->get_output_shape()[1]; channel++) {
                for (unsigned height = 0; height < a->get_output_shape()[2]; height++) {
                    for (unsigned width = 0; width < a->get_output_shape()[3]; width++) {
                        host_out->set({batch, channel, height, width}, host_grad->get({batch, channel, height, width}));
                    }
                }
            }
        }

        // Tensor<T> *out = new Tensor<T>(host_out->get_shape(), {NONE, {}}, this->mem_type);
        out->copy_from(*host_out);
        delete host_out;
        delete host_grad;
        return out;
    } else {
        Tensor<T> *host_out = new Tensor<T>(b->get_output_shape(), {CONSTANT, {1}}, HOST);
        for (unsigned batch = 0; batch < b->get_output_shape()[0]; batch++) {
            for (unsigned channel = 0; channel < b->get_output_shape()[1]; channel++) {
                for (unsigned height = 0; height < b->get_output_shape()[2]; height++) {
                    for (unsigned width = 0; width < b->get_output_shape()[3]; width++) {
                        host_out->set({batch, channel, height, width},
                                      host_grad->get({batch, channel + a->get_output_shape()[1], height, width}));
                    }
                }
            }
        }
        // Tensor<T> *out = new Tensor<T>(host_out->get_shape(), {NONE, {}}, this->mem_type);
        out->copy_from(*host_out);
        delete host_grad;
        delete host_out;
        return out;
    }
}
template class Concat4dOp<int>;
template class Concat4dOp<float>;
template class Concat4dOp<double>;

template <typename T>
Concat4dOp<T> *concat4d(Operation<T> *a, Operation<T> *b, unsigned axis, bool copy, bool needs_grad) {
    return new Concat4dOp<T>(a, b, axis, copy, needs_grad);
}
template Concat4dOp<int> *concat4d(Operation<int> *a, Operation<int> *b, unsigned axis, bool, bool needs_grad);
template Concat4dOp<float> *concat4d(Operation<float> *a, Operation<float> *b, unsigned axis, bool, bool needs_grad);
template Concat4dOp<double> *concat4d(Operation<double> *a, Operation<double> *b, unsigned axis, bool, bool needs_grad);

}  // namespace op
}  // namespace magmadnn