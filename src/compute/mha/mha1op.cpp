#include "compute/mha/mha1op.h"
#include "magmadnn.h" // for printing
#include <cassert>

#define DEBUG 0
namespace magmadnn {
    namespace op {
        // constructor
        template <typename T>
        MHA1Op<T>::MHA1Op(Operation<T> *input, Operation<T> *weights, ::magmadnn::math::attnOptions opts, bool needs_grad) 
        : Operation<T>::Operation({input, weights}, needs_grad),
        input(input),
        w(weights) {
            this->mem_type = input->get_memory_type();
            this->name = "MHA1Op";
            this->input_t = this->input->get_output_tensor();
            this->w_t = this->w->get_output_tensor();
            
            this->q_t = new Tensor<T>({(unsigned int) opts.attnBeamSize, (unsigned int) opts.attnBatchSize, (unsigned int) opts.attnQsize, (unsigned int) opts.attnSeqLenQ}, {CONSTANT,{(T)0}}, this->mem_type); // 
            this->k_t = new Tensor<T>({(unsigned int) opts.attnBeamSize, (unsigned int) opts.attnBatchSize, (unsigned int) opts.attnKsize, (unsigned int) opts.attnSeqLenK}, {CONSTANT,{(T)0}}, this->mem_type);
            this->v_t = new Tensor<T>({(unsigned int) opts.attnBeamSize, (unsigned int) opts.attnBatchSize, (unsigned int) opts.attnVsize, (unsigned int) opts.attnSeqLenK}, {CONSTANT,{(T)0}}, this->mem_type);

            this->dq_t = new Tensor<T>(q_t->get_shape(), {CONSTANT,{(T)0xFF}}, this->mem_type);
            this->dk_t = new Tensor<T>(k_t->get_shape(), {CONSTANT,{(T)0xFF}}, this->mem_type);
            this->dv_t = new Tensor<T>(v_t->get_shape(), {CONSTANT,{(T)0xFF}}, this->mem_type);
            this->dw_t = new Tensor<T>(w_t->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);

            
            this->mha_settings.mainCfg.beamSize    = this->q_t->get_shape(0); // this->q->get_output_shape(0); // {beam size, batch size, seq length, x size}
            
            this->mha_settings.mainCfg.batchSize   = this->q_t->get_shape(1); // this->q->get_output_shape(1);
                    
            this->mha_settings.mainCfg.seqLenQ     = this->q_t->get_shape(3); // this->q->get_output_shape(3);
                    
            this->mha_settings.mainCfg.seqLenK     = this->k_t->get_shape(3); // this->k->get_output_shape(3);
                
            this->mha_settings.mainCfg.qSize       = this->q_t->get_shape(2); // this->q->get_output_shape(2);
                    
            this->mha_settings.mainCfg.kSize       = this->k_t->get_shape(2); // this->k->get_output_shape(2);
                    
            this->mha_settings.mainCfg.vSize       = this->v_t->get_shape(2); // this->v->get_output_shape(2);

            this->_grad_cache[(uintptr_t) this->input] = NULL;

            this->init_settings(opts);

            this->o_t = new Tensor<T>(
                {q_t->get_shape(0), q_t->get_shape(1), (unsigned int) this->mha_settings.mainCfg.oLength(), q_t->get_shape(3)}, 
                {CONSTANT,{(T)0}}, 
                this->mem_type
            );

            this->do_t = new Tensor<T>(o_t->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);

            this->output_shape = o_t->get_shape();
            this->output_tensor = o_t;

	        // std::cout << "output size: " <<this->output_tensor->get_size() << std::endl;
        }
        
        // destructor
        template <typename T>
        MHA1Op<T>::~MHA1Op() {
        #if defined(MAGMADNN_HAVE_CUDA)
            cudnnDestroy(this->mha_settings.handle);
            this->mha_settings.handle = NULL;

            cudnnDestroyAttnDescriptor(this->mha_settings.attn_desc);
            this->mha_settings.attn_desc = NULL;

            cudnnDestroyDropoutDescriptor(this->mha_settings.drop_desc);
            this->mha_settings.drop_desc = NULL;

            cudnnDestroySeqDataDescriptor(this->mha_settings.q_desc);
            this->mha_settings.q_desc = NULL;

            cudnnDestroySeqDataDescriptor(this->mha_settings.k_desc);
            this->mha_settings.k_desc = NULL;

            cudnnDestroySeqDataDescriptor(this->mha_settings.v_desc);
            this->mha_settings.v_desc = NULL;

            cudnnDestroySeqDataDescriptor(this->mha_settings.o_desc);
            this->mha_settings.o_desc = NULL;

            cudaFree(this->mha_settings.dropoutBuf);
            this->mha_settings.dropoutBuf = NULL;

            cudaFree(this->mha_settings.devWkspace);
            this->mha_settings.devWkspace = NULL;

            cudaFree(this->mha_settings.devReserve);
            this->mha_settings.devReserve = NULL;

            free(this->mha_settings.qSeqArray);
            this->mha_settings.qSeqArray = NULL;

            free(this->mha_settings.kSeqArray);
            this->mha_settings.kSeqArray = NULL;

            free(this->mha_settings.loWinIdx);
            this->mha_settings.loWinIdx = NULL;

            free(this->mha_settings.hiWinIdx);
            this->mha_settings.hiWinIdx = NULL;   
        #endif       
        }
        
        // eval
        template <typename T>
        Tensor<T> *MHA1Op<T>::_eval(bool recompute) {
            // if (this->o_t == NULL) {
            //     printf("set parameters!\n");
            //     exit(-1);
            // }
            this->has_been_run = false; // TODO: ?
            // update input operation: now it gives correct values after calculation
            // this->q->eval(recompute);
            // this->k->eval(recompute);
            // this->v->eval(recompute);
            this->input->eval(recompute);
            this->w->eval(recompute);

            #if defined(MAGMADNN_HAVE_CUDA)
                cuda_forward();
            #endif
            this->output_tensor = this->o_t; // assign
            return this->output_tensor;
        }
        
        // grad
        template <typename T>
        Tensor<T> *MHA1Op<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
            // TODO: allocate memory
            Tensor<T> *out = this->_grad_cache[(uintptr_t) var];
            this->do_t->copy_from(*grad, 0, grad->get_size());

            if (var == this->input) {
                // handle out
                printf("input\n");
                if (out == NULL) {
                    out = new Tensor<T> (this->input->get_output_shape(), {NONE, {}}, this->mem_type);
            #if defined(MAGMADNN_HAVE_CUDA)
                    out->set_custream(this->get_custream());
                    out->set_cublas_handle(this->get_cublas_handle());
            #endif
            		this->_grad_cache[(uintptr_t) var] = out;
                }
                // if has not been run    
                if (!this->has_been_run){
                    ::magmadnn::math::mha_grad_data_device(
                        this->q_t, 
                        this->dq_t, 
                        this->k_t, 
                        this->dk_t, 
                        this->v_t, 
                        this->dv_t, 
                        this->w_t, 
                        this->do_t, 
                        this->mha_settings);
                    // this->has_been_run = true;
                }
                unsigned int dq_size = this->dq_t->get_size();
                unsigned int dk_size = this->dk_t->get_size();
                unsigned int dv_size = this->dv_t->get_size();
                unsigned int dq_mem = this->dq_t->get_memory_size();
                unsigned int dk_mem = this->dk_t->get_memory_size();
                unsigned int dv_mem = this->dv_t->get_memory_size();
                if (dq_size + dk_size + dv_size != out->get_size()) {
                    fprintf(stderr, "ERROR: Memory allocated (%d) is not sufficient.\n\n", out->get_size());
                    exit(-1);
                }
                cudaErrchk(cudaMemcpy(out->get_ptr(), this->dq_t->get_ptr(), dq_mem, cudaMemcpyDeviceToDevice));
                cudaErrchk(cudaMemcpy(out->get_ptr() + dq_size, this->dk_t->get_ptr(), dk_mem, cudaMemcpyDeviceToDevice));
                cudaErrchk(cudaMemcpy(out->get_ptr() + dq_size + dk_size, this->dv_t->get_ptr(), dv_mem, cudaMemcpyDeviceToDevice));
                this->has_been_run = true;
            } else if (var == this->w) {
                // handle out
                if (out == NULL) {
                    out = new Tensor<T> (this->w->get_output_shape(), {NONE, {}}, this->mem_type);
            #if defined(MAGMADNN_HAVE_CUDA)
                    out->set_custream(this->get_custream());
                    out->set_cublas_handle(this->get_cublas_handle());
            #endif
            		this->_grad_cache[(uintptr_t) var] = out;
                }
                // if (!this->has_been_run) {

                    // DEBUG: call mha_grad_data_device before call mha_grad_data_device_weights
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    ::magmadnn::math::mha_grad_data_device(
                        this->q_t, 
                        this->dq_t, 
                        this->k_t, 
                        this->dk_t, 
                        this->v_t, 
                        this->dv_t, 
                        this->w_t, 
                        this->do_t, 
                        this->mha_settings);

                    ::magmadnn::math::mha_grad_data_device_weights(
                        this->q_t, 
                        this->k_t, 
                        this->v_t, 
                        this->do_t, 
                        this->w_t,
                        this->dw_t,
                        this->mha_settings);
                // }
                out->copy_from(*this->dw_t, 0, this->dw_t->get_size());
                // this->has_been_run = true;
            }
            if (!this->get_async()) {
                cudaStreamSynchronize(this->get_custream());
            }	

            return out;
        }

        // print parameters
        template <typename T>
        void MHA1Op<T>::print_params(){
            // const char standardAxes[CUDNN_SEQDATA_DIM_COUNT] = {'T', 'N', 'B', 'V'};
            // char dataAxes[CUDNN_SEQDATA_DIM_COUNT];
            // for (int ii = 0; ii < CUDNN_SEQDATA_DIM_COUNT; ++ii) {
            //     this->mha_settings.mainCfg.dataAxes[ii] = standardAxes[this->mha_settings.mainCfg.dataAxes[ii]];
            // }
            printf("Print parameters:\n\n");
            // printf("#### attnTrain       = %d (%s)\n", IS_TRAINING, IS_TRAINING ? "training" : "inference");
            printf("#### attnDataType    = %d \n", this->mha_settings.mainCfg.dataType);
            printf("#### attnCompPrec    = %d \n", this->mha_settings.mainCfg.compPrec);
            printf("#### attnNumHeads    = %d\n", this->mha_settings.mainCfg.numHeads);
            printf("#### attnBatchSize   = %d\n", this->mha_settings.mainCfg.batchSize);
            printf("#### attnBeamSize    = %d\n", this->mha_settings.mainCfg.beamSize);
            printf("#### attnSmScaler    = %.4e\n", this->mha_settings.mainCfg.smScaler);
            printf("#### attnDropoutRate = %.4f\n", this->mha_settings.mainCfg.dropoutRate);
            printf("#### attnQsize       = %d\n", this->mha_settings.mainCfg.qSize);
            printf("#### attnKsize       = %d\n", this->mha_settings.mainCfg.kSize);
            printf("#### attnVsize       = %d\n", this->mha_settings.mainCfg.vSize);
            printf("#### attnProjQsize   = %d%s\n", this->mha_settings.mainCfg.qProjSize, this->mha_settings.mainCfg.qProjSize ? "" : " (no Q weights)");
            printf("#### attnProjKsize   = %d%s\n", this->mha_settings.mainCfg.kProjSize, this->mha_settings.mainCfg.kProjSize ? "" : " (no K weights)");
            printf("#### attnProjVsize   = %d%s\n", this->mha_settings.mainCfg.vProjSize, this->mha_settings.mainCfg.vProjSize ? "" : " (no V weights)");
            printf("#### attnProjOsize   = %d%s\n", this->mha_settings.mainCfg.oProjSize, this->mha_settings.mainCfg.oProjSize ? "" : " (no O weights)");
            printf("#### attnSeqLenQ     = %d\n", this->mha_settings.mainCfg.seqLenQ);
            printf("#### attnSeqLenK     = %d\n", this->mha_settings.mainCfg.seqLenK);
            // printf("#### attnDataLayout  = %d (%c,%c,%c,%c)\n", this->mha_settings.mainCfg.dataLayout, dataAxes[0], dataAxes[1], dataAxes[2], dataAxes[3]);
            printf("#### attnResLink     = %d\n", this->mha_settings.mainCfg.resLink);
            printf("#### attnProjBias    = %d\n", this->mha_settings.mainCfg.projBias);
            printf("#### attnSweep       = %d\n", this->mha_settings.mainCfg.sweep);
            printf("#### attnRandGeom    = %d\n", this->mha_settings.mainCfg.randGeom);
            printf("#### attnRandSeed    = %d\n", this->mha_settings.mainCfg.randSeed);
            printf("#### attnFileDump    = %d\n\n", this->mha_settings.mainCfg.fileDump);
        }

        template <typename T>
        void MHA1Op<T>::save_data(const char *fName, int batch, int beam, int nDims, int dimA[4], cudnnSeqDataAxis_t ordA[4], T *dataBuf) {
            if (nDims != 4) {
                fprintf(stderr, "ERROR: unexpected number of dimensions %d!=4 in seqdata\n\n", nDims);
                exit(-1);
            }

            if (batch < 0 || batch >= dimA[CUDNN_SEQDATA_BATCH_DIM]) {
                fprintf(stderr, "ERROR: invalid batch=%d for file dump\n\n", batch);
                exit(-1);
            }

            if (beam < 0 || beam >= dimA[CUDNN_SEQDATA_BEAM_DIM]) {
                fprintf(stderr, "ERROR: invalid beam=%d for file dump\n\n", beam);
                exit(-1);
            }

            FILE *fp = fopen(fName, "w");
            if (fp == NULL) {
                const char *reason = (errno ? strerror(errno) : "unknown reason");
                fprintf(stderr, "ERROR: failed to open '%s' file (%s)\n\n", fName, reason);
                exit(-1);
            }

            // The length of embedding vector (same as CUDNN_SEQDATA_VECT_DIM).
            int veclen = dimA[nDims - 1];

            // Actual strides in memory (layout dependent).
            size_t strA[4] = {0};

            // Compute strides from dimensions (SeqData is a packed container).
            strA[nDims - 1] = 1;
            size_t stride   = veclen;
            for (int i = nDims - 2; i >= 0; i--) {
                if (ordA[i] < nDims - 1 && strA[ordA[i]] == 0) {
                    strA[ordA[i]] = stride;
                    stride *= dimA[ordA[i]];
                } else {
                    fprintf(stderr, "ERROR: invalid re-order index ordA[i=%d]=%d\n\n", i, ordA[i]);
                    exit(-1);
                }
            }

            // Number of decimal digits when saving as text.
            int decDigs = (sizeof(T) == sizeof(double) ? 16 : 8);

            // Write one full sentence (all time-steps for the given batch/beam).
            size_t base = size_t(batch) * strA[CUDNN_SEQDATA_BATCH_DIM] + size_t(beam) * strA[CUDNN_SEQDATA_BEAM_DIM];
            for (int vect = 0; vect < veclen; vect++) {
                for (int time = 0; time < dimA[CUDNN_SEQDATA_TIME_DIM]; time++) {
                    size_t idx = size_t(time) * strA[CUDNN_SEQDATA_TIME_DIM] + size_t(vect) * strA[CUDNN_SEQDATA_VECT_DIM];
                    fprintf(fp, " % -.*e", decDigs, double(dataBuf[base + idx]));
                }
                fprintf(fp, "\n");
            }
            if (fclose(fp) != 0) {
                const char *reason = (errno ? strerror(errno) : "unknown reason");
                fprintf(stderr, "ERROR: failed to write to '%s' file (%s)\n\n", fName, reason);
                exit(-1);
            }
        }
        
        // init setting
        template <typename T>
        void MHA1Op<T>::init_settings(::magmadnn::math::attnOptions opts) {
            if (this->mem_type == HOST) {
                std::fprintf(stderr, "Error: MHA1Op::init_settings requires GPU.\n");
            }

            // this->mha_settings.handle = this->get_cudnn_handle();

            this->mha_settings.attn_desc = NULL;
            this->mha_settings.drop_desc = NULL;
            this->mha_settings.q_desc    = NULL;
            this->mha_settings.k_desc    = NULL;
            this->mha_settings.v_desc    = NULL;
            this->mha_settings.o_desc    = NULL;

            this->mha_settings.dropoutBuf     = NULL;
            this->mha_settings.dropoutBufSize = 0;

            this->mha_settings.devWkspace = NULL;
            this->mha_settings.devReserve = NULL;

            this->mha_settings.maxWeights = 0;
            this->mha_settings.maxWkspace = 0;
            this->mha_settings.maxReserve = 0;

            this->mha_settings.sizeWeights = 0;
            this->mha_settings.sizeWkspace = 0;
            this->mha_settings.sizeReserve = 0;

            this->mha_settings.qSeqArray = NULL;
            this->mha_settings.kSeqArray = NULL;

            this->mha_settings.loWinIdx = NULL;
            this->mha_settings.hiWinIdx = NULL;

            this->mha_settings.mainCfg.numHeads    = opts.attnNumHeads; // number of heads
            // this->mha_settings.mainCfg.batchSize   = 1; // opts.attnBatchSize; // batch size
            // this->mha_settings.mainCfg.beamSize    = 1; // opts.attnBeamSize; // beam size
            // this->mha_settings.mainCfg.seqLenQ     = 2; // opts.attnSeqLenQ; // sequence length of Q
            // this->mha_settings.mainCfg.seqLenK     = 2; // opts.attnSeqLenK; // sequence length of K
            // this->mha_settings.mainCfg.qSize       = 3; // opts.attnQsize; // size of Q
            // this->mha_settings.mainCfg.kSize       = 3; // opts.attnKsize; // size of K
            // this->mha_settings.mainCfg.vSize       = 3; // opts.attnVsize; // size of V            
            this->mha_settings.mainCfg.smScaler    = opts.attnSmScaler;
            this->mha_settings.mainCfg.dropoutRate = opts.attnDropoutRate; // dropout rate
            this->mha_settings.mainCfg.qProjSize   = opts.attnProjQsize;
            this->mha_settings.mainCfg.kProjSize   = opts.attnProjKsize;
            this->mha_settings.mainCfg.vProjSize   = opts.attnProjVsize;
            this->mha_settings.mainCfg.oProjSize   = opts.attnProjOsize;
            this->mha_settings.mainCfg.resLink     = opts.attnResLink == 0 ? false : true;
            this->mha_settings.mainCfg.projBias    = opts.attnProjBias == 0 ? false : true;
            this->mha_settings.mainCfg.sweep       = opts.attnSweep;
            this->mha_settings.mainCfg.randGeom    = opts.attnRandGeom != 0 ? 1 : 0;
            this->mha_settings.mainCfg.randSeed    = opts.attnRandSeed; // seed for random 
            this->mha_settings.mainCfg.dataType    = cudnnDataType_t(opts.attnDataType); // data type
            this->mha_settings.mainCfg.compPrec    = cudnnDataType_t(opts.attnCompPrec);
            this->mha_settings.mainCfg.fileDump    = opts.attnFileDump != 0 ? 1 : 0; // log file
            this->mha_settings.mainCfg.train       = opts.attnTrain; 
  
            // attention mode
            if (opts.attnQueryMap == 0) {
                this->mha_settings.mainCfg.attnMode = (this->mha_settings.mainCfg.attnMode | CUDNN_ATTN_QUERYMAP_ALL_TO_ONE);
            } else if (opts.attnQueryMap == 1) {
                this->mha_settings.mainCfg.attnMode = (this->mha_settings.mainCfg.attnMode | CUDNN_ATTN_QUERYMAP_ONE_TO_ONE);
            } else {
                fprintf(stderr, "ERROR: wrong -attnQueryMap value\n\n");
                exit(-1);
            }

            if (opts.attnProjBias == 0) {
                this->mha_settings.mainCfg.attnMode = (this->mha_settings.mainCfg.attnMode | CUDNN_ATTN_DISABLE_PROJ_BIASES);
            } else if (opts.attnProjBias == 1) {
                this->mha_settings.mainCfg.attnMode = (this->mha_settings.mainCfg.attnMode | CUDNN_ATTN_ENABLE_PROJ_BIASES);
            } else {
                fprintf(stderr, "ERROR: wrong -attnProjBias value\n\n");
                exit(-1);
            }

            // check args
            if (this->mha_settings.mainCfg.numHeads <= 0 || this->mha_settings.mainCfg.batchSize <= 0 || this->mha_settings.mainCfg.beamSize <= 0) {
                fprintf(stderr, "ERROR: wrong attention NumHeads/BatchSize/BeamSize arguments\n\n");
                exit(-1);
            }

            // error file
            if (this->mha_settings.mainCfg.fileDump != 0) {
                if (this->mha_settings.mainCfg.batchSize > 1) {
                    fprintf(stderr, "ERROR: -attnFileDump%d requires -attnBatchSize=1\n\n", opts.attnFileDump);
                    exit(-1);
                }

                if (this->mha_settings.mainCfg.beamSize > 1) {
                    fprintf(stderr, "ERROR: -attnFileDump%d requires -attnBeamSize=1\n\n", opts.attnFileDump);
                    exit(-1);
                }

                if (this->mha_settings.mainCfg.randGeom != 0) {
                    fprintf(stderr, "ERROR: -attnFileDump%d requires -attnRandGeom=0\n\n", opts.attnFileDump);
                    exit(-1);
                }

                if (this->mha_settings.mainCfg.projBias != 0) {
                    fprintf(stderr, "ERROR: -attnFileDump%d requires -attnProjBias=0\n\n", opts.attnFileDump);
                    exit(-1);
                }

                if (this->mha_settings.mainCfg.train && this->mha_settings.mainCfg.dropoutRate > 0.0) {
                    fprintf(stderr, "ERROR: -attnFileDump%d requires -attnDropoutRate=0\n\n", opts.attnFileDump);
                    exit(-1);
                }
            }

            // int qProjLen = this->mha_settings.mainCfg.qLength();
            // int kProjLen = this->mha_settings.mainCfg.kLength();
            // int outLen   = this->mha_settings.mainCfg.oLength();

            this->mha_settings.mainCfg.dataLayout = opts.attnDataLayout;

            switch (this->mha_settings.mainCfg.dataLayout) {
                case 0:  // dataAxes = [T, N, B]
                    this->mha_settings.mainCfg.dataAxes[0] = CUDNN_SEQDATA_TIME_DIM;
                    this->mha_settings.mainCfg.dataAxes[1] = CUDNN_SEQDATA_BATCH_DIM;
                    this->mha_settings.mainCfg.dataAxes[2] = CUDNN_SEQDATA_BEAM_DIM;
                    break;

                case 1:  // dataAxes = [T, B, N]
                    this->mha_settings.mainCfg.dataAxes[0] = CUDNN_SEQDATA_TIME_DIM;
                    this->mha_settings.mainCfg.dataAxes[1] = CUDNN_SEQDATA_BEAM_DIM;
                    this->mha_settings.mainCfg.dataAxes[2] = CUDNN_SEQDATA_BATCH_DIM;
                    break;

                case 2:  // dataAxes = [N, T, B]
                    this->mha_settings.mainCfg.dataAxes[0] = CUDNN_SEQDATA_BATCH_DIM;
                    this->mha_settings.mainCfg.dataAxes[1] = CUDNN_SEQDATA_TIME_DIM;
                    this->mha_settings.mainCfg.dataAxes[2] = CUDNN_SEQDATA_BEAM_DIM;
                    break;

                case 3:  // dataAxes = [N, B, T]
                    this->mha_settings.mainCfg.dataAxes[0] = CUDNN_SEQDATA_BATCH_DIM;
                    this->mha_settings.mainCfg.dataAxes[1] = CUDNN_SEQDATA_BEAM_DIM;
                    this->mha_settings.mainCfg.dataAxes[2] = CUDNN_SEQDATA_TIME_DIM;
                    break;

                case 4:  // dataAxes = [B, T, N]
                    this->mha_settings.mainCfg.dataAxes[0] = CUDNN_SEQDATA_BEAM_DIM;
                    this->mha_settings.mainCfg.dataAxes[1] = CUDNN_SEQDATA_TIME_DIM;
                    this->mha_settings.mainCfg.dataAxes[2] = CUDNN_SEQDATA_BATCH_DIM;
                    break;

                case 5:  // dataAxes = [B, N, T]
                    this->mha_settings.mainCfg.dataAxes[0] = CUDNN_SEQDATA_BEAM_DIM;
                    this->mha_settings.mainCfg.dataAxes[1] = CUDNN_SEQDATA_BATCH_DIM;
                    this->mha_settings.mainCfg.dataAxes[2] = CUDNN_SEQDATA_TIME_DIM;
                    break;

                default:
                    fprintf(stderr, "ERROR: wrong -attnDataLayout%d option\n\n", opts.attnDataLayout);
                    exit(-1);
            }

            this->mha_settings.mainCfg.dataAxes[3] = CUDNN_SEQDATA_VECT_DIM;

            cudnnErrchk(cudnnCreate(&this->mha_settings.handle));
            cudnnErrchk(cudnnCreateAttnDescriptor(&this->mha_settings.attn_desc)); // attention descriptor
            cudnnErrchk(cudnnCreateDropoutDescriptor(&this->mha_settings.drop_desc));
            cudnnErrchk(cudnnCreateSeqDataDescriptor(&this->mha_settings.q_desc));
            cudnnErrchk(cudnnCreateSeqDataDescriptor(&this->mha_settings.k_desc));
            cudnnErrchk(cudnnCreateSeqDataDescriptor(&this->mha_settings.v_desc));
            cudnnErrchk(cudnnCreateSeqDataDescriptor(&this->mha_settings.o_desc));

            // max number of tokens [Q, K]
            // size_t maxQTokens = size_t(this->mha_settings.mainCfg.seqLenQ) * this->mha_settings.mainCfg.batchSize * this->mha_settings.mainCfg.beamSize;
            // size_t maxKTokens = size_t(this->mha_settings.mainCfg.seqLenK) * this->mha_settings.mainCfg.batchSize;

            // Buffer Q/K/V/O capacity in elements.
            // size_t maxElemQ = maxQTokens * this->mha_settings.mainCfg.qSize;
            // size_t maxElemK = maxKTokens * this->mha_settings.mainCfg.kSize;
            // size_t maxElemV = maxKTokens * this->mha_settings.mainCfg.vSize;
            // size_t maxElemO = maxQTokens * outLen;
            // size_t maxElemA = maxQTokens * this->mha_settings.mainCfg.numHeads * this->mha_settings.mainCfg.seqLenK;

            // size_t maxElemQbar = maxQTokens * this->mha_settings.mainCfg.numHeads * this->mha_settings.mainCfg.qProjSize;
            // size_t maxElemKbar = maxKTokens * this->mha_settings.mainCfg.numHeads * this->mha_settings.mainCfg.kProjSize;
            // size_t maxElemVbar = maxKTokens * this->mha_settings.mainCfg.numHeads * this->mha_settings.mainCfg.vProjSize;
            // size_t maxElemHbar = maxQTokens * this->mha_settings.mainCfg.numHeads * this->mha_settings.mainCfg.vProjSize;

            // Allocate input and output buffers (forward/inference pass).
            // CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devQ, this->mha_settings.maxElemQ * this->mha_settings.size_t_elem));
            // CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devK, this->mha_settings.maxElemK * this->mha_settings.size_t_elem));
            // CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devV, this->mha_settings.maxElemV * this->mha_settings.size_t_elem));
            // CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devO, this->mha_settings.maxElemO * this->mha_settings.size_t_elem));

            // Allocate input and output buffers (backward/training pass).
            // if (this->mha_settings.is_training) {
            //     CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devDQ, this->mha_settings.maxElemQ * this->mha_settings.size_t_elem));
            //     CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devDK, this->mha_settings.maxElemK * this->mha_settings.size_t_elem));
            //     CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devDV, this->mha_settings.maxElemV * this->mha_settings.size_t_elem));
            //     CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devDO, this->mha_settings.maxElemO * this->mha_settings.size_t_elem));
            // }            

            // allocate dropout buffer
            cudnnErrchk(cudnnDropoutGetStatesSize(this->mha_settings.handle, &this->mha_settings.dropoutBufSize));
            cudaErrchk(cudaMalloc((void **)&this->mha_settings.dropoutBuf, this->mha_settings.dropoutBufSize));

            cudnnErrchk(cudnnSetDropoutDescriptor(this->mha_settings.drop_desc, 
            this->mha_settings.handle, 
            this->mha_settings.mainCfg.dropoutRate, 
            this->mha_settings.dropoutBuf, 
            this->mha_settings.dropoutBufSize, 
            0));

            cudnnErrchk(cudnnSetAttnDescriptor(this->mha_settings.attn_desc,            // done
                                                this->mha_settings.mainCfg.attnMode,    // done
                                                this->mha_settings.mainCfg.numHeads,    // done
                                                this->mha_settings.mainCfg.smScaler,    // done
                                                this->mha_settings.mainCfg.dataType,    // done
                                                this->mha_settings.mainCfg.compPrec,    // done
                                                CUDNN_DEFAULT_MATH,                     // done
                                                this->mha_settings.mainCfg.train          // done
                                                && this->mha_settings.mainCfg.dropoutRate > 0.0 
                                                ? this->mha_settings.drop_desc : NULL,
                                                NULL,
                                                this->mha_settings.mainCfg.qSize,       // done
                                                this->mha_settings.mainCfg.kSize,       // done
                                                this->mha_settings.mainCfg.vSize,       // done
                                                this->mha_settings.mainCfg.qProjSize,   // done
                                                this->mha_settings.mainCfg.kProjSize,   // done
                                                this->mha_settings.mainCfg.vProjSize,   // done
                                                this->mha_settings.mainCfg.oProjSize,   // done
                                                this->mha_settings.mainCfg.seqLenQ,     // done
                                                this->mha_settings.mainCfg.seqLenK,     // done
                                                this->mha_settings.mainCfg.batchSize,   // done
                                                this->mha_settings.mainCfg.beamSize)); 
            // get weight size
            if (this->mha_settings.mainCfg.train) {
                cudnnErrchk(cudnnGetMultiHeadAttnBuffers(this->mha_settings.handle, 
                this->mha_settings.attn_desc, 
                &this->mha_settings.maxWeights, 
                &this->mha_settings.maxWkspace, 
                &this->mha_settings.maxReserve));
            } else {
                cudnnErrchk(cudnnGetMultiHeadAttnBuffers(this->mha_settings.handle, 
                this->mha_settings.attn_desc, 
                &this->mha_settings.maxWeights, 
                &this->mha_settings.maxWkspace, 
                NULL));
            }
            // TODO: check consistent weight size


            // if (this->mha_settings.maxWeights > 0) {
            //     CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devW, this->mha_settings.maxWeights));
            //     if (this->mha_settings.is_training) {
            //         CHECK_CUDA_ERR(cudaMalloc((void **)&this->mha_settings.devDW, this->mha_settings.maxWeights));
            //     }
            // }


            this->mha_settings.qSeqArray = (int *)calloc(this->mha_settings.mainCfg.batchSize * this->mha_settings.mainCfg.beamSize, sizeof(int));
            this->mha_settings.kSeqArray = (int *)calloc(this->mha_settings.mainCfg.batchSize, sizeof(int));

            if (this->mha_settings.loWinIdx == NULL && this->mha_settings.hiWinIdx == NULL) {
                this->mha_settings.loWinIdx = (int *)calloc(this->mha_settings.mainCfg.seqLenQ, sizeof(int));
                this->mha_settings.hiWinIdx = (int *)calloc(this->mha_settings.mainCfg.seqLenQ, sizeof(int));
            }

            // Allocate weight and data buffers on the CPU side.
            // TODO: check type
            // if (this->mha_settings.size_t_elem == sizeof(int)) {
            //     // int
            //     if (this->mha_settings.maxWeights > 0) {
            //         this->mha_settings.hostW  = (int *)malloc(this->mha_settings.maxWeights);
            //         this->mha_settings.hostDW = (int *)malloc(this->mha_settings.maxWeights);
            //     }

            //     this->mha_settings.hostQ = (int *)malloc(this->mha_settings.maxElemQ * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostK = (int *)malloc(this->mha_settings.maxElemK * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostV = (int *)malloc(this->mha_settings.maxElemV * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostO = (int *)malloc(this->mha_settings.maxElemO * this->mha_settings.size_t_elem);

            //     // Allocate input and output buffers (backward/training pass).
            //     if (this->mha_settings.is_training) {
            //         this->mha_settings.hostDQ = (int *)malloc(this->mha_settings.maxElemQ * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDK = (int *)malloc(this->mha_settings.maxElemK * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDV = (int *)malloc(this->mha_settings.maxElemV * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDO = (int *)malloc(this->mha_settings.maxElemO * this->mha_settings.size_t_elem);
            //     }   
            // } else if (this->mha_settings.size_t_elem == sizeof(double)) {
            //     // double
            //     if (this->mha_settings.maxWeights > 0) {
            //         this->mha_settings.hostW  = (double *)malloc(this->mha_settings.maxWeights);
            //         this->mha_settings.hostDW = (double *)malloc(this->mha_settings.maxWeights);
            //     }

            //     this->mha_settings.hostQ = (double *)malloc(this->mha_settings.maxElemQ * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostK = (double *)malloc(this->mha_settings.maxElemK * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostV = (double *)malloc(this->mha_settings.maxElemV * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostO = (double *)malloc(this->mha_settings.maxElemO * this->mha_settings.size_t_elem);

            //     // Allocate input and output buffers (backward/training pass).
            //     if (this->mha_settings.is_training) {
            //         this->mha_settings.hostDQ = (double *)malloc(this->mha_settings.maxElemQ * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDK = (double *)malloc(this->mha_settings.maxElemK * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDV = (double *)malloc(this->mha_settings.maxElemV * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDO = (double *)malloc(this->mha_settings.maxElemO * this->mha_settings.size_t_elem);
            //     }   
            // } else if (this->mha_settings.size_t_elem == sizeof(float)) {
            //     // float
            //     if (this->mha_settings.maxWeights > 0) {
            //         this->mha_settings.hostW  = (float *)malloc(this->mha_settings.maxWeights);
            //         this->mha_settings.hostDW = (float *)malloc(this->mha_settings.maxWeights);
            //     }

            //     this->mha_settings.hostQ = (float *)malloc(this->mha_settings.maxElemQ * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostK = (float *)malloc(this->mha_settings.maxElemK * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostV = (float *)malloc(this->mha_settings.maxElemV * this->mha_settings.size_t_elem);
            //     this->mha_settings.hostO = (float *)malloc(this->mha_settings.maxElemO * this->mha_settings.size_t_elem);

            //     // Allocate input and output buffers (backward/training pass).
            //     if (this->mha_settings.is_training) {
            //         this->mha_settings.hostDQ = (float *)malloc(this->mha_settings.maxElemQ * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDK = (float *)malloc(this->mha_settings.maxElemK * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDV = (float *)malloc(this->mha_settings.maxElemV * this->mha_settings.size_t_elem);
            //         this->mha_settings.hostDO = (float *)malloc(this->mha_settings.maxElemO * this->mha_settings.size_t_elem);
            //     }   
            // } else {
            //         fprintf(stderr, "ERROR: unexpected T_ELEM (size of %d)\n\n", this->mha_settings.size_t_elem);
            //         exit(-1);
            // }          
            size_t qBatches = this->mha_settings.mainCfg.qSeqLenCount();
            size_t kBatches = this->mha_settings.mainCfg.kSeqLenCount();

            srand48(this->mha_settings.mainCfg.randSeed);

            if (this->mha_settings.mainCfg.randGeom != 0) {
                // TODO: random
            } else {
                // Fixed lengths for all sequences in a batch.
                for (size_t i = 0; i < qBatches; ++i) {
                    this->mha_settings.qSeqArray[i] = this->mha_settings.mainCfg.seqLenQ;
                    // qSeqArray[i] = testCfg->seqLenQ;
                }

                for (size_t i = 0; i < kBatches; ++i) {
                    this->mha_settings.kSeqArray[i] = this->mha_settings.mainCfg.seqLenK;
                }

                // Set the maximum attention window in all time-steps.
                for (int i = 0; i < this->mha_settings.mainCfg.seqLenQ; ++i) {
                    this->mha_settings.loWinIdx[i] = 0;
                    this->mha_settings.hiWinIdx[i] = this->mha_settings.mainCfg.seqLenK;
                }
            }   
            // print process
            // this->mha_settings.print_process();

        }

        template <typename T>
        void MHA1Op<T>::separate_tensor() {
            // initialize q, k, v
            int q_size = this->q_t->get_size();
            int k_size = this->k_t->get_size();
            int v_size = this->v_t->get_size();
            q_t->copy_from(*input_t, 0, q_size);
            k_t->copy_from(*input_t, q_size, k_size);
            v_t->copy_from(*input_t, q_size + k_size, v_size);
            // this->q = op::var("q", q_t);
            // this->v = op::var("v", v_t);
            // this->k = op::var("k", k_t);

            // separate tensor
            int o_len = this->mha_settings.mainCfg.oLength();
            int qSeqArraySize = this->mha_settings.mainCfg.beamSize * this->mha_settings.mainCfg.batchSize;
            int kSeqArraySize = this->mha_settings.mainCfg.batchSize;
            
            int dimA[CUDNN_SEQDATA_DIM_COUNT];
            cudnnSeqDataAxis_t ordA[CUDNN_SEQDATA_DIM_COUNT];
            int nbDims;
            // CUDNN_SEQDATA_TIME_DIM
            // CUDNN_SEQDATA_VECT_DIM
            // {beamsz, batchsz, qsz, seqlenq}
            dimA[CUDNN_SEQDATA_BEAM_DIM]  = this->mha_settings.mainCfg.beamSize;
            dimA[CUDNN_SEQDATA_BATCH_DIM] = this->mha_settings.mainCfg.batchSize;
            dimA[CUDNN_SEQDATA_TIME_DIM]  = this->mha_settings.mainCfg.seqLenQ;
            dimA[CUDNN_SEQDATA_VECT_DIM]  = this->mha_settings.mainCfg.qSize;

            cudnnErrchk(
                cudnnSetSeqDataDescriptor(
                    this->mha_settings.q_desc, 
                    this->mha_settings.mainCfg.dataType, 
                    CUDNN_SEQDATA_DIM_COUNT, dimA, 
                    this->mha_settings.mainCfg.dataAxes, 
                    qSeqArraySize, 
                    this->mha_settings.qSeqArray, NULL
                )
            );
            
            Tensor<float>* tmp = new Tensor<float>(q_t->get_shape(), {CONSTANT, {0.0f}}, HOST);
            
            if (this->mha_settings.mainCfg.fileDump) {
                cudnnErrchk(cudnnGetSeqDataDescriptor(this->mha_settings.q_desc, NULL, &nbDims, CUDNN_SEQDATA_DIM_COUNT, dimA, ordA, NULL, 0, NULL, NULL));
                tmp->copy_from(*q_t);
                save_data("q.dat", 0, 0, nbDims, dimA, ordA, tmp->get_ptr());
            }

            // {beamsz, batchsz, outlen, seqlenq}
            dimA[CUDNN_SEQDATA_BEAM_DIM]  = this->mha_settings.mainCfg.beamSize;
            dimA[CUDNN_SEQDATA_BATCH_DIM] = this->mha_settings.mainCfg.batchSize;
            dimA[CUDNN_SEQDATA_TIME_DIM]  = this->mha_settings.mainCfg.seqLenQ;
            dimA[CUDNN_SEQDATA_VECT_DIM]  = o_len;

            cudnnErrchk(
                cudnnSetSeqDataDescriptor(
                    this->mha_settings.o_desc, 
                    this->mha_settings.mainCfg.dataType, 
                    CUDNN_SEQDATA_DIM_COUNT, dimA, 
                    this->mha_settings.mainCfg.dataAxes, 
                    qSeqArraySize,
                    this->mha_settings.qSeqArray, NULL
                )
            );

            // {beamsz, batchsz, ksz, seqlenk}
            dimA[CUDNN_SEQDATA_BEAM_DIM]  = this->mha_settings.mainCfg.queryMap == CUDNN_ATTN_QUERYMAP_ONE_TO_ONE 
                                            ? this->mha_settings.mainCfg.beamSize : 1;
            dimA[CUDNN_SEQDATA_BATCH_DIM] = this->mha_settings.mainCfg.batchSize;
            dimA[CUDNN_SEQDATA_TIME_DIM]  = this->mha_settings.mainCfg.seqLenK;
            dimA[CUDNN_SEQDATA_VECT_DIM]  = this->mha_settings.mainCfg.kSize;

            cudnnErrchk(
                cudnnSetSeqDataDescriptor(
                    this->mha_settings.k_desc, 
                    this->mha_settings.mainCfg.dataType, 
                    CUDNN_SEQDATA_DIM_COUNT, dimA, 
                    this->mha_settings.mainCfg.dataAxes, 
                    kSeqArraySize, 
                    this->mha_settings.kSeqArray, NULL
                )
            );

            if (this->mha_settings.mainCfg.fileDump) {
                cudnnErrchk(cudnnGetSeqDataDescriptor(this->mha_settings.k_desc, NULL, &nbDims, CUDNN_SEQDATA_DIM_COUNT, dimA, ordA, NULL, 0, NULL, NULL));         
                tmp->copy_from(*k_t);
                save_data("k.dat", 0, 0, nbDims, dimA, ordA, tmp->get_ptr());
            }

            // {beamsz, batchsz, vsz, seqlenk}
            dimA[CUDNN_SEQDATA_BEAM_DIM]  = this->mha_settings.mainCfg.queryMap == CUDNN_ATTN_QUERYMAP_ONE_TO_ONE ? 
                                            this->mha_settings.mainCfg.beamSize : 1;
            dimA[CUDNN_SEQDATA_BATCH_DIM] = this->mha_settings.mainCfg.batchSize;
            dimA[CUDNN_SEQDATA_TIME_DIM]  = this->mha_settings.mainCfg.seqLenK; // seqlenk == seqlenv
            dimA[CUDNN_SEQDATA_VECT_DIM]  = this->mha_settings.mainCfg.vSize;

            cudnnErrchk(
                cudnnSetSeqDataDescriptor(
                    this->mha_settings.v_desc, 
                    this->mha_settings.mainCfg.dataType, 
                    CUDNN_SEQDATA_DIM_COUNT, dimA, 
                    this->mha_settings.mainCfg.dataAxes, 
                    kSeqArraySize, 
                    this->mha_settings.kSeqArray, NULL
                )
            );
            if (this->mha_settings.mainCfg.fileDump) {
                cudnnErrchk(cudnnGetSeqDataDescriptor(this->mha_settings.v_desc, NULL, &nbDims, CUDNN_SEQDATA_DIM_COUNT, dimA, ordA, NULL, 0, NULL, NULL));
                tmp->copy_from(*v_t);
                save_data("v.dat", 0, 0, nbDims, dimA, ordA, tmp->get_ptr());
            }
        }

        #if defined(MAGMADNN_HAVE_CUDA)
        template <typename T>
        void MHA1Op<T>::cuda_forward() {
            // this->mha_settings.handle = this->get_cudnn_handle();
            cudnnErrchk(
                cudnnSetDropoutDescriptor(
                    this->mha_settings.drop_desc, 
                    this->mha_settings.handle, 
                    this->mha_settings.mainCfg.dropoutRate, 
                    this->mha_settings.dropoutBuf, 
                    this->mha_settings.dropoutBufSize, 
                    0
                )
            );

            // Set attention descriptor according to generated testCfg.
            cudnnErrchk(
                cudnnSetAttnDescriptor(
                    this->mha_settings.attn_desc,
                    this->mha_settings.mainCfg.attnMode,
                    this->mha_settings.mainCfg.numHeads,
                    this->mha_settings.mainCfg.smScaler,
                    this->mha_settings.mainCfg.dataType,   
                    this->mha_settings.mainCfg.compPrec,
                    CUDNN_DEFAULT_MATH,
                    this->mha_settings.mainCfg.train && this->mha_settings.mainCfg.dropoutRate > 0.0 
                    ? this->mha_settings.drop_desc : NULL,
                    NULL,
                    this->mha_settings.mainCfg.qSize,
                    this->mha_settings.mainCfg.kSize,
                    this->mha_settings.mainCfg.vSize,
                    this->mha_settings.mainCfg.qProjSize,
                    this->mha_settings.mainCfg.kProjSize,
                    this->mha_settings.mainCfg.vProjSize,
                    this->mha_settings.mainCfg.oProjSize,
                    this->mha_settings.mainCfg.seqLenQ,
                    this->mha_settings.mainCfg.seqLenK,
                    this->mha_settings.mainCfg.batchSize,
                    this->mha_settings.mainCfg.beamSize
                )
            );

            if (this->mha_settings.mainCfg.train) {
                cudnnErrchk(cudnnGetMultiHeadAttnBuffers(this->mha_settings.handle, this->mha_settings.attn_desc, &this->mha_settings.sizeWeights, &this->mha_settings.sizeWkspace, &this->mha_settings.sizeReserve));
            } else {
                cudnnErrchk(cudnnGetMultiHeadAttnBuffers(this->mha_settings.handle, this->mha_settings.attn_desc, &this->mha_settings.sizeWeights, &this->mha_settings.sizeWkspace, NULL));
            }

            // Sanity check so we do not over-run the allocated buffers.
            if (this->mha_settings.sizeWeights > this->mha_settings.maxWeights || this->mha_settings.sizeWkspace > this->mha_settings.maxWkspace || this->mha_settings.sizeReserve > this->mha_settings.maxReserve) {
                fprintf(stderr, "ERROR: cudnnGetMultiHeadAttnBuffers() reported inconsistent buffer sizes\n\n");
                exit(-1);
            }

            if (this->mha_settings.sizeWkspace > 0) {
                cudaErrchk(cudaMalloc((void **)&this->mha_settings.devWkspace, this->mha_settings.sizeWkspace));
            }
            if (this->mha_settings.sizeReserve > 0) {
                cudaErrchk(cudaMalloc((void **)&this->mha_settings.devReserve, this->mha_settings.sizeReserve));

                // Fill with -NaN to deterct incorrect segment write for debugging.
                cudaErrchk(cudaMemset(this->mha_settings.devReserve, 0xff, this->mha_settings.sizeReserve));
            }

            int qSeqArraySize = this->mha_settings.mainCfg.beamSize * this->mha_settings.mainCfg.batchSize;
            int kSeqArraySize = this->mha_settings.mainCfg.batchSize;
            
            // host-to-device copies
            size_t size = sizeof(this->mha_settings.qSeqArray[0]) * qSeqArraySize;
            cudaErrchk(cudaMalloc((void **)&this->mha_settings.devQSeqArray, size));
            cudaErrchk(cudaMemcpy(this->mha_settings.devQSeqArray, this->mha_settings.qSeqArray, size, cudaMemcpyHostToDevice));

            size = sizeof(this->mha_settings.kSeqArray[0]) * kSeqArraySize;
            cudaErrchk(cudaMalloc((void **)&this->mha_settings.devKSeqArray, size));
            cudaErrchk(cudaMemcpy(this->mha_settings.devKSeqArray, this->mha_settings.kSeqArray, size, cudaMemcpyHostToDevice));

            // set separate descriptors
            separate_tensor();

            // forward
            ::magmadnn::math::mha_device(this->q_t, this->k_t, this->v_t, this->o_t, this->w_t, this->mha_settings);

            if (!this->get_async()) {
                cudaStreamSynchronize(this->get_custream());
            }
        }
        #endif     

        template class MHA1Op<float>;

        // template class MHA1Op<double>;

        // template <typename T>
        // MHA1Op<T> *mha1(Operation<T> *q, Operation<T> *k, 
		// 		Operation<T> *v, Operation<T> *weights, bool needs_grad) {
        //             return new MHA1Op<T>(q, k, v, weights, needs_grad);  
        // }

        template <typename T>
        MHA1Op<T> *mha1(Operation<T> *input, Operation<T> *weights, ::magmadnn::math::attnOptions opts, bool needs_grad) {
                    return new MHA1Op<T>(input, weights, opts, needs_grad);  
        }

        // template
        // MHA1Op<float> *mha1(Operation<float> *q, Operation<float> *k, 
		// 		Operation<float> *v, Operation<float> *weights, bool needs_grad);

        template 
        MHA1Op<float> *mha1(Operation<float> *input, Operation<float> *weights, ::magmadnn::math::attnOptions opts, bool needs_grad);

        // MHA1Op<double> *mha1(Operation<double> *q, Operation<double> *k, 
		// 		Operation<double> *v, Operation<double> *weights, bool needs_grad = true);
    } // namespace op
} // namespace magmadnn