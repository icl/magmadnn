/**
 * @file lstm_internal_device.cu
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2019-02-22
 */
#include <algorithm>
#include "compute/lstm/lstm_internal.h"
#include "magmadnn/math.h"
#include "tensor/tensor.h"
#include "math/wrappers.h"
#include "magma.h"
#include "tensor/tensor_io.h"

#define BLK_SIZE 1024

namespace magmadnn {
namespace internal {

//from slice_internal_device.cu
template <typename T>
__global__ void kernel_slice_gpu(T * A, T * B, unsigned int ind, unsigned int size, unsigned int ten_size, unsigned int Z, unsigned int width) {
        unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
        unsigned int stride = blockDim.x * gridDim.x;
        for (unsigned int i = idx; i < size; i += stride) {
                B[i] = A[ind * width + i % width + (i/width) * ten_size];
        }

}

//From sigmoid_internal_device.cu
template <typename T>
__global__ void kernel_sig_full_device(unsigned int size, T *x, T *out) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int i = idx; i < size; i += stride) {
        out[i] = 1 / (1 + exp(-x[i]));
    }
}

//from product_internal_device.cu
template <typename T>
__global__ void kernel_product_full_device(T *a, T *b, T *out, unsigned int arr_size) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int i = idx; i < arr_size; i += stride) {
//	printf("left val: %f   right val: %f   out val: %f\n",a[i],b[i],a[i]*b[i]);
        out[i] = a[i] * b[i];
    }
}


template <typename T>
__global__ void kernel_add_full_device(T *a, T *b, T *out, unsigned int arr_size) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int i = idx; i < arr_size; i += stride) {
        out[i] = a[i] + b[i];
    }
}

//from tanh_internal_device.cu
template <typename T>
__global__ void kernel_tanh_full_device(unsigned int size, T *x, T *out) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int stride = blockDim.x * gridDim.x;

    for (unsigned int i = idx; i < size; i += stride) {
        out[i] = tanh(x[i]);
    }
}


template <typename T>
__global__
void kernel_concat_gpu(T ** A, T * C,  unsigned int size, unsigned int axis,
                unsigned int ten_size, unsigned int width, unsigned int num_tens) {
        //TODO implement kernal for axis != 1
        unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
        unsigned int stride = blockDim.x * gridDim.x;
        for (unsigned int i = idx; i < size; i += stride) {
//              printf("\ni: %u,    i/width mod num_tens: %u,    width* (i/ten_size) + i mod width: %u\n", i,
//                              (i/width)%num_tens, width * (i/ten_size) + i % width);
//              C[i] = A[(i/width)%num_tens][width * (i/ten_size) + i % width];

                C[i] = A[(i/width)%num_tens][width * (i/(num_tens*width)) + i % width];

        }
}

template <typename T>
void concat_gpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output) {
        unsigned int size = output->get_size();
        const auto grid_dim = ceildiv(size, BLK_SIZE);

        //pointer to each tensor pointer in inputs
        T ** in;
        cudaMallocManaged(&in, inputs.size() * sizeof(long));
        for (unsigned int i = 0; i < inputs.size(); i++){
                in[i] = inputs[i]->get_ptr();
        }

        //TODO implement for axis != 1
        if (axis != 1){
                throw std::invalid_argument("ConcatOp gpu has only been implemented for axis = 1");
        }

        //kernel call
        kernel_concat_gpu<<<grid_dim, BLK_SIZE>>>
                (in, output->get_ptr(),
                 size, axis, inputs[0]->get_size(), inputs[0]->get_shape(1), inputs.size());

//	for (unsigned i = 0; i < inputs.size(); i++) delete in[i];
	cudaFree(in);
        //maybe free T ** in
	
}

//template void concat_gpu(std::vector<Tensor<int>* > inputs, unsigned int axis, Tensor<int> *output);
template void concat_gpu(std::vector<Tensor<float>* > inputs, unsigned int axis, Tensor<float> *output);
template void concat_gpu(std::vector<Tensor<double>* > inputs, unsigned int axis, Tensor<double> *output);



void lstm_gpu(cudaStream_t custream, Tensor<float> * input, std::vector<Tensor<float> *> weights, 
		Tensor<float> * h_init, Tensor<float> * c_init, bool return_sequences,	
		std::vector<Tensor<float> *> *X, std::vector<Tensor<float> *> *C,
		std::vector<Tensor<float> *> *H, std::vector<Tensor<float> *> *B,
		std::vector<Tensor<float> *> *F, std::vector<Tensor<float> *> *I,
		std::vector<Tensor<float> *> *O, std::vector<Tensor<float> *> *tC,
		Tensor<float> *output) {
	
	unsigned dim0 = input->get_shape(0);
	unsigned dim1 = input->get_shape(1);	
	unsigned dim2 = input->get_shape(2);
	unsigned num_nodes = weights[0]->get_shape(1);

	Tensor<float> * c_cur = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * h_cur = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	c_cur->copy_from(*(c_init));
	h_cur->copy_from(*(h_init));
	
	Tensor<float> * t1 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	t1->copy_from(*c_cur);
	C->push_back(t1);
	Tensor<float> * t2 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	t2->copy_from(*h_cur);
	H->push_back(t2);


	Tensor<float> * o1 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * o2 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * o3 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * o4 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);

//TODO get rid of extra temporary tensors	
	Tensor<float> * f1 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * f2 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * f3 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * f4 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);

	Tensor<float> * i1 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * i2 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * i3 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * i4 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);

	Tensor<float> * c1 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * c2 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * c3 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<float> * c4 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);

	
	int M; 
	int N; 
	int K; 


	//weights:
	//0 Wf, 1  Uf,  2 Bf
	//3 Wi, 4  Ui,  5 Bi
	//6 Wo, 7  Uo,  8 Bo
	//9 Wc, 10 Uc, 11 Bc

	for (unsigned int i = 0; i < dim1; i++){
		
		//calculating cur_op
	
	
		Tensor<float> * cur_op = new Tensor<float>({dim0, dim2}, {NONE,{}}, DEVICE);
		kernel_slice_gpu<<<ceildiv(cur_op->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(input->get_ptr(), cur_op->get_ptr(), i,cur_op->get_size(),
			input->get_shape(1) * input->get_shape(2), input->get_shape(0), input->get_shape(2));
		
		X->push_back(cur_op);
		
		//TODO replace with values that are already stored to avoid get_shape
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[6]->get_shape(1);



		//calculate o_t
		//o_t = sigmoid(xt * Wo + (ht-1) * Uo + Bo)
		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[6]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0.f, o1->get_ptr(), N);

		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[7]->get_shape(1);
		
		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[7]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0.f, o2->get_ptr(), N);
		
		kernel_add_full_device<<<(o3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o1->get_ptr(), o2->get_ptr(), o3->get_ptr(), o3->get_size());
		kernel_add_full_device<<<(o4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o3->get_ptr(), weights[8]->get_ptr(), o4->get_ptr(), o4->get_size());

		Tensor<float> * o_t = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_sig_full_device <<<ceildiv(o_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(o_t->get_size(), o4->get_ptr(), o_t->get_ptr());
		O->push_back(o_t);
		//calculating f_t
		//f_t = sigmoid(xt * Wf + (ht-1) * Uf + Bf)
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[0]->get_shape(1);
		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[0]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0.f, f1->get_ptr(), N);

		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[1]->get_shape(1);
		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[1]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0.f, f2->get_ptr(), N);
		
		kernel_add_full_device<<<(f3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(f1->get_ptr(), f2->get_ptr(), f3->get_ptr(), f3->get_size());
		
		kernel_add_full_device<<<(f4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(f3->get_ptr(), weights[2]->get_ptr(), f4->get_ptr(), f4->get_size());
  		
		Tensor<float> * f_t = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_sig_full_device <<<ceildiv(f_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(f_t->get_size(), f4->get_ptr(), f_t->get_ptr());
		F->push_back(f_t);
		

		//calculating i_t
		//i_t = sigmoid(xt * Wi + (ht-1) * Ui + Bi)
		
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[3]->get_shape(1);
		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[3]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0.f, i1->get_ptr(), N);

		
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[4]->get_shape(1);

		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[4]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0.f, i2->get_ptr(), N);
		
		kernel_add_full_device<<<(i3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(i1->get_ptr(), i2->get_ptr(), i3->get_ptr(), i3->get_size());
		
		kernel_add_full_device<<<(i4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(i3->get_ptr(), weights[5]->get_ptr(), i4->get_ptr(), i4->get_size());
  	

		Tensor<float> * i_t = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_sig_full_device <<<ceildiv(i_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(i_t->get_size(), i4->get_ptr(), i_t->get_ptr());	
		
		I->push_back(i_t);
	
		//calculating ct_t
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[9]->get_shape(1);

		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[9]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0.f, c1->get_ptr(), N);
	
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[10]->get_shape(1);
		magma_sgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1.f, weights[10]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0.f, c2->get_ptr(), N);
		kernel_add_full_device<<<(c3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(c1->get_ptr(), c2->get_ptr(), c3->get_ptr(), c3->get_size());
		kernel_add_full_device<<<(c4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(c3->get_ptr(), weights[11]->get_ptr(), c4->get_ptr(), c4->get_size());

		Tensor<float> * ct_t = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_tanh_full_device <<<ceildiv(ct_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(ct_t->get_size(), c4->get_ptr(), ct_t->get_ptr());
		tC->push_back(ct_t);


		//calculate c_cur	
		kernel_product_full_device<<<(o1->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(f_t->get_ptr(), c_cur->get_ptr(), o1->get_ptr(), o1->get_size());
		kernel_product_full_device<<<(o2->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(i_t->get_ptr(), ct_t->get_ptr(), o2->get_ptr(), o2->get_size());
		kernel_add_full_device<<<(c_cur->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o1->get_ptr(), o2->get_ptr(), c_cur->get_ptr(), c_cur->get_size());
	
		Tensor<float> * c_temp = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		c_temp->copy_from(*c_cur);
		C->push_back(c_temp);
	
	
		//calculate h_cur	

		Tensor<float> * h1 = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_tanh_full_device<<<(c_cur->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(c_cur->get_size(), c_cur->get_ptr(), h1->get_ptr());
		
		B->push_back(h1);

		kernel_product_full_device<<<(h_cur->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o_t->get_ptr(), h1->get_ptr(), h_cur->get_ptr(), h_cur->get_size());
		
		Tensor<float> * h_temp = new Tensor<float>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		h_temp->copy_from(*h_cur);
		
		H->push_back(h_temp);

		
	}
	if (!return_sequences) output->copy_from(*h_cur);

	delete o1;
        delete o2;
        delete o3;
        delete o4;
        delete f1;
        delete f2;
        delete f3;
        delete f4;
        delete i1;
        delete i2;
        delete i3;
        delete i4;
        delete c1;
        delete c2;
        delete c3;
        delete c4;
	delete h_cur;
	delete c_cur;
}

void lstm_gpu(cudaStream_t custream, Tensor<double> * input, std::vector<Tensor<double> *> weights, 
		Tensor<double> * h_init, Tensor<double> * c_init, bool return_sequences,	
		std::vector<Tensor<double> *> *X, std::vector<Tensor<double> *> *C,
		std::vector<Tensor<double> *> *H, std::vector<Tensor<double> *> *B,
		std::vector<Tensor<double> *> *F, std::vector<Tensor<double> *> *I,
		std::vector<Tensor<double> *> *O, std::vector<Tensor<double> *> *tC,
		Tensor<double> *output) {
	
	unsigned dim0 = input->get_shape(0);
	unsigned dim1 = input->get_shape(1);	
	unsigned dim2 = input->get_shape(2);
	unsigned num_nodes = weights[0]->get_shape(1);

	Tensor<double> * c_cur = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * h_cur = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	c_cur->copy_from(*(c_init));
	h_cur->copy_from(*(h_init));
	
	Tensor<double> * t1 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	t1->copy_from(*c_cur);
	C->push_back(t1);
	Tensor<double> * t2 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	t2->copy_from(*h_cur);
	H->push_back(t2);


	Tensor<double> * o1 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * o2 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * o3 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * o4 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);

//TODO get rid of extra temporary tensors	
	Tensor<double> * f1 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * f2 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * f3 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * f4 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);

	Tensor<double> * i1 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * i2 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * i3 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * i4 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);

	Tensor<double> * c1 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * c2 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * c3 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
	Tensor<double> * c4 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);

	
	int M; 
	int N; 
	int K; 


	//weights:
	//0 Wf, 1  Uf,  2 Bf
	//3 Wi, 4  Ui,  5 Bi
	//6 Wo, 7  Uo,  8 Bo
	//9 Wc, 10 Uc, 11 Bc

	for (unsigned int i = 0; i < dim1; i++){
		
		//calculating cur_op
	
	
		Tensor<double> * cur_op = new Tensor<double>({dim0, dim2}, {NONE,{}}, DEVICE);
		kernel_slice_gpu<<<ceildiv(cur_op->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(input->get_ptr(), cur_op->get_ptr(), i,cur_op->get_size(),
			input->get_shape(1) * input->get_shape(2), input->get_shape(0), input->get_shape(2));
		
		X->push_back(cur_op);
		
		//TODO replace with values that are already stored to avoid get_shape
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[6]->get_shape(1);



		//calculate o_t
		//o_t = sigmoid(xt * Wo + (ht-1) * Uo + Bo)
		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[6]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0., o1->get_ptr(), N);

		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[7]->get_shape(1);
		
		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[7]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0., o2->get_ptr(), N);
		
		kernel_add_full_device<<<(o3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o1->get_ptr(), o2->get_ptr(), o3->get_ptr(), o3->get_size());
		kernel_add_full_device<<<(o4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o3->get_ptr(), weights[8]->get_ptr(), o4->get_ptr(), o4->get_size());

		Tensor<double> * o_t = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_sig_full_device <<<ceildiv(o_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(o_t->get_size(), o4->get_ptr(), o_t->get_ptr());
		O->push_back(o_t);
		//calculating f_t
		//f_t = sigmoid(xt * Wf + (ht-1) * Uf + Bf)
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[0]->get_shape(1);
		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[0]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0., f1->get_ptr(), N);

		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[1]->get_shape(1);
		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[1]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0., f2->get_ptr(), N);
		
		kernel_add_full_device<<<(f3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(f1->get_ptr(), f2->get_ptr(), f3->get_ptr(), f3->get_size());
		
		kernel_add_full_device<<<(f4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(f3->get_ptr(), weights[2]->get_ptr(), f4->get_ptr(), f4->get_size());
  		
		Tensor<double> * f_t = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_sig_full_device <<<ceildiv(f_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(f_t->get_size(), f4->get_ptr(), f_t->get_ptr());
		F->push_back(f_t);
		

		//calculating i_t
		//i_t = sigmoid(xt * Wi + (ht-1) * Ui + Bi)
		
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[3]->get_shape(1);
		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[3]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0., i1->get_ptr(), N);

		
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[4]->get_shape(1);

		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[4]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0., i2->get_ptr(), N);
		
		kernel_add_full_device<<<(i3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(i1->get_ptr(), i2->get_ptr(), i3->get_ptr(), i3->get_size());
		
		kernel_add_full_device<<<(i4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(i3->get_ptr(), weights[5]->get_ptr(), i4->get_ptr(), i4->get_size());
  	

		Tensor<double> * i_t = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_sig_full_device <<<ceildiv(i_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(i_t->get_size(), i4->get_ptr(), i_t->get_ptr());	
		
		I->push_back(i_t);
	
		//calculating ct_t
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[9]->get_shape(1);

		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[9]->get_ptr(), N,
			       	cur_op->get_ptr(), K, 0., c1->get_ptr(), N);
	
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[10]->get_shape(1);
		magma_dgemm(MagmaNoTrans, MagmaNoTrans, N, M, K, 1., weights[10]->get_ptr(), N,
			       	h_cur->get_ptr(), K, 0., c2->get_ptr(), N);
		kernel_add_full_device<<<(c3->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(c1->get_ptr(), c2->get_ptr(), c3->get_ptr(), c3->get_size());
		kernel_add_full_device<<<(c4->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(c3->get_ptr(), weights[11]->get_ptr(), c4->get_ptr(), c4->get_size());

		Tensor<double> * ct_t = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_tanh_full_device <<<ceildiv(ct_t->get_size(), BLK_SIZE), BLK_SIZE, 0, custream>>>
			(ct_t->get_size(), c4->get_ptr(), ct_t->get_ptr());
		tC->push_back(ct_t);


		//calculate c_cur	
		kernel_product_full_device<<<(o1->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(f_t->get_ptr(), c_cur->get_ptr(), o1->get_ptr(), o1->get_size());
		kernel_product_full_device<<<(o2->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(i_t->get_ptr(), ct_t->get_ptr(), o2->get_ptr(), o2->get_size());
		kernel_add_full_device<<<(c_cur->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o1->get_ptr(), o2->get_ptr(), c_cur->get_ptr(), c_cur->get_size());
	
		Tensor<double> * c_temp = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		c_temp->copy_from(*c_cur);
		C->push_back(c_temp);
	
	
		//calculate h_cur	

		Tensor<double> * h1 = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		kernel_tanh_full_device<<<(c_cur->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(c_cur->get_size(), c_cur->get_ptr(), h1->get_ptr());
		
		B->push_back(h1);

		kernel_product_full_device<<<(h_cur->get_size() + BLK_SIZE - 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>
			(o_t->get_ptr(), h1->get_ptr(), h_cur->get_ptr(), h_cur->get_size());
		
		Tensor<double> * h_temp = new Tensor<double>({dim0, num_nodes}, {NONE,{}}, DEVICE);
		h_temp->copy_from(*h_cur);
		
		H->push_back(h_temp);

		
	}
	if (!return_sequences) output->copy_from(*h_cur);

	delete o1;
        delete o2;
        delete o3;
        delete o4;
        delete f1;
        delete f2;
        delete f3;
        delete f4;
        delete i1;
        delete i2;
        delete i3;
        delete i4;
        delete c1;
        delete c2;
        delete c3;
        delete c4;
	delete h_cur;
	delete c_cur;
}

// Gamma Bars
template <typename T>
__global__ void kernal_lstm_grad_gmft_full_device(unsigned int size, T *c_bar,
		T *ft, T *ctm1, T *gm_f_bar) {

	unsigned idx = blockDim.x*blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;

	for (unsigned i = idx; i < size; i += stride) {
		gm_f_bar[i] = c_bar[i] * ctm1[i] * ft[i] * (1-ft[i]);
	}

}

template <typename T>
__global__ void kernal_lstm_grad_gmit_full_device(unsigned int size, T *c_bar,
		T *it, T *tct, T *gm_i_bar) {

	unsigned idx = blockDim.x *blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;

	for (unsigned i = idx; i < size; i += stride) {
		gm_i_bar[i] = c_bar[i] * tct[i] * it[i] * (1-it[i]);
	}
}

template <typename T>
__global__ void kernal_lstm_grad_gmot_full_device(unsigned int size, T *h_bar,
		T *Bt, T *ot, T *gm_o_bar) {

	unsigned idx = blockDim.x *blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;

	for (unsigned i = idx; i < size; i += stride) {
		gm_o_bar[i] = h_bar[i] * Bt[i] * ot[i] * (1-ot[i]);
	}
}

template <typename T>
__global__ void kernal_lstm_grad_gmct_full_device(unsigned int size, T *c_bar,
		T *it, T *tct, T *gm_c_bar) {

	unsigned idx = blockDim.x *blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;

	for (unsigned i = idx; i < size; i += stride) {
		gm_c_bar[i] = c_bar[i] * it[i] * (1 - tct[i]*tct[i]);
	}
}

// c_bar
template <typename T>
__global__ void kernal_lstm_grad_c_bar_full_device(unsigned int size, T *h_bar,
		T *c_barp1, T *Bt, T *ftp1, T* ot, T* output) {
	unsigned idx = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;

	for (unsigned i = idx; i < size; i += stride) {
		output[i] = h_bar[i]*ot[i]*(1 - Bt[i]*Bt[i]) + ftp1[i]*c_barp1[i];
	}
}

// Utility
template <typename T>
__global__ void kernel_set_zeros_full_device(unsigned size, T *ten) {

	unsigned idx = blockDim.x *blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;

	for (unsigned i = idx; i < size; i += stride) {
		ten[i] = (T)0;
	}
}

template <typename T>
__global__ void kernel_add_many_full_device(unsigned size, T **in,
		unsigned num_tens, T *output) {

	unsigned idx = blockDim.x *blockIdx.x + threadIdx.x;
	unsigned stride = blockDim.x * gridDim.x;
	
	for (unsigned i = idx; i < size; i += stride) {
		for (unsigned j = 0; j < num_tens; ++j) {	
			output[i] += in[j][i];
		}
	}
}



void lstm_grad_device(cudaStream_t custream, Tensor<float> *grad, internal::WrtVariable wrt, 
		std::vector<Tensor<float> *> weights, std::vector<Tensor<float> *> X,
		std::vector<Tensor<float> *> C, std::vector<Tensor<float> *> H,
		std::vector<Tensor<float> *> B, std::vector<Tensor<float> *> F,
		std::vector<Tensor<float> *> I, std::vector<Tensor<float> *> O,
		std::vector<Tensor<float> *> tC, Tensor<float> *output) {
	
	unsigned batchsz = X[0]->get_shape(0);
	unsigned timesteps = X.size();
	unsigned features = X[0]->get_shape(1);
	unsigned num_nodes = H[0]->get_shape(1);
	unsigned osize = batchsz * num_nodes;
	bool return_sequences = false;
	
	Tensor<float> *h_bar;
	Tensor<float> *c_bar = new Tensor<float>(C[0]->get_shape(), {CONSTANT, {0.f}},
			DEVICE);
	if (grad->get_shape().size() == 3) {
		return_sequences = true;
		h_bar = new Tensor<float>(H[0]->get_shape(),
				{CONSTANT,{0.f}}, DEVICE);
	}
	else
	{
		h_bar = new Tensor<float>(H[0]->get_shape(),
				{CONSTANT, {0.f}}, DEVICE);
		h_bar->copy_from(*grad, 0, osize);
	}
	

	// These will all be stored in reverse order, i.e. last sequence step
	// at the first index
	std::vector<Tensor<float> *> outputs;
	std::vector<Tensor<float> *> gm_f_bars;
	std::vector<Tensor<float> *> gm_i_bars;
	std::vector<Tensor<float> *> gm_o_bars;
	std::vector<Tensor<float> *> gm_c_bars;
	
	// initialize to proper values
	if (wrt != internal::WrtVariable::X) {
		kernel_set_zeros_full_device
			<<<(output->get_size()+BLK_SIZE + 1)/BLK_SIZE, BLK_SIZE, 0, custream>>>
				(output->get_size(), output->get_ptr());
	}


	// TODO use _grad_cache in LSTMOp to avoid redundant calculations.

	// Calculate gamma_bars
	for (unsigned i = 0; i < timesteps; ++i) {
		

		if (return_sequences) {	
			Tensor<float> * h_in = new Tensor<float>(H[0]->get_shape(),
        	                {CONSTANT,{0.f}}, DEVICE);	
			kernel_slice_gpu
				<<<(osize + BLK_SIZE + 1)/BLK_SIZE, BLK_SIZE, 0, custream>>>
				(grad->get_ptr(), h_in->get_ptr(),
					timesteps-i-1, osize,
					grad->get_shape(1)*grad->get_shape(2),
					grad->get_shape(0), grad->get_shape(2));
			kernel_add_full_device<<<(osize + BLK_SIZE + 1), BLK_SIZE, 0, custream>>>
				(h_bar->get_ptr(), h_in->get_ptr(),
				 h_bar->get_ptr(), osize);
			delete h_in;
		}
		
		unsigned fi = 0;
		if (i == 0) {
			fi = 1;	
		}


		
		kernal_lstm_grad_c_bar_full_device
			<<<(osize + BLK_SIZE + 1)/BLK_SIZE, BLK_SIZE, 0, custream>>>
				(osize, h_bar->get_ptr(),
				c_bar->get_ptr(), B[B.size()-i-1]->get_ptr(),
				F[F.size()-i-fi]->get_ptr(),
				O[O.size()-i-1]->get_ptr(),
				c_bar->get_ptr());

		Tensor<float> * gm_t_bars[4];

		Tensor<float> *gm_f_bar = new Tensor<float>(F[0]->get_shape(),
				{NONE, {}}, DEVICE);
		Tensor<float> *gm_i_bar = new Tensor<float>(I[0]->get_shape(),
				{NONE,{}}, DEVICE);
		Tensor<float> *gm_o_bar = new Tensor<float>(O[0]->get_shape(),
				{NONE,{}}, DEVICE);
		Tensor<float> *gm_c_bar = new Tensor<float>(tC[0]->get_shape(),
				{NONE,{}}, DEVICE);
		//TODO run gamma calculation in parallel
		kernal_lstm_grad_gmft_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					c_bar->get_ptr(),
					F[F.size()-i-1]->get_ptr(),
					C[C.size()-i-2]->get_ptr(),
					gm_f_bar->get_ptr());
		

		kernal_lstm_grad_gmit_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					c_bar->get_ptr(),
					I[F.size()-i-1]->get_ptr(),
					tC[tC.size()-i-1]->get_ptr(),
					gm_i_bar->get_ptr());
		
		

		
		kernal_lstm_grad_gmot_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					h_bar->get_ptr(),
					B[B.size()-i-1]->get_ptr(),
					O[O.size()-i-1]->get_ptr(),
					gm_o_bar->get_ptr());
		

		kernal_lstm_grad_gmct_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					c_bar->get_ptr(),
					I[I.size() -i-1]->get_ptr(),
					tC[tC.size()-i-1]->get_ptr(),
					gm_c_bar->get_ptr());
		
		gm_f_bars.push_back(gm_f_bar);
		gm_i_bars.push_back(gm_i_bar);
		gm_o_bars.push_back(gm_o_bar);
		gm_c_bars.push_back(gm_c_bar);
			

		gm_t_bars[0] = gm_f_bar;
		gm_t_bars[1] = gm_i_bar;
		gm_t_bars[2] = gm_o_bar;
		gm_t_bars[3] = gm_c_bar;

		unsigned N = weights[1]->get_shape(0);
		unsigned K = gm_f_bar->get_shape(1);
		unsigned M = gm_f_bar->get_shape(0);

		// calculate next h_bar	
		magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
				weights[1]->get_ptr(), K, gm_f_bar->get_ptr(), K,
				0.f, h_bar->get_ptr(), N);
	
		
	
		magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
				weights[4]->get_ptr(), K, gm_i_bar->get_ptr(), K,
				1.f, h_bar->get_ptr(), N);
		

		magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
				weights[7]->get_ptr(), K, gm_o_bar->get_ptr(), K,
				1.f, h_bar->get_ptr(), N);

		
		magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
				weights[10]->get_ptr(), K, gm_c_bar->get_ptr(), K,
				1.f, h_bar->get_ptr(), N);
		
		if (wrt == internal::WrtVariable::X) { 
			// calculate xt_bar
			N = weights[0]->get_shape(0);
			K = gm_f_bar->get_shape(1);
			M = gm_f_bar->get_shape(0);
			Tensor<float> *xt_bar = new Tensor<float>(X[i]->get_shape(),
					{NONE, {}}, DEVICE);

			magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
					weights[0]->get_ptr(), K,
					gm_f_bar->get_ptr(), K,
					0.f, xt_bar->get_ptr(), N);

			magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
					weights[3]->get_ptr(), K,
					gm_i_bar->get_ptr(), K,
					1.f, xt_bar->get_ptr(), N);

			magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
					weights[6]->get_ptr(), K,
					gm_o_bar->get_ptr(), K,
					1.f, xt_bar->get_ptr(), N);

			magma_sgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.f,
					weights[9]->get_ptr(), K,
					gm_c_bar->get_ptr(), K,
					1.f, xt_bar->get_ptr(), N);

			outputs.push_back(xt_bar);
		}

		
		if (wrt == internal::WrtVariable::WF
				|| wrt == internal::WrtVariable::WI
				|| wrt == internal::WrtVariable::WO
				|| wrt == internal::WrtVariable::WC) {
			unsigned idx;
			if (wrt == internal::WrtVariable::WF)
				idx = 0;
			else if (wrt == internal::WrtVariable::WI)
				idx = 1;
			else if (wrt == internal::WrtVariable::WO)
				idx = 2;
			else
				idx = 3;

			N = gm_f_bar->get_shape(1);
			K = X[0]->get_shape(0);
			M = X[0]->get_shape(1);
			
			magma_sgemm(MagmaNoTrans, MagmaTrans, N, M, K, 1.f,
					gm_t_bars[idx]->get_ptr(), N,
					X[X.size() - i - 1]->get_ptr(), M,
					1.f, output->get_ptr(), N);	
		}
		if (wrt == internal::WrtVariable::UF
				|| wrt == internal::WrtVariable::UI
				|| wrt == internal::WrtVariable::UO
				|| wrt == internal::WrtVariable::UC) {
			unsigned idx;
			if (wrt == internal::WrtVariable::UF)
				idx = 0;
			else if (wrt == internal::WrtVariable::UI)
				idx = 1;
			else if (wrt == internal::WrtVariable::UO)
				idx = 2;
			else
				idx = 3;

			N = gm_f_bar->get_shape(1);
			K = H[0]->get_shape(0);
			M = H[0]->get_shape(1);
			
			magma_sgemm(MagmaNoTrans, MagmaTrans, N, M, K, 1.f,
					gm_t_bars[idx]->get_ptr(), N,
					H[H.size() - i - 2]->get_ptr(), M,
					1.f, output->get_ptr(), N);	
		}

	} // end sequence loop

	
	if (wrt == internal::WrtVariable::X) {
		std::reverse(outputs.begin(), outputs.end());

		// reverse outputs to be in sequential order, t0 to tn, and
		// swap vector for pointer, which is readable in a CUDA kernal
		
		float **in;
        	cudaMallocManaged(&in, timesteps * sizeof(long));
        	//NOTE this indexing may be wrong
		for (unsigned int i = outputs.size(); i > 0; --i){
                	in[i-1] = outputs[i-1]->get_ptr();
        	}
		
		kernel_concat_gpu<<<(output->get_size() + BLK_SIZE + 1) / BLK_SIZE,
		       	BLK_SIZE, 0, custream>>>
			(in, output->get_ptr(),  output->get_size(),
				1, outputs[0]->get_size(),
				outputs[0]->get_shape(1), outputs.size());
		for (unsigned i = 0; i < outputs.size(); i++) delete outputs[i];	
		cudaFree(in);
	}

	if (wrt == internal::WrtVariable::BF
				|| wrt == internal::WrtVariable::BI
				|| wrt == internal::WrtVariable::BO
				|| wrt == internal::WrtVariable::BC) {
			float **gms;
			cudaMallocManaged(&gms, timesteps * sizeof(long));
        		for (unsigned int i = 0; i < timesteps; ++i){
                		if (wrt == internal::WrtVariable::BF)
					gms[i] = gm_f_bars[i]->get_ptr();
				else if (wrt == internal::WrtVariable::BI)
					gms[i] = gm_i_bars[i]->get_ptr();
				else if (wrt == internal::WrtVariable::BO)
					gms[i] = gm_o_bars[i]->get_ptr();
				else
					gms[i] = gm_c_bars[i]->get_ptr();
        		}
			kernel_add_many_full_device<<<
				(osize + BLK_SIZE + 1)/BLK_SIZE,BLK_SIZE, 0, custream>>>
				(osize, gms, timesteps, output->get_ptr());
			
			cudaFree(gms);
	}
	delete c_bar;
        delete h_bar;
        for (unsigned i = 0; i < gm_f_bars.size(); i++){
                delete gm_f_bars[i];
                delete gm_i_bars[i];
                delete gm_o_bars[i];
                delete gm_c_bars[i];
        }
}

void lstm_grad_device(cudaStream_t custream, Tensor<double> *grad, internal::WrtVariable wrt, 
		std::vector<Tensor<double> *> weights, std::vector<Tensor<double> *> X,
		std::vector<Tensor<double> *> C, std::vector<Tensor<double> *> H,
		std::vector<Tensor<double> *> B, std::vector<Tensor<double> *> F,
		std::vector<Tensor<double> *> I, std::vector<Tensor<double> *> O,
		std::vector<Tensor<double> *> tC, Tensor<double> *output) {
	unsigned batchsz = X[0]->get_shape(0);
	unsigned timesteps = X.size();
	unsigned features = X[0]->get_shape(1);
	unsigned num_nodes = H[0]->get_shape(1);
	unsigned osize = batchsz * num_nodes;
	bool return_sequences = false;
	
	Tensor<double> *h_bar;
	Tensor<double> *c_bar = new Tensor<double>(C[0]->get_shape(), {CONSTANT, {0.f}},
			DEVICE);
	if (grad->get_shape().size() == 3) {
		return_sequences = true;
		h_bar = new Tensor<double>(H[0]->get_shape(),
				{CONSTANT,{0.f}}, DEVICE);
	}
	else
	{
		h_bar = new Tensor<double>(H[0]->get_shape(),
				{CONSTANT, {0.f}}, DEVICE);
		h_bar->copy_from(*grad, 0, osize);
	}
	

	// These will all be stored in reverse order, i.e. last sequence step
	// at the first index
	std::vector<Tensor<double> *> outputs;
	std::vector<Tensor<double> *> gm_f_bars;
	std::vector<Tensor<double> *> gm_i_bars;
	std::vector<Tensor<double> *> gm_o_bars;
	std::vector<Tensor<double> *> gm_c_bars;
	
	// initialize to proper values
	if (wrt != internal::WrtVariable::X) {
		kernel_set_zeros_full_device
			<<<(output->get_size()+BLK_SIZE + 1)/BLK_SIZE, BLK_SIZE, 0, custream>>>
				(output->get_size(), output->get_ptr());
	}


	// TODO use _grad_cache in LSTMOp to avoid redundant calculations.

	// Calculate gamma_bars
	for (unsigned i = 0; i < timesteps; ++i) {
		

		if (return_sequences) {	
			Tensor<double> * h_in = new Tensor<double>(H[0]->get_shape(),
        	                {CONSTANT,{0.}}, DEVICE);	
			kernel_slice_gpu
				<<<(osize + BLK_SIZE + 1)/BLK_SIZE, BLK_SIZE, 0, custream>>>
				(grad->get_ptr(), h_in->get_ptr(),
					timesteps-i-1, osize,
					grad->get_shape(1)*grad->get_shape(2),
					grad->get_shape(0), grad->get_shape(2));
			kernel_add_full_device<<<(osize + BLK_SIZE + 1), BLK_SIZE, 0, custream>>>
				(h_bar->get_ptr(), h_in->get_ptr(),
				 h_bar->get_ptr(), osize);
			delete h_in;
		}
		
		unsigned fi = 0;
		if (i == 0) {
			fi = 1;	
		}


		
		kernal_lstm_grad_c_bar_full_device
			<<<(osize + BLK_SIZE + 1)/BLK_SIZE, BLK_SIZE, 0, custream>>>
				(osize, h_bar->get_ptr(),
				c_bar->get_ptr(), B[B.size()-i-1]->get_ptr(),
				F[F.size()-i-fi]->get_ptr(),
				O[O.size()-i-1]->get_ptr(),
				c_bar->get_ptr());

		Tensor<double> * gm_t_bars[4];

		Tensor<double> *gm_f_bar = new Tensor<double>(F[0]->get_shape(),
				{NONE, {}}, DEVICE);
		Tensor<double> *gm_i_bar = new Tensor<double>(I[0]->get_shape(),
				{NONE,{}}, DEVICE);
		Tensor<double> *gm_o_bar = new Tensor<double>(O[0]->get_shape(),
				{NONE,{}}, DEVICE);
		Tensor<double> *gm_c_bar = new Tensor<double>(tC[0]->get_shape(),
				{NONE,{}}, DEVICE);
		//TODO run gamma calculation in parallel
		kernal_lstm_grad_gmft_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					c_bar->get_ptr(),
					F[F.size()-i-1]->get_ptr(),
					C[C.size()-i-2]->get_ptr(),
					gm_f_bar->get_ptr());
		

		kernal_lstm_grad_gmit_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					c_bar->get_ptr(),
					I[F.size()-i-1]->get_ptr(),
					tC[tC.size()-i-1]->get_ptr(),
					gm_i_bar->get_ptr());
		
		

		
		kernal_lstm_grad_gmot_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					h_bar->get_ptr(),
					B[B.size()-i-1]->get_ptr(),
					O[O.size()-i-1]->get_ptr(),
					gm_o_bar->get_ptr());
		

		kernal_lstm_grad_gmct_full_device
			<<<(osize + BLK_SIZE + 1) / BLK_SIZE, BLK_SIZE, 0, custream>>>(
					osize, 
					c_bar->get_ptr(),
					I[I.size() -i-1]->get_ptr(),
					tC[tC.size()-i-1]->get_ptr(),
					gm_c_bar->get_ptr());
		
		gm_f_bars.push_back(gm_f_bar);
		gm_i_bars.push_back(gm_i_bar);
		gm_o_bars.push_back(gm_o_bar);
		gm_c_bars.push_back(gm_c_bar);
			

		gm_t_bars[0] = gm_f_bar;
		gm_t_bars[1] = gm_i_bar;
		gm_t_bars[2] = gm_o_bar;
		gm_t_bars[3] = gm_c_bar;

		unsigned N = weights[1]->get_shape(0);
		unsigned K = gm_f_bar->get_shape(1);
		unsigned M = gm_f_bar->get_shape(0);

		// calculate next h_bar	
		magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
				weights[1]->get_ptr(), K, gm_f_bar->get_ptr(), K,
				0., h_bar->get_ptr(), N);
	
		
	
		magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
				weights[4]->get_ptr(), K, gm_i_bar->get_ptr(), K,
				1., h_bar->get_ptr(), N);
		

		magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
				weights[7]->get_ptr(), K, gm_o_bar->get_ptr(), K,
				1., h_bar->get_ptr(), N);

		
		magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
				weights[10]->get_ptr(), K, gm_c_bar->get_ptr(), K,
				1., h_bar->get_ptr(), N);
		
		if (wrt == internal::WrtVariable::X) { 
			// calculate xt_bar
			N = weights[0]->get_shape(0);
			K = gm_f_bar->get_shape(1);
			M = gm_f_bar->get_shape(0);
			Tensor<double> *xt_bar = new Tensor<double>(X[i]->get_shape(),
					{NONE, {}}, DEVICE);

			magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
					weights[0]->get_ptr(), K,
					gm_f_bar->get_ptr(), K,
					0., xt_bar->get_ptr(), N);

			magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
					weights[3]->get_ptr(), K,
					gm_i_bar->get_ptr(), K,
					1., xt_bar->get_ptr(), N);

			magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
					weights[6]->get_ptr(), K,
					gm_o_bar->get_ptr(), K,
					1., xt_bar->get_ptr(), N);

			magma_dgemm(MagmaTrans, MagmaNoTrans, N, M, K, 1.,
					weights[9]->get_ptr(), K,
					gm_c_bar->get_ptr(), K,
					1., xt_bar->get_ptr(), N);

			outputs.push_back(xt_bar);
		}

		
		if (wrt == internal::WrtVariable::WF
				|| wrt == internal::WrtVariable::WI
				|| wrt == internal::WrtVariable::WO
				|| wrt == internal::WrtVariable::WC) {
			unsigned idx;
			if (wrt == internal::WrtVariable::WF)
				idx = 0;
			else if (wrt == internal::WrtVariable::WI)
				idx = 1;
			else if (wrt == internal::WrtVariable::WO)
				idx = 2;
			else
				idx = 3;

			N = gm_f_bar->get_shape(1);
			K = X[0]->get_shape(0);
			M = X[0]->get_shape(1);
			
			magma_dgemm(MagmaNoTrans, MagmaTrans, N, M, K, 1.,
					gm_t_bars[idx]->get_ptr(), N,
					X[X.size() - i - 1]->get_ptr(), M,
					1., output->get_ptr(), N);	
		}
		if (wrt == internal::WrtVariable::UF
				|| wrt == internal::WrtVariable::UI
				|| wrt == internal::WrtVariable::UO
				|| wrt == internal::WrtVariable::UC) {
			unsigned idx;
			if (wrt == internal::WrtVariable::UF)
				idx = 0;
			else if (wrt == internal::WrtVariable::UI)
				idx = 1;
			else if (wrt == internal::WrtVariable::UO)
				idx = 2;
			else
				idx = 3;

			N = gm_f_bar->get_shape(1);
			K = H[0]->get_shape(0);
			M = H[0]->get_shape(1);
			
			magma_dgemm(MagmaNoTrans, MagmaTrans, N, M, K, 1.,
					gm_t_bars[idx]->get_ptr(), N,
					H[H.size() - i - 2]->get_ptr(), M,
					1., output->get_ptr(), N);	
		}

	} // end sequence loop

	
	if (wrt == internal::WrtVariable::X) {
		std::reverse(outputs.begin(), outputs.end());

		// reverse outputs to be in sequential order, t0 to tn, and
		// swap vector for pointer, which is readable in a CUDA kernal
		
		double **in;
        	cudaMallocManaged(&in, timesteps * sizeof(long));
        	//NOTE this indexing may be wrong
		for (unsigned int i = outputs.size(); i > 0; --i){
                	in[i-1] = outputs[i-1]->get_ptr();
        	}
		
		kernel_concat_gpu<<<(output->get_size() + BLK_SIZE + 1) / BLK_SIZE,
		       	BLK_SIZE, 0, custream>>>
			(in, output->get_ptr(),  output->get_size(),
				1, outputs[0]->get_size(),
				outputs[0]->get_shape(1), outputs.size());
		for (unsigned i = 0; i < outputs.size(); i++) delete outputs[i];	
		cudaFree(in);
	}

	if (wrt == internal::WrtVariable::BF
				|| wrt == internal::WrtVariable::BI
				|| wrt == internal::WrtVariable::BO
				|| wrt == internal::WrtVariable::BC) {
			double **gms;
			cudaMallocManaged(&gms, timesteps * sizeof(long));
        		for (unsigned int i = 0; i < timesteps; ++i){
                		if (wrt == internal::WrtVariable::BF)
					gms[i] = gm_f_bars[i]->get_ptr();
				else if (wrt == internal::WrtVariable::BI)
					gms[i] = gm_i_bars[i]->get_ptr();
				else if (wrt == internal::WrtVariable::BO)
					gms[i] = gm_o_bars[i]->get_ptr();
				else
					gms[i] = gm_c_bars[i]->get_ptr();
        		}
			kernel_add_many_full_device<<<
				(osize + BLK_SIZE + 1)/BLK_SIZE,BLK_SIZE, 0, custream>>>
				(osize, gms, timesteps, output->get_ptr());
			
			cudaFree(gms);
	}
	delete c_bar;
        delete h_bar;
        for (unsigned i = 0; i < gm_f_bars.size(); i++){
                delete gm_f_bars[i];
                delete gm_i_bars[i];
                delete gm_o_bars[i];
                delete gm_c_bars[i];
        }

}


} //namespace internal
} //namespace magmadnn
