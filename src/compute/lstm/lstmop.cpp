/*
 * @file lstmop.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-07-19
 * */

#include "compute/lstm/lstmop.h"
namespace magmadnn {
namespace op {

template <typename T>
LSTMOp<T>::LSTMOp(Operation<T> *input, std::vector<Operation<T> *> weights, Operation<T> *h_init, Operation<T> *c_init,
                  bool return_sequences, bool copy, bool needs_grad)
    : Operation<T>::Operation({weights[0], weights[1], weights[2], weights[3], weights[4], weights[5], weights[6],
                               weights[7], weights[8], weights[9], weights[10], weights[11], input},
                              needs_grad),
      input(input),
      weights(weights),
      h_init(h_init),
      c_init(c_init),
      return_sequences(return_sequences),
      copy(copy) {
    init();
}
template<typename T>
LSTMOp<T>::~LSTMOp(){
	std::cout << "in destructor\n";
}

template <typename T>
void LSTMOp<T>::init() {
    // TODO add dimension checks and memory type checks
    unsigned batchsz = this->input->get_output_shape(0);
    unsigned seqsteps = this->input->get_output_shape(1);
    unsigned nodes = this->weights[0]->get_output_shape(1);
    if (this->weights.size() != 12) {
        throw std::invalid_argument("The weights vector must have twelve elements");
    }

    if (copy == false) {
        throw std::invalid_argument("No implementation for copy == false");
    }

    if (return_sequences) {
        this->output_shape = {batchsz, seqsteps, nodes};
    } else {
        this->output_shape = {batchsz, nodes};
    }
    this->mem_type = this->input->get_memory_type();
    this->output_tensor = new Tensor<T>(this->output_shape, this->mem_type);
//TODO check if this is necessary
    this->_grad_cache[(uintptr_t) this->input] = NULL;
    for (int i = 0; i < 12; ++i) {
        this->_grad_cache[(uintptr_t) this->weights[i]] = NULL;
    }

    // build tensor weight vector
    for (unsigned int i = 0; i < this->weights.size(); ++i) {
        this->t_weights.push_back(weights[i]->get_output_tensor());
    }
}

template <typename T>
Tensor<T> *LSTMOp<T>::_eval(bool recompute) {
    this->input->eval(recompute);

    // These are later used by the gradient calculation.
    // TODO See if these are an issue
    for (unsigned int i = 0; i < X.size(); i++) {
	delete this->X[i];
    }
    for (unsigned int i = 0; i < C.size(); i++) {
	delete this->C[i];
    }
    for (unsigned int i = 0; i < H.size(); i++) {
	delete this->H[i];
    }
    for (unsigned int i = 0; i < B.size(); i++) {
	delete this->B[i];
    }
    for (unsigned int i = 0; i < F.size(); i++) {
	delete this->F[i];
    }
    for (unsigned int i = 0; i < I.size(); i++) {
	delete this->I[i];
    }
    for (unsigned int i = 0; i < O.size(); i++) {
	delete this->O[i];
    }
    for (unsigned int i = 0; i < tC.size(); i++) {
	delete this->tC[i];
    }

/*
    this->X = {};
    this->C = {};
    this->H = {};
    this->B = {};
    this->F = {};
    this->I = {};
    this->O = {};
    this->tC = {};
*/
    this->X.resize(0);
    this->C.resize(0);
    this->H.resize(0);
    this->B.resize(0);
    this->F.resize(0);
    this->I.resize(0);
    this->O.resize(0);
    this->tC.resize(0);
    
    
    if (this->mem_type == HOST) {
        internal::lstm_cpu(input->get_output_tensor(), this->t_weights, h_init->get_output_tensor(),
                           c_init->get_output_tensor(), this->return_sequences, &this->X, &this->C, &this->H, &this->B,
                           &this->F, &this->I, &this->O, &this->tC, this->output_tensor);
//	for(unsigned i = 0; i < this->F.size(); i++){
//		std::cout << F[i]->get(0) << std::endl;
//
//	}
//	for(unsigned i = 0; i < this->O.size(); i++){
//		std::cout << O[i]->get(0) << std::endl;
//
//	}
	if (return_sequences) {
            std::vector<Tensor<T> *> out = this->H;
            out.erase(out.begin());

            internal::concat_cpu(out, 1, this->output_tensor);
        }
    }
#if (MAGMADNN_HAVE_CUDA)
    else if (this->mem_type == DEVICE) {
        internal::lstm_gpu(this->get_custream(), input->get_output_tensor(), this->t_weights,
                           h_init->get_output_tensor(), c_init->get_output_tensor(), this->return_sequences, &this->X,
                           &this->C, &this->H, &this->B, &this->F, &this->I, &this->O, &this->tC, this->output_tensor);
	if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
//	for(unsigned i = 0; i < this->F.size(); i++){
//		std::cout << F[i]->get(0) << std::endl;
//
//	}
//	for(unsigned i = 0; i < this->O.size(); i++){
//		std::cout << O[i]->get(0) << std::endl;
//
//	}

        if (return_sequences) {
//	    std::vector<Tensor<T> *> out;
//	    for (unsigned i = 1; i < this->H.size(); i++){
//		out.push_back(this->H[i]);
//	    }
	    std::vector<Tensor<T> *> out = this->H;
            out.erase(out.begin());

            internal::concat_gpu(out, 1, this->output_tensor);
        }
    }
#endif
    else {
        throw std::invalid_argument("Invalid memory type, i.e. not HOST nor DEVICE");
    }

    return this->output_tensor;
}

template <typename T>
Tensor<T> *LSTMOp<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
    internal::WrtVariable wrt;
    this->input->eval(false);
    if (var == this->weights[0])
        wrt = internal::WrtVariable::WF;
    else if (var == this->weights[1])
        wrt = internal::WrtVariable::UF;
    else if (var == this->weights[2])
        wrt = internal::WrtVariable::BF;
    else if (var == this->weights[3])
        wrt = internal::WrtVariable::WI;
    else if (var == this->weights[4])
        wrt = internal::WrtVariable::UI;
    else if (var == this->weights[5])
        wrt = internal::WrtVariable::BI;
    else if (var == this->weights[6])
        wrt = internal::WrtVariable::WO;
    else if (var == this->weights[7])
        wrt = internal::WrtVariable::UO;
    else if (var == this->weights[8])
        wrt = internal::WrtVariable::BO;
    else if (var == this->weights[9])
        wrt = internal::WrtVariable::WC;
    else if (var == this->weights[10])
        wrt = internal::WrtVariable::UC;
    else if (var == this->weights[11])
        wrt = internal::WrtVariable::BC;
    else if (var == this->input)
        wrt = internal::WrtVariable::X;
    else
        throw std::invalid_argument("Unknown variable passed as argument");

    Tensor<T> *out = this->_grad_cache[(uintptr_t) var];

    if (out == NULL) {
        out = new Tensor<T>({var->get_output_shape(), {NONE, {}}, this->mem_type});
#if defined(MAGMADNN_HAVE_CUDA)
            out->set_custream(this->get_custream());
            out->set_cublas_handle(this->get_cublas_handle());
#endif
            this->_grad_cache[(uintptr_t) var] = out;

    }

    if (this->mem_type == HOST) {
        internal::lstm_grad_cpu(grad, wrt, this->t_weights, this->X, this->C, this->H, this->B, this->F, this->I,
                                this->O, this->tC, out);
    }
#if (MAGMADNN_HAVE_CUDA)
    else if (this->mem_type == DEVICE) {
        internal::lstm_grad_device(this->get_custream(), grad, wrt, this->t_weights, this->X, this->C, this->H, this->B,
                                   this->F, this->I, this->O, this->tC, out);
    	if (!this->get_async()) cudaStreamSynchronize(this->get_custream());

    }
#endif
    else {
        throw std::invalid_argument("Invalid memory type, i.e. not HOST nor DEVICE");
    }

    return out;
}

template <typename T>
LSTMOp<T> *lstm(Operation<T> *input, std::vector<Operation<T> *> weights, Operation<T> *h_init, Operation<T> *c_init,
                bool return_sequences, bool copy, bool needs_grad) {
    return new LSTMOp<T>(input, weights, h_init, c_init, return_sequences, copy, needs_grad);
}

// template class LSTMOp<int>;
template class LSTMOp<float>;
//template class LSTMOp<double>;

// template LSTMOp<int> *lstm(Operation<int> * input, std::vector<Operation<int> *> weights,
//		Operation<int> * h_init, Operation<int> * c_init, bool return_sequences,
//		bool copy,  bool needs_grad);
template LSTMOp<float> *lstm(Operation<float> *input, std::vector<Operation<float> *> weights, Operation<float> *h_init,
                             Operation<float> *c_init, bool return_sequences, bool copy, bool needs_grad);
/*template LSTMOp<double> *lstm(Operation<double> *input, std::vector<Operation<double> *> weights,
                              Operation<double> *h_init, Operation<double> *c_init, bool return_sequences, bool copy,
                              bool needs_grad);*/
}  // namespace op
}  // namespace magmadnn
