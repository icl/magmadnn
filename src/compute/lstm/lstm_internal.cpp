/**
 * @file lstm_internal.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-07-19
 *
 */
#include "compute/lstm/lstm_internal.h"
#include <algorithm>
#include <cassert>
#include "math/wrappers.h"
namespace magmadnn {
namespace internal {


template <typename T>
void concat_cpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output) {
    std::vector<unsigned int> in_shape = inputs[0]->get_shape();

    // sets values of output tensor based on given axis
    if (axis == 1) {
        for (unsigned int k = 0; k < inputs.size(); k++) {
            for (unsigned int i = 0; i < in_shape[0]; i++) {
                for (unsigned int j = 0; j < in_shape[1]; j++) {
                    output->set({i, k, j}, inputs[k]->get({i, j}));
                }
            }
        }
    } else if (axis == 0) {
        unsigned size = 0;
        for (unsigned k = 0; k < inputs.size(); k++) {
            for (unsigned int i = 0; i < inputs[k]->get_shape()[0]; i++) {
                for (unsigned int j = 0; j < in_shape[1]; j++) {
                    output->set({i + size, j}, inputs[k]->get({i, j}));
                }
            }
            size += inputs[k]->get_shape()[0];
        }

    } else if (axis == 2) {
        unsigned size = 0;
        for (unsigned k = 0; k < inputs.size(); k++) {
            for (unsigned int i = 0; i < in_shape[0]; i++) {
                for (unsigned int j = 0; j < inputs[k]->get_shape()[1]; j++) {
                    output->set({i, j + size}, inputs[k]->get({i, j}));
                }
            }
            size += inputs[k]->get_shape()[1];
        }
    }
}

template void concat_cpu(std::vector<Tensor<float> *> inputs, unsigned int axis, Tensor<float> *output);
template void concat_cpu(std::vector<Tensor<double> *> inputs, unsigned int axis, Tensor<double> *output);

template <typename T>
void slice_cpu(Tensor<T>* input, unsigned int axis, unsigned int index, Tensor<T>* output) {
    unsigned int d0, d1; 
    std::vector<unsigned int> myshape = input->get_shape();
    if (axis == 0) {
        d0 = myshape[1];
        d1 = myshape[2];
    } else if (axis == 1) {
        d0 = myshape[0];
        d1 = myshape[2];
    } else if (axis == 2) {
        d0 = myshape[0];
        d1 = myshape[1];
    } else {
        throw std::invalid_argument("slice_cpu only works for 3 dimensional tensor\n");
    }

    for (unsigned int i = 0; i < d0; i++) {
        for (unsigned int j = 0; j < d1; j++) {
            if (axis == 0) output->set({i, j}, input->get({index, i, j}));
            if (axis == 1) output->set({i, j}, input->get({i, index, j}));
            if (axis == 2) output->set({i, j}, input->get({i, j, index}));
        }
    }

}

template void slice_cpu(Tensor<float>* input, unsigned int axis, unsigned int index, Tensor<float>* output);
template void slice_cpu(Tensor<double>* input, unsigned int axis, unsigned int index, Tensor<double>* output);


template <typename T>
void sigmoid_cpu(Tensor<T> *x, Tensor<T> *out, bool fast) {
    T *x_ptr = x->get_ptr();
    T *out_ptr = out->get_ptr();
    unsigned int size = out->get_size();

    if (fast) {
        // fast sigmoid -- fast_sigmoid(x) = x / (1 + |x|)
        for (unsigned int i = 0; i < size; i++) out_ptr[i] = x_ptr[i] / (1 + abs(x_ptr[i]));
    } else {
        // normal sigmoid -- sigmoid(x) = 1 / (1 + exp(-x))
        for (unsigned int i = 0; i < size; i++) out_ptr[i] = 1 / (1 + exp(-x_ptr[i]));
    }
}

template void sigmoid_cpu(Tensor<float> *x, Tensor<float> *out, bool fast);
template void sigmoid_cpu(Tensor<double> *x, Tensor<double> *out, bool fast);


template <typename T>
void product_cpu(T alpha, Tensor<T> *a, Tensor<T> *b, Tensor<T> *out) {
    T *a_ptr = a->get_ptr();
    T *b_ptr = b->get_ptr();
    T *out_ptr = out->get_ptr();
    unsigned int size = out->get_size();
    for (unsigned int i = 0; i < size; i++) {
        out_ptr[i] = alpha * a_ptr[i] * b_ptr[i];
    }
}
template void product_cpu(float alpha, Tensor<float> *a, Tensor<float> *b, Tensor<float> *out);
template void product_cpu(double alpha, Tensor<double> *a, Tensor<double> *b, Tensor<double> *out);

template <typename T>
void add_cpu(T alpha, Tensor<T> *A, T beta, Tensor<T> *B, Tensor<T> *C) {
    T *a_ptr = A->get_ptr();
    T *b_ptr = B->get_ptr();
    T *c_ptr = C->get_ptr();
    unsigned int size = A->get_size();
   
    for (unsigned int i = 0; i < size; i++) {
        c_ptr[i] = (alpha * a_ptr[i]) + (beta * b_ptr[i]);
    }
}
template void add_cpu(float alpha, Tensor<float> *A, float beta, Tensor<float> *B, Tensor<float> *C);
template void add_cpu(double alpha, Tensor<double> *A, double beta, Tensor<double> *B, Tensor<double> *C);


template <typename T>
void tanh_cpu(Tensor<T> *x, Tensor<T> *out) {
    T *x_ptr = x->get_ptr();
    T *out_ptr = out->get_ptr();
    unsigned int size = out->get_size();

    for (unsigned int i = 0; i < size; i++) {
        out_ptr[i] = tanh(x_ptr[i]);
    }
}
template void tanh_cpu(Tensor<float> *x, Tensor<float> *out);
template void tanh_cpu(Tensor<double> *x, Tensor<double> *out);

template <typename T>
void lstm_cpu(Tensor<T> * input, std::vector<Tensor<T> *> weights, 
		Tensor<T> * h_init, Tensor<T> * c_init, bool return_sequences,
		std::vector<Tensor<T> *> *X, std::vector<Tensor<T> *> *C,
		std::vector<Tensor<T> *> *H, std::vector<Tensor<T> *> *B,
		std::vector<Tensor<T> *> *F, std::vector<Tensor<T> *> *I,
		std::vector<Tensor<T> *> *O, std::vector<Tensor<T> *> *tC,
		Tensor<T> *output) {

	assert(input->get_shape().size() == 3);
	unsigned dim0 = input->get_shape(0);
	unsigned dim1 = input->get_shape(1);	
	unsigned dim2 = input->get_shape(2);
	unsigned num_nodes = weights[0]->get_shape(1);

	Tensor<T> * c_cur = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * h_cur = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	c_cur->copy_from(*(c_init));
	h_cur->copy_from(*(h_init));
	
	Tensor<T> * t1 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	t1->copy_from(*c_cur);
	C->push_back(t1);
	Tensor<T> * t2 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	t2->copy_from(*h_cur);
	H->push_back(t2);

	Tensor<T> * o1 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * o2 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * o3 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * o4 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);

	Tensor<T> * f1 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * f2 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * f3 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * f4 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);

	Tensor<T> * i1 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * i2 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * i3 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * i4 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);

	Tensor<T> * c1 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * c2 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * c3 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
	Tensor<T> * c4 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);

	
	int M; 
	int N; 
	int K; 


	//weights:
	//0 Wf, 1  Uf,  2 Bf
	//3 Wi, 4  Ui,  5 Bi
	//6 Wo, 7  Uo,  8 Bo
	//9 Wc, 10 Uc, 11 Bc

	for (unsigned int i = 0; i < dim1; i++){
		
		//calculating cur_op
	
		Tensor<T> * cur_op = new Tensor<T>({dim0, dim2}, {NONE,{}}, HOST);
		slice_cpu(input, 1, i, cur_op);

		X->push_back(cur_op);
		
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[6]->get_shape(1);

		//calculate o_t
		//o_t = sigmoid(xt * Wo + (ht-1) * Uo + Bo)

		math::gemm(math::OP_T, math::OP_T, N, M, K, (T)1, weights[6]->get_ptr(), K,
			       	cur_op->get_ptr(), M, (T)0, o1->get_ptr(), N);
		
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[7]->get_shape(1);

		math::gemm(math::OP_T, math::OP_T, N, M, K, (T)1, weights[7]->get_ptr(), K,
			       	h_cur->get_ptr(), M, (T)0, o2->get_ptr(), N);

		add_cpu((T)1, o1, (T)1, o2, o3);
		
		add_cpu((T)1, o3, (T)1, weights[8], o4);
		
		Tensor<T> * o_t = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);

		sigmoid_cpu(o4, o_t, false);

		O->push_back(o_t);


		//calculating f_t
		//f_t = sigmoid(xt * Wf + (ht-1) * Uf + Bf)
		
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[0]->get_shape(1);

		math::gemm(math::OP_T, math::OP_T, N, M, K,(T) 1, weights[0]->get_ptr(), K,
			       	cur_op->get_ptr(), M, (T)0, f1->get_ptr(), N);
		
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[1]->get_shape(1);
		
		math::gemm(math::OP_T, math::OP_T, N, M, K, (T)1, weights[1]->get_ptr(), K,
			       	h_cur->get_ptr(), M, (T)0, f2->get_ptr(), N);
		
		add_cpu((T)1, f1, (T)1, f2, f3);
		
		add_cpu((T)1, f3, (T)1, weights[2], f4);
  		
		Tensor<T> * f_t = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
		sigmoid_cpu(f4, f_t, 0);
		F->push_back(f_t);
		//calculating i_t
		//i_t = sigmoid(xt * Wi + (ht-1) * Ui + Bi)
		
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[3]->get_shape(1);

		math::gemm(math::OP_T, math::OP_T, N, M, K, (T)1, weights[3]->get_ptr(), K,
			       	cur_op->get_ptr(), M, (T)0, i1->get_ptr(), N);

		
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[4]->get_shape(1);

		math::gemm(math::OP_T, math::OP_T, N, M, K, (T)1, weights[4]->get_ptr(), K,
			       	h_cur->get_ptr(), M, (T)0, i2->get_ptr(), N);

		add_cpu((T)1, i1, (T)1, i2, i3);
		
		add_cpu((T)1, i3, (T)1, weights[5], i4);
  	
		Tensor<T> * i_t = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);

		sigmoid_cpu(i4, i_t, 0);
		
		I->push_back(i_t);
	
		//calculating ct_t
		
		M = cur_op->get_shape(0);
		K = cur_op->get_shape(1);
		N = weights[9]->get_shape(1);

		math::gemm(math::OP_T, math::OP_T, N, M, K, (T)1, weights[9]->get_ptr(), K,
			       	cur_op->get_ptr(), M, (T)0, c1->get_ptr(), N);
	
		M = h_cur->get_shape(0);
		K = h_cur->get_shape(1);
		N = weights[10]->get_shape(1);
		
		math::gemm(math::OP_T, math::OP_T, N, M, K, (T)1, weights[10]->get_ptr(), K,
			       	h_cur->get_ptr(), M, (T)0, c2->get_ptr(), N);

		add_cpu((T)1, c1, (T)1, c2, c3);

		add_cpu((T)1, c3, (T)1, weights[11], c4);

		Tensor<T> * ct_t = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
		
		tanh_cpu(c4,ct_t);

		tC->push_back(ct_t);

		//calculate c_cur
		product_cpu((T)1, f_t, c_cur, o1);	

		product_cpu((T)1, i_t, ct_t, o2);	

		add_cpu((T)1, o1, (T)1, o2, c_cur);	

		Tensor<T> * c_temp = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
		c_temp->copy_from(*c_cur);
		C->push_back(c_temp);
	
	
		//calculate h_cur	

		Tensor<T> * h1 = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);

		tanh_cpu(c_cur, h1);
		
		B->push_back(h1);

		product_cpu((T)1, o_t, h1, h_cur);	
		
		Tensor<T> * h_temp = new Tensor<T>({dim0, num_nodes}, {NONE,{}}, HOST);
		h_temp->copy_from(*h_cur);
		
		H->push_back(h_temp);

		
	}
	if (!return_sequences) output->copy_from(*h_cur);

	delete o1;
	delete o2;
	delete o3;
	delete o4;
	delete f1;
	delete f2;
	delete f3;
	delete f4;
	delete i1;
	delete i2;
	delete i3;
	delete i4;
	delete c1;
	delete c2;
	delete c3;
	delete c4;
	delete h_cur;
	delete c_cur;
}
template void lstm_cpu(Tensor<float> * input, std::vector<Tensor<float> *> weights, 
		Tensor<float> * h_init, Tensor<float> * c_init,
		bool return_sequences, 
		std::vector<Tensor<float> *> *X, std::vector<Tensor<float> *> *C,
		std::vector<Tensor<float> *> *H, std::vector<Tensor<float> *> *B,
		std::vector<Tensor<float> *> *F, std::vector<Tensor<float> *> *I,
		std::vector<Tensor<float> *> *O, std::vector<Tensor<float> *> *tC,
		Tensor<float> *output);

template <typename T>
void lstm_grad_cpu(Tensor<T> * grad, WrtVariable wrt, std::vector<Tensor<T> *> weights,
		std::vector<Tensor<T> *> X, std::vector<Tensor<T> *> C,
		std::vector<Tensor<T> *> H, std::vector<Tensor<T> *> B,
		std::vector<Tensor<T> *> F, std::vector<Tensor<T> *> I,
		std::vector<Tensor<T> *> O, std::vector<Tensor<T> *> tC,
		Tensor<T> *output) {


	unsigned batchsz = X[0]->get_shape(0);
	unsigned timesteps = X.size();
	// unsigned features = X[0]->get_shape(1);
	unsigned num_nodes = H[0]->get_shape(1);
	unsigned osize = batchsz * num_nodes;
	bool return_sequences = false;
	Tensor<T> *h_bar;
	Tensor<T> *c_bar = new Tensor<T>(C[0]->get_shape(), {CONSTANT, {(T)0}},
			HOST);
	if (grad->get_shape().size() == 3) {
		return_sequences = true;
		h_bar = new Tensor<T>(H[0]->get_shape(),
				{CONSTANT,{0.f}}, HOST);
	}
	else
	{
		h_bar = new Tensor<T>(H[0]->get_shape(),
				{NONE, {}}, HOST);
		h_bar->copy_from(*grad, 0, osize);
	
	}
	
	// These will all be stored in reverse order, i.e. last sequence step
	// at the first index
	std::vector<Tensor<T> *> outputs;
	std::vector<Tensor<T> *> gm_f_bars;
	std::vector<Tensor<T> *> gm_i_bars;
	std::vector<Tensor<T> *> gm_o_bars;
	std::vector<Tensor<T> *> gm_c_bars;
	// initialize to proper values
	if (wrt != internal::WrtVariable::X) {
		T * outptr = output->get_ptr();
		for (unsigned i = 0; i < output->get_size(); i++){
			outptr[i] = 0;
		}
	}



	// Calculate gamma_bars
	for (unsigned i = 0; i < timesteps; ++i) {
		
		
		if (return_sequences) {	
			Tensor<T> * h_in = new Tensor<T>(H[0]->get_shape(),
				{CONSTANT,{0.f}}, HOST);
		
			slice_cpu(grad, 1, timesteps-i-1, h_in); 
			add_cpu((T)1, h_bar, (T)1, h_in, h_bar);
			delete h_in;
		}
		
		unsigned fi = 0;
		if (i == 0) {
			fi = 1;	
		}

		Tensor<T> * ot_temp = O[O.size()-i-1];
		Tensor<T> * ft_temp = F[F.size()-i-fi];
		Tensor<T> * bt_temp = B[B.size()-i-1];

		for (unsigned j = 0; j < batchsz * num_nodes; j++){
			c_bar->set(j, h_bar->get(j) * ot_temp->get(j) * (1- bt_temp->get(j) * bt_temp->get(j)) + ft_temp->get(j) * c_bar->get(j));
		}

		Tensor<T> * gm_t_bars[4];

		Tensor<T> *gm_f_bar = new Tensor<T>(F[0]->get_shape(),
				{NONE, {}}, HOST);
		Tensor<T> *gm_i_bar = new Tensor<T>(I[0]->get_shape(),
				{NONE,{}}, HOST);
		Tensor<T> *gm_o_bar = new Tensor<T>(O[0]->get_shape(),
				{NONE,{}}, HOST);
		Tensor<T> *gm_c_bar = new Tensor<T>(tC[0]->get_shape(),
				{NONE,{}}, HOST);

		ft_temp = F[F.size() - i - 1];
		Tensor<T> * ct_temp = C[C.size() - i - 2];
		for (unsigned j = 0; j < batchsz * num_nodes; j++){
			gm_f_bar->set(j, c_bar->get(j) * ct_temp->get(j) * ft_temp->get(j) * (1 - ft_temp->get(j)));
		}

		Tensor<T> * tct_temp = tC[tC.size() - i - 1];
		Tensor<T> * it_temp = I[F.size() - i - 1];
		for (unsigned j = 0; j < batchsz * num_nodes; j++){
			gm_i_bar->set(j, c_bar->get(j) * tct_temp->get(j) * it_temp->get(j) * (1 - it_temp->get(j)));
		}

		for (unsigned j = 0; j < batchsz * num_nodes; j++){
			gm_o_bar->set(j, h_bar->get(j) * bt_temp->get(j) * ot_temp->get(j) * (1 - ot_temp->get(j)));
		}

		for (unsigned j = 0; j < batchsz * num_nodes; j++){
			gm_c_bar->set(j, c_bar->get(j) * it_temp->get(j) * (1 - tct_temp->get(j) * tct_temp->get(j)));
		}

		gm_f_bars.push_back(gm_f_bar);
		gm_i_bars.push_back(gm_i_bar);
		gm_o_bars.push_back(gm_o_bar);
		gm_c_bars.push_back(gm_c_bar);
			

		gm_t_bars[0] = gm_f_bar;
		gm_t_bars[1] = gm_i_bar;
		gm_t_bars[2] = gm_o_bar;
		gm_t_bars[3] = gm_c_bar;

		unsigned N = weights[1]->get_shape(0);
		unsigned K = gm_f_bar->get_shape(1);
		unsigned M = gm_f_bar->get_shape(0);

		math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
				weights[1]->get_ptr(), K, gm_f_bar->get_ptr(), M,
				0.f, h_bar->get_ptr(), N);

		math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
				weights[4]->get_ptr(), K, gm_i_bar->get_ptr(), M,
				1.f, h_bar->get_ptr(), N);
		
		math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
				weights[7]->get_ptr(), K, gm_o_bar->get_ptr(), M,
				1.f, h_bar->get_ptr(), N);
		
		math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
				weights[10]->get_ptr(), K, gm_c_bar->get_ptr(), M,
				1.f, h_bar->get_ptr(), N);

		if (wrt == internal::WrtVariable::X) { 
			// calculate xt_bar
			N = weights[0]->get_shape(0);
			K = gm_f_bar->get_shape(1);
			M = gm_f_bar->get_shape(0);
			Tensor<T> *xt_bar = new Tensor<T>(X[i]->get_shape(),
					{NONE, {}}, HOST);

			math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
					weights[0]->get_ptr(), K,
					gm_f_bar->get_ptr(), M,
					0.f, xt_bar->get_ptr(), N);

			math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
					weights[3]->get_ptr(), K,
					gm_i_bar->get_ptr(), M,
					1.f, xt_bar->get_ptr(), N);

			math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
					weights[6]->get_ptr(), K,
					gm_o_bar->get_ptr(), M,
					1.f, xt_bar->get_ptr(), N);

			math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
					weights[9]->get_ptr(), K,
					gm_c_bar->get_ptr(), M,
					1.f, xt_bar->get_ptr(), N);
			
			outputs.push_back(xt_bar);
		}

		if (wrt == internal::WrtVariable::WF
				|| wrt == internal::WrtVariable::WI
				|| wrt == internal::WrtVariable::WO
				|| wrt == internal::WrtVariable::WC) {
			unsigned idx;
			if (wrt == internal::WrtVariable::WF)
				idx = 0;
			else if (wrt == internal::WrtVariable::WI)
				idx = 1;
			else if (wrt == internal::WrtVariable::WO)
				idx = 2;
			else
				idx = 3;

			N = gm_f_bar->get_shape(1);
			K = X[0]->get_shape(0);
			M = X[0]->get_shape(1);
			math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
					gm_t_bars[idx]->get_ptr(), K,
					X[X.size() - i - 1]->get_ptr(), M,
					1.f, output->get_ptr(), N);	
		}
		if (wrt == internal::WrtVariable::UF
				|| wrt == internal::WrtVariable::UI
				|| wrt == internal::WrtVariable::UO
				|| wrt == internal::WrtVariable::UC) {
			unsigned idx;
			if (wrt == internal::WrtVariable::UF)
				idx = 0;
			else if (wrt == internal::WrtVariable::UI)
				idx = 1;
			else if (wrt == internal::WrtVariable::UO)
				idx = 2;
			else
				idx = 3;

			N = gm_f_bar->get_shape(1);
			K = H[0]->get_shape(0);
			M = H[0]->get_shape(1);
			
			math::gemm(math::OP_T, math::OP_T, N, M, K, 1.f,
					gm_t_bars[idx]->get_ptr(), K,
					H[H.size() - i - 2]->get_ptr(), M,
					1.f, output->get_ptr(), N);	
		}	

	} // end sequence loop

	
	if (wrt == internal::WrtVariable::X) {
		std::reverse(outputs.begin(), outputs.end());
		concat_cpu(outputs, 1, output);
		for (unsigned i = 0; i < outputs.size(); i++) delete outputs[i];
	}

	if (wrt == internal::WrtVariable::BF
				|| wrt == internal::WrtVariable::BI
				|| wrt == internal::WrtVariable::BO
				|| wrt == internal::WrtVariable::BC) {
			std::vector<T *> gms;
        		for (unsigned int i = 0; i < timesteps; ++i){
				if (wrt == internal::WrtVariable::BF)
					gms.push_back(gm_f_bars[i]->get_ptr());
				else if (wrt == internal::WrtVariable::BI)
					gms.push_back(gm_i_bars[i]->get_ptr());
				else if (wrt == internal::WrtVariable::BO)
					gms.push_back(gm_o_bars[i]->get_ptr());
				else
					gms.push_back(gm_c_bars[i]->get_ptr());

			}
			
			for (unsigned i = 0; i < batchsz * num_nodes; i++){
				for (unsigned j = 0; j < gms.size(); j++){
					output->set(i, output->get(i) + gms[j][i]);
				}
			}

	}
	delete c_bar;
	delete h_bar;
	for (unsigned i = 0; i < gm_f_bars.size(); i++){
		delete gm_f_bars[i];
		delete gm_i_bars[i];
		delete gm_o_bars[i];
		delete gm_c_bars[i];
	}


	
}
template void lstm_grad_cpu(Tensor<float> *grad, internal::WrtVariable wrt, 
		std::vector<Tensor<float> *> weights,
		std::vector<Tensor<float> *> X, std::vector<Tensor<float> *> C,
		std::vector<Tensor<float> *> H, std::vector<Tensor<float> *> B,
		std::vector<Tensor<float> *> F, std::vector<Tensor<float> *> I,
		std::vector<Tensor<float> *> O, std::vector<Tensor<float> *> tC,
		Tensor<float> *output);


} //namespace internal
} //namespace magmadnn
