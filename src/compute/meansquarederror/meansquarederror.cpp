/**
 * @file meansquarederror.cpp
 * @author Daniel Nichols
 * @version 1.0
 * @date 2019-07-03
 *
 * @copyright Copyright (c) 2019
 *
 */
#include "compute/meansquarederror/meansquarederror.h"
namespace magmadnn {
namespace op {

template <typename T>
Operation<T> *meansquarederror(Operation<T> *ground_truth, Operation<T> *prediction) {
    unsigned int size;
    T norm;
    if(ground_truth->get_output_shape().size() == 1){
        size = ground_truth->get_output_shape(0);
        norm = static_cast<T>(1.0) / static_cast<T>(size);
        return op::scalarproduct(norm, op::reducesum(op::pow(op::add(ground_truth, op::negative(prediction)), 2), 0));
    } else if (ground_truth->get_output_shape().size() == 2){
        size = ground_truth->get_output_size();
        norm = static_cast<T>(1.0) / static_cast<T>(size);
        // printf("norm = %li\n", size);
        return op::scalarproduct(norm, op::reducesum(op::reducesum(op::pow(op::add(ground_truth, op::negative(prediction)), 2), 0), 1));
    } else if(ground_truth->get_output_shape().size() == 3) {
        size = ground_truth->get_output_size();
        norm = static_cast<T>(1.0) / static_cast<T>(size);
        // printf("norm = %li\n", size);
        return op::scalarproduct(norm, op::reducesum(op::reducesum(op::reducesum(op::pow(op::add(ground_truth, op::negative(prediction)), 2), 0), 1), 2));
    } else if (ground_truth->get_output_shape().size() == 4){
        // TOD0: to be modified
        size = ground_truth->get_output_size();
        norm = static_cast<T>(1.0) / static_cast<T>(size);
        // printf("norm = %li\n", size);
        return op::scalarproduct(norm, op::reducesum(op::reducesum(op::reducesum(op::reducesum(op::pow(op::add(ground_truth, op::negative(prediction)), 2), 0), 1), 2), 3));
    } else {
        printf("MSE: Dimension of tensor too high, Quitting...\n");
        exit(-1);
    }
}
template Operation<int> *meansquarederror(Operation<int> *ground_truth, Operation<int> *prediction);
template Operation<float> *meansquarederror(Operation<float> *ground_truth, Operation<float> *prediction);
template Operation<double> *meansquarederror(Operation<double> *ground_truth, Operation<double> *prediction);

}  // namespace op
}  // namespace magmadnn
