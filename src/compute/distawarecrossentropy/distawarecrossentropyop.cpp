#include "compute/distawarecrossentropy/distawarecrossentropyop.h"

namespace magmadnn {
namespace op {

template <typename T>
DistAwareCrossEntropyOp<T>::DistAwareCrossEntropyOp(Operation<T> *x, Operation<T> *y, bool copy, bool needs_grad)
    : Operation<T>::Operation({x, y}, needs_grad), x(x), y(y), copy(copy) {
    /*  x should be (n_samples x n_classes)
        y should be (n_samples x n_classes)
    */
    // make sure the predicted and ground truth are 4d tensors
    assert(x->get_output_shape(0) == y->get_output_shape(0));
    assert(x->get_output_shape(1) == y->get_output_shape(1));
    assert(x->get_output_shape(2) == y->get_output_shape(2));
    assert(x->get_output_shape(3) == y->get_output_shape(3));
    this->name = "DistAwareCrossEntropy";
    this->output_shape = x->get_output_shape();
    this->mem_type = x->get_memory_type();
    this->loss_tensor = new Tensor<T>({1}, {NONE, {}}, this->mem_type);

    if (copy) {
        this->output_tensor = new Tensor<T>(this->output_shape, {NONE, {}}, this->mem_type);
    } else {
        std::fprintf(stderr, "no copy cross entropy not supported yet.\n");
    }
}

template <typename T>
DistAwareCrossEntropyOp<T>::~DistAwareCrossEntropyOp() {}

template <typename T>
Tensor<T> *DistAwareCrossEntropyOp<T>::_eval(bool recompute) {
    x_tensor = x->eval(recompute);
    y_tensor = y->eval(recompute);
    T loss_sum = static_cast<T>(0);

    /* TEMPORARILY OUT OF COMMISSION, TAKES TOO LONG ON LARGE IMAGES */
    /* call the distance aware cross entropy */
    /*
            math::distawarecrossentropy(x_tensor, y_tensor, this->output_tensor);
            Tensor<T> *host_output_tensor = new Tensor<T>(this->output_shape, {NONE, {}}, HOST);
            host_output_tensor->copy_from(*this->output_tensor);

            // calculate the singular loss value
            for (int n = 0; n < host_output_tensor->get_shape(0); n++) {
                for (int c = 0; c < host_output_tensor->get_shape(1); c++) {
                    for (int h = 0; h < host_output_tensor->get_shape(2); h++) {
                        for (int w = 0; w < host_output_tensor->get_shape(3); w++) {
                            loss_sum = loss_sum + host_output_tensor->get({n, c, h, w});
                            num_pixels++;
                        }
                    }
                }
            }*/

    ///////////////////////////////////////////////////////////////////////////////////////////
    /* THIS IS A TEMPORARY FIX, THE IDEAL LOSS FUNCTION FOR IMAGE SEGMENTATION WOULD BE TO */
    /* FIX THE DISTANCE AWARE MATH FUNCTION SO THAT IT IS NOT AS SLOW FOR LARGE IMAGES. PERHAPS
        STORING THE CLOSEST FG PIXEL ONLY ONCE FOR EACH TENSOR WOULD REDUCE ALOT OF THE TIME. */
    ///////////////////////////////////////////////////////////////////////////////////////////

    /* L(y,y^)=−βylog⁡(y^)−(1−y)log(1−y^) */
    /* The β\betaβ parameter can be tuned, for example: to reduce the number of false-negative pixels,
        β>1\beta > 1β>1 , in order to reduce the number of false positives, set β<1\beta < 1β<1 */

    Tensor<T> *host_actual = new Tensor<T>(y_tensor->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_predicted = new Tensor<T>(x_tensor->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_output_tensor = new Tensor<T>(this->output_shape, {NONE, {}}, HOST);
    host_actual->copy_from(*y_tensor);
    host_predicted->copy_from(*x_tensor);
    T beta = static_cast<T>(2);
    for (unsigned int n = 0; n < host_actual->get_shape(0); n++) {
        for (unsigned int c = 0; c < host_actual->get_shape(1); c++) {
            for (unsigned int h = 0; h < host_actual->get_shape(2); h++) {
                for (unsigned int w = 0; w < host_actual->get_shape(3); w++) {
                    T pixel_loss = static_cast<T>(0);
                    T temp_result = static_cast<T>(0);
                    T actual = host_actual->get({n, c, h, w});
                    T predicted = host_predicted->get({n, c, h, w});

                    if (predicted == (T) 0.0 || predicted == (T) 1.0) {
                        if (predicted == (T) 0.0 && actual == (T) 1.0) {
                            loss_sum += (T) 1.0;
                            continue;
                        }

                        if (predicted == (T) 1.0 && actual == (T) 1.0) continue;

                        if (predicted == (T) 1.0 && actual == (T) 0.0) {
                            loss_sum += (T) 1.0;
                            continue;
                        }

                        if (predicted == (T) 0.0 && actual == (T) 0.0) continue;
                    }

                    temp_result = -beta * actual * (T) log10((double) predicted);
                    pixel_loss = temp_result - (((T) 1 - actual) * (T) log10(1.0 - (double) predicted));
                    loss_sum += pixel_loss;
                }
            }
        }
    }

    Tensor<T> *host_loss_tensor = new Tensor<T>({1}, {NONE, {}}, HOST);
    host_loss_tensor->set(0, loss_sum);
    this->loss_tensor->copy_from(*host_loss_tensor);

    delete host_loss_tensor;
    delete host_output_tensor;
    delete host_actual;
    delete host_predicted;
    return loss_tensor;
}

template <typename T>
Tensor<T> *DistAwareCrossEntropyOp<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
    /* return gradient in here ... */
    Tensor<T> *out = this->_grad_cache[(uintptr_t) var];
    if (out == NULL) {
        out = new Tensor<T>(this->output_shape, {NONE, {}}, this->mem_type);
        this->_grad_cache[(uintptr_t) var] = out;
    }

    Tensor<T> *host_actual = new Tensor<T>(y_tensor->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_predicted = new Tensor<T>(x_tensor->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_out_tensor = new Tensor<T>(this->output_shape, {NONE, {}}, HOST);
    host_actual->copy_from(*y_tensor);
    host_predicted->copy_from(*x_tensor);
    // T grad_value = grad->get(0);
    T beta = static_cast<T>(2);

    for (unsigned int n = 0; n < host_actual->get_shape(0); n++) {
        for (unsigned int c = 0; c < host_actual->get_shape(1); c++) {
            for (unsigned int h = 0; h < host_actual->get_shape(2); h++) {
                for (unsigned int w = 0; w < host_actual->get_shape(3); w++) {
                    T predicted = host_predicted->get({n, c, h, w});
                    T actual = host_actual->get({n, c, h, w});
                    T pixel_grad = static_cast<T>(0);
                    T temp_result1 = static_cast<T>(0);
                    T temp_result2 = static_cast<T>(0);

                    if (predicted == (T) 0.0 || predicted == (T) 1.0) {
                        if (predicted == (T) 0.0 && actual == (T) 1.0) {
                            host_out_tensor->set({n, c, h, w}, (T) -1);
                            continue;
                        }

                        if (predicted == (T) 1.0 && actual == (T) 1.0) {
                            host_out_tensor->set({n, c, h, w}, (T) 0);
                            continue;
                        }

                        if (predicted == (T) 1.0 && actual == (T) 0.0) {
                            host_out_tensor->set({n, c, h, w}, (T) 1);
                            continue;
                        }

                        if (predicted == (T) 0.0 && actual == (T) 0.0) {
                            host_out_tensor->set({n, c, h, w}, (T) 0);
                            continue;
                        }
                    }

                    temp_result1 = -(beta * actual) / predicted;
                    temp_result2 = actual / ((T) 1 - predicted);
                    pixel_grad = temp_result1 - temp_result2 + ((T) 1 / ((T) 1 - predicted));
                    host_out_tensor->set({n, c, h, w}, pixel_grad);
                }
            }
        }
    }
    out->copy_from(*host_out_tensor);
    delete host_actual;
    delete host_predicted;
    delete host_out_tensor;

    /* calculate the gradient tensor */
    /*
    if (var == this->x)
        math::distawarecrossentropy_grad(x_tensor, y_tensor, grad, out);
*/
    return out;
}
template class DistAwareCrossEntropyOp<int>;
template class DistAwareCrossEntropyOp<float>;
template class DistAwareCrossEntropyOp<double>;

template <typename T>
Operation<T> *distawarecrossentropy(Operation<T> *ground_truth, Operation<T> *predicted, unsigned int n_objects,
                                    bool copy, bool needs_grad) {
    // T norm;
    // norm = static_cast<T>(1.0) / static_cast<T>(n_objects); // normalize
    // return negative(op::scalarproduct(norm, reducesum(reducesum(reducesum(product(ground_truth, log(predicted,
    // true)), 3), 1), 0)));

    return new DistAwareCrossEntropyOp<T>(predicted, ground_truth);
}
template Operation<int> *distawarecrossentropy(Operation<int> *, Operation<int> *, unsigned int, bool, bool);
template Operation<float> *distawarecrossentropy(Operation<float> *, Operation<float> *, unsigned int, bool, bool);
template Operation<double> *distawarecrossentropy(Operation<double> *, Operation<double> *, unsigned int, bool, bool);

}  // namespace op
}  // namespace magmadnn