/**
 * @file concat_internal.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 *
 */
#include "compute/concat3d/concat_internal.h"
namespace magmadnn {
namespace internal {

template <typename T>
void concat_cpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output) {
    std::vector<unsigned int> in_shape = inputs[0]->get_shape();

    // sets values of output tensor based on given axis
    if (axis == 1) {
        for (unsigned int k = 0; k < inputs.size(); k++) {
            for (unsigned int i = 0; i < in_shape[0]; i++) {
                for (unsigned int j = 0; j < in_shape[1]; j++) {
                    output->set({i, k, j}, inputs[k]->get({i, j}));
                }
            }
        }
    } else if (axis == 0) {
        unsigned size = 0;
        for (unsigned k = 0; k < inputs.size(); k++) {
            for (unsigned int i = 0; i < inputs[k]->get_shape()[0]; i++) {
                for (unsigned int j = 0; j < in_shape[1]; j++) {
                    output->set({i + size, j}, inputs[k]->get({i, j}));
                }
            }
            size += inputs[k]->get_shape()[0];
        }

    } else if (axis == 2) {
        unsigned size = 0;
        for (unsigned k = 0; k < inputs.size(); k++) {
            for (unsigned int i = 0; i < in_shape[0]; i++) {
                for (unsigned int j = 0; j < inputs[k]->get_shape()[1]; j++) {
                    output->set({i, j + size}, inputs[k]->get({i, j}));
                }
            }
            size += inputs[k]->get_shape()[1];
        }
    }
}

template void concat_cpu(std::vector<Tensor<int> *> inputs, unsigned int axis, Tensor<int> *output);
template void concat_cpu(std::vector<Tensor<float> *> inputs, unsigned int axis, Tensor<float> *output);
template void concat_cpu(std::vector<Tensor<double> *> inputs, unsigned int axis, Tensor<double> *output);

template <typename T>
void conslice_cpu(Tensor<T> *input, unsigned int axis, unsigned int index, Tensor<T> *output) {
    unsigned int d0, d1;
    std::vector<unsigned int> myshape = input->get_shape();
    if (axis == 0) {
        d0 = myshape[1];
        d1 = myshape[2];
    } else if (axis == 1) {
        d0 = myshape[0];
        d1 = myshape[2];
    } else if (axis == 2) {
        d0 = myshape[0];
        d1 = myshape[1];
    } else {
        throw std::invalid_argument("slice_cpu only works for 3 dimensional tensor\n");
    }

    for (unsigned int i = 0; i < d0; i++) {
        for (unsigned int j = 0; j < d1; j++) {
            if (axis == 0) output->set({i, j}, input->get({index, i, j}));
            if (axis == 1) output->set({i, j}, input->get({i, index, j}));
            if (axis == 2) output->set({i, j}, input->get({i, j, index}));
        }
    }
}

template void conslice_cpu(Tensor<int> *input, unsigned int axis, unsigned int index, Tensor<int> *output);
template void conslice_cpu(Tensor<float> *input, unsigned int axis, unsigned int index, Tensor<float> *output);
template void conslice_cpu(Tensor<double> *input, unsigned int axis, unsigned int index, Tensor<double> *output);

}  // namespace internal
}  // namespace magmadnn
