/**
 * @file concat_internal_device.cu
 * @author Pierluigi Cambie-Fabris
 * @version 1.0
 * @date 2019-02-22
 */
#include "magmadnn/math.h"
#include "tensor/tensor.h"

#define BLK_SIZE 1024

namespace magmadnn {
namespace internal {

//kernel function for concat_gpu
template <typename T>
__global__ 
void kernel_concat_gpu(T ** A, T * C,  unsigned int size, unsigned int axis, 
		unsigned int ten_size, unsigned int width, unsigned int num_tens) {
	//TODO implement kernal for axis != 1
	unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int stride = blockDim.x * gridDim.x;
	for (unsigned int i = idx; i < size; i += stride) {
//		printf("\ni: %u,    i/width mod num_tens: %u,    width* (i/ten_size) + i mod width: %u\n", i,
//				(i/width)%num_tens, width * (i/ten_size) + i % width);
//		C[i] = A[(i/width)%num_tens][width * (i/ten_size) + i % width];

		C[i] = A[(i/width)%num_tens][width * (i/(num_tens*width)) + i % width];

	}
}

template <typename T>
void concat_gpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output) {
	unsigned int size = output->get_size();
	const auto grid_dim = ceildiv(size, BLK_SIZE);
	
	//pointer to each tensor pointer in inputs
	T ** in;
	cudaMallocManaged(&in, inputs.size() * sizeof(long));
	for (unsigned int i = 0; i < inputs.size(); i++){
		in[i] = inputs[i]->get_ptr();
	}

	//TODO implement for axis != 1
	if (axis != 1){
		throw std::invalid_argument("ConcatOp gpu has only been implemented for axis = 1");
	}

	//kernel call
	kernel_concat_gpu<<<grid_dim, BLK_SIZE>>>
		(in, output->get_ptr(), 
		 size, axis, inputs[0]->get_size(), inputs[0]->get_shape(1), inputs.size());

	//maybe free T ** in
}

template void concat_gpu(std::vector<Tensor<int>* > inputs, unsigned int axis, Tensor<int> *output);
template void concat_gpu(std::vector<Tensor<float>* > inputs, unsigned int axis, Tensor<float> *output);
template void concat_gpu(std::vector<Tensor<double>* > inputs, unsigned int axis, Tensor<double> *output);

template <typename T>
void concat_gpu(cudaStream_t custream, std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output){

	unsigned int size = output->get_size();
	const auto grid_dim = ceildiv(size, BLK_SIZE);
	
	//pointer to each tensor pointer in inputs
	T ** in;
	cudaMallocManaged(&in, inputs.size() * sizeof(long));
	for (unsigned int i = 0; i < inputs.size(); i++){
		in[i] = inputs[i]->get_ptr();
	}

	//TODO implement for axis != 1
	if (axis != 1){
		throw std::invalid_argument("ConcatOp gpu has only been implemented for axis = 1");
	}

	//kernel call
	kernel_concat_gpu<<<grid_dim, BLK_SIZE, 0, custream>>>
		(in, output->get_ptr(), 
		 size, axis, inputs[0]->get_size(), inputs[0]->get_shape(1), inputs.size());
}
template void concat_gpu(cudaStream_t custream, std::vector<Tensor<int>* > inputs, unsigned int axis, Tensor<int> *output);
template void concat_gpu(cudaStream_t custream, std::vector<Tensor<float>* > inputs, unsigned int axis, Tensor<float> *output);
template void concat_gpu(cudaStream_t custream, std::vector<Tensor<double>* > inputs, unsigned int axis, Tensor<double> *output);

template <typename T>
__global__
void kernel_conslice_gpu(T * A, T * B, unsigned int ind, unsigned int size, unsigned int ten_size, unsigned int Z, unsigned int width) {
        unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
        unsigned int stride = blockDim.x * gridDim.x;
        for (unsigned int i = idx; i < size; i += stride) {
        //      printf("i: %u, ind * width: %u, i mod width: %u, i/width * ten_size: %u,index: %u\n", 
         //                     i, ind * width, i%width, (i/width) * ten_size, ind * width + i % width + (i/width) * ten_size);
//           	printf("i: %u, val: %f\n", i, A[ind * width + i % width + (i/width) * ten_size]);
	   	B[i] = A[ind * width + i % width + (i/width) * ten_size];
// 		float t = 1.f;
//		printf("val2: %f\n", t);
//		fflush(1);
 	}

}

template <typename T>
void conslice_gpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output){
        unsigned int size = output->get_size();
        const auto grid_dim = ceildiv(size, BLK_SIZE);
        if (axis != 1){
                throw std::invalid_argument("SliceOp gpu has only been implemented for axis = 1");
        }

        kernel_conslice_gpu<<<grid_dim, BLK_SIZE>>>(input->get_ptr(), output->get_ptr(), index, output->get_size(),
                        input->get_shape(1) * input->get_shape(2), input->get_shape(0),input->get_shape(2));
}


template void conslice_gpu(Tensor<int> * input, unsigned int axis, unsigned int index, Tensor<int> *output);
template void conslice_gpu(Tensor<float> * input, unsigned int axis, unsigned int index, Tensor<float> *output);
template void conslice_gpu(Tensor<double> * input, unsigned int axis, unsigned int index, Tensor<double> *output);


template <typename T>
void conslice_gpu(cudaStream_t custream, Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output){
        unsigned int size = output->get_size();
        const auto grid_dim = ceildiv(size, BLK_SIZE);
        if (axis != 1){
                throw std::invalid_argument("SliceOp gpu has only been implemented for axis = 1");
        }

        kernel_conslice_gpu<<<grid_dim, BLK_SIZE>>>(input->get_ptr(), output->get_ptr(), index, output->get_size(),
                        input->get_shape(1) * input->get_shape(2), input->get_shape(0),input->get_shape(2));
}


template void conslice_gpu(cudaStream_t custream, Tensor<int> * input, unsigned int axis, unsigned int index, Tensor<int> *output);
template void conslice_gpu(cudaStream_t custream ,Tensor<float> * input, unsigned int axis, unsigned int index, Tensor<float> *output);
template void conslice_gpu(cudaStream_t custream, Tensor<double> * input, unsigned int axis, unsigned int index, Tensor<double> *output);




} //namespace internal
} //namespace magmadnn
