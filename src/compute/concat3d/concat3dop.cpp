/**
 * @file concat3dop.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */
#include "compute/concat3d/concat3dop.h"

namespace magmadnn {
namespace op {
template <typename T>
Concat3dOp<T>::Concat3dOp(std::vector<Operation<T> *> inputs, unsigned axis, bool copy, bool needs_grad)
    : Operation<T>(inputs, needs_grad), inputs(inputs), axis(axis), copy(copy) {
    init();
}

template <typename T>
void Concat3dOp<T>::init() {
    // TODO: implement for GPU

    unsigned num_tens = inputs.size();

    if (num_tens < 2) {
        throw std::invalid_argument("Concat3dOp needs atleast two input tensors.");
    }

    // TODO: Implement check to ensure the dimensions of inputs are valid

    if (inputs.empty()) std::cout << "\n inputs is NULL \n";

    // sets output shape based on axis value
    // TODO store unsigned size value to avoid recalculating it
    if (this->axis == 1) {
        this->output_shape = {this->inputs[0]->get_output_shape(0), num_tens, inputs[0]->get_output_shape(1)};
    } else if (this->axis == 0) {
        unsigned size = 0;
        for (unsigned i = 0; i < num_tens; i++) {
            size += inputs[i]->get_output_shape()[0];
        }

        this->output_shape = {size, inputs[0]->get_output_shape(1)};

    } else if (this->axis == 2) {
        unsigned size = 0;
        for (unsigned i = 0; i < num_tens; i++) {
            size += inputs[i]->get_output_shape(1);
        }
        //		std::cout << '\n' << size << '\n';

        this->output_shape = {inputs[0]->get_output_shape(0), size};

    } else {
        throw std::invalid_argument("Concat3dOp has only been implemented for axis = 0,1,2.");
    }

    // allocates memory for the output tensor
    this->mem_type = inputs[0]->get_memory_type();
    this->output_tensor = new Tensor<T>(this->output_shape, {NONE, {}}, this->mem_type);

    return;
}

template <typename T>
Tensor<T> *Concat3dOp<T>::_eval(bool recompute) {
    std::vector<Tensor<T> *> input_tens(inputs.size());

    // evaluates each input tensor and stores them in a vector
    for (unsigned i = 0; i < this->inputs.size(); i++) {
        inputs[i]->eval(recompute);
        input_tens[i] = inputs[i]->get_output_tensor();
    }

    // concatenates all the given tensors

    if (this->mem_type == HOST) {
        internal::concat_cpu(input_tens, this->axis, this->output_tensor);
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {

        internal::concat_gpu(this->get_custream(), input_tens, this->axis, this->output_tensor);
        if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
    }
#endif

    return this->output_tensor;
}

template <typename T>
Tensor<T> *Concat3dOp<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
    unsigned index = 0;

    Tensor<T> *out = this->_grad_cache[(uintptr_t) var];

    if (out == NULL) {
        out = new Tensor<T>(var->get_output_shape(), {NONE, {}}, this->mem_type);
#if defined(MAGMADNN_HAVE_CUDA)
        out->set_custream(this->get_custream());
        out->set_cublas_handle(this->get_cublas_handle());
#endif
        this->_grad_cache[(uintptr_t) var] = out;
    }

    // This works by depending upon the preserved order of this->inputs
    for (unsigned i = 0; i < inputs.size(); ++i) {
        if (inputs[i] == var) {
            // finds index of var
            index = i;
            break;
        }
    }

    // gets gradient based on given axis
    if (this->mem_type == HOST) {
        if (axis == 1) {
            internal::conslice_cpu(grad, this->axis, index, out);
        } else if (axis == 0) {
            unsigned size = 0;
            for (unsigned k = 0; k < index; k++) {
                size += this->inputs[k]->get_output_shape(0);
            }
            for (unsigned i = 0; i < var->get_output_shape(0); i++) {
                for (unsigned j = 0; j < var->get_output_shape(1); j++) {
                    out->set({i, j}, grad->get({i + size, j}));
                }
            }
        }

        else if (axis == 2) {
            unsigned size = 0;
            for (unsigned k = 0; k < index; k++) {
                size += this->inputs[k]->get_output_shape(1);
            }
            for (unsigned i = 0; i < var->get_output_shape(0); i++) {
                for (unsigned j = 0; j < var->get_output_shape(1); j++) {
                    out->set({i, j}, grad->get({i, j + size}));
                }
            }
        }
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {

        internal::conslice_gpu(this->get_custream(), grad, this->axis, index, out);
        if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
    }
#endif

    return out;
}

template class Concat3dOp<int>;
template class Concat3dOp<float>;
template class Concat3dOp<double>;

template <typename T>
Concat3dOp<T> *concat3d(std::vector<Operation<T> *> inputs, unsigned axis, bool copy, bool needs_grad) {
    return new Concat3dOp<T>(inputs, axis, copy, needs_grad);
}

template Concat3dOp<int> *concat3d(std::vector<Operation<int> *>, unsigned axis, bool copy, bool needs_grad);
template Concat3dOp<float> *concat3d(std::vector<Operation<float> *>, unsigned axis, bool copy, bool needs_grad);
template Concat3dOp<double> *concat3d(std::vector<Operation<double> *>, unsigned axis, bool copy, bool needs_grad);
}  // namespace op
}  // namespace magmadnn
