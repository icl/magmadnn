#include "compute/conv2dtranspose/conv2dtransposeop.h"
#include <iostream>

#if defined(MAGMADNN_CMAKE_BUILD)
    #include "magmadnn/config.h"
#endif

namespace magmadnn {
    namespace op {

        template <typename T>
        void Conv2DTransposeOp<T>::cuda_forward() {
            this->cudnn_settings.handle = this->get_cudnn_handle();
            // call the forward pass function of the transpose convolution
            ::magmadnn::math::conv2dtranspose_device
            (this->input_tensor, this->filter_tensor, 
            this->output_tensor, this->cudnn_settings);
            if( !this->get_async() )
                cudaStreamSynchronize(this->get_custream());
        }
        template class Conv2DTransposeOp<int>;
        template class Conv2DTransposeOp<float>;
        template class Conv2DTransposeOp<double>;
    }
}
