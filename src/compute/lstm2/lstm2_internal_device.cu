#include "compute/lstm2/lstm2_internal.h"
#include "magmadnn/math.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace internal {

template <typename T>
__global__
void initGPUData_ker(T *data, int numElements, T value) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid < numElements) {
        data[tid] = value;
    }
}

template <typename T>
void initGPUData(T *data, int numElements, T value) {
    dim3 gridDim;
    dim3 blockDim;

    blockDim.x = 1024;
    gridDim.x  = (numElements + blockDim.x - 1) / blockDim.x;

    initGPUData_ker<<<gridDim, blockDim>>>(data, numElements, value);
}

template void initGPUData(float *data, int numElements, float value);
}
}
