#include "compute/lstm2/lstm2op.h"


namespace magmadnn {
namespace op {
template <typename T>
LSTM2Op<T>::LSTM2Op(Operation<T> *input, Operation<T> *hx, Operation<T> *cx, Operation<T> *weights, bool return_sequences, bool needs_grad)
	: Operation<T>::Operation({input, hx, cx, weights}, needs_grad),
	input(input),
	hx(hx),
	cx(cx),
	weights(weights),
	return_sequences(return_sequences) {

	this->mem_type = input->get_memory_type();
	this->name = "LSTM2Op";
	this->num_nodes = 1;

	this->dx_t = new Tensor<T>(this->input->get_output_shape(), {CONSTANT,{(T)0}}, this->mem_type);
	this->input_t = this->input->get_output_tensor();
	this->hx_t = hx->get_output_tensor();
	this->cx_t = cx->get_output_tensor();
	this->weights_t = weights->get_output_tensor();

	this->dhx_t = new Tensor<T>(hx_t->get_shape(),{CONSTANT,{(T)0}}, this->mem_type);
	this->dcx_t = new Tensor<T>(cx_t->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);
	this->cy_t = new Tensor<T>(cx_t->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);
	this->hy_t = new Tensor<T>(hx_t->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);
	this->dcy_t = new Tensor<T>(cx_t->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);
	this->dhy_t = new Tensor<T>(hx_t->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);

	this->y_t = new Tensor<T>({this->input->get_output_shape(0), this->input->get_output_shape(1),
			this->hx->get_output_shape(1/*2*/)}, {CONSTANT,{(T)0}}, this->mem_type);

	//TODO find out why this needs to be initialized

	
	this->dy_t = new Tensor<T>(this->y_t/*hy_t*/->get_shape(), {CONSTANT,{(T)0}}, this->mem_type);
	this->_grad_cache[(uintptr_t) this->input] = NULL;

	if (this->return_sequences) {
		this->output_tensor = this->y_t;
		this->output_shape = this->y_t->get_shape();
	}
	//NOTE: return_sequences = false does not work as intended
	else {
		this->output_shape = this->hy_t->get_shape();
		this->output_tensor = this->hy_t;
	}
	std::cout << "output size: " <<this->output_tensor->get_size() << std::endl;
	

	this->init_settings();
}

template <typename T>
LSTM2Op<T>::~LSTM2Op() {

#if defined(MAGMADNN_HAVE_CUDA)


	cudaFree(this->lstm_settings.workSpace);
    cudaFree(this->lstm_settings.reserveSpace);
    cudaFree(this->lstm_settings.weightSpace);
    cudaFree(this->lstm_settings.dweightSpace);
	cudaFree(this->lstm_settings.devSeqLengthArray);

    cudnnDestroyRNNDataDescriptor(this->lstm_settings.xDesc);
    cudnnDestroyRNNDataDescriptor(this->lstm_settings.yDesc);

    cudnnDestroyTensorDescriptor(this->lstm_settings.hDesc);
    cudnnDestroyTensorDescriptor(this->lstm_settings.cDesc);

	free(this->lstm_settings.seqLengthArray);
	cudnnErrchk(cudnnDestroyRNNDescriptor(this->lstm_settings.rnnDesc));

#endif

}

template <typename T>
Tensor<T> *LSTM2Op<T>::_eval(bool recompute) {
	this->has_been_run = false;
	this->input->eval(recompute);
	this->hx->eval(recompute);
	this->cx->eval(recompute);
		
#if defined(MAGMADNN_HAVE_CUDA)
	cuda_forward();
	
#endif

	return this->output_tensor;
} 

template <typename T>
Tensor<T> *LSTM2Op<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
	Tensor<T> *out = this->_grad_cache[(uintptr_t) var];
	
	if (this->return_sequences){
		this->dy_t = grad;
	}
	else{
		//These are previous attempts at make return_sequences = false work
	/*

		for (unsigned i = 0; i < this->input->get_output_shape(0); i++){
			cudaErrchk(cudaMemcpy(dy_t->get_ptr() + i * this->input->get_output_shape(1) * this->hx->get_output_shape(1) + (this->input->get_output_shape(1) - 1) * this->hx->get_output_shape(1),
					grad->get_ptr() + i * this->hx->get_output_shape(1), sizeof(T) * this->hx->get_output_shape(1), 
					cudaMemcpyDeviceToDevice));
		}

		int ind = this->dy_t->get_size() - grad->get_size();
		cudaErrchk(cudaMemcpy(dy_t->get_ptr() + ind, grad->get_ptr(), sizeof(T) * grad->get_size(), cudaMemcpyDeviceToDevice));
	
		for (unsigned i = 0; i < this->input->get_output_shape(0) * this->hx->get_output_shape(1); i++){
			cudaErrchk(cudaMemcpy(dy_t->get_ptr() + this->input->get_output_shape(1) * i + this->input->get_output_shape(1) - 1,
					grad->get_ptr() + i, sizeof(T), 
					cudaMemcpyDeviceToDevice));

		}
	*/
		for (unsigned i = 0; i < this->input->get_output_shape(1); i++)
			cudaErrchk(cudaMemcpy(dy_t->get_ptr()+ i * grad->get_size(), grad->get_ptr(), sizeof(T) * grad->get_size(), cudaMemcpyDeviceToDevice));


	}

	if (var == this->input) {
		if (out == NULL) {
			out = new Tensor<T>(this->input->get_output_shape(), {NONE, {}}, this->mem_type);
	#if defined(MAGMADNN_HAVE_CUDA)
			out->set_custream(this->get_custream());
			out->set_cublas_handle(this->get_cublas_handle());
	#endif
			this->_grad_cache[(uintptr_t) var] = out;
		}

		
		
		if (!this->has_been_run){
			::magmadnn::math::lstm_grad_data_device(this->input_t, this->y_t, this->dy_t/*grad*/, /*this is out*/this->dx_t, this->hx_t, this->dhy_t, this->dhx_t, this->cx_t, this->dcy_t, this->dcx_t, this->weights_t, this->lstm_settings);
			this->has_been_run = true;
		}
		out->copy_from(*this->dx_t, 0, this->dx_t->get_size());
		this->has_been_run = true;
	}
	else if(var == this->weights) {
		if (out == NULL) {
			out = new Tensor<T>(this->weights->get_output_shape(), {NONE, {}}, this->mem_type);
	#if defined(MAGMADNN_HAVE_CUDA)
			out->set_custream(this->get_custream());
			out->set_cublas_handle(this->get_cublas_handle());
	#endif
			this->_grad_cache[(uintptr_t) var] = out;
		}
		::magmadnn::math::lstm_grad_data_device_weights(this->input_t, this->y_t, this->hx_t, this->lstm_settings);

		cudaErrchk(cudaMemcpy(out->get_ptr(), this->lstm_settings.dweightSpace, sizeof(float) * out->get_size(), cudaMemcpyDeviceToDevice));
	}
	else if (var == this->cx) {
		if (out == NULL) {
			out = new Tensor<T>(this->cx->get_output_shape(), {NONE, {}}, this->mem_type);
	#if defined(MAGMADNN_HAVE_CUDA)
			out->set_custream(this->get_custream());
			out->set_cublas_handle(this->get_cublas_handle());
	#endif
			this->_grad_cache[(uintptr_t) var] = out;
		}
		if (!this->has_been_run) {
			::magmadnn::math::lstm_grad_data_device(this->input_t, this->y_t, this->dy_t/*grad*/, /*temp*/this->dx_t, 
										this->hx_t, this->dhy_t, this->dhx_t, this->cx_t, this->dcy_t, /*out*/this->dcx_t, this->weights_t, this->lstm_settings);
			this->has_been_run = true;
		}

		//out = this->dcx_t;
		out->copy_from(*this->dcx_t, 0, this->dcx_t->get_size());
	}
	else if (var == this->hx) {
		if (out == NULL) {
			out = new Tensor<T>(this->hx->get_output_shape(), {NONE, {}}, this->mem_type);
	#if defined(MAGMADNN_HAVE_CUDA)
			out->set_custream(this->get_custream());
			out->set_cublas_handle(this->get_cublas_handle());
	#endif
			this->_grad_cache[(uintptr_t) var] = out;
		}

		if (!this->has_been_run){
			::magmadnn::math::lstm_grad_data_device(this->input_t, this->y_t, this->dy_t/*grad*/, this->dx_t/*temp*/,  
										this->hx_t, this->dhy_t, /*out8*/this->dhx_t, this->cx_t, this->dcy_t, this->dcx_t, this->weights_t, this->lstm_settings);
			this->has_been_run = true;
		}
		//out = this->dhx_t;
		out->copy_from(*this->dhx_t, 0, this->dhx_t->get_size());
	}

	if (!this->get_async()) cudaStreamSynchronize(this->get_custream());	

	return out;
}

template <typename T>
void LSTM2Op<T>::init_settings() {
	
	if (this->mem_type == HOST){
		std::fprintf(stderr, "Error: LSTM2Op::init_settings requires GPU.\n");

	}
	else{

		this->lstm_settings.handle = this->get_cudnn_handle();

		int seqLength = this->input->get_output_shape(1);
		int miniBatch = this->input->get_output_shape(0);
		cudnnDataType_t dataType = CUDNN_DATA_FLOAT;
		int inputSize = this->input->get_output_shape(2);

		int hiddenSize = this->hx->get_output_shape(/*2*/1);
		double paddingFill = 0.0;
		int numLayers = this->num_nodes;

		int dimHidden[3];
		dimHidden[0] = numLayers;
		dimHidden[1] = miniBatch;
		dimHidden[2] = hiddenSize;

		int strideHidden[3];
		strideHidden[0] = dimHidden[1] * dimHidden[2];
		strideHidden[1] = dimHidden[2];
		strideHidden[2] = 1;
   		
		size_t stateSize;
	    void * states = NULL;
		int dropout = 0;
		int seed = 1337ull;
		this->lstm_settings.seqLengthArray = (int *)malloc(miniBatch * sizeof(int));

		cudaErrchk(cudaMalloc((void **)&this->lstm_settings.devSeqLengthArray, miniBatch * sizeof(int)));


		for (int i = 0; i < miniBatch; i++) {
			this->lstm_settings.seqLengthArray[i] = seqLength;
		}
    		cudaErrchk(cudaMemcpy(this->lstm_settings.devSeqLengthArray, this->lstm_settings.seqLengthArray, 
				      miniBatch * sizeof(int), cudaMemcpyHostToDevice));

		cudnnErrchk(cudnnCreateRNNDataDescriptor(&this->lstm_settings.xDesc));
		cudnnErrchk(cudnnCreateRNNDataDescriptor(&this->lstm_settings.yDesc));

		cudnnErrchk(cudnnSetRNNDataDescriptor(this->lstm_settings.xDesc,
                                            dataType,
                                            CUDNN_RNN_DATA_LAYOUT_SEQ_MAJOR_PACKED,
                                            seqLength,
                                            miniBatch,
                                            inputSize,
                                            this->lstm_settings.seqLengthArray,
                                            &paddingFill));



		cudnnErrchk(cudnnSetRNNDataDescriptor(this->lstm_settings.yDesc,
                                            dataType,
                                            CUDNN_RNN_DATA_LAYOUT_SEQ_MAJOR_PACKED,
                                            seqLength,
                                            miniBatch,
                                            hiddenSize,
                                            this->lstm_settings.seqLengthArray,
                                            &paddingFill));

		cudnnErrchk(cudnnCreateTensorDescriptor(&this->lstm_settings.hDesc));
		cudnnErrchk(cudnnCreateTensorDescriptor(&this->lstm_settings.cDesc));


		cudnnErrchk(cudnnSetTensorNdDescriptor(this->lstm_settings.hDesc, dataType, 3, dimHidden, strideHidden));
		cudnnErrchk(cudnnSetTensorNdDescriptor(this->lstm_settings.cDesc, dataType, 3, dimHidden, strideHidden));

		cudnnDropoutDescriptor_t dropoutDesc;	
		cudnnErrchk(cudnnCreateDropoutDescriptor(&dropoutDesc));
		
		cudnnErrchk(cudnnDropoutGetStatesSize(this->lstm_settings.handle, &stateSize));

		cudaErrchk(cudaMalloc(&states, stateSize));

		cudnnErrchk(cudnnSetDropoutDescriptor(dropoutDesc,
                                            this->lstm_settings.handle,
                                            dropout,
                                            states,
                                            stateSize,
                                            seed));

    // Set up the RNN descriptor
		cudnnErrchk(cudnnCreateRNNDescriptor(&this->lstm_settings.rnnDesc));
		cudnnRNNAlgo_t algorithm = CUDNN_RNN_ALGO_STANDARD;
		cudnnRNNMode_t cellMode = CUDNN_LSTM;
		cudnnRNNBiasMode_t biasMode = CUDNN_RNN_SINGLE_INP_BIAS;
		cudnnDirectionMode_t dirMode = CUDNN_UNIDIRECTIONAL;
		cudnnRNNInputMode_t inputMode = CUDNN_LINEAR_INPUT;
		cudnnDataType_t mathPrecision = CUDNN_DATA_FLOAT;
		cudnnMathType_t mathType = CUDNN_DEFAULT_MATH;

		int projSize = hiddenSize;
		cudnnErrchk(cudnnSetRNNDescriptor_v8(this->lstm_settings.rnnDesc,
                                           algorithm,
                                           cellMode,
                                           biasMode,
                                           dirMode,
                                           inputMode,
                                           dataType,
                                           mathPrecision,
                                           mathType,
                                           inputSize,
                                           hiddenSize,
                                           projSize,
                                           numLayers,
                                           dropoutDesc,
                                           0));

		cudnnErrchk(cudnnGetRNNWeightSpaceSize(this->lstm_settings.handle, this->lstm_settings.rnnDesc, &this->lstm_settings.weightSpaceSize));
		cudaErrchk(cudaMalloc((void **)&this->lstm_settings.weightSpace, this->lstm_settings.weightSpaceSize));
		cudaErrchk(cudaMalloc((void **)&this->lstm_settings.dweightSpace, this->lstm_settings.weightSpaceSize));

		cudnnErrchk(cudnnGetRNNTempSpaceSizes(this->lstm_settings.handle,
                                            this->lstm_settings.rnnDesc,
                                            CUDNN_FWD_MODE_TRAINING,
                                            this->lstm_settings.xDesc,
                                            &this->lstm_settings.workSpaceSize,
                                            &this->lstm_settings.reserveSpaceSize));

		cudaErrchk(cudaMalloc((void **)&this->lstm_settings.workSpace, this->lstm_settings.workSpaceSize));
		cudaErrchk(cudaMalloc((void **)&this->lstm_settings.reserveSpace, this->lstm_settings.reserveSpaceSize));


	}
}

#if defined(MAGMADNN_HAVE_CUDA)
template <typename T>
void LSTM2Op<T>::cuda_forward() {
	this->lstm_settings.handle = this->get_cudnn_handle();
	::magmadnn::math::lstm_device(this->input_t, this->y_t, this->hx_t, this->hy_t, this->cx_t, this->cy_t, this->weights_t, this->lstm_settings);
	if (!this->get_async()) {
		cudaStreamSynchronize(this->get_custream());
	}


}
#endif


template class LSTM2Op<float>;

template <typename T>
LSTM2Op<T> * lstm2(Operation<T> *input, Operation<T> *hx, Operation<T> *cx, Operation<T> *weights, bool return_sequences, bool needs_grad){
	return new LSTM2Op<T>(input, hx, cx, weights, return_sequences, needs_grad);

}

template LSTM2Op<float> *lstm2(Operation<float> *input, 
		Operation<float> *hx, Operation<float> *cx, Operation<float> *weights, bool return_sequences, bool needs_grad);



} //namespace op
} //namespace magmadnn
