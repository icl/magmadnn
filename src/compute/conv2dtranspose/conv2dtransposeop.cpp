#include "compute/conv2dtranspose/conv2dtransposeop.h"

#include <iostream>

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif

namespace magmadnn {
namespace op {

template <typename T>
Conv2DTransposeOp<T>::Conv2DTransposeOp(Operation<T> *input, Operation<T> *filter, int pad_h, int pad_w, int out_pad_h,
                                        int out_pad_w, int vertical_stride, int horizontal_stride, int dilation_h,
                                        int dilation_w, bool use_cross_correlation, bool needs_grad)
    : Operation<T>::Operation({input, filter}, needs_grad),
      input(input),
      filter(filter),
#if defined(MAGMADNN_HAVE_MKLDNN)
      dnnl_cpu_engine_(dnnl::engine::kind::cpu, 0),
      dnnl_fwd_pdesc_(nullptr),
#endif
      pad_h(pad_h),
      pad_w(pad_w),
      out_pad_h(out_pad_h),
      out_pad_w(out_pad_w),
      vertical_stride(vertical_stride),
      horizontal_stride(horizontal_stride),
      dilation_h(dilation_h),
      dilation_w(dilation_w),
      use_cross_correlation(use_cross_correlation) {

    /* setup code in here */
    this->mem_type = input->get_memory_type();
    this->name = "Conv2DTransposeOp";

    /* initialize all the conv settings */
    this->input_tensor = this->input->get_output_tensor();
    this->init_settings();

#if defined(MAGMADNN_HAVE_MKLDNN)
    this->onednn_init_settings();
#endif
}

template <typename T>
Conv2DTransposeOp<T>::~Conv2DTransposeOp() {
    if (this->mem_type == HOST) {
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {

        cudaErrchk(cudaFree(this->cudnn_settings.workspace));
        cudaErrchk(cudaFree(this->cudnn_settings.grad_data_workspace));
        cudaErrchk(cudaFree(this->cudnn_settings.grad_filter_workspace));

        cudnnErrchk(cudnnDestroyFilterDescriptor(this->cudnn_settings.filter_desc));
        cudnnErrchk(cudnnDestroyConvolutionDescriptor(this->cudnn_settings.conv_desc));
    }
#endif
}

template <typename T>
Tensor<T> *Conv2DTransposeOp<T>::_eval(bool recompute) {
    this->input_tensor = input->eval(recompute);
    this->filter_tensor = filter->eval(recompute);

    // maybe add a math::conv2dtranspose function in the math library
    // and call the function here on the input tensor.

    if (this->mem_type == HOST) {
#if defined(MAGMADNN_HAVE_MKLDNN)
        this->onednn_forward();
#else
        // TODO: raise exception
        std::fprintf(stderr, "Error: Conv2dForward::_eval requires GPU\n");
#endif
    } else {
#if defined(MAGMADNN_HAVE_CUDA)
        this->cuda_forward();
#endif
    }

    return this->output_tensor;
}  // end _eval

template <typename T>
Tensor<T> *Conv2DTransposeOp<T>::_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad) {
    // return gradient in here
    Tensor<T> *out = this->_grad_cache[(uintptr_t) var];

    if (var == this->input) {
        if (out == NULL) {
            out = new Tensor<T>(this->input->get_output_shape(), {NONE, {}}, this->mem_type);
#if defined(MAGMADNN_HAVE_CUDA)
            out->set_custream(this->get_custream());
            out->set_cublas_handle(this->get_cublas_handle());
#endif
            this->_grad_cache[(uintptr_t) var] = out;
        }

        this->filter_tensor = this->filter->eval(false);

        if (this->mem_type == HOST) {
#if defined(MAGMADNN_HAVE_MKLDNN)

            this->onednn_backward_data(grad, out);
#else
            ::magmadnn::math::conv2dtranspose_grad_data(this->filter_tensor, grad, out);
#endif
        }
#if defined(MAGMADNN_HAVE_CUDA)
        else {
            this->cudnn_settings.handle = this->get_cudnn_handle();
            ::magmadnn::math::conv2dtranspose_grad_data_device(this->filter_tensor, grad, out, this->cudnn_settings);
            if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
        }
#endif

    } else if (var == this->filter) {
        if (out == NULL) {
            out = new Tensor<T>(this->filter->get_output_shape(), {NONE, {}}, this->mem_type);
#if defined(MAGMADNN_HAVE_CUDA)
            out->set_custream(this->get_custream());
            out->set_cublas_handle(this->get_cublas_handle());
#endif
            this->_grad_cache[(uintptr_t) var] = out;
        }

        this->input_tensor = this->input->eval(false);

        if (this->mem_type == HOST) {
#if defined(MAGMADNN_HAVE_MKLDNN)
            onednn_backward_weights(grad, out);
#else
            ::magmadnn::math::conv2dtranspose_grad_filter(this->input_tensor, grad, out);
#endif
        }
#if defined(MAGMADNN_HAVE_CUDA)
        else {
            this->cudnn_settings.handle = this->get_cudnn_handle();
            ::magmadnn::math::conv2dtranspose_grad_filter_device(this->input_tensor, grad, out, this->cudnn_settings);
            if (!this->get_async()) cudaStreamSynchronize(this->get_custream());
        }
#endif
    } else {
        std::fprintf(stderr, "Error: bad conv2d grad\n");
    }

    return out;
}  // end _grad

#if defined(MAGMADNN_HAVE_MKLDNN)
template <typename T>
void Conv2DTransposeOp<T>::onednn_init_settings() {
    // TODO
}
#endif

template <typename T>
void Conv2DTransposeOp<T>::init_settings() {
    // make sure it is running on GPU
    if (this->mem_type == HOST) {
#if !defined(MAGMADNN_HAVE_MKLDNN)
        std::fprintf(stderr, "Error: Conv2DForward::init_settings requires GPU.\n");
#endif
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        this->cudnn_settings.handle = this->get_cudnn_handle();

        // initialize the conv descriptor
        cudnnErrchk(cudnnCreateConvolutionDescriptor(&this->cudnn_settings.conv_desc));

        assert((vertical_stride > 0) && (horizontal_stride > 0));

        /**
         * set the convolution descriptor for the forward
         * pass of the transposed convolution
         */
        cudnnErrchk(cudnnSetConvolution2dDescriptor(
            this->cudnn_settings.conv_desc, pad_h, pad_w, vertical_stride, horizontal_stride, dilation_h, dilation_w,
            (use_cross_correlation) ? CUDNN_CROSS_CORRELATION : CUDNN_CONVOLUTION,
            ::magmadnn::internal::get_cudnn_data_type(static_cast<T>(0))));

        // init and create the filter descriptor
        int filter_dims[4];
        const std::vector<unsigned int> &filter_shape = this->filter->get_output_shape();
        for (unsigned int i = 0; i < 4; i++) {
            if (i >= filter_shape.size())
                filter_dims[i] = 1;
            else
                filter_dims[i] = filter_shape[i];
        }

        // init and create the filter descriptor
        cudnnErrchk(cudnnCreateFilterDescriptor(&this->cudnn_settings.filter_desc));
        // create the filter descriptor with the appropriate amount of dimensions
        cudnnErrchk(cudnnSetFilter4dDescriptor(
            this->cudnn_settings.filter_desc, ::magmadnn::internal::get_cudnn_data_type(static_cast<T>(0)),
            CUDNN_TENSOR_NCHW, filter_dims[0], filter_dims[1], filter_dims[2], filter_dims[3]));

        this->calculate_and_set_output_shape();

        /* use CUDNN to get the correct/optimal convolution algorithm */

#if CUDNN_MAJOR == 8                        // cudnn version 8 code
        int requested_algo_ct = 1;
        int *returned_algo_ct = NULL;

        // I switched the positions of the input and output tensors
        cudnnErrchk(cudnnGetConvolutionForwardAlgorithm_v7(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->cudnn_settings.filter_desc, this->cudnn_settings.conv_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), requested_algo_ct, returned_algo_ct,
            &this->cudnn_settings.algo));

        // get the workspace size and allocate that memory
        cudnnErrchk(cudnnGetConvolutionForwardWorkspaceSize(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->cudnn_settings.filter_desc, this->cudnn_settings.conv_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.algo.algo,
            &this->cudnn_settings.workspace_size));
        cudaErrchk(cudaMalloc((void **) &this->cudnn_settings.workspace, this->cudnn_settings.workspace_size));

        // get the best grad algorithms
        cudnnErrchk(cudnnGetConvolutionBackwardDataAlgorithm_v7(
            this->cudnn_settings.handle, this->cudnn_settings.filter_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(),                    /* use input for dy */
            this->cudnn_settings.conv_desc, this->output_tensor->get_cudnn_tensor_descriptor(), /* use output for dx */
            requested_algo_ct, returned_algo_ct, &this->cudnn_settings.bwd_data_algo));

        // get the best filter algorithm
        cudnnErrchk(cudnnGetConvolutionBackwardFilterAlgorithm_v7(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.conv_desc,
            this->cudnn_settings.filter_desc, requested_algo_ct, returned_algo_ct,
            &this->cudnn_settings.bwd_filter_algo));

        /* get the workspaces for each of the backward algorithms */
        cudnnErrchk(cudnnGetConvolutionBackwardDataWorkspaceSize(
            this->cudnn_settings.handle, this->cudnn_settings.filter_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.conv_desc,
            this->output_tensor->get_cudnn_tensor_descriptor(), this->cudnn_settings.bwd_data_algo.algo,
            &this->cudnn_settings.grad_data_workspace_size));
        cudaErrchk(cudaMalloc((void **) &this->cudnn_settings.grad_data_workspace,
                              this->cudnn_settings.grad_data_workspace_size));

        cudnnErrchk(cudnnGetConvolutionBackwardFilterWorkspaceSize(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.conv_desc,
            this->cudnn_settings.filter_desc, this->cudnn_settings.bwd_filter_algo.algo,
            &this->cudnn_settings.grad_filter_workspace_size));
#else


        // I switched the positions of the input and output tensors
        cudnnErrchk(cudnnGetConvolutionForwardAlgorithm(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->cudnn_settings.filter_desc, this->cudnn_settings.conv_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), CUDNN_CONVOLUTION_FWD_PREFER_FASTEST, 0,
            &this->cudnn_settings.algo));

        // get the workspace size and allocate that memory
        cudnnErrchk(cudnnGetConvolutionForwardWorkspaceSize(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->cudnn_settings.filter_desc, this->cudnn_settings.conv_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.algo,
            &this->cudnn_settings.workspace_size));
        cudaErrchk(cudaMalloc((void **) &this->cudnn_settings.workspace, this->cudnn_settings.workspace_size));

        // get the best grad algorithms
        cudnnErrchk(cudnnGetConvolutionBackwardDataAlgorithm(
            this->cudnn_settings.handle, this->cudnn_settings.filter_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(),                    /* use input for dy */
            this->cudnn_settings.conv_desc, this->output_tensor->get_cudnn_tensor_descriptor(), /* use output for dx */
            CUDNN_CONVOLUTION_BWD_FILTER_PREFER_FASTEST, 0, &this->cudnn_settings.bwd_data_algo));

        // get the best filter algorithm
        cudnnErrchk(cudnnGetConvolutionBackwardFilterAlgorithm(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.conv_desc,
            this->cudnn_settings.filter_desc, CUDNN_CONVOLUTION_BWD_FILTER_PREFER_FASTEST, 0,
            &this->cudnn_settings.bwd_filter_algo));

        /* get the workspaces for each of the backward algorithms */
        cudnnErrchk(cudnnGetConvolutionBackwardDataWorkspaceSize(
            this->cudnn_settings.handle, this->cudnn_settings.filter_desc,
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.conv_desc,
            this->output_tensor->get_cudnn_tensor_descriptor(), this->cudnn_settings.bwd_data_algo,
            &this->cudnn_settings.grad_data_workspace_size));
        cudaErrchk(cudaMalloc((void **) &this->cudnn_settings.grad_data_workspace,
                              this->cudnn_settings.grad_data_workspace_size));

        cudnnErrchk(cudnnGetConvolutionBackwardFilterWorkspaceSize(
            this->cudnn_settings.handle, this->output_tensor->get_cudnn_tensor_descriptor(),
            this->input->get_output_tensor()->get_cudnn_tensor_descriptor(), this->cudnn_settings.conv_desc,
            this->cudnn_settings.filter_desc, this->cudnn_settings.bwd_filter_algo,
            &this->cudnn_settings.grad_filter_workspace_size));
#endif
        cudaErrchk(cudaMalloc((void **) &this->cudnn_settings.grad_filter_workspace,
                              this->cudnn_settings.grad_filter_workspace_size));
    }
#endif
}  // end init_settings

template <typename T>
void Conv2DTransposeOp<T>::calculate_and_set_output_shape() {
    // define the batch, channels, height and width of output tensor
    int on = 0, oc = 0, oh = 0, ow = 0;

    /* calculate the correct output shape here */
    if (this->mem_type == HOST) {
#if defined(MAGMADNN_HAVE_MKLDNN)

        // calculate the shape of the output tensor

        int in = 0, ih = 0, iw = 0;  // input tensor dimensions
        int kh = 0, kw = 0;                  // kernel dimensions
        in = this->input_tensor->get_shape(0);
        ih = this->input_tensor->get_shape(2);
        iw = this->input_tensor->get_shape(3);

        kh = this->filter->get_output_shape()[2];
        kw = this->filter->get_output_shape()[3];
        // account for the case of dilation
        int dkh = 1 + (kh - 1) * (dilation_h + 1);
        int dkw = 1 + (kw - 1) * (dilation_w + 1);

        // set the output shape of the transposed convolution
        on = in;                                   // stays the same
        oc = this->filter->get_output_shape()[0];  // equal to the number of filters
        oh = ((ih - 1) * vertical_stride + dkh - 2 * pad_h + out_pad_h);
        ow = ((iw - 1) * horizontal_stride + dkw - 2 * pad_w + out_pad_w);

        this->output_shape = {static_cast<unsigned int>(on), static_cast<unsigned int>(oc),
                              static_cast<unsigned int>(oh), static_cast<unsigned int>(ow)};

#else
        std::fprintf(stderr, "Error: Conv2dForward::output_shape requires GPU.\n");
        this->output_shape = this->input->get_output_shape();
#endif
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        // calculate the shape of the output tensor

        int in = 0, ih = 0, iw = 0;  // input tensor dimensions
        int kh = 0, kw = 0;                  // kernel dimensions
        in = this->input_tensor->get_shape(0);
        ih = this->input_tensor->get_shape(2);
        iw = this->input_tensor->get_shape(3);

        kh = this->filter->get_output_shape()[2];
        kw = this->filter->get_output_shape()[3];
        // account for the case of dilation
        int dkh = 1 + (kh - 1) * (dilation_h);
        int dkw = 1 + (kw - 1) * (dilation_w);

        // set the output shape of the transposed convolution
        on = in;                                   // stays the same
        oc = this->filter->get_output_shape()[0];  // equal to the number of filters
        oh = (ih - 1) * vertical_stride            // output height
             - (2 * pad_h) + dkh + out_pad_h;
        ow = (iw - 1) * horizontal_stride  // output width
             - (2 * pad_w) + dkw + out_pad_w;

        this->output_shape = {static_cast<unsigned int>(on), static_cast<unsigned int>(oc),
                              static_cast<unsigned int>(oh), static_cast<unsigned int>(ow)};
    }
#endif
    this->output_tensor = new Tensor<T>(this->output_shape, {NONE, {}}, this->mem_type);
#if defined(MAGMADNN_HAVE_CUDA)
    this->output_tensor->set_custream(this->get_custream());
    this->output_tensor->set_cublas_handle(this->get_cublas_handle());
#endif
}  // end calculate_and_set_output_shape
template class Conv2DTransposeOp<int>;
template class Conv2DTransposeOp<float>;
template class Conv2DTransposeOp<double>;

template <typename T>
Conv2DTransposeOp<T> *conv2dtransposeop(Operation<T> *input, Operation<T> *filter, int pad_h, int pad_w, int out_pad_h,
                                        int out_pad_w, int vertical_stride, int horizontal_stride, int dilation_h,
                                        int dilation_w, bool use_cross_correlation, bool needs_grad) {
    return new Conv2DTransposeOp<T>(input, filter, pad_h, pad_w, out_pad_h, out_pad_w, vertical_stride,
                                    horizontal_stride, dilation_h, dilation_w, use_cross_correlation, needs_grad);
}
template Conv2DTransposeOp<int> *conv2dtransposeop(Operation<int> *input, Operation<int> *filter, int pad_h, int pad_w,
                                                   int out_pad_h, int out_pad_w, int vertical_stride,
                                                   int horizontal_stride, int dilation_h, int dilation_w,
                                                   bool use_cross_correlation, bool needs_grad);
template Conv2DTransposeOp<float> *conv2dtransposeop(Operation<float> *input, Operation<float> *filter, int pad_h,
                                                     int pad_w, int out_pad_h, int out_pad_w, int vertical_stride,
                                                     int horizontal_stride, int dilation_h, int dilation_w,
                                                     bool use_cross_correlation, bool needs_grad);
template Conv2DTransposeOp<double> *conv2dtransposeop(Operation<double> *input, Operation<double> *filter, int pad_h,
                                                      int out_pad_h, int out_pad_w, int pad_w, int vertical_stride,
                                                      int horizontal_stride, int dilation_h, int dilation_w,
                                                      bool use_cross_correlation, bool needs_grad);
}
}