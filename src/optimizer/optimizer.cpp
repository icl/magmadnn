/**
 * @file optimizer.cpp
 * @author
 * @date 2021-06-10
 *
 * 
 */
#include "optimizer/optimizer.h"

namespace magmadnn {
namespace optimizer {

template <typename T>
void Optimizer<T>::print_grad_table() {
	this->table.print();
}

template class Optimizer<int>;
template class Optimizer<float>;
template class Optimizer<double>;

}
}
