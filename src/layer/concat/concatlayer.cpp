/**
 * @file concatlayer.cpp
 * @author Spencer Smith
 * @brief
 * @version 0.1
 * @date 2022-07-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "layer/concat/concatlayer.h"

namespace magmadnn {
namespace layer {

template <typename T>
ConcatLayer<T>::ConcatLayer(op::Operation<T> *a, op::Operation<T> *b, unsigned int axis, bool copy, bool needs_grad)
    : Layer<T>::Layer(a->get_output_shape(), a), b(b), axis(axis), copy(copy), needs_grad(needs_grad) {
    init();
}

template <typename T>
std::vector<op::Operation<T> *> ConcatLayer<T>::get_weights() {
    return {};
}

template <typename T>
void ConcatLayer<T>::init() {
    this->name = "Concat";

    this->output = op::concat4d(this->input, b, axis, copy, needs_grad);
}
template class ConcatLayer<int>;
template class ConcatLayer<float>;
template class ConcatLayer<double>;

template <typename T>
ConcatLayer<T> *concat(op::Operation<T> *a, op::Operation<T> *b, unsigned int axis, bool copy, bool needs_grad) {
    return new ConcatLayer<T>(a, b, axis, copy, needs_grad);
}
template ConcatLayer<int> *concat(op::Operation<int> *a, op::Operation<int> *b, unsigned int axis, bool copy,
                                  bool needs_grad);
template ConcatLayer<float> *concat(op::Operation<float> *a, op::Operation<float> *b, unsigned int axis, bool copy,
                                    bool needs_grad);
template ConcatLayer<double> *concat(op::Operation<double> *a, op::Operation<double> *b, unsigned int axis, bool copy,
                                     bool needs_grad);

}  // namespace layer
}  // namespace magmadnn