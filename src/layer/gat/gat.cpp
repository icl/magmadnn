#include "layer/gat/gat.h"

namespace magmadnn {
    namespace layer {
        template <typename T>
        GATLayer<T>::GATLayer(op::Operation<T> *input, int feat_in, int feat_out, float dropout_rate) 
        : Layer<T>::Layer(input->get_output_shape(), input), feat_in(feat_in), feat_out(feat_out), dropout_rate(dropout_rate){
            init();
        }

        template<typename T>
        GATLayer<T>::~GATLayer() {
            // TODO: 
        }

        template <typename T>
        void GATLayer<T>::init() {
            this->name = "GAT";

            this->w_t = xavier_uniform(this->feat_in, this->feat_out);
            this->w = op::var("w", this->w_t);

            this->a1_t = xavier_uniform(this->feat_out, 1);
            this->a2_t = xavier_uniform(this->feat_out, 1);

            this->wh_t = op::matmul(this->input, this->w); // wh = w * h [in, out] * [N, in] = [N, out]
            this->wh = op::var("wh", this->wh_t);

            this->wh1 = op::matmul(this->wh_t, this->a1_t); // [N, out] * [out, 1] = [N, 1]
            this->wh2 = op::matmul(this->wh_t, this->a2_t);
            
            this->e_t = concatenate(this->wh1_t, this->wh2_t);
            this->e = op::relu(op::var("e", this->e_t));
            this->attn = op::dropout(op::softmax(this->e), this->dropout_rate);
            return op::matmul(this->attn, this->wh);
        }

        template <typename T>
        Tensor<T>* GATLayer<T>::xavier_uniform(const int i, const int j) {
            T alpha = sqrt(6.0 / (double)(i + j)); 
            return new Tensor<T>({i, j}, {UNIFORM, {-alpha, alpha}}, DEVICE);
        }

        template <typename T>
        Tensor<T>* GATLayer<T>::concatenate(Tensor<T>* tensor1, Tensor<T> *tensor2) {
            // TODO: debug
            size_t N = tensor1->get_shape(0);
            Tensor<T> *ret = new Tensor<T>({(unsigned int)N, 2}, {CONSTANT, {(T)0.0}}, DEVICE);
            cudaErrchk(cudaMemcpy(ret->get_ptr(), tensor1->get_ptr(), N));
            cudaErrchk(cudaMemcpy(ret->get_ptr() + N, tensor2->get_ptr(), N));
            return ret;
        }
    }
}