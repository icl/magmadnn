#include "layer/lstm2/lstm2layer.h"
namespace magmadnn {
namespace layer {

template <typename T>
LSTM2Layer<T>::LSTM2Layer(op::Operation<T> * input, int hidden_size, bool return_sequences)
	: Layer<T>::Layer(input->get_output_shape(), input),
	  hidden_size(hidden_size),
	  return_sequences(return_sequences) {
		std::cout << "hidden size: " << this->hidden_size << std::endl;
		init();
}

template <typename T>
LSTM2Layer<T>::~LSTM2Layer<T>() {
	//TODO
}

template <typename T>
std::vector<op::Operation<T>*> LSTM2Layer<T>::get_weights() {
	return { this->h_init,this->c_init,this->weights};
}

template <typename T> 
unsigned int LSTM2Layer<T>::get_num_params() {
	return this->h_init->get_output_size() + this->c_init->get_output_size() + this->weights->get_output_size();
}

template <typename T>
void LSTM2Layer<T>::init(){

	this->name = "LSTM2";
	//num_nodes always equals 1 for this implementation
	this->num_nodes = 1;
	
	unsigned int dim0 = this->input->get_output_shape(0); //batchsize
	// unsigned int dim1 = this->input->get_output_shape(1); //time steps
	unsigned int dim2 = this->input->get_output_shape(2); //features

	this->h_init_t = new Tensor<T>({/*this->num_nodes,*/ dim0, this->hidden_size/*dim2*/}, {CONSTANT, {(T)0}}, this->input->get_memory_type());	
	this->c_init_t = new Tensor<T>({/*this->num_nodes,*/ dim0, this->hidden_size/*dim2*/}, {CONSTANT, {(T)0}}, this->input->get_memory_type());	
	
	this->h_init = op::var("h_init", h_init_t);
	this->c_init = op::var("c_init", c_init_t);



	//TODO make it to where bias is optional
	
	std::cout << "hidden size: " << this->hidden_size << std::endl;
	std::cout << "weights size: " << 4*this->hidden_size*((this->num_nodes-1)*3 + (dim2 + this->hidden_size + 1) ) << std::endl;
	this->weights_t = new Tensor<T> ({4*this->hidden_size*((this->num_nodes-1)*3 + (dim2 + this->hidden_size + 1) )}, {UNIFORM, {(T)-0.005, (T)0.005}}, this->input->get_memory_type());

	this->weights = op::var("weights", weights_t);

	//NOTE: return_sequences = false does not work as intended
	this->output = op::lstm2(this->input, this->h_init, this->c_init, this->weights, this->return_sequences);

}

template class LSTM2Layer<float>;

template <typename T>
LSTM2Layer<T> *lstm2 (op::Operation<T> *input, int hidden_size, bool return_sequences){
	return new LSTM2Layer<T>(input, hidden_size, return_sequences);

}
template LSTM2Layer<float> * lstm2(op::Operation<float> * input, int hidden_size, bool return_sequences);


} //namespace layer
} //namespace magmadnn
