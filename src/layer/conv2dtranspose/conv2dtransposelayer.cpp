/**
 * @file conv2dtransposelayer.cpp
 * @author Spencer Smith
 * @brief CPP file defining the functions for the transpose convolution layer
 * @version 0.1
 * @date 2022-06-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "layer/conv2dtranspose/conv2dtransposelayer.h"
#include <iostream>

namespace magmadnn {
namespace layer {
    
template <typename T>
Conv2DTransposeLayer<T>::Conv2DTransposeLayer(op::Operation<T>* input, const std::vector<unsigned int>& filter_shape,
                                              int out_channels, const std::vector<unsigned int>& padding,
                                              const std::vector<unsigned int>& output_padding,
                                              const std::vector<unsigned int>& strides,
                                              const std::vector<unsigned int>& dilation_rates,
                                              bool use_cross_correlation, bool use_bias,
                                              tensor_filler_t<T> filter_initializer,
                                              tensor_filler_t<T> bias_initializer)
    : Layer<T>::Layer(input->get_output_shape(), input),
      out_channels(out_channels),
      use_cross_correlation(use_cross_correlation),
      use_bias(use_bias),
      filter_initializer(filter_initializer),
      bias_initializer(bias_initializer) {
    if (padding.size() == 2) {
        pad_h = padding[0];
        pad_w = padding[1];
    } else {
        fprintf(stderr, "Error: Expected padding to have 2 elements.\n");
    }
    if (output_padding.size() == 2) {
        out_pad_h = output_padding[0];
        out_pad_w = output_padding[1];
    } else {
        fprintf(stderr, "Error: Expected output padding to have 2 elements.\n");
    }
    if (strides.size() == 2) {
        stride_h = strides[0];
        stride_w = strides[1];
    } else {
        fprintf(stderr, "Error: Expected strides to have 2 elements\n");
    }
    if (dilation_rates.size() == 2) {
        dilation_h = dilation_rates[0];
        dilation_w = dilation_rates[1];
    } else {
        fprintf(stderr, "Error: Expected dilation_rates to have 2 elements\n");
    }

    init(filter_shape);
}

template <typename T>
Conv2DTransposeLayer<T>::~Conv2DTransposeLayer() {
    delete filter_tensor;
    if (use_bias) delete bias_tensor;
}

template <typename T>
std::vector<op::Operation<T>*> Conv2DTransposeLayer<T>::get_weights() {
    if (use_bias) {
        return {filter, bias};
    } else {
        return {filter};
    }
}

template <typename T>
unsigned int Conv2DTransposeLayer<T>::get_num_params() {
    return this->filter_tensor->get_shape(2) * this->filter_tensor->get_shape(3) * this->in_channels *
           this->out_channels;
}

template <typename T>
void Conv2DTransposeLayer<T>::init(const std::vector<unsigned int>& filter_shape) {
    this->name = "Conv2dTranspose";

    // Confirm that the right number of parameters have been passed in
    assert(this->input_shape.size() == 4);
    assert(filter_shape.size() == 2);

    this->in_channels = this->input_shape[1];

    // push back to the filter initializer
    filter_initializer.values.push_back(static_cast<T>(this->in_channels));
    filter_initializer.values.push_back(static_cast<T>(this->out_channels));
    // Assuming a square filter is used
    filter_initializer.values.push_back(static_cast<T>(filter_shape[0]));

    /* create filter tensor */
    this->filter_tensor =
        new Tensor<T>({static_cast<unsigned int>(this->out_channels), static_cast<unsigned int>(this->in_channels),
                       filter_shape[0], filter_shape[1]},
                      this->filter_initializer, this->input->get_memory_type());
    // create the filter variable, with its name and tensor
    // this is what gets added to the compute graph
    this->filter = op::var<T>("__" + this->name + "_layer_filter", this->filter_tensor);

    /* create the bias tensor if need be */
    if (use_bias) {
        /* TODO */
    }

    if (use_bias) {
        /* TODO */
    } else {
        // the output is the result of applying convolutional transpose
        // the conv2dtransposeop is in compute
        this->output = op::conv2dtransposeop(this->input, filter, pad_h, pad_w, out_pad_h, out_pad_w, stride_h,
                                             stride_w, dilation_h, dilation_w, use_cross_correlation);
    }
}

template class Conv2DTransposeLayer<int>;
template class Conv2DTransposeLayer<float>;
template class Conv2DTransposeLayer<double>;

template <typename T>
Conv2DTransposeLayer<T>* conv2dtranspose(op::Operation<T>* input, const std::vector<unsigned int>& filter_shape,
                                         int out_channels, const std::vector<unsigned int>& padding,
                                         const std::vector<unsigned int>& output_padding,
                                         const std::vector<unsigned int>& strides,
                                         const std::vector<unsigned int>& dilation_rates, bool use_cross_correlation,
                                         bool use_bias, tensor_filler_t<T> filter_initializer,
                                         tensor_filler_t<T> bias_initializer) {
    return new Conv2DTransposeLayer<T>(input, filter_shape, out_channels, padding, output_padding, strides,
                                       dilation_rates, use_cross_correlation, use_bias, filter_initializer,
                                       bias_initializer);
}
template Conv2DTransposeLayer<int>* conv2dtranspose(op::Operation<int>*, const std::vector<unsigned int>&, int,
                                                    const std::vector<unsigned int>&, const std::vector<unsigned int>&,
                                                    const std::vector<unsigned int>&, const std::vector<unsigned int>&,
                                                    bool, bool, tensor_filler_t<int>, tensor_filler_t<int>);
template Conv2DTransposeLayer<float>* conv2dtranspose(op::Operation<float>*, const std::vector<unsigned int>&, int,
                                                      const std::vector<unsigned int>&,
                                                      const std::vector<unsigned int>&,
                                                      const std::vector<unsigned int>&,
                                                      const std::vector<unsigned int>&, bool, bool,
                                                      tensor_filler_t<float>, tensor_filler_t<float>);
template Conv2DTransposeLayer<double>* conv2dtranspose(op::Operation<double>*, const std::vector<unsigned int>&, int,
                                                       const std::vector<unsigned int>&,
                                                       const std::vector<unsigned int>&,
                                                       const std::vector<unsigned int>&,
                                                       const std::vector<unsigned int>&, bool, bool,
                                                       tensor_filler_t<double>, tensor_filler_t<double>);

}  // namespace layer
}  // namespace magmadnn
