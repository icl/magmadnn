#include "layer/mha/mhalayer.h"
#include "magmadnn.h"

namespace magmadnn {
    namespace layer {
        template <typename T>
        MHALayer<T>::MHALayer(op::Operation<T> *input, 
        const std::vector<int> &orig_sz, 
        const std::vector<int> &proj_sz, 
        const std::vector<int> &seq_len, 
        const int head_n, const int batch_sz, bool need_grad) 
        : Layer<T>::Layer(input->get_output_shape(), input),
        numHead(head_n), batchSize(batch_sz), 
        projQ(proj_sz[0]), projK(proj_sz[1]), projV(proj_sz[2]), projO(proj_sz[3]),
        qSize(orig_sz[0]), kSize(orig_sz[1]), vSize(orig_sz[2]), 
        seqLenQ(seq_len[0]), seqLenK(seq_len[1]), 
        need_grad(need_grad){
            init();
        }

        template<typename T>
        MHALayer<T>::~MHALayer() {
            // TODO: 
        }

        template <typename T>
        std::vector<op::Operation<T>*> MHALayer<T>::get_weights() {
            return { this->w };
        }

        template <typename T> 
        unsigned int MHALayer<T>::get_num_params() {
            return this->w->get_output_size();
        }

        template <typename T>
        inline double MHALayer<T>::randRangeDbl(double bias, double range) {
            return range * drand48() + bias;
        }

        template <typename T>
        void MHALayer<T>::initTensor(Tensor<T> *tensor, size_t weightSz, double mean, double var) {
            srand48(2333);
            double range = sqrt(12.0 * var);
            double bias  = mean - 0.5 * range;
            for (size_t index = 0; index < weightSz; index++) {
                // image[index] = (float) randRangeDbl(bias, range);
                tensor->set(index, (float) randRangeDbl(bias, range));
            }
        }

        template <typename T>
        Tensor<T>* MHALayer<T>::xavier_uniform(unsigned int buffersize, const int embed_dim, const int num_heads) {
            T alpha = sqrt(6.0 / (4.0 * embed_dim * num_heads)); 
            return new Tensor<T>({buffersize}, {UNIFORM, {-alpha, alpha}}, this->input->get_memory_type());
        }

        template <typename T>
        void MHALayer<T>::init() {
            this->name = "MHA";
            // TODO: set options
            ::magmadnn::math::attnOptions options = ::magmadnn::math::attnOptions(
                1,                      // attnTrain (train mode by default)
                CUDNN_DATA_FLOAT,       // attnDataType
                CUDNN_DATA_FLOAT,       // attnCompType
                0,                      // attnQueryMap
                this->numHead,          // attnNumHeads
                1,                      // attnBeamSize
                1.0,                    // attnSmScaler
                0.0,                    // attnDropoutRate
                this->qSize,            // attnQsize
                this->kSize,            // attnKsize
                this->vSize,            // attnVsize
                this->projQ,            // attnProjQSize
                this->projK,            // attnProjKSize
                this->projV,            // attnProjVSize
                this->projO,            // attnProjOSize
                this->seqLenQ,          // attnSeqLenQ,
                this->seqLenK,          // attnSeqLenK,  
                this->batchSize,        // attnBatchSize,
                0,                      // attnDataLayout
                0,                      // attnResLink
                1,                      // attnProjBias
                0,                      // attnSweep
                0,                      // attnRandGeom
                1234,                   // attnRandSeed
                0                       // attnFileDump
            );

            



            // int weightSz = numHead * (projQ * qSize + projK * kSize + projV * vSize + projV * projO);
            int weightSz = options.attnNumHeads * (options.attnProjQsize * options.attnQsize + options.attnProjKsize * options.attnKsize + options.attnProjVsize * options.attnVsize + options.attnProjVsize * options.attnProjOsize);
            // this->q_t = new Tensor(desc.get_q_shape(), {CONSTANT, {(T)0}}, this->input->get_memory_type());
            // this->k_t = new Tensor(desc.get_k_shape(), {CONSTANT, {(T)0}}, this->input->get_memory_type());
            // this->v_t = new Tensor(desc.get_v_shape(), {CONSTANT, {(T)0}}, this->input->get_memory_type());

            // this->w_t = new Tensor<T>({(unsigned int) weightSz}, {CONSTANT, {(T)0}}, this->input->get_memory_type());
            // initTensor(this->w_t, weightSz, 0, 1e-2);
            
            this->w_t = xavier_uniform((unsigned int)weightSz, this->qSize, this->numHead);
            // io::print_tensor(this->w_t);

            // this->q = op::var("q", q_t);
            // this->k = op::var("k", k_t);
            // this->v = op::var("v", v_t);
            this->w = op::var("w", this->w_t);

            this->output = op::mha1(this->input, this->w, options, this->need_grad);
        }

        template class MHALayer<float>;

        template <typename T>
        MHALayer<T> *mha(op::Operation<T> *input,        
            const std::vector<int> &orig_sz, 
            const std::vector<int> &proj_sz, 
            const std::vector<int> &seq_len, 
            const int head_n, const int batch_sz, bool need_grad) {
                return new MHALayer<T>(input, orig_sz, proj_sz, seq_len, head_n, batch_sz, need_grad);
            }

        template MHALayer<float> *mha(op::Operation<float> *input,        
            const std::vector<int> &orig_sz, 
            const std::vector<int> &proj_sz, 
            const std::vector<int> &seq_len, 
            const int head_n, const int batch_sz, bool need_grad);
    }
}

// namespace magmadnn {
// namespace layer {

// // template <typename T>
// // LSTM2Layer<T>::LSTM2Layer(op::Operation<T> * input, int hidden_size, bool return_sequences)
// // 	: Layer<T>::Layer(input->get_output_shape(), input),
// // 	  hidden_size(hidden_size),
// // 	  return_sequences(return_sequences) {
// // 		std::cout << "hidden size: " << this->hidden_size << std::endl;
// // 		init();
// // }
// template <typename T>
// MHALayer<T>::MHALayer(op::Operation<T> *input, bool need_grad) 
//     : Layer<T>::Layer(input->get_output_shape(), input), need_grad(need_grad) {}

// template <typename T>
// MHALayer<T>::~MHALayer<T>() {
// 	//TODO
// }

// template <typename T>
// std::vector<op::Operation<T>*> MHALayer<T>::get_weights() {
// 	return { this->h_init,this->c_init,this->weights};
// }

// template <typename T> 
// unsigned int MHALayer<T>::get_num_params() {
// 	return this->h_init->get_output_size() + this->c_init->get_output_size() + this->weights->get_output_size();
// }

// template <typename T>
// void MHALayer<T>::init(){

// 	this->name = "MHA";
// 	//num_nodes always equals 1 for this implementation
// 	// this->num_nodes = 1;
	
// 	// unsigned int dim0 = this->input->get_output_shape(0); //batchsize
// 	// unsigned int dim1 = this->input->get_output_shape(1); //time steps
// 	// unsigned int dim2 = this->input->get_output_shape(2); //features
//     unsigned int beamSz = 1;
//     unsigned int batchSz = 1;
//     unsigned int qoSize = 3;
//     unsigned int kvSize = 3;
//     unsigned int projQ = 4;
//     unsigned int projK = 4;
//     unsigned int projV = 4;
//     unsigned int projO = 4;
//     unsigned int seqLenQO = 2;
//     unsigned int seqLenKV = 2;  
//     unsigned int weightSz = 104;
//     unsigned int numHead = 2;

// 	// this->h_init_t = new Tensor<T>({/*this->num_nodes,*/ dim0, this->hidden_size/*dim2*/}, {CONSTANT, {(T)0}}, this->input->get_memory_type());	
// 	// this->c_init_t = new Tensor<T>({/*this->num_nodes,*/ dim0, this->hidden_size/*dim2*/}, {CONSTANT, {(T)0}}, this->input->get_memory_type());	
	
// 	// this->h_init = op::var("h_init", h_init_t);
// 	// this->c_init = op::var("c_init", c_init_t);



// 	// //TODO make it to where bias is optional
	
// 	// std::cout << "hidden size: " << this->hidden_size << std::endl;
// 	// std::cout << "weights size: " << 4*this->hidden_size*((this->num_nodes-1)*3 + (dim2 + this->hidden_size + 1) ) << std::endl;
// 	// this->weights_t = new Tensor<T> ({4*this->hidden_size*((this->num_nodes-1)*3 + (dim2 + this->hidden_size + 1) )}, {UNIFORM, {(T)-0.005, (T)0.005}}, this->input->get_memory_type());

// 	// this->weights = op::var("weights", weights_t);
//     this->q_t = new Tensor<T>({beamSz, batchSz, qoSize, seqLenQO}, {CONSTANT, {(T)0}}, this->input->get_memory_type());
//     this->k_t = new Tensor<T>({beamSz, batchSz, kvSize, seqLenKV}, {CONSTANT, {(T)0}}, this->input->get_memory_type()); 
//     this->v_t = new Tensor<T>({beamSz, batchSz, kvSize, seqLenKV}, {CONSTANT, {(T)0}}, this->input->get_memory_type());
//     this->w_t = new Tensor<T>({weightSz}, {CONSTANT, {(T)0}}, this->input->get_memory_type());

//     op::Variable<T> *q = op::var<T>("Q", q_t);
//     op::Variable<T> *k = op::var<T>("K", k_t); 
//     op::Variable<T> *v = op::var<T>("V", v_t);
//     op::Variable<T> *w = op::var<T>("W", w_t);

// 	this->output = op::mha1(this->q, this->k, this->v, this->w, this->need_grad);
// }

// template class MHALayer<float>;

// // template <typename T>
// // MHALayer<T> *mha (op::Operation<T> *input, int hidden_size, bool return_sequences){
// // 	return new LSTM2Layer<T>(input, hidden_size, return_sequences);

// // }
// template MHALayer<float> * mha(op::Operation<float> * input, int hidden_size, bool return_sequences);


// } //namespace layer
// } //namespace magmadnn