/**
 * @file shortcutlayer.cpp
 * @author Sedrick Keh
 * @version 1.0
 * @date 2019-07-08
 *
 * @copyright Copyright (c) 2019
 */
#include "layer/shortcut/shortcutlayer.h"

namespace magmadnn {
namespace layer {

template <typename T>
ShortcutLayer<T>::ShortcutLayer(op::Operation<T> *input, op::Operation<T> *prev_add) : Layer<T>::Layer(input->get_output_shape(), input), prev_add(prev_add) {
    init();
}

template <typename T>
std::vector<op::Operation<T> *> ShortcutLayer<T>::get_weights() {
    return {};
}

template <typename T>
void ShortcutLayer<T>::init() {
    this->name = "ShortcutLayer";

    this->output = op::add(this->input, prev_add);
}
template class ShortcutLayer<int>;
template class ShortcutLayer<float>;
template class ShortcutLayer<double>;

template <typename T>
ShortcutLayer<T> *shortcut(op::Operation<T> *input, op::Operation<T> *prev_add) {
    return new ShortcutLayer<T>(input, prev_add);
}
template ShortcutLayer<int> *shortcut(op::Operation<int> *input, op::Operation<int> *prev_add);
template ShortcutLayer<float> *shortcut(op::Operation<float> *input, op::Operation<float> *prev_add);
template ShortcutLayer<double> *shortcut(op::Operation<double> *input, op::Operation<double> *prev_add);

}  // namespace layer
}  // namespace magmadnn