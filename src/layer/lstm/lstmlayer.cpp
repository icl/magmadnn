/**
 * @file lstmlayer.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */

#include "layer/lstm/lstm.h"
#include "tensor/tensor_io.h"
namespace magmadnn {
namespace layer {

template <typename T>
LSTMLayer<T>::LSTMLayer(op::Operation<T> *input, unsigned int num_nodes, bool return_sequences,
                        op::Operation<T> *h_init, op::Operation<T> *c_init, std::vector<op::Operation<T> *> weights)
    : Layer<T>::Layer(input->get_output_shape(), input),
      num_nodes(num_nodes),
      return_sequences(return_sequences),
      h_init(h_init),
      c_init(c_init),
      weights(weights) {
    init();
}

template <typename T>
LSTMLayer<T>::~LSTMLayer() {
    // TODO this might need some
}

template <typename T>
std::vector<op::Operation<T> *> LSTMLayer<T>::get_weights() {
    return {Wf, Uf, Bf, Wi, Ui, Bi, Wo, Uo, Bo, Wc, Uc, Bc};
}

template <typename T>
void LSTMLayer<T>::init() {
    this->name = "LSTMLayer";
    memory_t mem_type = this->input->get_memory_type();
    unsigned int dim0 = this->input->get_output_shape()[0];
    // unsigned int dim1 = this->input->get_output_shape()[1];
    unsigned int dim2 = this->input->get_output_shape()[2];

    if (this->h_init == nullptr) {
        this->h_init = op::var<T>("h_init", {dim0, this->num_nodes}, {CONSTANT, {(T) 0}}, mem_type);
    }
    if (this->c_init == nullptr) {
        this->c_init = op::var<T>("h_init", {dim0, this->num_nodes}, {CONSTANT, {(T) 0}}, mem_type);
    }
    if (this->weights.size() == 0) {
        // initialize weights and biases
        this->Wf = op::var<T>("Wf", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Uf = op::var<T>("Uf", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bf = op::var<T>("Bf", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);

        this->Wi = op::var<T>("Wi", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Ui = op::var<T>("Ui", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bi = op::var<T>("Bi", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);

        this->Wo = op::var<T>("Wo", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Uo = op::var<T>("Uo", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bo = op::var<T>("Bo", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);

        this->Wc = op::var<T>("Wc", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Uc = op::var<T>("Uc", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bc = op::var<T>("Bc", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);

        this->set_weights({this->Wf, this->Uf, this->Bf, this->Wi, this->Ui, this->Bi, this->Wo, this->Uo, this->Bo,
                           this->Wc, this->Uc, this->Bc});
    } else {
        this->set_weights(weights);
    }

    this->output = op::lstm(this->input, this->weights, this->h_init, this->c_init, this->return_sequences);

    this->output->eval();
}

template <typename T>
void LSTMLayer<T>::set_weights(std::vector<op::Operation<T> *> weights) {
    if (weights.size() != 12) {
        throw std::invalid_argument("The passed vector must have size 12");
    }

    unsigned int dim0 = this->input->get_output_shape()[0];
    unsigned int dim2 = this->input->get_output_shape()[2];

    // error checking
    for (unsigned i = 0; i < weights.size(); ++i) {
        std::vector<unsigned> shape = weights[i]->get_output_shape();
        if ((i % 3 == 0) && (shape[0] != dim2 || shape[1] != this->num_nodes)) {
            throw std::invalid_argument("Invalid shape for weight");
        }

        if ((i % 3 == 1) && (shape[0] != this->num_nodes || shape[1] != this->num_nodes)) {
            throw std::invalid_argument("Invalid shape for weight");
        }

        if ((i % 3 == 2) && (shape[0] != dim0 || shape[1] != this->num_nodes)) {
            throw std::invalid_argument("Invalid shape for weight");
        }
    }

    this->Wf = weights[0];
    this->Uf = weights[1];
    this->Bf = weights[2];
    this->Wi = weights[3];
    this->Ui = weights[4];
    this->Bi = weights[5];
    this->Wo = weights[6];
    this->Uo = weights[7];
    this->Bo = weights[8];
    this->Wc = weights[9];
    this->Uc = weights[10];
    this->Bc = weights[11];
    if (this->weights.size() == 0) {
        this->weights.resize(12);
        this->weights[0] = this->Wf;
        this->weights[1] = this->Uf;
        this->weights[2] = this->Bf;
        this->weights[3] = this->Wi;
        this->weights[4] = this->Ui;
        this->weights[5] = this->Bi;
        this->weights[6] = this->Wo;
        this->weights[7] = this->Uo;
        this->weights[8] = this->Bo;
        this->weights[9] = this->Wc;
        this->weights[10] = this->Uc;
        this->weights[11] = this->Bc;
    }
}

template <typename T>
unsigned int LSTMLayer<T>::get_num_params() {
    int tot = 0;
    for (unsigned i = 0; i < this->weights.size(); i++) {
        tot += this->weights[i]->get_output_tensor()->get_size();
    }
    return tot;
}

template class LSTMLayer<float>;
//template class LSTMLayer<double>;

// creates a new simple lstm layer
template <typename T>
LSTMLayer<T> *lstm(op::Operation<T> *input, unsigned int num_nodes, bool return_sequences, op::Operation<T> *h_init,
                   op::Operation<T> *c_init, std::vector<op::Operation<T> *> weights) {
    return new LSTMLayer<T>(input, num_nodes, return_sequences, h_init, c_init, weights);
}

template LSTMLayer<float> *lstm(op::Operation<float> *input, unsigned int num_nodes, bool return_sequences,
                                op::Operation<float> *h_init, op::Operation<float> *c_init,
                                std::vector<op::Operation<float> *> weights);
/*template LSTMLayer<double> *lstm(op::Operation<double> *input, unsigned int num_nodes, bool return_sequences,
                                 op::Operation<double> *h_init, op::Operation<double> *c_init,
                                 std::vector<op::Operation<double> *> weights);
*/
}  // namespace layer
}  // namespace magmadnn
