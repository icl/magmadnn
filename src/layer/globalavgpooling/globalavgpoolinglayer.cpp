/**
 * @file globalavgpooling.cpp
 * @author Jerry Ho
 * @version 1.0
 * @date 2021-11-19
 *
 * @copyright Copyright (c) 2019
 */
#include "layer/globalavgpooling/globalavgpooling.h"
#include <iostream>

namespace magmadnn {
namespace layer {

template <typename T>
GlobalAvgpoolingLayer<T>::GlobalAvgpoolingLayer(op::Operation<T> *input) : Layer<T>::Layer(input->get_output_shape(), input){
    init();
}

template <typename T>
std::vector<op::Operation<T> *> GlobalAvgpoolingLayer<T>::get_weights() {
    return {};
}

template <typename T>
void GlobalAvgpoolingLayer<T>::init() {
    this->name = "GlobalAvgpoolingLayer";
    auto inshape_ = this->input->eval()->get_shape();
    //Assume input is 4D, format: BCHW
    auto size = inshape_[2]*inshape_[3];
    // std::cout << "inshape: " << inshape_[0] << inshape_[1]<< inshape_[2] << inshape_[3]<< std::endl;
    T norm = static_cast<T>(1.0) / static_cast<T>(size);
    this->output = op::scalarproduct(norm,op::reducesum(op::reducesum(this->input,3),2));
    auto outshape_ = this->output->eval()->get_shape();
    // std::cout << "outshape: " << outshape_[0] << outshape_[1]<< outshape_[2] << outshape_[3]<< std::endl;

}

template class GlobalAvgpoolingLayer<int>;
template class GlobalAvgpoolingLayer<float>;
template class GlobalAvgpoolingLayer<double>;

template <typename T>
GlobalAvgpoolingLayer<T> *globalavgpooling(op::Operation<T> *input){
    return new GlobalAvgpoolingLayer<T>(input);
}

template GlobalAvgpoolingLayer<int> *globalavgpooling(op::Operation<int> *input);
template GlobalAvgpoolingLayer<float> *globalavgpooling(op::Operation<float> *input);
template GlobalAvgpoolingLayer<double> *globalavgpooling(op::Operation<double> *input);

}  // namespace layer
}  // namespace magmadnn