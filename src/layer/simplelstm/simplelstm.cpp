/**
 * @file simplelstm.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */

#include "layer/simplelstm/simplelstm.h"
#include "tensor/tensor_io.h"
namespace magmadnn {
namespace layer {

template <typename T>
SimpleLSTMLayer<T>::SimpleLSTMLayer(op::Operation<T> *input, unsigned int num_nodes, op::Operation<T> *h_cur,
                                    op::Operation<T> *c_cur, bool return_sequences,
                                    std::vector<op::Variable<T> *> weights)
    : Layer<T>::Layer(input->get_output_shape(), input),
      num_nodes(num_nodes),
      h_cur(h_cur),
      c_cur(c_cur),
      return_sequences(return_sequences),
      weights(weights) {
    init();
}

template <typename T>
SimpleLSTMLayer<T>::~SimpleLSTMLayer() {
    // TODO this might need some
}

template <typename T>
std::vector<op::Operation<T> *> SimpleLSTMLayer<T>::get_weights() {
    return {Wf, Uf, Bf, Wi, Ui, Bi, Wo, Uo, Bo, Wc, Uc, Bc};
}

template <typename T>
void SimpleLSTMLayer<T>::init() {
    this->name = "SimpleLSTMLayer";
    memory_t mem_type = this->input->get_memory_type();
    unsigned int dim0 = this->input->get_output_shape()[0];
    unsigned int dim1 = this->input->get_output_shape()[1];
    unsigned int dim2 = this->input->get_output_shape()[2];

    if (this->weights.size() == 0) {
        // initialize weights and biases
        this->Wf = op::var<T>("Wf", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Uf = op::var<T>("Uf", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bf = op::var<T>("Bf", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);

        this->Wi = op::var<T>("Wi", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Ui = op::var<T>("Ui", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bi = op::var<T>("Bi", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);

        this->Wo = op::var<T>("Wo", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Uo = op::var<T>("Uo", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bo = op::var<T>("Bo", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);

        this->Wc = op::var<T>("Wc", {dim2, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Uc = op::var<T>("Uc", {this->num_nodes, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
        this->Bc = op::var<T>("Bc", {dim0, this->num_nodes}, {UNIFORM, {(T) 0, (T) 1}}, mem_type);
    } else {
        this->set_weights(weights);
    }
    std::vector<op::Operation<T> *> outputs;

    op::Operation<T> *cur_op;
    // loops through each time interval
    for (unsigned int i = 0; i < dim1; i++) {
        // LSTM equations
        cur_op = op::slice(this->input, 1, i);

        auto f_t = op::sigmoid(op::add(op::add(op::matmul(cur_op, Wf), op::matmul(this->h_cur, Uf)), Bf));
        auto i_t = op::sigmoid(op::add(op::add(op::matmul(cur_op, Wi), op::matmul(this->h_cur, Ui)), Bi));

        auto o_t = op::sigmoid(op::add(op::add(op::matmul(cur_op, Wo), op::matmul(this->h_cur, Uo)), Bo));

        auto ct_t = op::tanh(op::add(op::add(op::matmul(cur_op, Wc), op::matmul(this->h_cur, Uc)), Bc));

        this->c_cur = op::add(op::product(f_t, this->c_cur), op::product(i_t, ct_t));

        this->h_cur = op::product(o_t, op::tanh(this->c_cur));

        if (return_sequences) {
            outputs.push_back(this->h_cur);
        }

        this->output = h_cur;
    }

    if (return_sequences) {
        this->output = op::concat3d(outputs, 1);
    }

    this->output->eval();
}

template <typename T>
void SimpleLSTMLayer<T>::set_weights(std::vector<op::Variable<T> *> weights) {
    if (weights.size() != 12) {
        throw std::invalid_argument("The passed vector must have size 12");
    }

    unsigned int dim0 = this->input->get_output_shape()[0];
    unsigned int dim2 = this->input->get_output_shape()[2];

    // error checking
    for (unsigned i = 0; i < weights.size(); ++i) {
        std::vector<unsigned> shape = weights[i]->get_output_shape();
        if ((i % 3 == 0) && (shape[0] != dim2 || shape[1] != this->num_nodes)) {
            throw std::invalid_argument("Invalid shape for weight");
        }

        if ((i % 3 == 1) && (shape[0] != this->num_nodes || shape[1] != this->num_nodes)) {
            throw std::invalid_argument("Invalid shape for weight");
        }

        if ((i % 3 == 2) && (shape[0] != dim0 || shape[1] != this->num_nodes)) {
            throw std::invalid_argument("Invalid shape for weight");
        }
    }

    this->Wf = weights[0];
    this->Uf = weights[1];
    this->Bf = weights[2];
    this->Wi = weights[3];
    this->Ui = weights[4];
    this->Bi = weights[5];
    this->Wo = weights[6];
    this->Uo = weights[7];
    this->Bo = weights[8];
    this->Wc = weights[9];
    this->Uc = weights[10];
    this->Bc = weights[11];
}

template <typename T>
unsigned int SimpleLSTMLayer<T>::get_num_params() {
    return 6942069;
}

template class SimpleLSTMLayer<int>;
template class SimpleLSTMLayer<float>;
template class SimpleLSTMLayer<double>;

// creates a new simple lstm layer
template <typename T>
SimpleLSTMLayer<T> *simplelstm(op::Operation<T> *input, unsigned int num_nodes, op::Operation<T> *h_cur,
                               op::Operation<T> *c_cur, bool return_sequences, std::vector<op::Variable<T> *> weights) {
    return new SimpleLSTMLayer<T>(input, num_nodes, h_cur, c_cur, return_sequences, weights);
}

template SimpleLSTMLayer<int> *simplelstm(op::Operation<int> *input, unsigned int num_nodes, op::Operation<int> *h_cur,
                                          op::Operation<int> *c_cur, bool return_sequences,
                                          std::vector<op::Variable<int> *> weights);
template SimpleLSTMLayer<float> *simplelstm(op::Operation<float> *input, unsigned int num_nodes,
                                            op::Operation<float> *h_cur, op::Operation<float> *c_cur,
                                            bool return_sequences, std::vector<op::Variable<float> *> weights);
template SimpleLSTMLayer<double> *simplelstm(op::Operation<double> *input, unsigned int num_nodes,
                                             op::Operation<double> *h_cur, op::Operation<double> *c_cur,
                                             bool return_sequences, std::vector<op::Variable<double> *> weights);

}  // namespace layer
}  // namespace magmadnn
