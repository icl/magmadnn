/**
 * @file tensor_io.cpp
 * @author Daniel Nichols
 * @version 1.0
 * @date 2019-04-05
 *
 * @copyright Copyright (c) 2019
 */
#include "tensor/tensor_io.h"
#include <cassert>
#include <iostream>
#include <typeinfo>

namespace magmadnn {
namespace io {

template <typename T>
magmadnn_error_t read_csv_to_tensor(Tensor<T>& t, const std::string& file_name, char delim) {
    magmadnn_error_t err = (magmadnn_error_t) 0;

    std::ifstream file_stream(file_name);

    if (!file_stream.is_open()) {
        /* error on opening file */
        return (magmadnn_error_t) 1;
    }

    std::stringstream ss;
    std::string line, token;
    T val;
    int cur_idx = 0;

    unsigned int axes = t.get_shape().size();
    std::vector<unsigned int> indices(axes, 0);

    /* read in every line of the file */
    while (std::getline(file_stream, line)) {
        /* used to process the current line */
        ss.str(line);

        /* read in each item separated by delim into num */
        while (std::getline(ss, token, delim)) {
            /* use iss's stream operator to correctly read value into val */
            if (!(std::istringstream(token) >> val)) {
                /* could not read into val */
                return (magmadnn_error_t) 2;
            }

            /* use tensor's built in flattened index set method */
            t.set(cur_idx, val);

            cur_idx++;
        }

        ss.clear();
    }
    file_stream.close();

    return err;
}
template magmadnn_error_t read_csv_to_tensor(Tensor<int>&, const std::string&, char);
template magmadnn_error_t read_csv_to_tensor(Tensor<float>&, const std::string&, char);
template magmadnn_error_t read_csv_to_tensor(Tensor<double>&, const std::string&, char);

template <typename T>
magmadnn_error_t write_tensor_to_csv(const Tensor<T>& t, const std::string& file_name, char delim, bool create) {
    magmadnn_error_t err = (magmadnn_error_t) 0;

    std::ofstream file_stream(file_name);
    if (!file_stream.is_open()) {
        /* failed to open file */
        return (magmadnn_error_t) 1;
    }

    T val;
    unsigned int size = t.get_size();

    for (unsigned int i = 0; i < size; i++) {
        val = t.get(i);
        if (!(file_stream << val << delim)) {
            /* for some reason errored while writing value */
            return (magmadnn_error_t) 2;
        }
    }

    file_stream.flush();
    file_stream.close();

    return err;
}
template magmadnn_error_t write_tensor_to_csv(const Tensor<int>&, const std::string&, char, bool);
template magmadnn_error_t write_tensor_to_csv(const Tensor<float>&, const std::string&, char, bool);
template magmadnn_error_t write_tensor_to_csv(const Tensor<double>&, const std::string&, char, bool);

template <typename T>
Tensor<T>* matrix_to_tensor(T* matrix_addr, unsigned int num_dims, memory_t mem, std::vector<unsigned int> dims) {
    assert(num_dims == dims.size());
    Tensor<T>* new_tensor;
    new_tensor = new Tensor<T>(dims, {CONSTANT, {1}}, mem);

    unsigned int M, N, K, Q;

    switch (num_dims) {
        case 1:
            M = dims.at(0);
            for (unsigned int i = 0; i < M; i++) {
                new_tensor->set(std::vector<unsigned int>{i}, *(matrix_addr + i));
            }
            break;
        case 2:  //(MxN)

            M = dims.at(0);
            N = dims.at(1);
            for (unsigned int i = 0; i < M; i++) {
                for (unsigned int j = 0; j < N; j++) {
                    new_tensor->set(std::vector<unsigned int>{i, j}, *(matrix_addr + i + M * j));
                    //printf("%d, ", i + M * j);
                }
            }
            break;
        case 3:  //(QxMxN)
            Q = dims.at(0);
            M = dims.at(1);
            N = dims.at(2);
            for (unsigned int k = 0; k < Q; k++) {
                for (unsigned int i = 0; i < M; i++) {
                    for (unsigned int j = 0; j < N; j++) {
                        new_tensor->set(std::vector<unsigned int>{k, i, j}, *(matrix_addr + i + M * j + M * N * k));
                        //printf("%d, ", i + M * j + M * N * k);
                    }
                }
            }
            break;
        case 4:  //(QxKxMxN)
            Q = dims.at(0);
            K = dims.at(1);
            M = dims.at(2);
            N = dims.at(3);
            for (unsigned int l = 0; l < Q; l++) {
                for (unsigned int k = 0; k < K; k++) {
                    for (unsigned int i = 0; i < M; i++) {
                        for (unsigned int j = 0; j < N; j++) {
                            new_tensor->set(std::vector<unsigned int>{l, k, i, j},
                                            *(matrix_addr + i + M * j + M * N * k + K * M * N * l));
                        }
                    }
                }
            }
            break;
    }
    return new_tensor;
}

template Tensor<int>* matrix_to_tensor(int*, unsigned int, memory_t, std::vector<unsigned int>);
template Tensor<float>* matrix_to_tensor(float*, unsigned int, memory_t, std::vector<unsigned int>);
template Tensor<double>* matrix_to_tensor(double*, unsigned int, memory_t, std::vector<unsigned int>);

template <typename T>
void print_tensor(const Tensor<T>* t, bool print_flat, const char *begin, const char *end, const char *delim)
{
int (*print)(const char *, ...) = std::printf;

    if (print_flat) {
        unsigned int size = t->get_size();

        print("%s", begin);
        for (unsigned int i = 0; i < size; i++) {
            print("%8.5f%s", t->get(i), delim);
            if (i != size) print(" ");
        }
        print("%s", end);
    } else {

        int t_n = 1, t_c = 1, t_h = 1, t_w = 1;
        unsigned int t_axes = t->get_shape().size();

        if (t_axes == 4) {
            t_w = t->get_shape(3);
			t_h = t->get_shape(2);
			t_c = t->get_shape(1);
			t_n = t->get_shape(0);
        }
        if (t_axes == 3) {
            t_w = t->get_shape(2);
			t_h = t->get_shape(1);
			t_c = t->get_shape(0);
        }
        if (t_axes == 2) {
            t_w = t->get_shape(1);
			t_h = t->get_shape(0);
        }
        if (t_axes == 1) {
            t_w = t->get_shape(0);
        }

		printf("Tensor size of {%i, %i, %i, %i}\n", t_n, t_c, t_h, t_w);

        for (int i = 0; i < t_n; i++) {
			print("%s\n", begin);
            for (int j = 0; j < t_c; j++) {
				print("  %s\n", begin);
                for (int k = 0; k < t_h; k++) {
					print("    |");
                    for (int l = 0; l < t_w; l++) {
                        print("% 2.5f%s", t->get(l + k * t_w + j * t_w * t_h + i * t_c * t_h * t_w), delim);
                        if (l != t_w - 1){
							 print(" ");
						} else{
							print("|\n");
						}
                    }
                }
				print("  %s", end);
            }
			print("%s", end);
        }
    }                  
}

template void print_tensor(const Tensor<float>*, bool, const char *, const char *, const char *);
template void print_tensor(const Tensor<double>*, bool, const char *, const char *, const char *);

template  <>
void print_tensor(const Tensor<int>* t, bool print_flat, const char *begin, const char *end, const char *delim){
    int (*print)(const char *, ...) = std::printf;

    if (print_flat) {
        unsigned int size = t->get_size();

        print("%s", begin);
        for (unsigned int i = 0; i < size; i++) {
            print("% 3i%s", t->get(i), delim);
            if (i != size) print(" ");
        }
        print("%s", end);
    } else {

        int t_n = 1, t_c = 1, t_h = 1, t_w = 1;
        unsigned int t_axes = t->get_shape().size();

        if (t_axes == 4) {
            t_w = t->get_shape(3);
			t_h = t->get_shape(2);
			t_c = t->get_shape(1);
			t_n = t->get_shape(0);
        }
        if (t_axes == 3) {
            t_w = t->get_shape(2);
			t_h = t->get_shape(1);
			t_c = t->get_shape(0);
        }
        if (t_axes == 2) {
            t_w = t->get_shape(1);
			t_h = t->get_shape(0);
        }
        if (t_axes == 1) {
            t_w = t->get_shape(0);
        }

		printf("Tensor size of (%i, %i, %i, %i)\n", t_n, t_c, t_h, t_w);

        for (int i = 0; i < t_n; i++) {
			print("%s\n", begin);
            for (int j = 0; j < t_c; j++) {
				print("  %s\n", begin);
                for (int k = 0; k < t_h; k++) {
					print("    |");
                    for (int l = 0; l < t_w; l++) {
                        print("% 3i%s", t->get(l + k * t_w + j * t_w * t_h + i * t_c * t_h * t_w), delim);
                        if (l != t_w - 1){
							 print(" ");
						} else{
							print("|\n");
						}
                    }
                }
				print("  %s", end);
            }
			print("%s", end);
        }
    }  
}

}  // namespace io
}  // namespace magmadnn