/**
 * @file distawarecrossentropy.cpp
 * @author Spencer Smith
 * @brief a cpp file to compute the loss. mainly used for image segmentation problems.
 * @version 0.1
 * @date 2022-07-01
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "math/distawarecrossentropy.h"
#include <cassert>
#include "math/gaussian2d.h"

namespace magmadnn {
namespace math {

template <typename T>
std::vector<unsigned int> find_closest_fg(Tensor<T> *image, std::vector<unsigned int> *image_dims,
                                          std::vector<unsigned int> *bg_pixel) {
    // make sure image tensor is 4d
    assert(image->get_shape().size() == 4);
    assert(image_dims->size() == 2);
    assert(bg_pixel->size() == 2);
    assert(image->get_memory_type() == HOST);  // make sure tensor is on host so ->get() is faster
    auto image_dim = image->get_shape();
    int image_x = (int) image_dim[2];
    int image_y = (int) image_dim[3];
    const unsigned int b = image_dims->at(0);
    const unsigned int c = image_dims->at(1);
    unsigned int x = (unsigned int) bg_pixel->at(0);
    unsigned int y = (unsigned int) bg_pixel->at(1);
    std::vector<std::vector<unsigned int>> diagonal_pixel;

    // make sure the given bg pixel coords are withing the bounds of the image dimensions
    if ((int) x >= image_x) {
        fprintf(stderr, "Index out of bounds.\n");
        return {10000, 10000};
    }
    if ((int) y >= image_y) {
        fprintf(stderr, "Index out of bounds.\n");
        return {10000, 10000};
    }
    // check adjacent tiles first
    // check diagnoal tiles second
    // if no fg pixel is found, increase the offset by one and check again
    int offset = 1;
    bool entered = false;

    // check to see if the current pixel is a foreground
    if (image->get({b, c, (unsigned int) x, (unsigned int) y}) == static_cast<T>(1)) return {x, y};

    while (true) {
        /////////////////// CHECK ADJACENT TILES ///////////////////

        // make sure adding the offset to x won't set it out of bounds
        if ((int) (x + offset) < image_x) {
            entered = true;
            // check tile to the right
            unsigned int offset_x = static_cast<unsigned int>(x + offset);
            T pixel_value = image->get({b, c, offset_x, static_cast<unsigned int>(y)});
            // printf("{%d, %d} = %f\n", offset_x, y, pixel_value);
            if (pixel_value == static_cast<T>(1)) return {offset_x, static_cast<unsigned int>(y)};
        }
        // make sure adding the offset to y won't set it out of bounds
        if ((int) (y + offset) < image_y) {
            entered = true;
            // check tile below
            unsigned int offset_y = static_cast<unsigned int>(y + offset);
            T pixel_value = image->get({b, c, static_cast<unsigned int>(x), offset_y});
            // printf("{%d, %d} = %f\n", x, offset_y, pixel_value);
            if (pixel_value == static_cast<T>(1)) return {static_cast<unsigned int>(x), offset_y};
        }
        // make sure subtracting the offset to x won't set it out of bounds
        if ((int) (x - offset) >= 0) {
            entered = true;
            // check tile to the left
            unsigned int offset_x = static_cast<unsigned int>(x - offset);
            T pixel_value = image->get({b, c, offset_x, static_cast<unsigned int>(y)});
            if (pixel_value == static_cast<T>(1)) return {offset_x, static_cast<unsigned int>(y)};
        }
        // make sure subtracting the offset to y won't set it out of bounds
        if ((int) (y - offset) >= 0) {
            entered = true;
            // check the tile above
            unsigned int offset_y = static_cast<unsigned int>(y - offset);
            T pixel_value = image->get({b, c, static_cast<unsigned int>(x), offset_y});
            if (pixel_value == static_cast<T>(1)) return {static_cast<unsigned int>(x), offset_y};
        }

        ////////////////////// CHECK DIAGONAL TILES LAST ////////////////////////////

        // make sure that adding both offsets won't set it out of bounds
        if (((int) (x + offset) < image_x) || ((int) (y + offset) < image_y)) {
            // check tiles to the southeast
            entered = true;
            bool x_out_of_bounds = ((int)(x + offset) >= image_x);
            bool y_out_of_bounds = ((int)(y + offset) >= image_y);
            unsigned int offset_x = 0;
            unsigned int offset_y = 0;
            int x_while_value = 0;
            int y_while_value = 0;

            if (x_out_of_bounds) {
                offset_x = static_cast<unsigned int>(image_x - x - 1);
                x_while_value = offset_x;
            } else {
                offset_x = static_cast<unsigned int>(x + offset);
                x_while_value = offset;
            }

            if (y_out_of_bounds) {
                offset_y = static_cast<unsigned int>(image_y - y - 1);
                y_while_value = offset_y;
            } else {
                offset_y = static_cast<unsigned int>(y + offset);
                y_while_value = offset;
            }

            int temp_offset = 1;
            while (temp_offset < x_while_value) {
                unsigned int temp_offset_x = static_cast<unsigned int>(x + temp_offset);
                T pixel_value = image->get({b, c, temp_offset_x, offset_y});
                // printf("{%d, %d} = %f\n", temp_offset_x, offset_y, pixel_value);
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({temp_offset_x, offset_y});
                    break;
                }
                temp_offset++;
            }
            temp_offset = 1;
            while (temp_offset < y_while_value) {
                unsigned int temp_offset_y = static_cast<unsigned int>(y + temp_offset);
                T pixel_value = image->get({b, c, offset_x, temp_offset_y});
                // printf("{%d, %d} = %f\n", offset_x, temp_offset_y, pixel_value);
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({offset_x, temp_offset_y});
                    break;
                }
                temp_offset++;
            }

            T pixel_value = image->get({b, c, offset_x, offset_y});
            // printf("{%d, %d} = %f\n", offset_x, offset_y, pixel_value);
            if (pixel_value == static_cast<T>(1)) diagonal_pixel.push_back({offset_x, offset_y});
        }

        if (((int) (x + offset) < image_x) || ((int) (y - offset) >= 0)) {
            // check tiles to the northeast
            entered = true;
            bool x_out_of_bounds = ((int)(x + offset) >= image_x);
            bool y_out_of_bounds = ((y - offset) < 0);
            unsigned int offset_x = 0;
            unsigned int offset_y = 0;
            int x_while_value = 0;
            int y_while_value = 0;

            if (x_out_of_bounds) {
                offset_x = static_cast<unsigned int>(image_x - x - 1);
                x_while_value = offset_x;
            } else {
                offset_x = static_cast<unsigned int>(x + offset);
                x_while_value = offset;
            }

            if (y_out_of_bounds) {
                offset_y = static_cast<unsigned int>(y);
                y_while_value = offset_y;
            } else {
                offset_y = static_cast<unsigned int>(y - offset);
                y_while_value = offset;
            }

            int temp_offset = 1;
            while (temp_offset < x_while_value) {
                unsigned int temp_offset_x = static_cast<unsigned int>(x + temp_offset);
                T pixel_value = image->get({b, c, temp_offset_x, offset_y});
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({temp_offset_x, offset_y});
                    break;
                }
                temp_offset++;
            }
            temp_offset = 1;
            while (temp_offset < y_while_value) {
                unsigned int temp_offset_y = static_cast<unsigned int>(y - temp_offset);
                T pixel_value = image->get({b, c, offset_x, temp_offset_y});
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({offset_x, temp_offset_y});
                    break;
                }
                temp_offset++;
            }

            T pixel_value = image->get({b, c, offset_x, offset_y});
            if (pixel_value == static_cast<T>(1)) diagonal_pixel.push_back({offset_x, offset_y});
        }

        if (((int) (x - offset) >= 0) || ((int) (y + offset) < image_y)) {
            // check tiles to the southwest
            entered = true;
            bool x_out_of_bounds = ((x - offset) < 0);
            bool y_out_of_bounds = ((int)(y + offset) >= image_y);
            unsigned int offset_x = 0;
            unsigned int offset_y = 0;
            int x_while_value = 0;
            int y_while_value = 0;

            if (x_out_of_bounds) {
                offset_x = static_cast<unsigned int>(x);
                x_while_value = offset_x;
            } else {
                offset_x = static_cast<unsigned int>(x - offset);
                x_while_value = offset;
            }

            if (y_out_of_bounds) {
                offset_y = static_cast<unsigned int>(image_y - y - 1);
                y_while_value = offset_y;
            } else {
                offset_y = static_cast<unsigned int>(y + offset);
                y_while_value = offset;
            }

            int temp_offset = 1;
            while (temp_offset < x_while_value) {
                unsigned int temp_offset_x = static_cast<unsigned int>(x - temp_offset);
                T pixel_value = image->get({b, c, temp_offset_x, offset_y});
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({temp_offset_x, offset_y});
                    break;
                }
                temp_offset++;
            }
            temp_offset = 1;
            while (temp_offset < y_while_value) {
                unsigned int temp_offset_y = static_cast<unsigned int>(y + temp_offset);
                T pixel_value = image->get({b, c, offset_x, temp_offset_y});
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({offset_x, temp_offset_y});
                    break;
                }
                temp_offset++;
            }

            T pixel_value = image->get({b, c, offset_x, offset_y});
            if (pixel_value == static_cast<T>(1)) diagonal_pixel.push_back({offset_x, offset_y});
        }

        if (((int) (x - offset) >= 0) || ((int) (y - offset) >= 0)) {
            // check tiles to north west
            entered = true;
            bool x_out_of_bounds = ((x - offset) < 0);
            bool y_out_of_bounds = ((y - offset) < 0);
            unsigned int offset_x = 0;
            unsigned int offset_y = 0;
            int x_while_value = 0;
            int y_while_value = 0;

            if (x_out_of_bounds) {
                offset_x = static_cast<unsigned int>(x);
                x_while_value = offset_x;
            } else {
                offset_x = static_cast<unsigned int>(x - offset);
                x_while_value = offset;
            }

            if (y_out_of_bounds) {
                offset_y = static_cast<unsigned int>(y);
                y_while_value = offset_y;
            } else {
                offset_y = static_cast<unsigned int>(y - offset);
                y_while_value = offset;
            }

            int temp_offset = 1;
            while (temp_offset < x_while_value) {
                unsigned int temp_offset_x = static_cast<unsigned int>(x - temp_offset);
                T pixel_value = image->get({b, c, temp_offset_x, offset_y});
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({temp_offset_x, offset_y});
                    break;
                }
                temp_offset++;
            }
            temp_offset = 1;
            while (temp_offset < y_while_value) {
                unsigned int temp_offset_y = static_cast<unsigned int>(y - temp_offset);
                T pixel_value = image->get({b, c, offset_x, temp_offset_y});
                if (pixel_value == static_cast<T>(1)) {
                    diagonal_pixel.push_back({offset_x, temp_offset_y});
                    break;
                }
                temp_offset++;
            }

            T pixel_value = image->get({b, c, offset_x, offset_y});
            if (pixel_value == static_cast<T>(1)) diagonal_pixel.push_back({offset_x, offset_y});
        }

        // if none of the if's above have been entered, then that means there are no
        // ones in the ground_truth image. It is unlikely to happen, but it is just a
        // failsafe.
        if (!entered) {
            printf("no if statements entered... value of offset = %d\n", offset);
            return {10000, 10000};
        }

        if (diagonal_pixel.size() > 0) {
            float min_distance = 10.0;
            int min_coord = 0;
            for (unsigned int i = 0; i < diagonal_pixel.size(); i++) {
                int temp_x = diagonal_pixel[i][0] - x;
                int temp_y = diagonal_pixel[i][1] - y;
                float distance = (float) (temp_y / temp_x);
                if (distance < min_distance) {
                    min_distance = distance;
                    min_coord = i;
                }
            }
            return diagonal_pixel[min_coord];
        }

        // if a fg pixel was not found, increase offset and go again.
        offset++;
        entered = false;
    }

    return {10000, 10000};
}
template std::vector<unsigned int> find_closest_fg(Tensor<int> *image, std::vector<unsigned int> *image_dims,
                                                   std::vector<unsigned int> *bg_pixel);
template std::vector<unsigned int> find_closest_fg(Tensor<float> *image, std::vector<unsigned int> *image_dims,
                                                   std::vector<unsigned int> *bg_pixel);
template std::vector<unsigned int> find_closest_fg(Tensor<double> *image, std::vector<unsigned int> *image_dims,
                                                   std::vector<unsigned int> *bg_pixel);

template <typename T>
void distawarecrossentropy(Tensor<T> *predicted, Tensor<T> *ground_truth, Tensor<T> *out) {
    // copy the Tensors to host
    Tensor<T> *host_predicted = new Tensor<T>(predicted->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_ground_truth = new Tensor<T>(ground_truth->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_out = new Tensor<T>(out->get_shape(), {NONE, {}}, HOST);
    host_predicted->copy_from(*predicted);
    host_ground_truth->copy_from(*ground_truth);
    unsigned int batch_size = host_predicted->get_shape(0);
    unsigned int n_channels = host_predicted->get_shape(1);
    unsigned int height = host_predicted->get_shape(2);
    unsigned int width = host_predicted->get_shape(3);

    // hyper-parameters for the function, which can be tuned
    double alpha = static_cast<double>(2);
    double beta = static_cast<double>(4);

    std::vector<unsigned int> *bg_pixel_coords = new std::vector<unsigned int>;
    std::vector<unsigned int> *batch_and_channel = new std::vector<unsigned int>;

    for (unsigned int b = 0; b < batch_size; b++) {
        for (unsigned int c = 0; c < n_channels; c++) {
            for (unsigned int h = 0; h < height; h++) {
                for (unsigned int w = 0; w < width; w++) {
                    // store the coordinates of the bg_pixel
                    bg_pixel_coords->push_back(h);
                    bg_pixel_coords->push_back(w);
                    // store the batch and channel index
                    batch_and_channel->push_back(b);
                    batch_and_channel->push_back(c);
                    // get the closest fg pixel
                    auto closest_fg_pixel = find_closest_fg(host_ground_truth, batch_and_channel, bg_pixel_coords);
                    // check for errors in the closest_fg function
                    if (closest_fg_pixel[0] == 10000) {
                        fprintf(stderr, "an error occured for pixel %d x %d\n", h, w);
                        exit(EXIT_FAILURE);
                    }
                    // get the 2d gaussian distribution
                    auto gamma = gaussian2d<T>(&closest_fg_pixel, bg_pixel_coords);
                    bg_pixel_coords->pop_back();
                    bg_pixel_coords->pop_back();
                    batch_and_channel->pop_back();
                    batch_and_channel->pop_back();
                    T predicted = host_predicted->get({b, c, h, w});
                    double temp_result1 = static_cast<double>(0);
                    double temp_result2 = static_cast<double>(0);
                    T pixel_loss = static_cast<T>(0);

                    // if the current pixel is a fg pixel
                    if (gamma == static_cast<double>(1)) {
                        if (predicted == 0) {
                            host_out->set({b, c, h, w}, (T) 1);
                            break;
                        }
                        temp_result1 = pow(static_cast<double>((T) 1 - predicted), alpha);
                        temp_result2 = log10(static_cast<double>(predicted));
                        pixel_loss = static_cast<T>(-1) * static_cast<T>(temp_result1 * temp_result2);
                        // printf("pixel loss (y = 1 & p = %f): %f\n", predicted, pixel_loss);
                        host_out->set({b, c, h, w}, pixel_loss);
                    } else {  // else it is a background pixel
                        if (predicted == 1) {
                            host_out->set({b, c, h, w}, (T) 1);
                            break;
                        }
                        temp_result1 = pow(static_cast<double>((T) 1 - gamma), beta);
                        temp_result2 = pow(static_cast<double>(predicted), alpha);
                        auto temp_result3 = log10(static_cast<double>((T) 1 - predicted));
                        pixel_loss = -(static_cast<T>(temp_result1 * temp_result2 * temp_result3));
                        // printf("temp result 2 = %f\n", temp_result2);
                        // printf("pixel loss (y != 1 & p = %f): %f\n", predicted, pixel_loss);
                        host_out->set({b, c, h, w}, pixel_loss);
                    }
                }
            }
        }
    }

    out->copy_from(*host_out);
    delete batch_and_channel;
    delete bg_pixel_coords;
    delete host_ground_truth;
    delete host_predicted;
    delete host_out;
}
template void distawarecrossentropy(Tensor<int> *predicted, Tensor<int> *ground_truth, Tensor<int> *out);
template void distawarecrossentropy(Tensor<float> *predicted, Tensor<float> *ground_truth, Tensor<float> *out);
template void distawarecrossentropy(Tensor<double> *predicted, Tensor<double> *ground_truth, Tensor<double> *out);

template <typename T>
void distawarecrossentropy_grad(Tensor<T> *predicted, Tensor<T> *ground_truth, Tensor<T> *grad, Tensor<T> *out) {
    // copy the Tensors to host
    Tensor<T> *host_predicted = new Tensor<T>(predicted->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_ground_truth = new Tensor<T>(ground_truth->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_grad = new Tensor<T>(grad->get_shape(), {NONE, {}}, HOST);
    Tensor<T> *host_out = new Tensor<T>(out->get_shape(), {NONE, {}}, HOST);
    host_predicted->copy_from(*predicted);
    host_ground_truth->copy_from(*ground_truth);
    host_grad->copy_from(*grad);
    unsigned int batch_size = host_predicted->get_shape(0);
    unsigned int n_channels = host_predicted->get_shape(1);
    unsigned int height = host_predicted->get_shape(2);
    unsigned int width = host_predicted->get_shape(3);

    assert(host_grad->get_shape().size() == 1);
    T grad_value = host_grad->get(0);

    // hyper-parameters for the function, which can be tuned
    const double alpha = static_cast<double>(2);
    const double beta = static_cast<double>(4);
    std::vector<unsigned int> *bg_pixel_coords = new std::vector<unsigned int>;
    std::vector<unsigned int> *batch_and_channel = new std::vector<unsigned int>;

    for (unsigned int b = 0; b < batch_size; b++) {
        for (unsigned int c = 0; c < n_channels; c++) {
            for (unsigned int h = 0; h < height; h++) {
                for (unsigned int w = 0; w < width; w++) {
                    // store the coordinates of the bg_pixel
                    bg_pixel_coords->push_back(h);
                    bg_pixel_coords->push_back(w);
                    // store the batch and channel index
                    batch_and_channel->push_back(b);
                    batch_and_channel->push_back(c);
                    // get the closest fg pixel
                    auto closest_fg_pixel = find_closest_fg(host_ground_truth, batch_and_channel, bg_pixel_coords);
                    // check for errors in the closest_fg function
                    if (closest_fg_pixel[0] == 10000) {
                        fprintf(stderr, "an error occured for pixel %d x %d\n", h, w);
                        exit(EXIT_FAILURE);
                    }
                    // get the 2d gaussian distribution
                    auto gamma = gaussian2d<T>(&closest_fg_pixel, bg_pixel_coords);
                    bg_pixel_coords->pop_back();
                    bg_pixel_coords->pop_back();
                    batch_and_channel->pop_back();
                    batch_and_channel->pop_back();
                    T predicted = host_predicted->get({b, c, h, w});
                    double temp_result1 = static_cast<double>(0);
                    double temp_result2 = static_cast<double>(0);
                    double temp_result3 = static_cast<double>(0);
                    T pixel_grad = static_cast<T>(0);
                    if (gamma == static_cast<T>(1)) {
                        /*
                        if (predicted == 0) {
                            host_out->set({b, c, h, w}, (T)-1);
                            break;
                        } */

                        temp_result1 = -1.0 * alpha * pow((double) (1 - predicted), alpha - 1.0);
                        temp_result2 = log10(abs((double) predicted)) + (1.0 / (double) predicted);
                        temp_result3 = pow((1.0 - (double) predicted), alpha);
                        pixel_grad = static_cast<T>(-1) * static_cast<T>(temp_result1 * temp_result2 * temp_result3);
                        /*if( b == 2 ) {
                            printf("gamma == 1\n");
                            printf("pixel_grad = %f\n", pixel_grad);
                        }*/
                    } else {
                        /*
                        if (predicted == 1) {
                            host_out->set({b, c, h, w}, (T)1);
                            break;
                        }*/
                        temp_result1 = pow((1.0 - (double) gamma), beta);
                        temp_result2 =
                            alpha * pow((double) predicted, (alpha - 1.0)) * log10(1.0 - abs((double) predicted));
                        temp_result3 = (pow((double) predicted, alpha) / (double) ((T) 1 - predicted));
                        pixel_grad = static_cast<T>(-1) * static_cast<T>(temp_result1 * (temp_result2 - temp_result3));
                        /*if( b == 2 ) {
                            printf("gamma < 1\n");
                            printf("pixel_grad = %f\n", pixel_grad);
                        }*/
                    }
                    host_out->set({b, c, h, w}, pixel_grad * grad_value);
                }
            }
        }
    }

    out->copy_from(*host_out);
    delete batch_and_channel;
    delete bg_pixel_coords;
    delete host_ground_truth;
    delete host_predicted;
    delete host_out;
}
template void distawarecrossentropy_grad(Tensor<int> *predicted, Tensor<int> *ground_truth, Tensor<int> *grad,
                                         Tensor<int> *out);
template void distawarecrossentropy_grad(Tensor<float> *predicted, Tensor<float> *ground_truth, Tensor<float> *grad,
                                         Tensor<float> *out);
template void distawarecrossentropy_grad(Tensor<double> *predicted, Tensor<double> *ground_truth, Tensor<double> *grad,
                                         Tensor<double> *out);
}  // namespace math
}  // namespace magmadnn
