#include "math/mha.h"

#define PRINT_INPUT 0
#define PRINT_OUTPUT 0

#if PRINT_INPUT || PRINT_OUTPUT
#include "magmadnn.h"
#endif

namespace magmadnn {
namespace math {
    #if defined(MAGMADNN_HAVE_CUDA)
    template <typename T>
    void mha_device(Tensor<T> *q_t, Tensor<T> *k_t, Tensor<T> *v_t, Tensor<T> *o_t, Tensor<T> *weights_t, mha_cudnn_settings settings) {
        if (settings.mainCfg.train) {
            // TODO: training mode
            if (settings.sizeReserve == 0) {
                fprintf(stderr, "ERROR: zero reserve buffer size in training mode\n\n");
                exit(-1);
            }
            cudnnErrchk(
                cudnnMultiHeadAttnForward(
                    settings.handle, //done
                    settings.attn_desc, //done
                    -1, //int currIdx,
                    settings.loWinIdx, //done * 
                    settings.hiWinIdx, //done * 
                    settings.devQSeqArray,//done * 
                    settings.devKSeqArray,//done * 
                    settings.q_desc, //done
                    q_t->get_ptr(), //done *
                    settings.mainCfg.resLink ? q_t->get_ptr() : NULL, //done
                    settings.k_desc, //done
                    k_t->get_ptr(), //done *
                    settings.v_desc, //done
                    v_t->get_ptr(), //done *
                    settings.o_desc, //done
                    o_t->get_ptr(), //done
                    settings.sizeWeights, //done *
                    settings.sizeWeights>0 ? weights_t->get_ptr() : NULL, //done
                    settings.sizeWkspace, //done * 
                    settings.devWkspace,
                    settings.sizeReserve,
                    settings.devReserve
                )
            );
        } else {
            // TODO: inference mode
            if (settings.sizeReserve != 0) {
                fprintf(stderr, "ERROR: non-zero reserve buffer size in inference mode\n\n");
                exit(-1);
            }
            if (settings.mainCfg.sweep == 0) {
                for (int idx = 0; idx < settings.mainCfg.seqLenQ; idx++) {
                    printf("Calling cudnnMultiHeadAttnForward(currIdx = %d)\n", idx);
                    cudnnErrchk(
                        cudnnMultiHeadAttnForward(
                            settings.handle, //done
                            settings.attn_desc, //done
                            idx, //int currIdx,
                            settings.loWinIdx, //done * 
                            settings.hiWinIdx, //done * 
                            settings.devQSeqArray,//done * 
                            settings.devKSeqArray,//done * 
                            settings.q_desc, //done
                            q_t->get_ptr(), //done *
                            settings.mainCfg.resLink ? q_t->get_ptr() : NULL, //done
                            settings.k_desc, //done
                            k_t->get_ptr(), //done *
                            settings.v_desc, //done
                            v_t->get_ptr(), //done *
                            settings.o_desc, //done
                            o_t->get_ptr(), //done
                            settings.sizeWeights, //done *
                            settings.sizeWeights>0 ? weights_t->get_ptr() : NULL, //done
                            settings.sizeWkspace, //done * 
                            settings.devWkspace,
                            0, //done
                            NULL
                        )
                    );
                }
            } else {
                cudnnErrchk(
                    cudnnMultiHeadAttnForward(
                        settings.handle, //done
                        settings.attn_desc, //done
                        -1, //int currIdx,
                        settings.loWinIdx, //done * 
                        settings.hiWinIdx, //done * 
                        settings.devQSeqArray,//done * 
                        settings.devKSeqArray,//done * 
                        settings.q_desc, //done
                        q_t->get_ptr(), //done *
                        settings.mainCfg.resLink ? q_t->get_ptr() : NULL, //done
                        settings.k_desc, //done
                        k_t->get_ptr(), //done *
                        settings.v_desc, //done
                        v_t->get_ptr(), //done *
                        settings.o_desc, //done
                        o_t->get_ptr(), //done
                        settings.sizeWeights, //done *
                        settings.sizeWeights>0 ? weights_t->get_ptr() : NULL, //done
                        settings.sizeWkspace, //done * 
                        settings.devWkspace,
                        0, //done
                        NULL
                    )
                );
            }
        }
    }

    template void mha_device(Tensor<float> *q_t, 
                            Tensor<float> *k_t, 
                            Tensor<float> *v_t, 
                            Tensor<float> *o_t, 
                            Tensor<float> *weights_t, 
                            mha_cudnn_settings settings);

    template <typename T>
    void mha_grad_data_device(Tensor<T> *q_t, Tensor<T> *dq_t, Tensor<T> *k_t, Tensor<T> *dk_t, Tensor<T> *v_t, Tensor<T> *dv_t, Tensor<T> *w_t, Tensor<T> *do_t, mha_cudnn_settings settings){
        if (settings.mainCfg.train == 0) {
            fprintf(stderr, "ERROR: call mha_grad_data_device() in inference mode\n\n");
            exit(-1);
        }
        if (settings.sizeReserve == 0) {
            fprintf(stderr, "ERROR: zero reserve buffer size in training mode\n\n");
            exit(-1);
        }
        #if PRINT_INPUT
        printf("Tensor Q:\n");
        io::print_tensor(q_t);
        printf("Tensor K:\n");
        io::print_tensor(k_t);
        printf("Tensor V:\n");
        io::print_tensor(v_t);
        printf("Tensor W:\n");
        io::print_tensor(w_t);
        printf("Tensor DO:\n");
        io::print_tensor(do_t);
        #endif
        cudnnErrchk(cudnnMultiHeadAttnBackwardData(
            settings.handle,
            settings.attn_desc,
            settings.loWinIdx,
            settings.hiWinIdx,
            settings.devQSeqArray,
            settings.devKSeqArray,
            settings.o_desc,
            do_t->get_ptr(),
            settings.q_desc,
            dq_t->get_ptr(),
            q_t->get_ptr(),
            settings.k_desc,
            dk_t->get_ptr(),
            k_t->get_ptr(),
            settings.v_desc,
            dv_t->get_ptr(),
            v_t->get_ptr(),
            settings.sizeWeights,
            settings.sizeWeights > 0 ? w_t->get_ptr() : NULL,
            settings.sizeWkspace,
            settings.devWkspace,
            settings.sizeReserve,
            settings.devReserve));
    #if PRINT_OUTPUT
        printf("tensor DQ:\n");
        io::print_tensor(dq_t);
        printf("tensor DK:\n");
        io::print_tensor(dk_t);
        printf("tensor DV:\n");
        io::print_tensor(dv_t);
    #endif
    }

    template void mha_grad_data_device(
        Tensor<float> *q, 
        Tensor<float> *dq_t, 
        Tensor<float> *k, 
        Tensor<float> *dk_t, 
        Tensor<float> *v, 
        Tensor<float> *dv_t, 
        Tensor<float> *w_t, 
        Tensor<float> *do_t, 
        mha_cudnn_settings settings);


    template <typename T>
    void mha_grad_data_device_weights(Tensor<T> *q_t, Tensor<T> *k_t, Tensor<T> *v_t, Tensor<T> *do_t, Tensor<T> *w_t, Tensor<T> *dw_t, mha_cudnn_settings settings){
    #if PRINT_INPUT
        printf("tensor Q:\n");
        io::print_tensor(q_t);
        printf("tensor K:\n");
        io::print_tensor(k_t);
        printf("tensor V:\n");
        io::print_tensor(v_t);
        printf("tensor W:\n");
        io::print_tensor(w_t);
        // printf("tensor DW:\n");
        // io::print_tensor(dw_t);
        printf("tensor DO:\n");
        io::print_tensor(do_t);

        // printf("sizeWeights: %d, sizeWkspace: %d, sizeReserve: %d\n", settings.sizeWeights, settings.sizeWkspace, settings.sizeReserve);

        // if (settings.devWkspace == NULL) { printf("NULL Wkspace\n"); }
        // if (settings.devReserve == NULL) { printf("NULL Reserve\n"); }
        // if (settings.handle == NULL) { printf("NULL handle\n"); }
        // if (settings.attn_desc == NULL) { printf("NULL attn_desc\n"); }
        // if (settings.q_desc == NULL) { printf("NULL q desc\n"); }
        // if (settings.k_desc == NULL) { printf("NULL k desc\n"); }
        // if (settings.v_desc == NULL) { printf("NULL v desc\n"); }
        // if (settings.o_desc == NULL) { printf("NULL o desc\n"); }
    #endif
        cudnnErrchk(cudnnMultiHeadAttnBackwardWeights(
            settings.handle,
            settings.attn_desc,
            CUDNN_WGRAD_MODE_ADD,
            settings.q_desc,
            q_t->get_ptr(),
            settings.k_desc,
            k_t->get_ptr(),
            settings.v_desc,
            v_t->get_ptr(),
            settings.o_desc,
            do_t->get_ptr(),
            settings.sizeWeights,
            settings.sizeWeights > 0 ? w_t->get_ptr() : NULL,
            settings.sizeWeights > 0 ? dw_t->get_ptr() : NULL,
            settings.sizeWkspace,
            settings.devWkspace,
            settings.sizeReserve,
            settings.devReserve));
    #if PRINT_OUTPUT
        // printf("tensor W:\n");
        // io::print_tensor(w_t);
        printf("tensor DW:\n");
        io::print_tensor(dw_t);
        // printf("tensor DO:\n");
        // io::print_tensor(do_t);
    #endif
    }

    template void mha_grad_data_device_weights(
        Tensor<float> *q_t, 
        Tensor<float> *k_t, 
        Tensor<float> *v_t, 
        Tensor<float> *do_t, 
        Tensor<float> *w_t, 
        Tensor<float> *dw_t, 
        mha_cudnn_settings settings);
    #endif
} //namespace math
} //namespace magmadnn