/**
 * @file conv2dtranspose.cpp
 * @author Spencer Smith
 * @brief Implementation for convolutional transpose with cuDNN
 * @version 0.1
 * @date 2022-06-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "math/conv2dtranspose.h"
#include <cassert>

namespace magmadnn {
namespace math {

template <typename T>
void conv2dtranspose(Tensor<T> *x, Tensor<T> *w, Tensor<T> *out) {
    assert(T_IS_SAME_MEMORY_TYPE(x, w) && T_IS_SAME_MEMORY_TYPE(w, out));

    if (out->get_memory_type() == HOST) {
        fprintf(stderr, "__Conv2dtranspose CPU not supported yet.\n");
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        fprintf(stderr, "For Conv2dtranspose GPU please use conv2dtranspose_device.\n");
    }
#endif
}
template void conv2dtranspose(Tensor<int> *x, Tensor<int> *w, Tensor<int> *out);
template void conv2dtranspose(Tensor<float> *x, Tensor<float> *w, Tensor<float> *out);
template void conv2dtranspose(Tensor<double> *x, Tensor<double> *w, Tensor<double> *out);

template <typename T>
void conv2dtranspose_grad_data(Tensor<T> *w, Tensor<T> *grad, Tensor<T> *out) {
    assert(T_IS_SAME_MEMORY_TYPE(w, grad) && T_IS_SAME_MEMORY_TYPE(grad, out));

    if (out->get_memory_type() == HOST) {
        fprintf(stderr, "__Conv2dtranspose_grad_data CPU not supported yet.\n");
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        fprintf(stderr, "For Conv2dtranspose_grad_data GPU please use conv2dtranspose_grad_data_device.\n");
    }
#endif
}
template void conv2dtranspose_grad_data(Tensor<int> *w, Tensor<int> *grad, Tensor<int> *out);
template void conv2dtranspose_grad_data(Tensor<float> *w, Tensor<float> *grad, Tensor<float> *out);
template void conv2dtranspose_grad_data(Tensor<double> *w, Tensor<double> *grad, Tensor<double> *out);

template <typename T>
void conv2dtranspose_grad_filter(Tensor<T> *w, Tensor<T> *grad, Tensor<T> *out) {
    assert(T_IS_SAME_MEMORY_TYPE(w, grad) && T_IS_SAME_MEMORY_TYPE(grad, out));

    if (out->get_memory_type() == HOST) {
        fprintf(stderr, "__Conv2dtranspose_grad_filter CPU not supported yet.\n");
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        fprintf(stderr, "For Conv2dtranspose_grad_filter GPU please use conv2dtranspose_grad_filter_device.\n");
    }
#endif
}
template void conv2dtranspose_grad_filter(Tensor<int> *x, Tensor<int> *grad, Tensor<int> *out);
template void conv2dtranspose_grad_filter(Tensor<float> *x, Tensor<float> *grad, Tensor<float> *out);
template void conv2dtranspose_grad_filter(Tensor<double> *x, Tensor<double> *grad, Tensor<double> *out);

#if defined(MAGMADNN_HAVE_CUDA)
/**
 * Forward Transposed convolution is algorithmically equivalent
 * to the backward pass of regular convolution.
 * The HxW of the output tensor should be larger than the HxW
 * of the input tensor.
 * Reference:
 * https://towardsdatascience.com/transposed-convolution-demystified-84ca81b4baba#:~:text=Finally%2C%20it%20is%20also%20referred%20to%20as%20Backward,suffer%20from%20chequered%20board%20effects%20as%20shown%20below.
 */
#if CUDNN_MAJOR == 8                        // cudnn version 8 code
template <typename T>
void conv2dtranspose_device(Tensor<T> *x, Tensor<T> *w, Tensor<T> *out, conv2dtranspose_cudnn_settings settings) {
    // set alpha to one and beta to 0
    T alpha = static_cast<T>(1), beta = static_cast<T>(0);
    // call the cudnn backward convolution
    // W represents the filter tensor
    // X represents the input tensor
    // out represents the output tensor
    cudnnErrchk(cudnnConvolutionBackwardData(::magmadnn::internal::MAGMADNN_SETTINGS->cudnn_handle, &alpha,
                                             settings.filter_desc, w->get_ptr(), x->get_cudnn_tensor_descriptor(),
                                             x->get_ptr(), settings.conv_desc, settings.bwd_data_algo.algo,
                                             settings.grad_data_workspace, settings.grad_data_workspace_size, &beta,
                                             out->get_cudnn_tensor_descriptor(), out->get_ptr()));
}
template void conv2dtranspose_device(Tensor<int> *x, Tensor<int> *w, Tensor<int> *out,
                                     conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_device(Tensor<float> *x, Tensor<float> *w, Tensor<float> *out,
                                     conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_device(Tensor<double> *x, Tensor<double> *w, Tensor<double> *out,
                                     conv2dtranspose_cudnn_settings settings);

/**
 *  The backward pass of a transposed convolution is the same as
 *  the forward pass of a normal convolution; however, the input
 *  parameters are little different.
 */
template <typename T>
void conv2dtranspose_grad_data_device(Tensor<T> *w, Tensor<T> *grad, Tensor<T> *out,
                                      conv2dtranspose_cudnn_settings settings) {
    T alpha = static_cast<T>(1), beta = static_cast<T>(0);
    // the gradient tensor from previous convolution is used as input
    // the weight tensor is used in its normal place
    // out is the output tensor
    cudnnErrchk(cudnnConvolutionForward(
        ::magmadnn::internal::MAGMADNN_SETTINGS->cudnn_handle, &alpha, grad->get_cudnn_tensor_descriptor(),
        grad->get_ptr(), settings.filter_desc, w->get_ptr(), settings.conv_desc, settings.algo.algo, settings.workspace,
        settings.workspace_size, &beta, out->get_cudnn_tensor_descriptor(), out->get_ptr()));
}
template void conv2dtranspose_grad_data_device(Tensor<int> *w, Tensor<int> *grad, Tensor<int> *out,
                                               conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_data_device(Tensor<float> *w, Tensor<float> *grad, Tensor<float> *out,
                                               conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_data_device(Tensor<double> *w, Tensor<double> *grad, Tensor<double> *out,
                                               conv2dtranspose_cudnn_settings settings);

/**
 *  Computing the filter gradient of a transposed convolution is the
 *  same as normal convolution.
 *  computes the gradient of the tensor dy, where y is the output of
 *  the conv2dtranspose_device function.
 *  the output is the update weight tensor.
 */
template <typename T>
void conv2dtranspose_grad_filter_device(Tensor<T> *x, Tensor<T> *grad, Tensor<T> *out,
                                        conv2dtranspose_cudnn_settings settings) {
    T alpha = static_cast<T>(1), beta = static_cast<T>(0);
    cudnnErrchk(cudnnConvolutionBackwardFilter(
        ::magmadnn::internal::MAGMADNN_SETTINGS->cudnn_handle, &alpha, grad->get_cudnn_tensor_descriptor(),
        grad->get_ptr(), x->get_cudnn_tensor_descriptor(), x->get_ptr(), settings.conv_desc,
        settings.bwd_filter_algo.algo, settings.grad_filter_workspace, settings.grad_filter_workspace_size, &beta,
        settings.filter_desc, out->get_ptr()));
}
template void conv2dtranspose_grad_filter_device(Tensor<int> *x, Tensor<int> *grad, Tensor<int> *out,
                                                 conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_filter_device(Tensor<float> *x, Tensor<float> *grad, Tensor<float> *out,
                                                 conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_filter_device(Tensor<double> *x, Tensor<double> *grad, Tensor<double> *out,
                                                 conv2dtranspose_cudnn_settings settings);
#else               // cudnn 7 code
template <typename T>
void conv2dtranspose_device(Tensor<T> *x, Tensor<T> *w, Tensor<T> *out, conv2dtranspose_cudnn_settings settings) {
    // set alpha to one and beta to 0
    T alpha = static_cast<T>(1), beta = static_cast<T>(0);
    // call the cudnn backward convolution
    // W represents the filter tensor
    // X represents the input tensor
    // out represents the output tensor
    cudnnErrchk(cudnnConvolutionBackwardData(::magmadnn::internal::MAGMADNN_SETTINGS->cudnn_handle, &alpha,
                                             settings.filter_desc, w->get_ptr(), x->get_cudnn_tensor_descriptor(),
                                             x->get_ptr(), settings.conv_desc, settings.bwd_data_algo,
                                             settings.grad_data_workspace, settings.grad_data_workspace_size, &beta,
                                             out->get_cudnn_tensor_descriptor(), out->get_ptr()));
}
template void conv2dtranspose_device(Tensor<int> *x, Tensor<int> *w, Tensor<int> *out,
                                     conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_device(Tensor<float> *x, Tensor<float> *w, Tensor<float> *out,
                                     conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_device(Tensor<double> *x, Tensor<double> *w, Tensor<double> *out,
                                     conv2dtranspose_cudnn_settings settings);

/**
 *  The backward pass of a transposed convolution is the same as
 *  the forward pass of a normal convolution; however, the input
 *  parameters are little different.
 */
template <typename T>
void conv2dtranspose_grad_data_device(Tensor<T> *w, Tensor<T> *grad, Tensor<T> *out,
                                      conv2dtranspose_cudnn_settings settings) {
    T alpha = static_cast<T>(1), beta = static_cast<T>(0);
    // the gradient tensor from previous convolution is used as input
    // the weight tensor is used in its normal place
    // out is the output tensor
    cudnnErrchk(cudnnConvolutionForward(
        ::magmadnn::internal::MAGMADNN_SETTINGS->cudnn_handle, &alpha, grad->get_cudnn_tensor_descriptor(),
        grad->get_ptr(), settings.filter_desc, w->get_ptr(), settings.conv_desc, settings.algo, settings.workspace,
        settings.workspace_size, &beta, out->get_cudnn_tensor_descriptor(), out->get_ptr()));
}
template void conv2dtranspose_grad_data_device(Tensor<int> *w, Tensor<int> *grad, Tensor<int> *out,
                                               conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_data_device(Tensor<float> *w, Tensor<float> *grad, Tensor<float> *out,
                                               conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_data_device(Tensor<double> *w, Tensor<double> *grad, Tensor<double> *out,
                                               conv2dtranspose_cudnn_settings settings);

/**
 *  Computing the filter gradient of a transposed convolution is the
 *  same as normal convolution.
 *  computes the gradient of the tensor dy, where y is the output of
 *  the conv2dtranspose_device function.
 *  the output is the update weight tensor.
 */
template <typename T>
void conv2dtranspose_grad_filter_device(Tensor<T> *x, Tensor<T> *grad, Tensor<T> *out,
                                        conv2dtranspose_cudnn_settings settings) {
    T alpha = static_cast<T>(1), beta = static_cast<T>(0);
    cudnnErrchk(cudnnConvolutionBackwardFilter(
        ::magmadnn::internal::MAGMADNN_SETTINGS->cudnn_handle, &alpha, grad->get_cudnn_tensor_descriptor(),
        grad->get_ptr(), x->get_cudnn_tensor_descriptor(), x->get_ptr(), settings.conv_desc,
        settings.bwd_filter_algo, settings.grad_filter_workspace, settings.grad_filter_workspace_size, &beta,
        settings.filter_desc, out->get_ptr()));
}
template void conv2dtranspose_grad_filter_device(Tensor<int> *x, Tensor<int> *grad, Tensor<int> *out,
                                                 conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_filter_device(Tensor<float> *x, Tensor<float> *grad, Tensor<float> *out,
                                                 conv2dtranspose_cudnn_settings settings);
template void conv2dtranspose_grad_filter_device(Tensor<double> *x, Tensor<double> *grad, Tensor<double> *out,
                                                 conv2dtranspose_cudnn_settings settings);
#endif
#endif
}  // namespace math
}  // namespace magmadnn