#include "math/elu.h"

#include <cassert>

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif

namespace magmadnn {
namespace math {

template <typename T>
void elu(Tensor<T> *x, double a, Tensor<T> *out) {
    assert(T_IS_SAME_MEMORY_TYPE(x, out));

    if (out->get_memory_type() == HOST) {
        T *x_ptr = x->get_ptr();
        T *out_ptr = out->get_ptr();
        unsigned int size = out->get_size();

        for (unsigned int i = 0; i < size; i++) {
            out_ptr[i] = (x_ptr[i] > static_cast<T>(0)) ? x_ptr[i] : static_cast<T>(a * exp(x_ptr[i] - 1));
        }
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        fprintf(stderr, "For GPU elu please use elu_device\n");
    }
#endif
}
template void elu(Tensor<int> *x, double a, Tensor<int> *out);
template void elu(Tensor<float> *x, double a, Tensor<float> *out);
template void elu(Tensor<double> *x, double a, Tensor<double> *out);

template <typename T>
void elu_grad(Tensor<T> *x, Tensor<T> *elu_out, double a, Tensor<T> *grad, Tensor<T> *out) {
    assert(T_IS_SAME_MEMORY_TYPE(x, grad) && T_IS_SAME_MEMORY_TYPE(grad, out));

    if (out->get_memory_type() == HOST) {
        T *x_ptr = x->get_ptr();
        T *grad_ptr = grad->get_ptr();
        T *out_ptr = out->get_ptr();
        unsigned int size = out->get_size();

        for (unsigned int i = 0; i < size; i++) {
            out_ptr[i] = (x_ptr[i] > static_cast<T>(0)) ? grad_ptr[i] : static_cast<T>(a * exp(x_ptr[i]));
        }
    }
#if defined(MAGMADNN_HAVE_CUDA)
    else {
        fprintf(stderr, "For GPU elu_grad please use elu_grad_device\n");
    }
#endif
}
template void elu_grad(Tensor<int> *x, Tensor<int> *elu_out, double a, Tensor<int> *grad, Tensor<int> *out);
template void elu_grad(Tensor<float> *x, Tensor<float> *elu_out, double a, Tensor<float> *grad, Tensor<float> *out);
template void elu_grad(Tensor<double> *x, Tensor<double> *elu_out, double a, Tensor<double> *grad, Tensor<double> *out);

#if defined(MAGMADNN_HAVE_CUDA)
template <typename T>
void elu_device(Tensor<T> *x, Tensor<T> *out, elu_cudnn_settings_t settings) {
    T alpha = static_cast<T>(1);
    T beta = static_cast<T>(0);
    cudnnErrchk(cudnnActivationForward(settings.handle, settings.descriptor, &alpha, x->get_cudnn_tensor_descriptor(),
                                       x->get_ptr(), &beta, out->get_cudnn_tensor_descriptor(), out->get_ptr()));
}
template void elu_device(Tensor<int> *x, Tensor<int> *out, elu_cudnn_settings_t settings);
template void elu_device(Tensor<float> *x, Tensor<float> *out, elu_cudnn_settings_t settings);
template void elu_device(Tensor<double> *x, Tensor<double> *out, elu_cudnn_settings_t settings);

template <typename T>
void elu_grad_device(Tensor<T> *x, Tensor<T> *elu_out, Tensor<T> *grad, Tensor<T> *out,
                      elu_cudnn_settings_t settings) {
    T alpha = static_cast<T>(1);
    T beta = static_cast<T>(0);
    cudnnErrchk(cudnnActivationBackward(
        settings.handle, settings.descriptor, &alpha, elu_out->get_cudnn_tensor_descriptor(), elu_out->get_ptr(),
        grad->get_cudnn_tensor_descriptor(), grad->get_ptr(), x->get_cudnn_tensor_descriptor(), x->get_ptr(), &beta,
        out->get_cudnn_tensor_descriptor(), out->get_ptr()));
}
template void elu_grad_device(Tensor<int> *x, Tensor<int> *elu_out, Tensor<int> *grad, Tensor<int> *out,
                               elu_cudnn_settings_t settings);
template void elu_grad_device(Tensor<float> *x, Tensor<float> *elu_out, Tensor<float> *grad, Tensor<float> *out,
                               elu_cudnn_settings_t settings);
template void elu_grad_device(Tensor<double> *x, Tensor<double> *elu_out, Tensor<double> *grad, Tensor<double> *out,
                               elu_cudnn_settings_t settings);
#endif

}  // namespace math
}  // namespace magmadnn
