/**
 * @file tensormatmul.cpp
 * @author Stephen Qiu
 * @version 0.1
 * @date 2021-07-15
 *
 * @copyright Copyright (c) 2021
 */
#include "math/tensormatmul.h"

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "math/wrappers.h"

namespace magmadnn {
namespace math {

template <>
void tensormatmul(float alpha, bool trans_A, Tensor<float> *A, bool trans_B, Tensor<float> *B, float beta, Tensor<float> *C/*, unsigned int c, unsigned int M,
                    unsigned int N, unsigned int K*/) {
    // unsigned int ldda, lddb, lddc;
    unsigned int M, N, K, Q, ldda, lddb, lddc;

    /* op(A) : QxMxK ; op(B) : KxN ; C : QxMxN */

    Q = C->get_shape(0); 
    M = C->get_shape(1);                 /* rows of C and op(A) */
    N = C->get_shape(2);                 /* columns of C and op(B) */
    K = A->get_shape((trans_A) ? 1 : 2); /* columns of op(A) and rows of op(B) */
    ldda = (trans_A) ? M : K;            /* leading dimension of op(A) */
    lddb = (trans_B) ? K : N;            /* leading dimension of op(B) */
    lddc = N;

    // A: MxK  B: KxN  C: MxN
    // (MxR)(RxN) + (MxN) = (MxN) + (MxN) = (MxN)
   
    if (A->get_memory_type() == HOST) {

        // Assuming row-major storage
        operation a_trans = (trans_A) ? OP_T : OP_N;
        operation b_trans = (trans_B) ? OP_T : OP_N;

        for(unsigned int i = 0; i < Q; i++){

        gemm(b_trans, a_trans, N, M, K, alpha, B->get_ptr(), lddb, A->get_ptr() + i * M * N, ldda, beta, C->get_ptr() + i * M * N, lddc);
        
        }
    }
/*#if defined(MAGMADNN_HAVE_CUDA)
    else {
        // since magma is column-major we'll need the transpose of everything
        // i.e. (AB)^T = (C)^T and the fact that (AB)^T = (B^T)(A^T)

        if (C->get_cublas_handle()) {
            cublasOperation_t a_trans = (trans_A) ? CUBLAS_OP_T : CUBLAS_OP_N;
            cublasOperation_t b_trans = (trans_B) ? CUBLAS_OP_T : CUBLAS_OP_N;

            cublasSgemm(C->get_cublas_handle(), b_trans, a_trans, N, M, K, &alpha, B->get_ptr(), lddb, A->get_ptr(),
                        ldda, &beta, C->get_ptr(), lddc);
        } else {
            magma_trans_t a_trans = (trans_A) ? MagmaTrans : MagmaNoTrans;
            magma_trans_t b_trans = (trans_B) ? MagmaTrans : MagmaNoTrans;

            MAGMA_SGEMM_ROWMAJOR(A->get_ptr(), B->get_ptr(), C->get_ptr(), M, N, K, alpha, beta, a_trans, b_trans, ldda,
                                 lddb, lddc);
        }
        // TODO: CUDA error management
    }
#endif
*/
}
}  //namespace math
}  //namespace magmadnn