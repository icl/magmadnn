/**
 * @file gaussian2d.cpp
 * @author Spencer Smith
 * @brief the cpp file for computing the gaussian function
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "math/gaussian2d.h"
#include <cassert>

namespace magmadnn {
namespace math {

    /*
        f(x, y) = Aexp[-[(x - x0)^2 / 2gammax^2 + (y - y0)^2 / 2gammay^2]]

        A = 1; gammax = gammay = 3; (x, y) are the bg_pixel; (x0, y0) are the fg_pixel
    */
    template <typename T>
    T gaussian2d(std::vector<unsigned int>* fg_pixel, std::vector<unsigned int>* bg_pixel) {

        /* the height of the bell curve */
        T A = static_cast<T>(1.0);
        /* the x and y spread of the bell */
        T gammax = static_cast<T>(3.0);
        T gammay = static_cast<T>(3.0);

        auto x = static_cast<T>(bg_pixel->at(0));
        auto y = static_cast<T>(bg_pixel->at(1));
        auto x0 = static_cast<T>(fg_pixel->at(0));
        auto y0 = static_cast<T>(fg_pixel->at(1));

        auto first = ((pow((x - x0), 2)) / (2 * pow(gammax, 2)));
        //printf("First = %f\n", first);
        auto second = (pow((y - y0), 2) / (2 * pow(gammay, 2)));
        //printf("Second = %f\n", second);
        auto power = -(first + second);
        //printf("Power = %f\n", power);
        auto result = A * exp(power);

        return result;
    }
    template int gaussian2d(std::vector<unsigned int>* fg_pixel, std::vector<unsigned int>* bg_pixel);
    template float gaussian2d(std::vector<unsigned int>* fg_pixel, std::vector<unsigned int>* bg_pixel);
    template double gaussian2d(std::vector<unsigned int>* fg_pixel, std::vector<unsigned int>* bg_pixel);

} // namespace math
} // namespace magmadnn