#include "math/lstm.h"

namespace magmadnn {
namespace math {

#if defined(MAGMADNN_HAVE_CUDA)

template <typename T>
void lstm_device(Tensor<T> *x, Tensor<T> *y, Tensor<T> *hx, Tensor<T> *hy, Tensor<T> *cx, Tensor<T> *cy, Tensor<T> *weights_t, lstm_cudnn_settings settings) {
	cudnnErrchk(cudnnRNNForward(
		settings.handle, settings.rnnDesc, CUDNN_FWD_MODE_TRAINING, 
		settings.devSeqLengthArray, settings.xDesc, x->get_ptr(), settings.yDesc, y->get_ptr(), 
		settings.hDesc, hx->get_ptr(), hy->get_ptr(), settings.cDesc, cx->get_ptr(),
		cy->get_ptr(), settings.weightSpaceSize, weights_t->get_ptr(), settings.workSpaceSize, 
        settings.workSpace,settings.reserveSpaceSize, settings.reserveSpace));

}

template void lstm_device(Tensor<float> *x, Tensor<float> *y, Tensor<float> *hx, 
		Tensor<float> *hy, Tensor<float> *cx, Tensor<float> *cy, Tensor<float> *weights_t, lstm_cudnn_settings settings);

template <typename T>
void lstm_grad_data_device(Tensor<T> *x, Tensor<T> *y, Tensor<T> *dy, Tensor<T> *dx, Tensor<T> *hx, Tensor<T> *dhy, Tensor<T> *dhx, 
		Tensor<T> *cx, Tensor<T> *dcy, Tensor<T> *dcx, Tensor<T> *weights_t, lstm_cudnn_settings settings){

    cudnnErrchk(cudnnRNNBackwardData_v8(
		settings.handle, settings.rnnDesc, settings.devSeqLengthArray, 
		settings.yDesc, y->get_ptr(), dy->get_ptr(), settings.xDesc, dx->get_ptr(), settings.hDesc,
		hx->get_ptr(), dhy->get_ptr(), dhx->get_ptr(), settings.cDesc, cx->get_ptr(), dcy->get_ptr(),
		dcx->get_ptr(), settings.weightSpaceSize, weights_t->get_ptr(), settings.workSpaceSize,
		settings.workSpace, settings.reserveSpaceSize, settings.reserveSpace));
}

template void lstm_grad_data_device(Tensor<float> *x, Tensor<float> *y, Tensor<float> *dy, Tensor<float> *dx, Tensor<float> *hx,
	       	Tensor<float> *dhy, Tensor<float> *dhx, Tensor<float> *cx, Tensor<float> *weights_t, Tensor<float> *dcy, 
		    Tensor<float> *dcx, lstm_cudnn_settings settings);

template <typename T>
void lstm_grad_data_device_weights(Tensor<T> *x, Tensor<T> *y, Tensor<T> *hx, lstm_cudnn_settings settings){
    cudaErrchk(cudaMemset(/*dweights_t->get_ptr()*/settings.dweightSpace, 0, settings.weightSpaceSize));
    
    cudnnErrchk(cudnnRNNBackwardWeights_v8(
    settings.handle,
    settings.rnnDesc,
    CUDNN_WGRAD_MODE_ADD,
    settings.devSeqLengthArray,
    settings.xDesc,
    x->get_ptr(),
    settings.hDesc,
    hx->get_ptr(),
    settings.yDesc,
    y->get_ptr(),
    settings.weightSpaceSize,
    settings.dweightSpace/*dweights_t->get_ptr()*/,
    settings.workSpaceSize,
    settings.workSpace,
    settings.reserveSpaceSize,
    settings.reserveSpace));
 
}

template void lstm_grad_data_device_weights(Tensor<float> *x, Tensor<float> *y, Tensor<float> *hx, lstm_cudnn_settings settings);

#endif
}
}
