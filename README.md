# MagmaDNN

A neural network library in c++ aimed at providing a simple, modularized framework for deep learning that is accelerated for heterogeneous architectures. MagmaDNN's releases are located at [https://bitbucket.org/icl/magmadnn](https://bitbucket.org/icl/magmadnn) (and [here](https://icl.cs.utk.edu/magma/)).

__Latest Version__ 1.6.0

In version 1.0 MagmaDNN offers a strong tensor core with standard machine learning and DNN functionalities built around it.

Version 1.6.0 now offers support for CuDNN LSTM and Attention operations.

MagmaDNN is optimized towards heterogeneous architectures (multi-core CPU and GPU), so it is advised to use with a modern NVIDIA GPU. However, MagmaDNN does support a CPU only install. This is mainly meant for testing and is not nearly as optimized as the GPU version.


The documentation can be found on the [docs site](https://magmadnn.github.io/magmadnn/html). For the most recent version of the documentation see the [build & install tutorial](./Install.md) on how to build the docs from source. The [todo page](./docs/todo.md) contains information on the future of the package and the [troubleshooting page](./docs/troubleshooting.md) walks through common issues and there solution.


### Tutorials
-------------
There are several tutorials in [docs/tutorials](./docs/tutorials). These give an introduction into installing and using the library.


### Examples
-----------
For examples of what MagmaDNN code looks like see the [examples/ folder](./examples). If MagmaDNN is downloaded and installed, then the examples can be made and run with `make examples`.

### Development Activity
-----------------------

_current contributers:_ Stephen Qiu, Julian Halloy, Pierluigi Cambie-Fabris, Kwai Wong, Stanimire Tomov

_previous contributers:_ Daniel Nichols, Florent Lopez, Sedrick Keh, Rocco Febbo, Jerry Ho, Joshua Zingale, Edward Karak, Spencer Smith, Nicole Chow