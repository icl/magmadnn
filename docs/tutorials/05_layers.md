## Tutorial 05: Layers
-----------------------------
Layers are important building blocks to creating a neural network in MagmaDNN.
Layers take an operation as input and produce and operation as output.
Each layer contains an operation within it along with trainable weights that are adjusted while a model.

Each layer is created differently based on its constructor.

Below are examples of the creation of some commonly used layers:

```c++
auto input = layer::input(x_batch); /* This creates an input layer */
auto fc = layer::fullyconnected(input->out(), 10); /* This creates a fully connected layer with 10 nodes */
auto act = layer::activation(fc->out(), layer::SOFTMAX); /* This creates an activation layer using softmax */
auto output = layer::output(act->out()); /* This creates an output layer */
```
The input and output layers are especially important since any neural network model begin and end with them respectively. Input will take your x_batch as input and Output will take you the output of your final layer as input.


To see more supported layers and their respective documentation build the docs as shown in [tutorial 00](/docs/tutorials/00_installing.md).



