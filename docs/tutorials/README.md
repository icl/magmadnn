## Tutorials
-------------------------

#### Overview

In this folder are tutorials walking you through the installation and basic components of the MagmaDNN framework.
Each tutorial goes through a different topic and had code within it that you can follow and run.
The fully completed runable code is in the `tutorial_samples` directory. The code might be adapted so it can run and produce output.

#### Tutorial Samples

To run the examples, run `make` in the `tutorial_samples` directory and the run the executables. To run 06_neural_network, you will need to first download the MNIST dataset which can be done by running `sh MNIST-download.sh`
