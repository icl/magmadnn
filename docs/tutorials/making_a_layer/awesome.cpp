/**
 * @file awesome.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 0.1
 * @date 2022-06-27
 */
#include"layer/awesome/awesome.h"
#include "tensor/tensor_io.h"
namespace magmadnn {
namespace layer {
	
	template<typename T>
	AwesomeLayer<T>::AwesomeLayer(op::Operation<T> *input):Layer<T>::Layer(input->get_output_shape(), input) {		
		init();
	}

	template<typename T>
	AwesomeLayer<T>::~AwesomeLayer() {
		delete this->weight;
	}
	
	template<typename T>
	std::vector<op::Operation<T> *> AwesomeLayer<T>::get_weights() {
		return {this->weight};
	}
	
	template<typename T>
	void AwesomeLayer<T>::init() {
		this->name = "AwesomeLayer";

		this->wTensor = new Tensor<T>({this->input->get_output_shape()[1],this->input->get_output_shape()[1]},
				{UNIFORM, {(T)0,(T)1}}, HOST);
		
		this->weight = op::var("__" + this->name + "_layer_weight", wTensor);

		this->output = op::matmul(this->input, this->weight);
		this->output->eval();

	}
	
	template<typename T>
	unsigned int AwesomeLayer<T>::get_num_params() {return this->weight->get_output_size();}

	template class AwesomeLayer<int>;
	template class AwesomeLayer<float>;
	template class AwesomeLayer<double>;

	template<typename T>
	AwesomeLayer<T> *awesome(op::Operation<T> *input) {
		return new AwesomeLayer<T>(input);
	}

	template AwesomeLayer<int> *awesome(op::Operation<int> *input);
	template AwesomeLayer<float> *awesome(op::Operation<float> *input);
	template AwesomeLayer<double> *awesome(op::Operation<double> *input);
}
}
