# How to Make a MagmaDNN Layer

This tutorial will walk through how to build a simple layer within the MagmaDNN framework. 
The layer in this tutorial will take a two-dimensional input *X* and then output *XW*, where
*W* is a learned weight matrix: it shall be named *AwesomeLayer*.

Only two files will need to be created (*awesomelayer.h* and *awesomelayer.cpp*) and one will 
need to be modified (*layers.h*) to implement the working layer. Additionally, one file will
be used to test the layer. The files should be placed thus:

*  magmadnn/include/layer/layers.h
*  magmadnn/include/layer/awesome/awesome.h
*  magmadnn/src/layer/awesome/awesome.cpp

The different files created in this tutorial are available in this repository.

*awesomelayer.h*
--

First, we shall create *awesome.h* inside */include/layer/awesome/*.

*awesomelayer.h* should begin like so:
```

#pragma once
#include "tensor/tensor.h"
#include "layer/layer.h"
#include "compute/operation.h"
#include "compute/tensor_operations.h"

namespace magmadnn {
namespace layer {
...
// BODY HERE
...
}
}
```

*#pragma once* is a preprocessor instruction that prevents the compiler from including the
same header twise. *tensor.h* contains the *Tensor* type; contained in *layer.h* is the *Layer*
super class; within *operation.h* is the *Operation* abstract class; *tensor_operations.h* contains
the *matmul()* operation, which will be used in the .cpp file. 

The two namespaces are present by convention. This places our layer within the namespace 
*magmadnn::layer*, as all layers are.

The body of the code, inside the namespaces, should contain this:
```

	template<typename T>
	class AwesomeLayer: public Layer<T> {
	
		public:
			AwesomeLayer(op::Operation<T> *input);
			virtual ~AwesomeLayer();

			std::vector<op::Operation<T> *> get_weights();

			unsigned int get_num_params();


		protected:
			void init();

			Tensor<T> *wTensor;

			op::Operation<T> *weight;

	};


	template<typename T>
	AwesomeLayer<T> *awesome(op::Operation<T> *input);
```

All layers should be templated to work with three data types--*int*, *float*, and *double*--
and should descend from the *Layer* class. The first two publicly declared members of the
layer are the constuctor and deconstructor. The next member, get_weights(), must be declared
and defined in every created layer because the super class *Layer* so specifies. The last
public member is *get_num_params()*.

The first protected member, *init()*, is not necessary, but present by convention. The
second protected member will be discussed later and is necessary; it will contain, in
*Tensor* form, the internal weight matrix for the layer. The final protected
member will contain the internal weight matrix as an *Operation*.

At the bottom is defined one function, *awesome()*. By convention, all layers have such function 
associated therewith, that it calls the constructor of the layer and returns a pointer to
the newly created object.

*awesomelayer.cpp*
--

Next, we shall create *awesomelayer.cpp* inside */src/layer/awesomelayer/awesomelayer.cpp*.

The code therein should begin thus:

```

#include"layer/awesome/awesome.h"
namespace magmadnn {
namespace layer {
...
// BODY HERE
...
}
}
```

All which need be included are in the header file. So, we only need to include *awesome.h*.
Just as in the header file, the code, by MagmaDNN convention, should be within the namespace
*magmadnn::layer*.

In the body of the code, which is inside the namespaces, we define the members which were
declared in *awesome.h*. The beginning of the body shoud look like this:

```

template<typename T>
	AwesomeLayer<T>::AwesomeLayer(op::Operation<T> *input):Layer<T>::Layer(input->get_output_shape(), input) {		
		init();
	}

	
	template<typename T>
	void AwesomeLayer<T>::init() {
		this->name = "AwesomeLayer";

		this->wTensor = new Tensor<T>({this->input->get_output_shape()[1],this->input->get_output_shape()[1]},
				{UNIFORM, {(T)0,(T)1}}, HOST);
		
		this->weight = op::var("__" + this->name + "_layer_weight", wTensor);

		this->output = op::matmul(this->input, this->weight);
		this->output->eval();

	}
        template<typename T>
	AwesomeLayer<T>::~AwesomeLayer() {
		delete this->weight;
	}
    ... // More code in next block
```

By convention, layer constructors do nothing but call an initialization function--in this case,
*init()*. Moreover, layer constructors use a syntax to allow for the superclass to be initialized
and certain class members to be defined before the constructor code ever runs. This is done by
placed a colon after the argument list's closing parenthesis. Read more about that here--
https://stackoverflow.com/a/2785639.

Inside *init()*--or, more generally, inside the constructor of a layer in MagmaDNN--the name of
the layer must be defined and the the output of the layer must be defined. All else is 
implementation-specific.

The former is acomplished by setting the field *this->name* to be "AwesomeLayer" and the latter
is acomplished by setting the field *this->output* to be equal to a *matmul()* operation and
then evaluating said operation. Some operation classes in MagmaDNN, such as *op::LinearForwardOp*,
implicitely evaluate themselves. However, some, such as *op::MatmulOp*, do not and will cause
an error when building a model if not evaluated through *.eval()* in the constructor.

*wTensor* is defined as a building block for the *matmul()* operation. *wTensor* is passed into
*op::var()*, which in turn returns an *Operation* that we save in field *this->weights*. From
there, *this->weights* and input into the layer are the arguments for the *matmul()* operation.

This is an important note: although *wTensor* never needs to be directly accessed, as it is
used only to construct the *weights* operation, *wTensor* still must be saved as a field in
the class. If *wTensor* were not saved as a field for the class, the memory for it would be
freed once the *AwesomeLayer* constructor exits and the *weights* *Operation* would break, causing the
*output* to break, in a hard to debug way.

Still within the body, the following code should also be present:

```

	template<typename T>
	std::vector<op::Operation<T> *> AwesomeLayer<T>::get_weights() {
		return {this->weight};
	}
	
	template<typename T>
	unsigned int AwesomeLayer<T>::get_num_params() {return this->weight->get_output_size();}

	template class AwesomeLayer<int>;
	template class AwesomeLayer<float>;
	template class AwesomeLayer<double>;

	template<typename T>
	AwesomeLayer<T> *awesome(op::Operation<T> *input) {
		return new AwesomeLayer<T>(input);
	}

	template AwesomeLayer<int> *awesome(op::Operation<int> *input);
	template AwesomeLayer<float> *awesome(op::Operation<float> *input);
	template AwesomeLayer<double> *awesome(op::Operation<double> *input);

```

The *get_weights()* method must be defined within every class that descends from *Layer*. It should
return all of the trainable "Operation" objects that are used to compute the output for the layer--in 
this case used to calculate the output for *AwesomeLayer*--inside a C++ vector. Note that any
*Operation* objects returned by *get_weights()* must appear in the calclulation for *this->output*.
This is because, to calculate gradients, the *Operation* objects returned by *get_weights()* are
sought within the compute tree built by the *Operation* objects fed through the *this->output* field
for each layer of a model. If an *Operation* returned by *get_weights()* is not contained in the compute
tree, a hard to debug error will occur in the training process of a model.

The *get_num_params()* function should return the number of trainable parameters in the layer.

Then, the *AwesomeLayer* class is templated to work with *int*, *float*, and *double* types.

Finally, by convention, we define a function that calls the layer's constructor and returns a pointer
to the newly created object, also templating the function to work with int*, *float*, and *double* types.

layers.h
--

Now, we must add one line to *include/layer/layers.h*. The line of code will add our new layer to the
list of layers to include. Place
```
#include"awesome/awesome.h"
```

inside *layers.h*.


Compilation
--

Now that all of the files are in created, modified, and in their correct location, we must recompile MagmaDNN.
To recompile MagmaDNN, navigate to the MagmaDNN home directory, */magmadnn/*, inside 
of which are */include* and */src/*. Now, open a command line in the same directory and run *make*. (To greately 
speed up compilation, add the flag -j2 if you have two CPU cores, -j6 if you have six cores, etc..) After the
compilation finishes, run the command *sudo make install*.



Testing the Layer
--

Having successfully created *AwesomeLayer*, we should now test the layer. In whatever directory is advantageous,
ideally an empty one, create a .cpp file. My file shall be named *first_nn.cpp*.

In this program, we shall first build a model with three layers: *InputLayer*, *AwesomeLayer*, *OutputLayer*, 
in that order. We shall then train it to multiply a one number input by two. To accomplish this, we shall
generate some training data by first creating a Tensor of inputs and then setting the outputs to be twise the
inputs.

Here is the code with some comments to explain each step of the process:

```
#include "magmadnn.h"
using namespace magmadnn;

int main() {
	
	magmadnn_init();

	//set model parameters
	model::nn_params_t params;
	params.batch_size = 100;
	params.n_epochs = 2;
	params.learning_rate = 0.5;
	
	/*
	 * create empty batch with the dimensions bathc_size x 1
	 * Note that the second dimension is 1 because this is 
	 * assuming that each test value has one input
	 */
	op::Variable<float> *x_batch = op::var<float>("x_batch", {params.batch_size, 1},
			{NONE, {}}, HOST);

	/* constructs a model that has three layers: 
	 * input->awesome->output
	 */
	layer::InputLayer<float> *input = layer::input(x_batch);
	layer::AwesomeLayer<float> *fc = layer::awesome(input->out());
	layer::OutputLayer<float> *output = layer::output(fc->out());

	//vector of layers to pass into model 
	std::vector<layer::Layer<float> *> layers = {input, fc, output};

	//create model
	model::NeuralNetwork<float> sqModel(layers, optimizer::MSE,
			optimizer::SGD, params);
	
	//create test 10000 X training values
	Tensor<float> X({10000,1}, {UNIFORM, {-1.f,1.f}}, HOST);

	//make a variable with the training value tensor
	op::Variable<float> *X_var = op::var<float>("X", &X);


	//creates Y training values. Here we are training the model to multiply by 2 
	//thus it takes 2.f as a parameter
	op::Operation<float> *Y_ptr = op::scalarproduct(2.f,X_var);
	
	//Important to evaluate operation so the operation tensor has correct values
	Y_ptr->eval();
	
	//prints summary of mdoel
	sqModel.summary();

	//needed for fit()
	model::metric_t metric_out;

	//fit model
	sqModel.fit(&X, Y_ptr->get_output_tensor(), metric_out, true);

	//creates X values to test
	Tensor<float> X_test ({5, 1}, {UNIFORM, {-1.f,1.f}}, HOST);
	io::print_tensor(X_test);
	

	//create tensor of prediction based on test values
	Tensor<float> *Y_pred = sqModel.predict(&X_test);
	//note that print_tensor prints out # of values equal to batch size so
	//only observe the first 5 values (size of X_test)
	io::print_tensor(*Y_pred);

	


	magmadnn_finalize();
	return 0;
}
```

Once the the .cpp file has been saved, it should be compiled thus:


g++ -o first_nn first_nn.cpp -I/path-to-magma/include -I/path-to-magmadnn/include -L/path-to-magma/lib -L/path-to-magmadnn/lib -lmagmadnn -DMAGMADNN_HAVE_CUDA


Running the above code, the first five values of the second output tensor should be almost exactly twice the values of the first tensor (ignore the values after the first 5 in the second tensor). 
If not, something is likely faulting durring the training process.

Happy coding.

*This tutorial was created by Joshua Zingale and Pierluigi Cambie-Fabris for the University of Tennessee REU in 2022*