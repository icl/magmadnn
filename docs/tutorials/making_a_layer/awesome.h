/**
 * @file awesome.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 0.1
 * @date 2022-06-27
 */
/*
 * A testing layer, designed for didactic purposes
 * by Joshua Zingale and Pierluigi Cambie-Fabris.
 *
 * It functions as a FullyConnected layer, only it
 * does not have a bias. This is to say, it takes
 * a two-dimensional input and then multiplies
 * it by a two-dimensional weight matrix, resulting
 * in the output.
 */

#pragma once
#include "tensor/tensor.h"
#include "layer/layer.h"
#include "compute/operation.h"
#include "compute/tensor_operations.h"

namespace magmadnn {
namespace layer {

	template<typename T>
	class AwesomeLayer: public Layer<T> {
	
		public:
			AwesomeLayer(op::Operation<T> *input);
			virtual ~AwesomeLayer();

			std::vector<op::Operation<T> *> get_weights();

			unsigned int get_num_params();


		protected:
			void init();

			Tensor<T> *wTensor;

			op::Operation<T> *weight;

	};


	template<typename T>
	AwesomeLayer<T> *awesome(op::Operation<T> *input);

}
}


