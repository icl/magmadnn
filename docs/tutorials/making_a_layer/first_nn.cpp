/**
 * @file first_nn.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 0.1
 * @date 2022-06-27
 */

/*
 * first_nn.cpp
 * simple use of the Awesome layer to teach a machine how to multiply
 * by a constant value (in this case 2.f)
 * 
 * by Pierluigi Cambie-Fabris & Joshua Zingale
 */
#include "magmadnn.h"
using namespace magmadnn;

int main() {
	
	magmadnn_init();

	//set model parameters
	model::nn_params_t params;
	params.batch_size = 100;
	params.n_epochs = 2;
	params.learning_rate = 0.5;
	
	/*
	 * create empty batch with the dimensions bathc_size x 1
	 * Note that the second dimension is 1 because this is 
	 * assuming that each test value has one input
	 */
	op::Variable<float> *x_batch = op::var<float>("x_batch", {params.batch_size, 1},
			{NONE, {}}, HOST);

	/* constructs a model that has three layers: 
	 * input->awesome->output
	 */
	layer::InputLayer<float> *input = layer::input(x_batch);
	layer::AwesomeLayer<float> *fc = layer::awesome(input->out());
	layer::OutputLayer<float> *output = layer::output(fc->out());

	//vector of layers to pass into model 
	std::vector<layer::Layer<float> *> layers = {input, fc, output};

	//create model
	model::NeuralNetwork<float> sqModel(layers, optimizer::MSE,
			optimizer::SGD, params);
	
	//create test 10000 X training values
	Tensor<float> X({10000,1}, {UNIFORM, {-1.f,1.f}}, HOST);

	//make a variable with the training value tensor
	op::Variable<float> *X_var = op::var<float>("X", &X);


	//creates Y training values. Here we are training the model to multiply by 2 
	//thus it takes 2.f as a parameter
	op::Operation<float> *Y_ptr = op::scalarproduct(2.f,X_var);
	
	//Important to evaluate operation so the operation tensor has correct values
	Y_ptr->eval();

	
	
	
	//prints summary of mdoel
	sqModel.summary();

	//needed for fit()
	model::metric_t metric_out;

	//fit model
	sqModel.fit(&X, Y_ptr->get_output_tensor(), metric_out, true);

	//creates X values to test
	Tensor<float> X_test ({5, 1}, {UNIFORM, {-1.f,1.f}}, HOST);
	io::print_tensor(X_test);
	

	//create tensor of prediction based on test values
	Tensor<float> *Y_pred = sqModel.predict(&X_test);
	//note that print_tensor prints out # of values equal to batch size so
	//only observe the first 5 values (size of X_test)
	io::print_tensor(*Y_pred);

	


	magmadnn_finalize();
	return 0;
}

