#include <cstdio>
#include "magmadnn.h"

using namespace magmadnn;

int main() {
    magmadnn_init();

    /* first, initialize the variables */
    op::Operation<float> *w = op::var<float>("W", {10,5}, {UNIFORM, {0.0f, 1.0f}}, HOST);

    /* the auto keyword is helpful when initializing operations */
    auto x = op::var<float>("X", {5,6}, {UNIFORM, {0.0f, 1.0f}}, HOST);
    auto b = op::var<float>("B", {10,6}, {UNIFORM, {0.0f, 1.0f}}, HOST);
    

    std::cout << "W tensor:\n";
    io::print_tensor(w->get_output_tensor());


    std::cout << "X tensor:\n";
    io::print_tensor(x->get_output_tensor());

    std::cout << "B tensor:\n";
    io::print_tensor(b->get_output_tensor());


    std::cout << "\nWe represent the expression WX + B with the compute graph:\n\n";
    std::cout << "         add\n       /     \\\n    matmul    B\n   /    \\\n  W      X\n\n";

    /* this constructs the compute graph */
    auto out = op::add(op::matmul(w,x), b);

    Tensor<float> *out_tensor = out->eval();

    std::cout << "This evaluates to:\n";
    io::print_tensor(out_tensor);

    magmadnn_finalize();
}

