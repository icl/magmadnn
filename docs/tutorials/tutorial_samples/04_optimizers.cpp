#include <cstdio>
#include "magmadnn.h"

using namespace magmadnn;

int main() {
    magmadnn_init();

    auto x = op::var<double> ("x", {1}, {CONSTANT, {5.0}}, HOST);
    auto c = op::var<double> ("c", {1}, {CONSTANT, {10.0}}, HOST);
    auto expr = op::add(op::pow(x,2), c);
    expr->eval();
    std::cout << "Here, we use Stochastic Gradient Descent to minimize the expression 'x^2 + c' w.r.t variable 'x' (initially x = 5, c = 10)\n\n";
    std::cout << "Expression before optimization:\n";
    io::print_tensor(expr->get_output_tensor());
    /* create our optimizer */
    double learning_rate = 0.05;
    double momentum = 0.0;
    optimizer::GradientDescent<double> optim (learning_rate, momentum);

    unsigned int n_iter = 100;
    for (unsigned int i = 0; i < n_iter; i++) {
        optim.minimize(expr, {x});
    }
    expr->eval();
    std::cout << "\nExpression after optimization:\n";
    io::print_tensor(expr->get_output_tensor());
    magmadnn_finalize();
}
