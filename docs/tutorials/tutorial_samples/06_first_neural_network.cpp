#include <cstdio>
#include "magmadnn.h"

using namespace magmadnn;

int main() {
    magmadnn_init();

    model::nn_params_t params;
    params.batch_size = 100;    /* batch size: the number of samples to process in each mini-batch */
    params.n_epochs = 50;    /* # of epochs: the number of passes over the entire training set */
    params.learning_rate = 0.05;    /* learning rate of model */


    std::string const mnist_dir = "./Data";
    // Load MNIST trainnig dataset
    magmadnn::data::MNIST<float> train_set(mnist_dir, magmadnn::data::Train);
    magmadnn::data::MNIST<float> test_set(mnist_dir, magmadnn::data::Test);

    auto x_batch = op::var<float>(
      "x_batch",
      {params.batch_size, train_set.nchanels(),  train_set.nrows(), train_set.ncols()},
      {NONE, {}}, DEVICE);

    auto input = layer::input(x_batch);
    auto flatten = layer::flatten(input->out());
    auto fc = layer::fullyconnected(flatten->out(), 10);
    auto act = layer::activation(fc->out(), layer::SOFTMAX);
    auto output = layer::output(act->out());
    std::vector<layer::Layer<float> *> layers = {input, flatten, fc, act, output};   /* create our vector of layers */

    /* use cross entropy loss and stochastic gradient descent as an optimizer */
    model::NeuralNetwork<float> model(layers, optimizer::CROSS_ENTROPY, optimizer::SGD, params);

    model.summary();

    model::metric_t metric_out;
    bool is_verbose = true;

    /* x_train and y_train are tensors containing the data set */
    model.fit(&train_set.images(), &train_set.labels(), metric_out, is_verbose);

    magmadnn_finalize();
}

