#pragma once
#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "compute/operation.h"
// #include "compute/elu/elu_internal.h"
#include "math/elu.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace op {

template <typename T>
class EluOp : public Operation<T> {
   public:
    EluOp(Operation<T> *x, double a, bool copy = true, bool needs_grad = true);
    virtual ~EluOp();

    std::string to_string() { return "ELU( " + x->to_string() + " )"; }

   protected:
    Tensor<T> *_eval(bool recompute = true);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);

    Operation<T> *x;
    Tensor<T> *x_tensor;
    double a;

#if defined(MAGMADNN_HAVE_CUDA)
    ::magmadnn::math::elu_cudnn_settings_t cudnn_settings;
#endif

    bool copy;
};

template <typename T>
EluOp<T> *elu(Operation<T> *x, double a, bool copy = true, bool needs_grad = true);

}  // namespace op
}  // namespace magmadnn
