/**
 * @file dgetm_internal.h
 * @author Stephen Qiu
 * @version 1.0
 * @date 2021-07-15
 *
 * @copyright Copyright (c) 2021
 */
#pragma once

#include "tensor/tensor.h"

#if defined(MAGMADNN_HAVE_CUDA)
#include "magma.h"
#endif

namespace magmadnn {
namespace internal {


/** Computes the matrix product C = alpha*(AB) + beta*C
 * @tparam T
 * @param alpha
 * @param A
 * @param B
 * @param beta
 * @param C
 */
template <typename T>
void dgetm_full(T alpha, T beta, bool trans_A, bool trans_B, Tensor<T> *A, Tensor<T> *B, Tensor<T> *C);

}  // namespace internal
}  // namespace magmadnn
