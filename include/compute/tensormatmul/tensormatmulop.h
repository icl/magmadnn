/**
 * @file tensormatmul.h
 * @author Stephen Qiu
 * @version 1.0
 * @date 2021-07-14
 *
 * @copyright Copyright (c) 2021
 */
#pragma once
#include <vector>
#include "compute/dot/dotop.h"
#include "compute/operation.h"
#include "compute/transpose/transposeop.h"
#include "compute/variable.h"
#include "math/dot.h"
#include "tensor/tensor.h"
#include "compute/tensormatmul/dgetm_internal.h"


namespace magmadnn {
namespace op {

template <typename T>
class TensorMatmulOp : public Operation<T> {
   public:
    TensorMatmulOp(T alpha, Operation<T> *a, Operation<T> *b, T beta, Operation<T> *c, bool copy = true,
             bool needs_grad = true);

    std::string to_string() { return "(" + a->to_string() + " x " + b->to_string() + ")"; }

   protected:
    Tensor<T> *_eval(bool recompute = true);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);


    Operation<T> *a;
    Operation<T> *b;
    Operation<T> *c;

    Tensor<T> *a_tensor;
    Tensor<T> *b_tensor;
    Tensor<T> *c_tensor;

    T alpha;
    T beta;
    bool copy;
};


/** Computes the full gemm C = alpha*(AB) + beta*(C). Overwrites C and returns it if copy is false. If true,
 * 	then it returns a copy of C.
 * @tparam T
 * @param alpha
 * @param a
 * @param b
 * @param beta
 * @param c
 * @param copy
 * @return TensorMatmulOp<T>*
 */
template <typename T>
TensorMatmulOp<T> *tensormatmul(T alpha, Operation<T> *a, Operation<T> *b, T beta, Operation<T> *c, bool copy = true,
                    bool needs_grad = true);

}  // namespace op
}  // namespace magmadnn
