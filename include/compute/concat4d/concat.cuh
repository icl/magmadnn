/**
 * @file concat.cuh
 * @author Edward Karak
 * @version 1.0
 * @date 2022-07-08
 *
 * @copyright Copyright (c) 2022
 */
#pragma once
#include <vector>
#include "../include/tensor/tensor.h"
using namespace magmadnn;

namespace magmadnn {
namespace internal {
/** Concatenate NCHW tensors `a` and `b` on the device along the axis channel
 * @tparam T
 * @param a
 * @param b
 * @param out Output tensor. Assumes memory has been allocated for it.
 */
template <typename T>
void dev_concat4D_C(Tensor<T> *a, Tensor<T> *b, Tensor<T> *out);
}
}
