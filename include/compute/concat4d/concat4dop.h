/**
 * @file concat4dop.h
 * @author Edward Karak
 * @version 1.0
 * @date 2022-06-27
 *
 * @copyright Copyright (c) 2022
 */
#pragma once
#include <vector>
#include "compute/operation.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace op {

/**	A concatenation operation on two tensors.
 * @tparam T
 */
template <typename T>
class Concat4dOp : public Operation<T> {
   public:
    /** Creates a Concat4d Operation, which concatenates two tensors together on an axis.
     * @param a a tensor
     * @param b a tensor
     * @param axis axis on which to concatenate (0=N, 1=C, 2=H, 3=W)
     * @param copy copy into new tensor
     * @param needs_grad if this needs a gradient
     */
    Concat4dOp(Operation<T> *a, Operation<T> *b, unsigned axis, 
                bool copy = true, bool needs_grad = true);

    std::string to_string() { return "(" + a->to_string() + " & " + b->to_string() + ")"; }

   protected:
    Tensor<T> *_eval(bool recompute = true);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);

    Operation<T> *a;
    Operation<T> *b;

    Tensor<T> *a_tensor;
    Tensor<T> *b_tensor;
    unsigned axis;

    bool copy;
};

/** Returns a new Concat4d operation (@see Concat4dOp<T>).
 * @tparam T
 * @param a
 * @param b
 * @param axis Axis on which to concatenate a and b. (0=N, 1=C, 2=H, 3=W). Default is 1.
 * @param copy If copy is true then it returns a new tensor, if false then b=a+b.
 * @return Concat4dOp<T>*
 */
template <typename T>
Concat4dOp<T> *concat4d(Operation<T> *a, Operation<T> *b, unsigned axis = 1, bool copy = true, bool needs_grad = true);

/** Returns a new Concat operation (@see Concat4dOp<T>).
 * @tparam T
 * @param a
 * @param b
 * @param axis Axis on which to concatenate a and b. (0=N, 1=C, 2=H, 3=W). Default is 1.
 * @param copy If copy is true then it returns a new tensor, if false then b=a+b.
 * @return Concat4dOp<T>*
 */
template <typename T>
Tensor<T> *concat4d(const Tensor<T> *a, const Tensor<T> *b, unsigned axis = 1, bool copy = true, bool needs_grad = true);

}  // namespace op
}  // namespace magmadnn
