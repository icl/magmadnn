#pragma once

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif

#include "compute/conv2dtranspose/conv2dtranspose_internal.h"
#include "compute/operation.h"
#include "math/conv2dtranspose.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace op {

// implement class here
template <typename T>
class Conv2DTransposeOp : public Operation<T> {
   public:
    Conv2DTransposeOp(Operation<T> *input, Operation<T> *filter, int pad_h = 0, int pad_w = 0, int out_pad_h = 0,
                      int out_pad_w = 0, int vertical_stride = 1, int horizontal_stride = 1, int dilation_h = 1,
                      int dilation_w = 1, bool use_cross_correlation = true, bool needs_grad = true);
    ~Conv2DTransposeOp();

    std::string to_string() { return "Conv2DTranspose(" + input->to_string() + ")"; }

   protected:
    Tensor<T> *_eval(bool recompute);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);
    void calculate_and_set_output_shape();

#if defined(MAGMADNN_HAVE_CUDA)
    void cuda_forward();  // defined in src/math/cuda
#endif

    void init_settings();

#if defined(MAGMADNN_HAVE_MKLDNN)
    // all defined in src/math/onednn
    void onednn_backward_data(Tensor<T> *grad, Tensor<T> *out);

    void onednn_backward_weights(Tensor<T> *grad, Tensor<T> *out);

    void onednn_forward();

    void onednn_init_settings();
#endif

    Operation<T> *input, *filter;
    Tensor<T> *input_tensor, *filter_tensor;

    int pad_h, pad_w, out_pad_h, out_pad_w, vertical_stride, horizontal_stride, dilation_h, dilation_w;
    bool use_cross_correlation;

#if defined(MAGMADNN_HAVE_CUDA)
    // the settings struct defined in conv2dtranspose.h
    ::magmadnn::math::conv2dtranspose_cudnn_settings cudnn_settings;
#endif

#if defined(MAGMADNN_HAVE_MKLDNN)
    dnnl::engine dnnl_cpu_engine_;

    // Pooling DNNL primitive descriptor
    std::unique_ptr<dnnl::convolution_forward::primitive_desc> dnnl_fwd_pdesc_;

    // Pooling DNNL primitive
    std::unique_ptr<dnnl::convolution_forward> dnnl_fwd_;
#endif
};

template <typename T>
Conv2DTransposeOp<T> *conv2dtransposeop(Operation<T> *input, Operation<T> *filter, int pad_h = 0, int pad_w = 0,
                                        int out_pad_h = 0, int out_pad_w = 0, int vertical_stride = 1,
                                        int horizontal_stride = 1, int dilation_h = 1, int dilation_w = 1,
                                        bool use_cross_correlation = true, bool needs_grad = true);
}}