#pragma once

#include "compute/operation.h"
#include "math/mha.h"
#include "tensor/tensor.h"
#include "compute/mha/mha1_internal.h"

namespace magmadnn{
namespace op { 

    template <typename T>
    class MHA1Op : public Operation<T> {
        public: 
        // input: q, k, v, weights
        // MHA1Op(Operation<T> *q, Operation<T> *k, 
        //     Operation<T> *v, Operation<T> *weights, bool needs_grad = true);

        MHA1Op(Operation<T> *input, Operation<T> *weights, ::magmadnn::math::attnOptions opts, bool needs_grad = true);

        ~MHA1Op();

        std::string to_string() { return "MHA1Op"; }

        // void set_params(::magmadnn::math::attnOptions opts);

        void init_settings(::magmadnn::math::attnOptions attention_options); // init mha_settings

        void print_params();

        void save_data(const char *fName, int batch, int beam, int nDims, int dimA[4], cudnnSeqDataAxis_t ordA[4], T *dataBuf);

        protected: 
        Tensor<T> *_eval(bool recompute);
        Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);
        
        void separate_tensor();
        
        #if defined(MAGMADNN_HAVE_CUDA)
                void cuda_forward();
        #endif

        Tensor<T> *input_t;
        Tensor<T> *q_t;
        Tensor<T> *k_t;
        Tensor<T> *v_t;
        Tensor<T> *o_t;
        Tensor<T> *w_t;

        Tensor<T> *dq_t;
        Tensor<T> *dk_t;
        Tensor<T> *dv_t;
        Tensor<T> *do_t;
        Tensor<T> *dw_t;            
        
        Operation<T> *input;
        Operation<T> *q;
        Operation<T> *k;
        Operation<T> *v;
        Operation<T> *o;
        Operation<T> *w;

        bool has_been_run;	

        // ::magmadnn::math::attnOptions opts; // by default

        #if defined(MAGMADNN_HAVE_CUDA)
        ::magmadnn::math::mha_cudnn_settings mha_settings;
        #endif
    }; 

    // template <typename T>
    // MHA1Op<T> *mha1(Operation<T> *q, Operation<T> *k, 
    //         Operation<T> *v, Operation<T> *weights, bool needs_grad = true); // TODO: pass parameters
    
    template <typename T>
    MHA1Op<T> *mha1(Operation<T> *input, Operation<T> *weights, ::magmadnn::math::attnOptions opts, bool needs_grad = true);
} //namespace op
} //namespace magmadnn