/**
 * @file concat3dop.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 *
 */

#include "compute/concat3d/concat_internal.h"
#include "compute/operation.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace op {

template <typename T>
class Concat3dOp : public Operation<T> {
   public:
    /** Initializes tensor with the correct output shape given axis and number of inputs.
     * @param inputs is a vector containing operations with the tensors to be concatenated
     * @param axis is the axis that the tensor will be attached by
     * @param copy only true is implemented
     * @param needs_grad unused
     */

    Concat3dOp(std::vector<Operation<T> *> inputs, unsigned axis, bool copy = true, bool needs_grad = true);

    /* You've been concated! */
    std::string to_string() { return "Concated!"; }

   protected:
    void init();
    Tensor<T> *_eval(bool recompute = true);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);

    std::vector<Operation<T> *> inputs;
    unsigned axis;
    bool copy;
};

/** Returns a new operation of type concatop. It return an operation with a new tensor
 * that is a concatenation of the tensors of the given operations in inputs about axis.
 * @tparam T
 * @param inputs
 * @param axis
 * @return SliceOp<T>*
 */
template <typename T>
Concat3dOp<T> *concat3d(std::vector<Operation<T> *> inputs, unsigned axis, bool copy = true, bool needs_grad = true);

}  // namespace op
}  // namespace magmadnn
