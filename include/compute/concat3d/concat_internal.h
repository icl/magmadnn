/**
 * @file concat_internal.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 *
 */

#pragma once
#include "tensor/tensor.h"

/**
 * These are the helpers for ConcatOp
 */
namespace magmadnn {
namespace internal {

/** Takes a vector of tensors and concatenates them about the given axis.
 * Resulting tensors are stored in ouput
 * @tparam T
 * @param inputs holds the tensors to concatenate
 * @param axis determines which axis the concatenation will occur on
 * @param output holds the concatenated tensor
 */

template <typename T>
void concat_cpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output);


/** Takes a vector of tensors and concatenates them about the given axis on GPU.
 * Resulting tensors are stored in ouput
 * This is only implemented for axis = 1
 * @tparam T
 * @param inputs holds the tensors to concatenate
 * @param axis determines which axis the concatenation will occur on
 * @param output holds the concatenated tensor
 */
#if defined(MAGMADNN_HAVE_CUDA)
template <typename T>
void concat_gpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output);
template <typename T>
void concat_gpu(cudaStream_t custream, std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output);
#endif

/** Cuts input tensor about the given axis and the given index and puts those value into output. 
 * This is the same function as in slice_internal.h. Used for gradient calculation.
 * @tparam T
 * @param input holds the tensor that is cut
 * @param axis determines which axis the cross section will be taken from
 * @param index determines the index of the cross section to be taken
 * @param output holds the cut tensor
 */
template <typename T>
void conslice_cpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output);

/** Same utility as above function but does gradient on GPU instead of CPU
 * @tparam T
 * @param input 
 * @param axis 
 * @param index 
 * @param output 
 */
#if defined(MAGMADNN_HAVE_CUDA)
template <typename T>
void conslice_gpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output);
template <typename T>
void conslice_gpu(cudaStream_t custream, Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output);

#endif

}  // namespace internal
}  // namespace magmadnn


