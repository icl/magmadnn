#pragma once

#include "compute/operation.h"
#include "tensor/tensor.h"
#include "math/lstm.h"
#include "compute/lstm2/lstm2_internal.h"

namespace magmadnn {
namespace op {

template <typename T>
class LSTM2Op : public Operation<T> {
	public:
		//TODO add params to operation
		LSTM2Op(Operation<T> *input, Operation<T> *hx,
				Operation<T> *cx, Operation<T> * weights, bool return_sequences = false, bool needs_grad = true);
		~LSTM2Op();

		std::string to_string() { return "LSTM2Op(" + input->to_string() + ")";}
	
	protected:
		Tensor<T> *_eval(bool recompute);
		Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);
		
		void init_settings();
		
#if defined(MAGMADNN_HAVE_CUDA)
		void cuda_forward();
#endif

		Tensor<T> *input_t;
		Tensor<T> *y_t;
		Tensor<T> *hx_t;
		Tensor<T> *hy_t;
		Tensor<T> *cx_t;
		Tensor<T> *cy_t;
		
		Tensor<T> *dx_t;
		Tensor<T> *dy_t;
		Tensor<T> *dhx_t;
		Tensor<T> *dhy_t;
		Tensor<T> *dcx_t;
		Tensor<T> *dcy_t;
		Tensor<T> *weights_t;

		
		Operation<T> *input;
		Operation<T> *y;
		Operation<T> *hx;
		Operation<T> *hy;
		Operation<T> *cx;
		Operation<T> *cy;
		Operation<T> *weights;

		int num_nodes;
		bool has_been_run;	
		bool return_sequences;
		

#if defined(MAGMADNN_HAVE_CUDA)
		::magmadnn::math::lstm_cudnn_settings lstm_settings;
#endif

};
	
template <typename T>
LSTM2Op<T> *lstm2(Operation<T> *input, Operation<T> *hx, 
				Operation<T> *cx, Operation<T> *weights, bool return_sequences = false, bool needs_grad = true);


} //namespace op
} //namespace magmadnn
