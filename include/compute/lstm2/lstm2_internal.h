#include "tensor/tensor.h"

namespace magmadnn {
namespace internal {



template <typename T>
void initGPUData(T *data, int numElements, T value);



}
}
