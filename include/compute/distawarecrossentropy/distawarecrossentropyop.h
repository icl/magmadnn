/**
 * @file distawarecrossentropyop.h
 * @author Spencer Smith
 * @brief A loss function specific to image segmentation. The distance from the current
 *         pixel to the nearest foreground pixel is taken into account when performing
 *          cross entropy.
 * @version 0.1
 * @date 2022-06-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <cassert>
#include "compute/add/addop.h"
#include "compute/flatten/flattenop.h"
#include "compute/log/logop.h"
#include "compute/negative/negativeop.h"
#include "compute/operation.h"
#include "compute/pow/powop.h"
#include "compute/product/productop.h"
#include "compute/reducesum/reducesumop.h"
#include "compute/scalarproduct/scalarproductop.h"
#include "math/distawarecrossentropy.h"
#include "math/gaussian2d.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace op {

template <typename T>
class DistAwareCrossEntropyOp : public Operation<T> {
   public:
    DistAwareCrossEntropyOp(Operation<T> *x, Operation<T> *y, bool copy = true, bool needs_grad = true);
    ~DistAwareCrossEntropyOp();

    std::string to_string() { return "DistAwareCrossEntropy(Softmax(" + x->to_string() + "), " + y->to_string() + ")"; }

   protected:
    Tensor<T> *_eval(bool recompute = true);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);

    Operation<T> *x, *y;
    Tensor<T> *x_tensor, *y_tensor, *softmax; /* scratch is used in the interal calc */
    Tensor<T> *loss_tensor;

    bool copy;
};

/**
 * @brief A modified version of normal binary cross entropy. First the normal
 *       cross entropy was modified to a focal loss, then it is modified so it
 *      is aware of distance. There are two parts of the functions. If the current
 *      pixel is meant to be classified as one, then the distance calculation is
 *      negated. Other wise it is kept in.
 *
 *      (1 - Pcij)^alpha * log(Pcij) if Ycij = 1
 *      (1 - Ycij)^beta * (Pcij)^alpha * log(1 - Pcij)  otherwise
 *
 *      c = the current channel
 *      i = the current row (height)
 *      j = the current col (width)
 *      alpha and beta are hyper-parameters (a = 2, b = 4)
 *      (1 - Ycij) reduces penalty around foreground pixels
 *
 * @tparam T
 * @param ground_truth ground truth operation
 * @param predicted    predicted operation
 * @param copy
 * @param needs_grad
 * @return Operation<T>*
 */
template <typename T>
Operation<T> *distawarecrossentropy(Operation<T> *ground_truth, Operation<T> *predicted, unsigned int n_objects,
                                    bool copy = true, bool needs_grad = true);

}  // namespace op
}  // namespace magmadnn