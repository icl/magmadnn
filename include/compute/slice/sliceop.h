/**
 * @file sliceop.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-22
 *
 */

#include "compute/operation.h"
#include "compute/slice/slice_internal.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace op {

template <typename T>
class SliceOp : public Operation<T> {
   public:
    /** Initializes tensor with the correct dimensions depending on axis and index.
     * @param input is the operation with the tensor that will be sliced
     * @param axis is the axis that the function take a cross section with respect to
     * @param index which index is cut out from the chosen axis
     * @param copy only true is implemented
     * @param needs_grad unused
     */
    SliceOp(Operation<T> *input, unsigned axis, unsigned index, bool copy = true, bool needs_grad = true);

    /** You've been Sliced!
     */
    std::string to_string() { return "Sliced!"; }

   protected:
    void init();
    Tensor<T> *_eval(bool recompute = true);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);

    Operation<T> *input;
    unsigned axis;
    unsigned index;

    bool copy;
};

/** Returns a new operation of type SliceOp<T>*. It return an operation with a new tensor
 * that is cut as specified by axis and index.
 * @tparam T
 * @param input
 * @param axis
 * @param index
 * @return SliceOp<T>*
 */

template <typename T>
SliceOp<T> *slice(Operation<T> *input, unsigned axis, unsigned index, bool copy = true, bool needs_grad = true);

}  // namespace op
}  // namespace magmadnn
