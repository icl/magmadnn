/**
 * @file slice_internal.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-22
 *
 */

#pragma once
#include "tensor/tensor.h"

/**
 * This is the helper for SliceOp
 * It is only implemented on CPU currently and only for 3D tensors
 *
 */
namespace magmadnn {
namespace internal {

/** Cuts input tensor about the given axis and the given index and puts those value into output
 * @tparam T
 * @param input holds the tensor that is cut
 * @param axis determines which axis the cross section will be taken from
 * @param index determines the index of the cross section to be taken
 * @param output holds the cut tensor
 */
template <typename T>
void slice_cpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output);

#if defined(MAGMADNN_HAVE_CUDA)
/** Same as CPU counterpart but performs calculations on GPU
 * Only implemented for axis = 1.
 * @tparam T
 * @param input 
 * @param axis 
 * @param index 
 * @param output 
 */
template <typename T>
void slice_gpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> *output);
template <typename T>
void slice_gpu(cudaStream_t custream, Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> * output);


/** Takes a upstream gradient tensor and calculate the gradient of the slice function with respect
 * to the variable at given index. Only implemented for axis = 1. 
 * @tparam T
 * @param input holds the upstream gradient
 * @param axis determines which axis the gradient is taken on
 * @param index determines the variable the gradient is taken with respect to
 * @param output holds the resulting gradient tensor
 */
template <typename T>
void slice_grad_gpu(Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> * output);
template <typename T>
void slice_grad_gpu(cudaStream_t custream, Tensor<T> * input, unsigned int axis, unsigned int index, Tensor<T> * output);

#endif
}  // namespace internal
}  // namespace magmadnn

