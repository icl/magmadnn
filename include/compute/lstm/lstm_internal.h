/**
 * @file lstm_internal.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-07-19
 *
 */

#pragma once
#include "tensor/tensor.h"
/**
 * These are the helpers for LSTMOp
 */
namespace magmadnn {
namespace internal {

// The associated numbers matter because they correspond to the index in the
// vector which holds the weights.
enum class WrtVariable {WF = 0, UF = 1, BF = 2, WI = 3, UI = 4, BI = 5, WO = 6, UO = 7,
	BO = 8, WC = 9, UC = 10, BC = 11, X = 12};

/** Calculate forward pass of LSTM on cpu
 * @tparam T
 * @param input holds the tensor that is cut
 * @param weights holds the weight values of the LSTM
 * @param X is used to store all the x values found during forward pass calculation
 * @param C same as X but for c values
 * @param H same as X but for h values
 * @param B same as X but for tanh(c) values
 * @param F same as X but for f values
 * @param I same as X but for i values
 * @param O same as X but for o values
 * @param tC same as X but for c_tilda values
 * @param output the value of the forward pass
 */
template <typename T>
void lstm_cpu(Tensor<T> * input, std::vector<Tensor<T> *> weights, 
		Tensor<T> * h_init, Tensor<T> * c_init, bool return_sequences,
		std::vector<Tensor<T> *> *X, std::vector<Tensor<T> *> *C,
		std::vector<Tensor<T> *> *H, std::vector<Tensor<T> *> *B,
		std::vector<Tensor<T> *> *F, std::vector<Tensor<T> *> *I,
		std::vector<Tensor<T> *> *O, std::vector<Tensor<T> *> *tC,
		Tensor<T> *output); 

/** CPU function for concatenating tensors about the given axis
 * @tparam T
 * @param inputs contains tensors to be concatenated
 * @param axis determines about which axis the concatenation is done
 * @param output contains concatenated tensor
 */
template <typename T>
void concat_cpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output);

/** CPU function for slicing a tensor about the given axis
 * @tparam T
 * @param input contains tensor to be sliced
 * @param axis determines about which axis the slice is done
 * @param output contains slice of the tensor
 */
template <typename T>
void slice_cpu(Tensor<T>* input, unsigned int axis, unsigned int index, Tensor<T>* output);

/** CPU function for calculating sigmoid of a tensor
 * @tparam T
 * @param x contains input tensor
 * @param out contains output tensor
 * @param fast determines which sigmoid calculation is used
 */
template <typename T>
void sigmoid_cpu(Tensor<T> *x, Tensor<T> *out, bool fast);

/** CPU function for calculating element wise product of two tensors
 * @tparam T
 * @param alpha is the constant by which the product is multiplied
 * @param a contains first tensor
 * @param b contains second tensor
 * @param out contains output tensor
 */
template <typename T>
void product_cpu(T alpha, Tensor<T> *a, Tensor<T> *b, Tensor<T> *out);

/** CPU function for adding two tensors
 * @tparam T
 * @param alpha is the constant by which first tensor is multiplied by
 * @param A is the first tensor
 * @param beta is the constant by which the second tensor is multiplied
 * @param B is the second tensor
 * @param C is the output tensor
 */
template <typename T>
void add_cpu(T alpha, Tensor<T> *A, T beta, Tensor<T> *B, Tensor<T> *C);

/** CPU function for concatenating tensors about the given axis
 * @tparam T
 * @param x is the input tensor
 * @param out is the output tensor
 */
template <typename T>
void tanh_cpu(Tensor<T> *x, Tensor<T> *out);

/** Calculate backward pass of LSTM on cpu
 * @tparam T
 * @param grad contains the given upstream gradient
 * @param wrt determines which variable to take the gradient with respect to
 * @param weights contains the LSTM weights
 * @param X contains all x values calculated during forward pass
 * @param C same as X but for c values
 * @param H same as X but for h values
 * @param B same as X but for tanh(c) values
 * @param F same as X but for f values
 * @param I same as X but for i values
 * @param O same as X but for o values
 * @param tC same as X but for c_tilda values
 * @param output the value of backward pass
 */
template <typename T>
void lstm_grad_cpu(Tensor<T> * grad, WrtVariable wrt, std::vector<Tensor<T> *> weights,
		std::vector<Tensor<T> *> X, std::vector<Tensor<T> *> C,
		std::vector<Tensor<T> *> H, std::vector<Tensor<T> *> B,
		std::vector<Tensor<T> *> F, std::vector<Tensor<T> *> I,
		std::vector<Tensor<T> *> O, std::vector<Tensor<T> *> tC,
		Tensor<T> *output);
#if defined(MAGMADNN_HAVE_CUDA)
/** Concatenates a vector of tensors (3D only) along the given axis. (same as in concat_internal.h)
 * @tparam T
 * @param inputs contains tensors to be concatenated
 * @param axis determines axis about which the tensors are concatenated
 * @param output contains the concatenated tensor
 */
template <typename T>
void concat_gpu(std::vector<Tensor<T> *> inputs, unsigned int axis, Tensor<T> *output);

/*NOTE: this operations likely takes large amounts of memory 
and will cause problem on larger data sets*/

/** Calculate forward pass of LSTM on GPU
 * @param input holds the tensor that is cut
 * @param weights holds the weight values of the LSTM
 * @param X is used to store all the x values found during forward pass calculation
 * @param C same as X but for c values
 * @param H same as X but for h values
 * @param B same as X but for tanh(c) values
 * @param F same as X but for f values
 * @param I same as X but for i values
 * @param O same as X but for o values
 * @param tC same as X but for c_tilda values
 * @param output the value of the forward pass
 */
void lstm_gpu(cudaStream_t custream,
		Tensor<float> * input, std::vector<Tensor<float> *> weights, 
		Tensor<float> * h_init, Tensor<float> * c_init, bool return_sequences,	
		std::vector<Tensor<float> *> *X, std::vector<Tensor<float> *> *C,
		std::vector<Tensor<float> *> *H, std::vector<Tensor<float> *> *B,
		std::vector<Tensor<float> *> *F, std::vector<Tensor<float> *> *I,
		std::vector<Tensor<float> *> *O, std::vector<Tensor<float> *> *tC,
		Tensor<float> *output);
void lstm_gpu(cudaStream_t custream,
		Tensor<double> * input, std::vector<Tensor<double> *> weights, 
		Tensor<double> * h_init, Tensor<double> * c_init, bool return_sequences,	
		std::vector<Tensor<double> *> *X, std::vector<Tensor<double> *> *C,
		std::vector<Tensor<double> *> *H, std::vector<Tensor<double> *> *B,
		std::vector<Tensor<double> *> *F, std::vector<Tensor<double> *> *I,
		std::vector<Tensor<double> *> *O, std::vector<Tensor<double> *> *tC,
		Tensor<double> *output);

/** Calculate backward pass of LSTM on GPU
 * @tparam T
 * @param grad contains the given upstream gradient
 * @param wrt determines which variable to take the gradient with respect to
 * @param weights contains the LSTM weights
 * @param X contains all x values calculated during forward pass
 * @param C same as X but for c values
 * @param H same as X but for h values
 * @param B same as X but for tanh(c) values
 * @param F same as X but for f values
 * @param I same as X but for i values
 * @param O same as X but for o values
 * @param tC same as X but for c_tilda values
 * @param output the value of backward pass
 */
void lstm_grad_device(cudaStream_t custream,
		Tensor<float>*grad, WrtVariable wrt, std::vector<Tensor<float>*> weights,
		std::vector<Tensor<float> *> X, std::vector<Tensor<float> *> C,
		std::vector<Tensor<float> *> H, std::vector<Tensor<float> *> B,
		std::vector<Tensor<float> *> F, std::vector<Tensor<float> *> I,
		std::vector<Tensor<float> *> O, std::vector<Tensor<float> *> tC,
		Tensor<float> *output);
void lstm_grad_device(cudaStream_t custream,
		Tensor<double>*grad, WrtVariable wrt, std::vector<Tensor<double>*> weights,
		std::vector<Tensor<double> *> X, std::vector<Tensor<double> *> C,
		std::vector<Tensor<double> *> H, std::vector<Tensor<double> *> B,
		std::vector<Tensor<double> *> F, std::vector<Tensor<double> *> I,
		std::vector<Tensor<double> *> O, std::vector<Tensor<double> *> tC,
		Tensor<double> *output);
#endif
}  // namespace internal
}  // namespace magmadnn

