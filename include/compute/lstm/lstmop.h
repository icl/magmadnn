/**
 * @file lstmop.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-07-19
 *
 */
#pragma once
#include "compute/lstm/lstm_internal.h"
#include "compute/operation.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace op {

template <typename T>
class LSTMOp : public Operation<T> {
   public:
    /** Constructs LSTM operation, allocates space for output, and sets initial
     * values from given parameters
     * @param input is the input from the LSTM layer
     * @param weights is a vector with all of the training weights from the LSTM layer
     * @param h_init holds initial h values
     * @param c_init holds initial c values
     * @param return_sequences determine if the sequence of output or only the final output are returned
     * @param copy
     * @param needs_grad
     */
    LSTMOp(Operation<T> *input, std::vector<Operation<T> *> weights, Operation<T> *h_init, Operation<T> *c_init,
           bool return_sequences, bool copy = true, bool needs_grad = true);
    virtual ~LSTMOp();
    /* You've been LSTMed! */
    std::string to_string() { return "LSTMed!"; }

   protected:
    void init();
    Tensor<T> *_eval(bool recompute = true);
    Tensor<T> *_grad(Operation<T> *consumer, Operation<T> *var, Tensor<T> *grad);

    Operation<T> *input;
    std::vector<Operation<T> *> weights;
    std::vector<Tensor<T> *> t_weights;
    Operation<T> *h_init;
    Operation<T> *c_init;
    bool return_sequences;
    bool copy;

    // the _eval function stores intermediate calculations into these
    // tensors, which are then used to calculate gradients.
    std::vector<Tensor<T> *> X, C, H, B, F, I, O, tC;
};
/** Returns a new LSTM operations
 * @tparam T
 * @param input
 * @param weights
 * @param h_init
 * @param c_init
 * @param return_sequences
 * @param copy
 * @param needs_grad
 * @return LSTMOp<T>*
 */
template <typename T>
LSTMOp<T> *lstm(Operation<T> *input, std::vector<Operation<T> *> weights, Operation<T> *h_init, Operation<T> *c_init,
                bool return_sequences, bool copy = true, bool needs_grad = true);

}  // namespace op
}  // namespace magmadnn
