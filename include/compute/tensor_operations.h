#pragma once

#include "compute/op_utilities.h"

#include "add/addop.h"
#include "sum/sumop.h"
#include "dot/dotop.h"
#include "matmul/matmulop.h"
#include "scalarproduct/scalarproductop.h"

#include "log/logop.h"
#include "relu/reluop.h"
#include "sigmoid/sigmoidop.h"
#include "tanh/tanhop.h"

#include "div/divop.h"

#include "negative/negativeop.h"

#include "reducesum/reducesumop.h"

#include "crossentropy/crossentropyop.h"
#include "meansquarederror/meansquarederror.h"
#include "distawarecrossentropy/distawarecrossentropyop.h"

#include "transpose/transposeop.h"

#include "conv2dforward/conv2dforwardop.h"
#include "conv2dtranspose/conv2dtransposeop.h"
#include "linearforward/linearforwardop.h"
#include "pow/powop.h"
#include "softmax/softmaxop.h"

#include "batchnorm/batchnormop.h"
#include "dropout/dropoutop.h"
#include "flatten/flattenop.h"
#include "pooling/poolingop.h"

#include "tensormatmul/tensormatmulop.h"
#include "concat4d/concat4dop.h"
#include "concat4d/concat.cuh"

#include "lstm/lstmop.h"
#include "concat3d/concat3dop.h"
#include "slice/sliceop.h"
#include "lstm2/lstm2op.h"
#include "mha/mha1op.h"