/**
 * @file unet.h
 * @author Stephen Qiu
 * @version 1.0
 * @date 2022-08-02
 *
 * @copyright Copyright (c) 2022
 */
#pragma once

#include "magmadnn.h"

namespace magmadnn {

template <typename T>
class Unet {
      public:
      
    static std::vector<layer::Layer<T> *> DecoderMiniBlock(op::Operation<T> *prev_layer_input,
                                                           op::Operation<T> *skip_layer_input, int n_filters) {
        /* apply upsampling + 3x3 convolution */
        auto up_scale = layer::conv2dtranspose(prev_layer_input, {3, 3}, n_filters * 2);
        auto up_scale2 = layer::conv2d(up_scale->out(), {3, 3}, n_filters, layer::SAME);

        /* concat the skip connection with the upsampled tensor */
        auto concat = layer::concat(up_scale2->out(), skip_layer_input, 1);
        /* apply double convolution with batch norm + RELU */
        auto conv1 = layer::conv2d<T>(concat->out(), {3, 3}, n_filters, layer::SAME);
        auto bn1 = layer::batchnorm(conv1->out());
        auto act1 = layer::activation<T>(bn1->out(), layer::RELU);
        // second convolution
        auto conv2 = layer::conv2d<T>(act1->out(), {3, 3}, n_filters, layer::SAME);
        auto bn2 = layer::batchnorm(conv2->out());
        auto act2 = layer::activation<T>(bn2->out(), layer::RELU);
        std::vector<layer::Layer<T> *> layers = {up_scale, up_scale2, concat, conv1, bn1, act1, conv2, bn2, act2};

        return layers;
    }

    static std::vector<layer::Layer<T> *> EncoderMiniBlock(op::Operation<T> *input, int n_filters, float dropout_prob,
                                                           bool max_pooling) {
        layer::Layer<T> *next_layer;
        layer::Layer<T> *skip_connection;
        std::vector<layer::Layer<T> *> layers;

        /* apply double convolution with batchnorm + activation */
        auto conv1 = layer::conv2d<T>(input, {3, 3}, n_filters, layer::SAME);
        layers.push_back(conv1);
        auto bn1 = layer::batchnorm(conv1->out());
        layers.push_back(bn1);
        auto act1 = layer::activation<T>(bn1->out(), layer::RELU);
        layers.push_back(act1);
        auto conv2 = layer::conv2d<T>(act1->out(), {3, 3}, n_filters, layer::SAME);
        layers.push_back(conv2);
        auto bn2 = layer::batchnorm(conv2->out());
        layers.push_back(bn2);
        auto act2 = layer::activation<T>(bn2->out(), layer::RELU);
        layers.push_back(act2);
        // normalize the batch

        /* apply dropout if it is specified */
        if (dropout_prob > (float) 0) {
            skip_connection = layer::dropout(act2->out(), dropout_prob);
            layers.push_back(skip_connection);
        } else
            skip_connection = act2;

        /* apply pooling if specified */
        if (max_pooling) {
            next_layer = layer::pooling(skip_connection->out(), {2, 2}, {0, 0}, {2, 2}, MAX_POOL);
            layers.push_back(next_layer);
        } else {
            next_layer = skip_connection;
            // layers.push_back(next_layer);
        }

        layers.push_back(skip_connection);
        return layers;
    }

};  // Class Unet

}  // namespace magmadnn
