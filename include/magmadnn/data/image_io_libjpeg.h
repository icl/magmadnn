#pragma once

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "tensor/tensor.h"

//using C code in c++
extern "C" {
    #include <jpeglib.h>
}

namespace magmadnn {
namespace data {


struct imgRawImage {
    unsigned int numComponents;
    unsigned long int width, height;

    unsigned char *lpData;
};

struct imgRawImage *read_jpeg(const char *lpFilename);

void write_jpeg(const char *lpFilename, unsigned int width, unsigned int height, float *buffer, bool is_trimap);

template <typename T>
void add_jpeg_to_tensor(const char *filename, const bool trimap,
                         magmadnn::Tensor<T> *images_tensor, unsigned int image_idx);

template <typename T>
void tensor_to_jpeg(const char *filename, const int height, const int width,
                        magmadnn::Tensor<T> *images_tensor, unsigned int image_idx, bool is_trimap = false);
}}
// End of namespace magmadnn::data
