#pragma once

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "tensor/tensor.h"

//using C code in c++
extern "C" {
    #include <png.h>
}

namespace magmadnn {
namespace data {


png_infop read_png(const char *filename);

void write_png(const char* filename, int width, int height, float *buffer, bool is_trimap);

template <typename T>
void add_png_to_tensor(const char *filename, const bool trimap,
                         magmadnn::Tensor<T> *images_tensor, unsigned int image_idx);

template <typename T>
void tensor_to_png(const char *filename, const int height, const int width,
                        magmadnn::Tensor<T> *images_tensor, unsigned int image_idx, bool is_trimap = false);
}}
// End of namespace magmadnn::data
