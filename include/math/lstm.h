#pragma once

#include "magmadnn/utilities_internal.h"
#include "tensor/tensor.h"

#if defined(MAGMADNN_HAVE_CUDA)
#include "cudnn.h"
#endif


namespace magmadnn {
namespace math {

#if defined(MAGMADNN_HAVE_CUDA)


struct lstm_cudnn_settings {
	cudnnHandle_t handle;
	cudnnRNNDescriptor_t rnnDesc;
	int * seqLengthArray;
	int * devSeqLengthArray;

	cudnnRNNDataDescriptor_t xDesc;	
	cudnnRNNDataDescriptor_t yDesc;
	cudnnTensorDescriptor_t hDesc;
	cudnnTensorDescriptor_t cDesc;
	size_t weightSpaceSize;
	void *weightSpace;
	size_t reserveSpaceSize;
	void *reserveSpace;
	void *dweightSpace;

	size_t workSpaceSize;
	void *workSpace;

	size_t gradWeightSpaceSize;
	void *gradWeightSpace;
	size_t gradReserveSpaceSize;
	void *gradReserveSpace;
	size_t gradWorkSpaceSize;
	void *gradWorkSpace;

};
	
template <typename T>
void lstm_device(Tensor<T> *x, Tensor<T> *y, Tensor<T> *hx, Tensor<T> *hy, Tensor<T> *cx, Tensor<T> *cy, Tensor<T> *weights_t, lstm_cudnn_settings settings);

template <typename T>
void lstm_grad_data_device(Tensor<T> *x, Tensor<T> *y, Tensor<T> *dy, Tensor<T> *dx, Tensor<T> *hx, Tensor<T> *dhy, Tensor<T> *dhx,
                Tensor<T> *cx, Tensor<T> *dcy, Tensor<T> *dcx, Tensor<T> *weights_t, lstm_cudnn_settings settings);

template <typename T>
void lstm_grad_data_device_weights(Tensor<T> *x, Tensor<T> *y, Tensor<T> *hx, lstm_cudnn_settings settings);

#endif
} //namespace math
} //namespace magmadnn
