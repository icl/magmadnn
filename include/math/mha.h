#pragma once

#include <string.h> // TODO: memset
#include "tensor/tensor.h"

#if defined(MAGMADNN_HAVE_CUDA)
#include "cudnn.h"
#endif

/**
 * mha cudnn setting 
 * 
*/

namespace magmadnn {
namespace math {
    #if defined(MAGMADNN_HAVE_CUDA)

    struct attnOptions {
        // attnOptions() { 
        //     memset(this, 0, sizeof(*this));
        //     this->attnTrain       = 0;
        //     this->attnDataType    = CUDNN_DATA_FLOAT;
        //     this->attnCompPrec    = CUDNN_DATA_FLOAT;
        //     this->attnQueryMap    = 0;
        //     this->attnNumHeads    = 2;
        //     this->attnSmScaler    = 1.0;
        //     this->attnDropoutRate  = 0.0;
        //     this->attnProjQsize   = 8;
        //     this->attnProjKsize   = 8;
        //     this->attnProjVsize   = 8;
        //     this->attnProjOsize   = 8;
        //     this->attnDataLayout  = 0;
        //     this->attnResLink     = 0;
        //     this->attnProjBias    = 0;
        //     this->attnSweep       = 0;
        //     this->attnRandGeom    = 0;
        //     this->attnRandSeed    = 1234;
        //     this->attnFileDump    = 0; 
        // }

        attnOptions(int attnTrain,
        int attnDataType,
        int attnCompPrec,
        int attnQueryMap,
        int attnNumHeads,
        int attnBeamSize,
        double attnSmScaler,
        float attnDropoutRate,
        int attnQsize,
        int attnKsize,
        int attnVsize,
        int attnProjQsize,
        int attnProjKsize,
        int attnProjVsize,
        int attnProjOsize,
        int attnSeqLenQ,
        int attnSeqLenK,
        int attnBatchSize,
        int attnDataLayout,
        int attnResLink,
        int attnProjBias,
        int attnSweep,
        int attnRandGeom,
        int attnRandSeed,
        int attnFileDump
        ) : attnTrain(attnTrain), 
        attnDataType(attnDataType),
        attnCompPrec(attnCompPrec),
        attnQueryMap(attnQueryMap),
        attnNumHeads(attnNumHeads),
        attnBeamSize(attnBeamSize),
        attnSmScaler(attnSmScaler),
        attnDropoutRate(attnDropoutRate),
        attnQsize(attnQsize),
        attnKsize(attnKsize),
        attnVsize(attnVsize),
        attnProjQsize(attnProjQsize),
        attnProjKsize(attnProjKsize),
        attnProjVsize(attnProjVsize),
        attnProjOsize(attnProjOsize),
        attnSeqLenQ(attnSeqLenQ),
        attnSeqLenK(attnSeqLenK),
        attnBatchSize(attnBatchSize),
        attnDataLayout(attnDataLayout),
        attnResLink(attnResLink),
        attnProjBias(attnProjBias),
        attnSweep(attnSweep),
        attnRandGeom(attnRandGeom),
        attnRandSeed(attnRandSeed),
        attnFileDump(attnFileDump) {}

        int attnTrain;
        int attnDataType;
        int attnCompPrec;
        int attnQueryMap;
        int attnNumHeads;
        int attnBeamSize;
        double attnSmScaler;
        float attnDropoutRate;
        int attnQsize;
        int attnKsize;
        int attnVsize;
        int attnProjQsize;
        int attnProjKsize;
        int attnProjVsize;
        int attnProjOsize;
        int attnSeqLenQ;
        int attnSeqLenK;
        int attnBatchSize;
        int attnDataLayout;
        int attnResLink;
        int attnProjBias;
        int attnSweep;
        int attnRandGeom;
        int attnRandSeed;
        int attnFileDump;
    };

    struct attnConfig {
        attnConfig() { memset(this, 0, sizeof(*this)); }  // sets queryMap=ALL_TO_ONE

        cudnnAttnQueryMap_t queryMap;  // queryMap mode

        int numHeads;       // number of attention heads
        int beamSize;       // number of candidates of the same sentence
        double smScaler;    // softmax smoothing or sharpening coefficient
        float dropoutRate;  // dropout probability
        int qSize;          // original vector length of "queries"
        int kSize;          // original vector length of "keys"
        int vSize;          // original vector length of "values"
        int qProjSize;      // "queries" after projection (0=no projection)
        int kProjSize;      // "keys" after projection (0=no projection)
        int vProjSize;      // "values" after projection (0=no projection)
        int oProjSize;      // "output" after projection (0=no projection)
        int seqLenQ;        // max seq length for Q, R, O buffers
        int seqLenK;        // max seq length for K, V buffers
        int batchSize;      // batch size for Q, R, K, V, O buffers
        bool resLink;       // enable/disable residual connections
        bool projBias;      // enable/disable residual connections
        bool train;         // is training?
        int sweep;          // sweep all time-steps in inference mode
        int randGeom;       // randomize poblem dimensions
        int randSeed;       // random number generator seed
        int fileDump;       // save single sentence data to file

        unsigned attnMode;  // Attention Mode parameter

        // Attention window boundaries for every time-step.
        int *loWinIdx;
        int *hiWinIdx;

        // Query and key sequence lengths (for each batch/beam sentence).
        int *qSeqLen;
        int *kSeqLen;

        int dataLayout;                                        // data layout, map to one of 6 possible dataAxes
        cudnnSeqDataAxis_t dataAxes[CUDNN_SEQDATA_DIM_COUNT];  // data order for T, N, and B dim

        cudnnDataType_t dataType;  // data type for Q,K,V inputs, weights, output
        cudnnDataType_t compPrec;  // compute precision

        int qLength() {
            return this->qProjSize > 0 ? this->qProjSize : this->qSize;
        }

        int kLength() {
            return this->kProjSize > 0 ? this->kProjSize : this->kSize;
        }

        int vLength() {
            return this->vProjSize > 0 ? this->vProjSize : this->vSize;
        }

        int oLength() {
            return this->oProjSize > 0 ? this->oProjSize : this->vLength() * this->numHeads;
        }

        size_t qoTokens() {
            return size_t(this->seqLenQ) * this->batchSize * this->beamSize;
        }

        size_t kvTokens() {
            size_t t = size_t(this->seqLenK) * this->batchSize;
            if (this->queryMap == CUDNN_ATTN_QUERYMAP_ONE_TO_ONE) {
                t *= this->beamSize;
            }
            return t;
        }

        size_t qAllData() {
            return this->qoTokens() * this->qSize;
        }

        size_t kAllData() {
            return this->kvTokens() * this->kSize;
        }

        size_t vAllData() {
            return this->kvTokens() * this->vSize;
        }

        size_t oAllData() {
            return this->qoTokens() * this->oLength();
        }

        size_t qAllWeights() {
            size_t q_weights = (this->qProjSize > 0 ? size_t(this->qSize) * this->qProjSize : 0);
            return q_weights * this->numHeads;
        }

        size_t kAllWeights() {
            size_t k_weights = (this->kProjSize > 0 ? size_t(this->kSize) * this->kProjSize : 0);
            return k_weights * this->numHeads;
        }

        size_t vAllWeights() {
            size_t v_weights = (this->vProjSize > 0 ? size_t(this->vSize) * this->vProjSize : 0);
            return v_weights * this->numHeads;
        }

        size_t oAllWeights() {
            size_t o_weights = (this->oProjSize > 0 ? size_t(this->vLength()) * this->oProjSize : 0);
            return o_weights * this->numHeads;
        }

        size_t qSeqLenCount() {
            return this->batchSize * this->beamSize;
        }

        size_t kSeqLenCount() {
            return this->batchSize * (this->queryMap == CUDNN_ATTN_QUERYMAP_ONE_TO_ONE ? this->beamSize : 1);
        }
    };

    struct mha_cudnn_settings {
            cudnnHandle_t handle;
            attnConfig mainCfg;

            cudnnAttnDescriptor_t attn_desc;
            cudnnDropoutDescriptor_t drop_desc;
            cudnnSeqDataDescriptor_t q_desc;
            cudnnSeqDataDescriptor_t k_desc;
            cudnnSeqDataDescriptor_t v_desc;
            cudnnSeqDataDescriptor_t o_desc;

            // bool isTraining;
            // Attention in/out buffers on the GPU side.
            // T_ELEM *devQ;
            // T_ELEM *devK;
            // T_ELEM *devV;
            // T_ELEM *devO;
            // T_ELEM *devDQ;
            // T_ELEM *devDK;
            // T_ELEM *devDV;
            // T_ELEM *devDO;
            // T_ELEM *devW;
            // T_ELEM *devDW;

            // Buffers with in/out data and weights on the CPU side.
            // T_ELEM *hostQ;
            // T_ELEM *hostK;
            // T_ELEM *hostV;
            // T_ELEM *hostO;
            // T_ELEM *hostDQ;
            // T_ELEM *hostDK;
            // T_ELEM *hostDV;
            // T_ELEM *hostDO;
            // T_ELEM *hostW;
            // T_ELEM *hostDW;

            // Work-space and reserve-space GPU buffers required by API.
            void *devWkspace;
            void *devReserve;

            // Capacity of weight/wkspace/reserve buffers (in bytes).
            size_t sizeWeights;
            size_t sizeWkspace;
            size_t sizeReserve;

            size_t maxWeights;
            size_t maxWkspace;
            size_t maxReserve;

            // Capacity of each "seq" data container (in elements).
            // size_t maxElemQ;
            // size_t maxElemK;
            // size_t maxElemV;
            // size_t maxElemO;
            // size_t maxElemA;

            // size_t maxElemQbar;
            // size_t maxElemKbar;
            // size_t maxElemVbar;
            // size_t maxElemHbar;

            // Dropout descriptor settings.
            size_t dropoutBufSize;
            void *dropoutBuf;

            // Sequence length arrays for Q,R,O and K,V.
            int *qSeqArray;
            int *kSeqArray;

            int *devQSeqArray;
            int *devKSeqArray;

            // Attention window.
            int *loWinIdx;
            int *hiWinIdx;

            void print_process() {
                size_t qBatches = mainCfg.qSeqLenCount();
                size_t kBatches = mainCfg.kSeqLenCount();
                for (size_t i = 0; i < qBatches; ++i) {
                    printf("sequence_length_q[idx=%lu]=%d\n", i, qSeqArray[i]);
                }
                printf("\n");

                for (size_t i = 0; i < kBatches; ++i) {
                    printf("sequence_length_k[idx=%lu]=%d\n", i, kSeqArray[i]);
                }
                printf("\n");

                for (int i = 0; i < mainCfg.seqLenQ; ++i) {
                    printf("attention_window[time=%d]=%d:%d\n", i, loWinIdx[i], hiWinIdx[i]);
                }
                printf("\n");
            }
    };

    template <typename T>
    void mha_device(
        Tensor<T> *q_t, 
        Tensor<T> *k_t, 
        Tensor<T> *v_t, 
        Tensor<T> *o_t, 
        Tensor<T> *weights_t, 
        mha_cudnn_settings settings);

    template <typename T>
    void mha_grad_data_device(
        Tensor<T> *q_t, 
        Tensor<T> *dq_t, 
        Tensor<T> *k_t, 
        Tensor<T> *dk_t, 
        Tensor<T> *v_t, 
        Tensor<T> *dv_t, 
        Tensor<T> *w_t, 
        Tensor<T> *do_t, 
        mha_cudnn_settings settings);

    template <typename T>
    void mha_grad_data_device_weights(
        Tensor<T> *q_t, 
        Tensor<T> *k_t, 
        Tensor<T> *v_t, 
        Tensor<T> *do_t, 
        Tensor<T> *w_t, 
        Tensor<T> *dw_t,
        mha_cudnn_settings settings);
    #endif
} //namespace math
} //namespace magmadnn