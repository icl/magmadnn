/**
 * @file gaussian2d.h
 * @author Spencer Smith
 * @brief a 2d guassian math operation
 * @version 0.1
 * @date 2022-07-05
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <cmath>
#include "magmadnn/types.h"
#include "magmadnn/utilities_internal.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace math {

/**
 * @brief
 *
 * @tparam T
 * @param fg_pixel This is the "center" of the gaussian function
 * @param bg_pixel The pixel at question.
 * @return T
 */
template <typename T>
T gaussian2d(std::vector<unsigned int>* fg_pixel, std::vector<unsigned int>* bg_pixel);

}  // namespace math
}  // namespace magmadnn