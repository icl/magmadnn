/**
 * @file distawarecrossentropy.h
 * @author Spencer Smith
 * @brief
 * @version 0.1
 * @date 2022-07-01
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <cmath>
#include "magmadnn/utilities_internal.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace math {

/**
 * @brief utility function for distawarecrossentropy, given a background pixel it
 *      finds the nearest foreground pixel.
 *
 * @tparam T
 * @param image the image
 * @param image_dims the batch and channel dimensions
 * @param bg_pixel the coordinates to the background pixel
 * @return std::vector<unsigned int>* returns the coordinates to the fg pixel, if it
 *          returns {10000, 10000} that means an error has occured.
 */
template <typename T>
std::vector<unsigned int> find_closest_fg(Tensor<T> *image, std::vector<unsigned int> *image_dims,
                                          std::vector<unsigned int> *bg_pixel);

/**
 * @brief the tensors passed in should be 4d tensors.
 *      A modified version of normal binary cross entropy. First the normal
 *      cross entropy was modified to a focal loss, then it is modified so it
 *      is aware of distance. There are two parts of the functions. If the current
 *      pixel is meant to be classified as one, then the distance calculation is
 *      negated. Other wise it is kept in.
 *
 *              (1 - Pcij)^alpha * log(Pcij)            if Ycij = 1
 *      (1 - Ycij)^beta * (Pcij)^alpha * log(1 - Pcij)  otherwise
 *
 *      c = the current channel
 *      i = the current row (height)
 *      j = the current col (width)
 *      alpha and beta are hyper-parameters (a = 2, b = 4)
 *      (1 - Ycij) reduces penalty around foreground pixels
 *
 * @tparam T
 * @param predicted tensor holding the predicted
 * @param ground_truth the tensor holding the ground truth
 * @param out holds the result
 */
template <typename T>
void distawarecrossentropy(Tensor<T> *predicted, Tensor<T> *ground_truth, Tensor<T> *out);

/**
 * @brief Used for computing the gradients of the loss
 *
 * @tparam T
 * @param predicted
 * @param ground_truth
 * @param out
 */
template <typename T>
void distawarecrossentropy_grad(Tensor<T> *predicted, Tensor<T> *ground_truth, Tensor<T> *grad, Tensor<T> *out);
}
}