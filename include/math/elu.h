#pragma once

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "tensor/tensor.h"

#if defined(MAGMADNN_HAVE_CUDA)
#include "cudnn.h"
#endif

namespace magmadnn {
namespace math {

template <typename T>
void elu(Tensor<T> *x, double a, Tensor<T> *out);

template <typename T>
void elu_grad(Tensor<T> *x, Tensor<T> *elu_out, double a, Tensor<T> *grad, Tensor<T> *out);

#if defined(MAGMADNN_HAVE_CUDA)

struct elu_cudnn_settings_t {
    cudnnActivationDescriptor_t descriptor;
    cudnnHandle_t handle;
    double a;
};

template <typename T>
void elu_device(Tensor<T> *x, Tensor<T> *out, elu_cudnn_settings_t settings);

template <typename T>
void elu_grad_device(Tensor<T> *x, Tensor<T> *elu_out, Tensor<T> *grad, Tensor<T> *out,
                      elu_cudnn_settings_t settings);
#endif

}  // namespace math
}  // namespace magmadnn
