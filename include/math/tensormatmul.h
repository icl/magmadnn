/**
 * @file tensormatmul.h
 * @author Stephen Qiu
 * @version 0.1
 * @date 2021-07-15
 *
 * @copyright Copyright (c) 2021
 */
#pragma once

#include "tensor/tensor.h"
#if defined(MAGMADNN_HAVE_CUDA)
#include "magma.h"
#endif

namespace magmadnn {
namespace math {

template <typename T>
void tensormatmul(T alpha, bool trans_A, Tensor<T> *A, bool trans_B, Tensor<T> *B, T beta, Tensor<T> *C /*unsigned int c, unsigned int M,
                    unsigned int N, unsigned int K*/);
}   //namespace math
}  // namespace magmadnn
