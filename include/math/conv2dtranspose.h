/**
 * @file conv2dtranspose.h
 * @author Spencer Smith and Edward Karak
 * @brief Header file for the cpp implementation of convolutional transpose
 * @version 0.1
 * @date 2022-06-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#if defined(MAGMADNN_CMAKE_BUILD)
#include "magmadnn/config.h"
#endif
#include "magmadnn/utilities_internal.h"
#include "tensor/tensor.h"

#if defined(MAGMADNN_HAVE_CUDA)
#include "cudnn.h"
#endif

namespace magmadnn {
namespace math {

template <typename T>
void conv2dtranspose(Tensor<T> *x, Tensor<T> *w, Tensor<T> *out);

template <typename T>
void conv2dtranspose_grad_data(Tensor<T> *x, Tensor<T> *w, Tensor<T> *out);

template <typename T>
void conv2dtranspose_grad_filter(Tensor<T> *x, Tensor<T> *w, Tensor<T> *out);

#if defined(MAGMADNN_HAVE_CUDA)

struct conv2dtranspose_cudnn_settings {
    cudnnConvolutionDescriptor_t conv_desc;
    cudnnFilterDescriptor_t filter_desc;
#if CUDNN_MAJOR == 8
    cudnnConvolutionFwdAlgoPerf_t algo;
    cudnnConvolutionBwdDataAlgoPerf_t bwd_data_algo;
    cudnnConvolutionBwdFilterAlgoPerf_t bwd_filter_algo;
#else
    cudnnConvolutionFwdAlgo_t algo;
    cudnnConvolutionBwdDataAlgo_t bwd_data_algo;
    cudnnConvolutionBwdFilterAlgo_t bwd_filter_algo;
#endif
    void *workspace;
    size_t workspace_size;
    void *grad_data_workspace;
    size_t grad_data_workspace_size;
    void *grad_filter_workspace;
    size_t grad_filter_workspace_size;
    cudnnHandle_t handle;

};

template <typename T>
void conv2dtranspose_device(Tensor<T> *x, Tensor<T> *w, Tensor<T> *out, conv2dtranspose_cudnn_settings settings);

template <typename T>
void conv2dtranspose_grad_data_device(Tensor<T> *w, Tensor<T> *grad, Tensor<T> *out,
                                      conv2dtranspose_cudnn_settings settings);

template <typename T>
void conv2dtranspose_grad_filter_device(Tensor<T> *x, Tensor<T> *grad, Tensor<T> *out,
                                        conv2dtranspose_cudnn_settings settings);

#endif
}
}