/**
 * @file shortcutlayer.h
 * @author Stephen Qiu
 * @version 1.0
 * @date 2021-10-22
 *
 * @copyright Copyright (c) 2019
 */
#include <vector>
#include "compute/operation.h"
#include "compute/tensor_operations.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace layer {

template <typename T>
class ShortcutLayer : public Layer<T> {
   public:
    ShortcutLayer(op::Operation<T> *input, op::Operation<T> *prev_add);

    virtual std::vector<op::Operation<T> *> get_weights();

   protected:
    void init();
    op::Operation<T> *prev_add;
};

/** An add layer. Used to add the output values of two layers together.
 * 
 * @tparam T numeric
 * @param input initial layer
 * @param prev_add layer to add
 * @return ShortcutLayer<T>* Shortcut layer
 */
template <typename T>
ShortcutLayer<T> *shortcut(op::Operation<T> *input, op::Operation<T> *prev_add);

}  // namespace layer
}  // namespace magmadnn