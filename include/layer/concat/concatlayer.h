/**
 * @file concatlayer.h
 * @author Spencer Smith
 * @brief
 * @version 0.1
 * @date 2022-07-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <vector>
#include "compute/concat4d/concat4dop.h"
#include "compute/operation.h"
#include "compute/variable.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace layer {
    
template <typename T>
class ConcatLayer : public Layer<T> {
   public:
    ConcatLayer(op::Operation<T> *a, op::Operation<T> *b, unsigned int axis, bool copy = true, bool needs_grad = true);

    virtual std::vector<op::Operation<T> *> get_weights();

   protected:
    void init();
    op::Operation<T> *a, *b;
    unsigned int axis;
    bool copy;
    bool needs_grad;
};

/**
 * @brief returns a new concat layer
 *
 * @tparam T
 * @param a tensor to be concatenated
 * @param b tensor to be concatenated
 * @param axis along which axis the concat will be performed
 * @param copy defaults to true, if true it returns a new tensor
 * @param needs_grad defaults to true
 * @return ConcatLayer<T>*
 */
template <typename T>
ConcatLayer<T> *concat(op::Operation<T> *a, op::Operation<T> *b, unsigned int axis, bool copy = true,
                       bool needs_grad = true);
}
}