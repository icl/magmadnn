/**
 * @file lstm.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-28
 */

#pragma once
#include "compute/operation.h"
#include "compute/tensor_operations.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace layer {

template <typename T>
class LSTMLayer : public Layer<T> {
   public:
    LSTMLayer(op::Operation<T> *input, unsigned int num_nodes, bool return_sequences = false,
              op::Operation<T> *h_init = nullptr, op::Operation<T> *c_init = nullptr,
              std::vector<op::Operation<T> *> weights = {});

    virtual ~LSTMLayer();

    std::vector<op::Operation<T> *> get_weights();

    unsigned int get_num_params();

   protected:
    void init();

    unsigned int num_nodes;

    bool return_sequences;

    op::Operation<T> *h_init;
    op::Operation<T> *c_init;

    std::vector<op::Operation<T> *> weights;

    op::Operation<T> *Wi;
    op::Operation<T> *Ui;
    op::Operation<T> *Bi;

    op::Operation<T> *Wf;
    op::Operation<T> *Uf;
    op::Operation<T> *Bf;

    op::Operation<T> *Wo;
    op::Operation<T> *Uo;
    op::Operation<T> *Bo;

    op::Operation<T> *Wc;
    op::Operation<T> *Uc;
    op::Operation<T> *Bc;

    void set_weights(std::vector<op::Operation<T> *>);
};

/** Create an LSTM Layer. Only implemented for GPU.
 * @tparam T can only be float or double
 * @param input contains input in the form of a 3D tensor with dimensions:
 * batch size, time steps, features
 * @param num_nodes determines number of LSTM nodes in the layer
 * @param h_init contains initial h values
 * @param c_init contains initial c values
 * @param return_sequences determines if the output is the full
 * sequence of h values or just the final one
 * @param weights contains initial weight values
 * @return LSTMLayer<T>* a new layer
 */
template <typename T>
LSTMLayer<T> *lstm(op::Operation<T> *input, unsigned int num_nodes, bool return_sequences = false,
                   op::Operation<T> *h_init = nullptr, op::Operation<T> *c_init = nullptr,
                   std::vector<op::Operation<T> *> weights = {});

}  // namespace layer
}  // namespace magmadnn
