#pragma once
#include "compute/operation.h"
#include "compute/tensor_operations.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
    namespace layer {
        template <typename T>
        class GATLayer : public Layer<T> {
            public:
                GATLayer(op::Operation<T> *input, int feat_in, int feat_out, float dropout_rate=0.0);

                virtual ~GATLayer();

                std::string to_string() { return "GATLayer()";}

                std::vector<op::Operation<T> *> get_weights();

                unsigned int get_num_params();

            protected:
                void init();
                
                Tensor<T> *xavier_uniform(const int i, const int j);
                Tensor<T> *concatenate(Tensor<T> *tensor1, Tensor<T> *tensor2);

                int feat_in; 
                int feat_out;
                float dropout_rate;

                Tensor<T> *w_t; 
                Tensor<T> *wh_t;
                Tensor<T> *wh1_t;
                Tensor<T> *wh2_t;
                Tensor<T> *a1_t;
                Tensor<T> *a2_t;
                Tensor<T> *e_t;

                op::Operation<T> *input;
                op::Operation<T> *w;
                op::Operation<T> *wh;
                op::Operation<T> *e;
                op::Operation<T> *attn;
        }; 
    }
}