#pragma once
#include "compute/operation.h"
#include "compute/tensor_operations.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
    namespace layer {
        template <typename T>
        class MHALayer : public Layer<T> {
        public:
            MHALayer(op::Operation<T> *input, 
            const std::vector<int> &orig_sz, 
            const std::vector<int> &proj_sz, 
            const std::vector<int> &seq_len,
            const int head_n, const int batch_sz,
            bool need_grad = true);

            virtual ~MHALayer();
            std::string to_string() { return "MHALayer()";}

            std::vector<op::Operation<T> *> get_weights();

            unsigned int get_num_params();

        protected:
            void init();

            void initTensor(Tensor<T> *tensor, size_t weightSz, double mean, double var);

            inline double randRangeDbl(double bias, double range);

            Tensor<T> *xavier_uniform(unsigned int buffersize, const int embed_dim, const int num_heads); 

            const int numHead;
            const int batchSize;
            const int projQ; 
            const int projK; 
            const int projV; 
            const int projO; 
            const int qSize; 
            const int kSize; 
            const int vSize; 
            const int seqLenQ;
            const int seqLenK; // = seqLenV

            bool need_grad;

            // Tensor<T> q_t; 
            // Tensor<T> k_t;
            // Tensor<T> v_t;
            Tensor<T> *w_t;
            Tensor<T> *qkv_t;

            // op::Operation<T> *q;
            // op::Operation<T> *k;
            // op::Operation<T> *v;
            op::Operation<T> *flatten_qkv;
            op::Operation<T> *w;
            // void initWeights(Tensor<T>* tensor, size_t imageSize, double mean, double var);
};

        template <typename T>
        MHALayer<T> *mha(op::Operation<T> *input,        
            const std::vector<int> &orig_sz, 
            const std::vector<int> &proj_sz, 
            const std::vector<int> &seq_len, 
            const int head_n, const int batch_sz, bool need_grad);

    }  // namespace layer
}  // namespace magmadnn