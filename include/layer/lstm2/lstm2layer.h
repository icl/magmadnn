/**
 * @file lstm2.h
 * @author Pierluigi Cambie-Fabris
 * @version 1.0
 * @date 2022-06-28
 */

#pragma once
#include "compute/operation.h"
#include "compute/tensor_operations.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace layer {

template <typename T>
class LSTM2Layer : public Layer<T> {
   public:
    LSTM2Layer(op::Operation<T> *input, int hidden_size = 1, bool return_sequences = false);

    virtual ~LSTM2Layer();
    std::string to_string() { return "LSTM2Layer()";}

    std::vector<op::Operation<T> *> get_weights();

    unsigned int get_num_params();

   protected:
    void init();

    unsigned int num_nodes;
    unsigned int hidden_size;
    bool return_sequences;

   

    Tensor<T> *h_init_t;
    Tensor<T> *c_init_t;
    Tensor<T> *weights_t;

    op::Operation<T> *h_init;
    op::Operation<T> *c_init;

    op::Operation<T> *weights;
    

};


template <typename T>
LSTM2Layer<T> *lstm2(op::Operation<T> *input, int hidden_size = 1, bool return_sequences = false);

}  // namespace layer
}  // namespace magmadnn



