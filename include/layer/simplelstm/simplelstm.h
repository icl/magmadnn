/**
 * @file simplelstm.h
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-28
 */

#pragma once
#include "compute/operation.h"
#include "compute/tensor_operations.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace layer {

template <typename T>
class SimpleLSTMLayer : public Layer<T> {
   public:
    SimpleLSTMLayer(op::Operation<T> *input, unsigned int num_nodes, op::Operation<T> *h_cur, op::Operation<T> *c_cur,
                    bool return_sequences = false, std::vector<op::Variable<T> *> weights = {});

    virtual ~SimpleLSTMLayer();

    std::vector<op::Operation<T> *> get_weights();

    unsigned int get_num_params();

   protected:
    void init();

    unsigned int num_nodes;

    op::Operation<T> *h_cur;
    op::Operation<T> *c_cur;

    op::Variable<T> *Wi;
    op::Variable<T> *Ui;
    op::Variable<T> *Bi;

    op::Variable<T> *Wf;
    op::Variable<T> *Uf;
    op::Variable<T> *Bf;

    op::Variable<T> *Wo;
    op::Variable<T> *Uo;
    op::Variable<T> *Bo;

    op::Variable<T> *Wc;
    op::Variable<T> *Uc;
    op::Variable<T> *Bc;

    bool return_sequences;
    
    std::vector<op::Variable<T> *> weights;
    
    void set_weights(std::vector<op::Variable<T> *>);
};

/** Create a simple LSTM Layer. Computes the output on the CPU at the moment.
 * @tparam T numeric
 * @param input is the input of the layer and must be a 3D tensor
 * @param num_nodes represents the number of nodes that will be in the layer
 * @param h_cur is the initial h values (2D tensor)
 * @param c_cur is the initial c values (2D tensor)
 * @return SimpleLSTMLayer<T>* a new layer
 */
template <typename T>
SimpleLSTMLayer<T> *simplelstm(op::Operation<T> *input, unsigned int num_nodes, op::Operation<T> *h_cur,
                               op::Operation<T> *c_cur, bool return_sequences = false,
                               std::vector<op::Variable<T> *> weights = {});

}  // namespace layer
}  // namespace magmadnn
