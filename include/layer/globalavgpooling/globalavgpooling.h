/**
 * @file globalavgpooling.h
 * @author Jerry Ho
 * @version 1.0
 * @date 2021-11-19
 *
 * @copyright Copyright (c) 2019
 */
#include <vector>
#include "compute/operation.h"
#include "compute/tensor_operations.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace layer {

template <typename T>
class GlobalAvgpoolingLayer : public Layer<T> {
   public:
    GlobalAvgpoolingLayer(op::Operation<T>* input);

    virtual std::vector<op::Operation<T>*> get_weights();

   protected:
    void init();

};

/** A new GlobalAvgPooling2d layer.
 * @tparam T numeric
 * @param input input, usually output of an activated convolutional layer
 * @param propagate_nan propagate nan values
 * @return GlobalAvgpoolingLayer<T>* a pooling layer
 * 
 */
template <typename T>
GlobalAvgpoolingLayer<T> *globalavgpooling(op::Operation<T> *input);

}  // namespace layer
}  // namespace magmadnn