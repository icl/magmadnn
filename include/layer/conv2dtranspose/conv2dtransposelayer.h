/**
 * @file conv2dtransposelayer.h
 * @author Spencer Smith
 * @brief Header file for the transpose convolution layer
 * @version 0.1
 * @date 2022-06-20
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include <vector>
#include "compute/conv2dtranspose/conv2dtransposeop.h"
#include "compute/operation.h"
#include "compute/variable.h"
#include "layer/layer.h"
#include "tensor/tensor.h"

namespace magmadnn {
namespace layer {
    
// enum padding_t { SAME, VALID };

template <typename T>
class Conv2DTransposeLayer : public Layer<T> {
   public:
    /**
     * @brief Construct a new Conv 2D Transpose Layer object
     *
     * @param input Operation
     * @param filter_shape Defaults to {3, 3}
     * @param padding Defaults to {1, 1}
     * @param output_padding defaults to {1, 1}
     * @param strides Defaults to {2, 2}
     * @param dilation_rates Defaults to {1, 1}
     * @param use_cross_correlation Defaults to true
     * @param use_bias Defualts to false
     * @param filter_initializer
     * @param bias_initializer
     */
    Conv2DTransposeLayer(op::Operation<T>* input, const std::vector<unsigned int>& filter_shape = {3, 3},
                         int out_channels = 1, const std::vector<unsigned int>& padding = {1, 1},
                         const std::vector<unsigned int>& output_padding = {2, 2},
                         const std::vector<unsigned int>& strides = {2, 2},
                         const std::vector<unsigned int>& dilation_rates = {1, 1}, bool use_cross_correlation = true,
                         bool use_bias = false, tensor_filler_t<T> filter_initializer = {GLOROT, {0.0, 0.2f}},
                         tensor_filler_t<T> bias_initializer = {GLOROT, {0.0, 0.2f}});

    // deconstructor
    virtual ~Conv2DTransposeLayer();

    // return the weights for this layer (filter weights)
    virtual std::vector<op::Operation<T>*> get_weights();

    // get the number of parameters in this layers
    virtual unsigned int get_num_params();

    // returns filter
    op::Operation<T>* get_filter() { return filter; }
    // returns bias
    op::Operation<T>* get_bias() { return bias; }
    // returns memory size of the filter tensor
    std::size_t get_memory_size() const { return this->filter_tensor->get_memory_size(); }

   protected:
    void init(const std::vector<unsigned int>& filter_shape);

    Tensor<T>* filter_tensor;
    Tensor<T>* bias_tensor;

    op::Operation<T>* filter;
    op::Operation<T>* bias;

    int in_channels, out_channels;
    bool use_cross_correlation, use_bias;
    int pad_h, pad_w, stride_h, stride_w, dilation_h, dilation_w;
    int out_pad_h, out_pad_w;

    tensor_filler_t<T> filter_initializer, bias_initializer;
};

/**
 * @brief Functions that will actually be called in MagmaDNN Neural Nets.
 *          All of the parameters after 'out_channels' is very touchy. Any change
 *          you make to them might cause errors. The default parameters are set to
 *          double the size of the input.
 *
 * @tparam T numeric
 * @param input the input data; must be a 4D tensor in format NCHW (N-batch, C-Channel, H-height, W-Width)
 * @param filter_shape shape of the convolution kernel. must be a 2 dimensional vector; defaults to {3,3}
 * @param out_channels number of output filters; defaults to 1
 * @param padding the padding size to use. must be a two dimensional vector; defaults to {1,1}
 * @param output_padding the output padding size to use. must be a two dimensional vector; defaults to {0, 0}
 * @param strides striding of convolution. must be a two dimensional vector; defaults to {1,1}
 * @param dilation_rates rate of dilation. must be a two dimensional vector; defaults to {1,1}
 * @param use_cross_correlation whether to do a cross correlation convolution or standard convolution; defaults to cross correlation
 * @param use_bias use convolutional bias or not; defaults to false
 * @param filter_initializer how to initialize the filter tensor; defaults to {GLOROT,{0.0f,0.2f}}
 * @param bias_initializer how to initialize the bias tensor; defaults to {GLOROT, {0.0f,0.2f}}
 * @return Conv2dLayerTranspose<T>* a new layer
 */
template <typename T>
Conv2DTransposeLayer<T>* conv2dtranspose(op::Operation<T>* input,
                                         const std::vector<unsigned int>& filter_shape = {3, 3}, int out_channels = 1,
                                         const std::vector<unsigned int>& padding = {1, 1},
                                         const std::vector<unsigned int>& output_padding = {1, 1},
                                         const std::vector<unsigned int>& strides = {2, 2},
                                         const std::vector<unsigned int>& dilation_rates = {1, 1},
                                         bool use_cross_correlation = true, bool use_bias = false,
                                         tensor_filler_t<T> filter_initializer = {GLOROT, {0.0, 0.2f}},
                                         tensor_filler_t<T> bias_initializer = {GLOROT, {0.0, 0.2f}});
}
}
