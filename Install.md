## Installing
-------------------------

#### Dependencies
First make sure you have the right set up to install MagmaDNN. It is recommended to use MagmaDNN on a modern CPU paired with a recent GPU. It is also recommended that you have >=8GB of memory. Below are the listed software requirements:

Any MagmaDNN install:

- c++11 capable compiler (recommended g++ >= 8)
- BLAS implementation (openblas, atlas, intel-mkl, etc...)
- make
- git

MagmaDNN-GPU install:

- [CUDA](https://developer.nvidia.com/cuda-downloads) >=10 (recommended CUDA >=11.0)
- [CuDNN](https://developer.nvidia.com/rdp/cudnn-download) >=7.6 (recommended CuDNN >=8.0)
- [Magma](https://icl.utk.edu/magma/software) >=2.5.0

_Note:_ if installing dependencies in non-standard paths make sure to update `LD_LIBRARY_PATH` so that your operating system can find the shared object files.

OpenBLAS is the easiest BLAS to install, while intel-mkl might be the fastest. OpenBLAS, on Ubuntu, can be easily installed with `apt install libopenblas-dev`.

#### Downloading

Currently MagmaDNN does not offer any pre-compiled binaries, so it must be built from source. Once you have installed all of the dependencies the source can be downloaded
by cloning the [repository](https://bitbucket.org/icl/magmadnn) 

```sh
git clone https://bitbucket.org/icl/magmadnn.git
cd magmadnn
```

Or downloading and extracting the [tar file](https://bitbucket.org/icl/magmadnn/downloads/?tab=tags)

#### OpenCV

Currently needed for UNet examples

MagmaDNN allows for images and data to be read through OpenCV but it is not necessary for it to run. 

To install OpenCV, follow the documentation [here](https://docs.opencv.org/4.x/d7/d9f/tutorial_linux_install.html)

If not needed you can turn off the `HAVE_OPENCV` flag (located on line 17 in the makefile) when compiling

#### Compiling and Installing

MagmaDNN uses a `make.inc` file to set compile flags/options. The `make.inc-examples` directory contains examples. Copy one over and adjust your compile settings accordingly. The example make.inc files document which compile settings can be set.
In the magmadnn directory, run:

```sh
cp ./make.inc-examples/make.inc-standard ./make.inc
vim make.inc  # set make settings
# or `nano make.inc` if you don't have vim
```

Once you have set up the `make.inc` file it is time to compile and install using

```sh
make
make install
```

The MagmaDNN compilation can take a couple minutes especially if compiling the GPU version. To speed it up compile in parallel with `make -j4` (or `-jn` where _n_ is the number of cores in your CPU). 
`sudo` may be necessary when running `make install` if compiling to `usr/local`.

Or you can compile using cmake.
In the magmadnn directory, run:

```sh
cmake .
cmake --build . -jn # where n is the number of cores in the CPU
make install
```

#### Testing
It is good to test your install to make sure everything is working. MagmaDNN comes with a suite of testers that will ensure your install is working correctly. To run them,
In the magmadnn directory, run:

```sh
make testing
cd testing
sh run_tests.sh
```

#### Docs
MagmaDNN uses doxygen for its documentation. To build the docs you must have `doxygen` installed. 

```sh
apt-get install graphviz            # installs dot for the call graphs
apt install doxygen texlive-full    # installs doxygen and latex
```

To make the docs,
In the magmadnn directory, run:

```sh
make docs
```

#### Examples
There are several example files in the `examples/` folder. They are simple and commented to give an idea for what MagmaDNN code typically looks like. They can be made with
In the magmadnn directory, run:

```sh
make examples
```

Follow the README.md in the `examples/` folder for instructions on how to run examples

