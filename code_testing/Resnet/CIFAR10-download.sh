# Download CIFAR-10
wget https://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz

# extract files
tar -xvf cifar-10-binary.tar.gz

# move files to Data and then remove them
mv ./cifar-10-batches-bin/*.bin ./Data/
rm -r cifar-10-batches-bin/
rm cifar-10-binary.tar.gz
