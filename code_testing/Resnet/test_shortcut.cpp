/* resnet_test.cpp

   tests resent 18 on CIFAR10 data
   implements own basic block in code
   no shortcut layer between blocks

*/
#include "Arguments.h"
#include "magmadnn.h"
#include "models/resnet.h"

#include <iostream>

using namespace magmadnn;

int main(int argc, char **argv) {
    std::string context = "resnet_cifar10";

    // Data type
    using T = float;

#if defined(MAGMADNN_HAVE_MPI)
    MPI_Init(&argc, &argv);
#endif

    magmadnn_init();

    magmadnn::Arguments args;
    args.parse(context, argc, argv);

    // Location of the CIFAR-10 dataset
    std::string const cifar10_dir = "./Data";
    // Load CIFAR-10 trainnig dataset
    magmadnn::data::CIFAR10<T> train_set(cifar10_dir, magmadnn::data::Debug);
    magmadnn::data::CIFAR10<T> test_set(cifar10_dir, magmadnn::data::Test);

    // Training parameters
    magmadnn::model::nn_params_t params;
    params.batch_size = 32;
    // params.batch_size = 256;
    // params.n_epochs = 500;
    params.n_epochs = 1;
    if (args.learning_rate > 0) {
        params.learning_rate = args.learning_rate;
    } else {
        params.learning_rate = 0.1;
        // params.learning_rate = 0.05;
        // params.learning_rate = 0.01;
        // params.learning_rate = 0.001;
        // params.learning_rate = 0.002;
        // params.learning_rate = 1e-4;
        // params.learning_rate = 1e-5;
        // params.learning_rate = 1e-6;
        // params.learning_rate = 1.0;
        // params.decaying_factor = 0.99;
    }

    int num_stacked_blocks = 1;

    // Memory
    magmadnn::memory_t training_memory_type;
#if defined(MAGMADNN_HAVE_CUDA)
    int devid = 0;
    // cudaSetDevice(1);
    cudaGetDevice(&devid);
    std::cout << "[" << context << "] GPU training (" << devid << ")" << std::endl;
    training_memory_type = DEVICE;
#else
    training_memory_type = HOST;
#endif

    // bool enable_shortcut = true;
    bool enable_shortcut = false;

    // std::cout << "[" << context << "] Image dimensions: " << train_set.nrows() << " x " << train_set.ncols()
    //           << std::endl;
    // std::cout << "[" << context << "] Number of chanels: " << train_set.nchanels() << std::endl;
    // std::cout << "[" << context << "] Number of classes: " << train_set.nclasses() << std::endl;
    // std::cout << "[" << context << "] Training set size: " << train_set.nimages() << std::endl;

    auto x_batch =
        op::var<T>("x_batch", {params.batch_size, train_set.nchanels(), train_set.nrows(), train_set.ncols()},
                   {NONE, {}}, training_memory_type);

    auto input = layer::input<T>(x_batch);

    auto conv2d1 = layer::conv2d<T>(input->out(), {3, 3}, 64, {1, 1}, {1, 1}, {1, 1});
    auto bn1 = layer::batchnorm(conv2d1->out());

    std::vector<layer::Layer<T> *> blocks(0, nullptr);

    for (int i = 0; i < num_stacked_blocks; ++i) {
        int out_layer;
        op::Operation<T> *block1_input = nullptr;
        std::vector<unsigned int> strides;

        if (i == 0) {
            // First block
            block1_input = bn1->out();
            out_layer = 64;
            strides = {1, 1};
        } else {
            block1_input = blocks.back()->out();
            out_layer = 128;
            strides = {2, 2};
        }

        auto block1 = Resnet<T>::basic_block(block1_input, out_layer, strides, enable_shortcut);
        auto block2 = Resnet<T>::basic_block(block1.back()->out(), out_layer, {1, 1}, enable_shortcut);

        blocks.insert(std::end(blocks), std::begin(block1), std::end(block1));
        blocks.insert(std::end(blocks), std::begin(block2), std::end(block2));
    }

    auto inshape_ = blocks.back()->out()->eval()->get_shape();
    //  T norm = static_cast<T>(1.0) / static_cast<T>(size);
    auto globalavg = layer::pooling<T>(blocks.back()->out(), {inshape_[2], inshape_[3]}, {0, 0}, {1, 1}, AVERAGE_POOL);
    auto flatten = layer::flatten<T>(globalavg->out());

    auto fc1 = layer::fullyconnected<T>(flatten->out(), train_set.nclasses(), false);
    auto act2 = layer::activation<T>(fc1->out(), layer::SOFTMAX);

    auto output = layer::output<T>(act2->out());

    std::vector<layer::Layer<T> *> layers;

    layers.insert(std::end(layers), input);

    layers.insert(std::end(layers), conv2d1);
    layers.insert(std::end(layers), bn1);

    layers.insert(std::end(layers), std::begin(blocks), std::end(blocks));
    layers.insert(std::end(layers), globalavg);
    layers.insert(std::end(layers), flatten);
    layers.insert(std::end(layers), fc1);
    layers.insert(std::end(layers), act2);

    layers.insert(std::end(layers), output);

    model::NeuralNetwork<T> model(layers, optimizer::CROSS_ENTROPY, optimizer::SGD, params);

    model::metric_t metrics;
	 model.fit(&train_set.images(), &train_set.labels(), metrics,true);

   // op::utility::print_compute_graph(x_batch, false);

    delete output;

    magmadnn_finalize();

#if defined(MAGMADNN_HAVE_MPI)
    MPI_Finalize();
#endif

    return 0;
}
