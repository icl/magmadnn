import torchvision
import torch 

from torchvision import models
from torchsummary import summary

device = 'cuda' if torch.cuda.is_available() else 'cpu'

model = models.resnet18()
if torch.cuda.is_available():
    model.cuda()

summary(model, (3, 32, 32))
# print(model)
