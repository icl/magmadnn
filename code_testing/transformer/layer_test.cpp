/**
 * @file layer_test.cpp
 * @author Xiaoyang Li
 * @author Winglim Lau
 * @version 1.0
 * @date 2023-07-28
 *
 * @copyright Copyright (c) 2023
 *
 * Tests the time, prediction loss, training loss of the MHA layer, where the task is to predict all zero outputs.
 */

#include "magmadnn.h"
#include "ctime"
#include <iostream>
#include <stdio.h>
using namespace magmadnn;
using T = float;

#define e_idx 0
#define l_idx 1
#define s_idx 3

double metric(Tensor<T> *prediction) {
    double loss = 0.0;
    unsigned int entry_n = prediction->get_size();
    for (int i = 0; i < entry_n; i++) {
        double entry = prediction->get(i);
        loss += entry * entry; 
    }
    return loss / (double) entry_n;  
}

int main()
{
    magmadnn_init();
    std::vector<int> sz = {4, 8, 16, 32};
    std::vector<unsigned int> epoch_n = {1000, 2000, 3000};
    std::vector<double> lr = {1e-3, 1e-4, 1e-5};

    model::nn_params_t params;
    params.batch_size = 4;
    // params.n_epochs = epoch_n[e_idx];
    params.n_epochs = 145;
    params.learning_rate = lr[l_idx];
    const unsigned int numHead = 2;
    const unsigned int projQ = 2;
    const unsigned int projK = 2;
    const unsigned int projV = 2;
    const unsigned int projO = 2;
    const unsigned int qSize = sz[s_idx];
    const unsigned int kSize = sz[s_idx];
    const unsigned int vSize = sz[s_idx];
    const unsigned int seqLenQ = sz[s_idx];
    const unsigned int seqLenK = sz[s_idx]; // = seqLenV
    const unsigned int batchSz = params.batch_size;
    const unsigned int inputSz = (qSize * seqLenQ + kSize * seqLenK + vSize * seqLenK);

    model::metric_t metric_out;

    op::Variable<T> *x_batch = op::var<T>("x_batch",
                                          {params.batch_size, inputSz},
                                          {NONE, {}}, DEVICE);

    layer::InputLayer<T> *input = layer::input(x_batch);
    printf("input layer...\n\n");

    // layer::FlattenLayer<T> *flatten = layer::flatten(input->out());
    // printf("flatten layer...\n\n");

    layer::MHALayer<T> *mha = layer::mha(input->out(),
                                         {(int)qSize, (int)kSize, (int)vSize},
                                         {projQ, projK, projV, projO},
                                         {(int)seqLenQ, (int)seqLenK}, numHead, batchSz, true);
    printf("MHA layer...\n\n");

    // layer::FlattenLayer<T> *flatten = layer::flatten(mha->out());

    layer::OutputLayer<T> *output = layer::output(mha->out());
    printf("output layer...\n\n");

    std::vector<layer::Layer<T> *> layers = {input, /*flatten,*/ mha, output};
    printf("stacking layers...\n\n");

    model::NeuralNetwork<T> mhaModel(layers, optimizer::MSE, optimizer::SGD, params); // TODO: debug
    printf("finish setting up model...\n\n");

    std::vector<unsigned int> x_shape = {/*params.batch_size*/800, inputSz};
    std::vector<unsigned int> y_shape = {/*params.batch_size*/800, projO, seqLenQ};

    Tensor<T> *Y = new Tensor<T>(y_shape, {CONSTANT, {(T)0.0}}, DEVICE);
    Tensor<T> *X = new Tensor<T>(x_shape, {UNIFORM, {(T)-1.0, (T)1.0}}, DEVICE);
    // Tensor<T> *X = new Tensor<T>({1, 3, 4, 2}, {CONSTANT, {(T)1.0}}, DEVICE);
    // X->set(0, -2.57385194e-01);
    // X->set(1,  4.56258357e-02);
    // X->set(2, -8.33792746e-01);
    // X->set(3, -1.17590308e+00);
    // X->set(4,  6.14247143e-01);
    // X->set(5, -5.32708943e-01);
    // X->set(6, -8.32816307e-03);
    // X->set(7,  9.45007801e-01);
    // X->set(8,  7.47850657e-01);
    // X->set(9,  9.42320824e-01);
    // X->set(10, 5.45689464e-01);
    // X->set(11, 2.16631100e-01);

    // Y->set(0, -1.67855084e+00);
    // Y->set(1,  1.40041947e+00);
    // Y->set(2, -2.18901157e+00);
    // Y->set(3,  1.78114355e+00);

    // io::print_tensor(X);
    // io::print_tensor(Y);

    mhaModel.summary();
    mhaModel.fit(X, Y, metric_out, true);

    Tensor<T> *X_test = new Tensor<T>({params.batch_size, inputSz}, {UNIFORM, {(T)-1.0, (T)1.0}}, DEVICE);
    Tensor<T> *Y_pred = mhaModel.predict(X_test);
    // printf("########## input ############\n");
    // io::print_tensor(X_test);
    // printf("########## ground truth ############\n");
    // io::print_tensor(Y);
    printf("########## prediction ############\n");
    io::print_tensor(Y_pred);
    printf("Prediction Loss: %.4g\n\n", metric(Y_pred));

    magmadnn::magmadnn_finalize();
}