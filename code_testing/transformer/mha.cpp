/**
 * @file mha.cpp
 * @author Xiaoyang Li
 * @author Winglim Lau
 * @version 1.0
 * @date 2023-07-28
 *
 * @copyright Copyright (c) 2023
 *
 * Tests if our implementation of the MHA operator op::mha1 generates the same output as the cuDNN sample code.
 * To use this, change base to be the directory whatever/cudnn_samples_v8/multiHeadAttention/ which contains the output files of
 * the cuDNN code, such as q.dat, k.dat, v.dat.
 * Set the seeds that generate the outputs to be same.
 */

#include <cstdio>
#include <cassert>
#include "magmadnn.h"

using namespace magmadnn;
using T = float;
#define MEM DEVICE
// for reading cuDNN sample outputs
#define gc(f) fgetc(f)
#define isdigit(x) (x >= '0' && x <= '9')
#define DBG_FLAG 0

const uint32_t numHead = 2; // need
const uint32_t projQ = 2; // need
const uint32_t projK = 2; // need
const uint32_t projV = 2; // need
const uint32_t projO = 2; // need
const uint32_t beamSz = 1; // always 1
const uint32_t batchSz = 1;
const uint32_t seqLenQ = 2; // i.e. number of rows in the matrix Q
const uint32_t seqLenK = 2; // = seqLenV = number of rows in the matrix K or V   
const uint32_t qSize = 2; // need
const uint32_t kSize = 2; // need
const uint32_t vSize = 2; // need
const uint32_t weightSz = 32; /* #head * (\sum projSz * Sz + projV * projO) => 2 * (3 * 3 * 4 + 4 * 4) */
const std::string base = "./Data"; // directory which contains the cuDNN outputs
const size_t seed = 2333; // change it so that it is the same as the one used in cuDNN code


// for reading files
template <typename T>
void read(T &x, FILE *f) {
    float tmp = 1.0;
    bool flag = 0;
    x = 0;
    char ch = gc(f);
    // skip
    for ( ; !isdigit(ch); ch=gc(f)) {
        if (ch == '-') {
            flag = 1;
        }
    }
    // integer
    for ( ; isdigit(ch); ch=gc(f)) {
        x = 10 * x + (ch - '0');
    }
    // fraction
    if (ch == '.') {
        for (ch=gc(f); isdigit(ch); ch=gc(f)) {
            tmp /= 10.0;
            x += tmp * (ch - '0');
        }
    }
    if (ch == 'e') {
        int exp = 0;
        ch = gc(f);
        if (ch == '+') {
            for (ch=gc(f); isdigit(ch); ch=gc(f)) {
                exp = 10 * exp + (ch - '0');
            }
            while(exp--){
                x *= 10;
            }
        } else if (ch == '-'){
            for (ch=gc(f); isdigit(ch); ch=gc(f)) {
                exp = 10 * exp + (ch - '0');
            }
            while(exp--){
                x /= 10;
            }
        }
    }
    if (flag) {
        x = -x;
    }
}

// for reading files
void readFile(op::Variable<T> *var, std::string filePathName) {
    FILE *fp = fopen(filePathName.c_str(), "r");
    if (fp == NULL) {
        const char *reason = (errno ? strerror(errno) : "unknown reason");
        fprintf(stderr, "ERROR: failed to open '%s' file (%s)\n\n", filePathName.c_str(), reason);
        exit(-1);
    }
    float value = 0.0;
    uint32_t idx = 0;
    uint32_t size = var->get_output_size();
    while(true) {
        read(value, fp);
        var->get_output_tensor()->set(idx++, value);
        if (idx >= size) {
            break;
        }
    }
    fclose(fp);
}

// for reading files
void readFileV2(op::Variable<T> *var, std::string filePathName, int rMax, int cMax) {
    FILE *fp = fopen(filePathName.c_str(), "r");
    if (fp == NULL) {
        const char *reason = (errno ? strerror(errno) : "unknown reason");
        fprintf(stderr, "ERROR: failed to open '%s' file (%s)\n\n", filePathName.c_str(), reason);
        exit(-1);
    }
    float value = 0.0;
    uint32_t idx = 0;
    int r = 0;
    int c = 0;
    uint32_t size = var->get_output_size();
    while(true) {
        read(value, fp);
        var->get_output_tensor()->set(r + c * rMax, value);
        if (c == cMax - 1 && r == rMax - 1) {
            break;
        }
        r += (c + 1) / cMax;
        c = (c + 1) % cMax;
    }
    fclose(fp);
}

// random number generator
inline double randRangeDbl(double bias, double range) {
    return range * drand48() + bias;
}

// initialize tensor with random numbers using seed
void initBuffer(Tensor<T> *tensor, size_t imageSize, double mean, double var) {
    srand48(seed);
    double range = sqrt(12.0 * var);
    double bias  = mean - 0.5 * range;
    for (size_t index = 0; index < imageSize; index++) {
#if DBG_FLAG
        printf("%d\n", index);
#endif
        // image[index] = (float) randRangeDbl(bias, range);
        tensor->set(index, (float) randRangeDbl(bias, range));
    }
}

int main(){
    magmadnn_init();

    op::Variable<T> *q = op::var<T>("Q", {beamSz, batchSz, qSize, seqLenQ}, {CONSTANT, {0.0f}}, MEM);
    op::Variable<T> *k = op::var<T>("K", {beamSz, batchSz, kSize, seqLenK}, {CONSTANT, {0.0f}}, MEM); 
    op::Variable<T> *v = op::var<T>("V", {beamSz, batchSz, vSize, seqLenK}, {CONSTANT, {0.0f}}, MEM);
    op::Variable<T> *w = op::var<T>("W", {weightSz}, {CONSTANT, {0.0f}}, MEM); 


/**
 * Q
Tensor size of {1, 1, 3, 2}
{
  {
    |-1.06894,  -1.08879, |
    | 1.13586,  -0.83866, |
    |-1.21149,  -0.34243, |
  }
}
 * 
*/
    readFileV2(q, base + "q.dat", qSize, seqLenQ);
    io::print_tensor(q->get_output_tensor());

/**
 * K
Tensor size of {1, 1, 3, 2}
{
  {
    | 0.09655,   0.97182, |
    | 0.10509,  -0.97218, |
    | 0.30904,   0.34822, |
  }
}
 * 
*/
    
    readFileV2(k, base + "k.dat", kSize, seqLenK);
    io::print_tensor(k->get_output_tensor());

/**
 * V
Tensor size of {1, 1, 3, 2}
{
  {
    | 0.01279,  -1.14005, |
    |-0.35558,  -0.22725, |
    | 1.14163,  -0.23881, |
  }
}
 * 
*/

    readFileV2(v, base + "v.dat", vSize, seqLenK);
    io::print_tensor(v->get_output_tensor());

/**
 * 
Tensor size of {1, 1, 1, 104}
{
  {
    | 0.59003,  -0.69924,  -0.39634,  -0.43323,   
    1.22156,  -0.51468,  -1.20007,   0.01676,   
    0.21981,  -0.54033,  -0.20228,  -0.34052, 
    -0.30566,  -0.42204,  -0.76110,   0.51723,   
    0.07102,  -0.43383,   1.13664,   1.22332,   
    0.21585,   0.43421,   0.85966,   0.57300,   
    0.83723,  -0.16164,   0.47975,   0.30879,  
    -0.90207,   0.58709,  -0.70064,   0.25257,  
    -0.56439,   0.05537,   0.15098,  -0.82433,  
    0.10505,  -0.00881,  -0.77136,  -0.66814,   
    0.56695,  -1.20755,  -0.62613,   0.23379,   
    0.78856,  -0.19998,   0.49378,   0.92069,  
    -0.86758,   0.26577,  -0.30674,   0.39362,  
    -1.19055,  -0.82288,  -0.76558,  -0.26377,  
    -0.14637,  -0.68886,  -0.65166,   0.38725,  
    -0.77607,   0.74311,   0.20857,  -0.77225,   
    0.87180,   0.10480,  -0.29728,  -1.17612,  
    -0.95112,   0.13342,  -0.53752,   0.96707,  
    -1.02985,  -0.64885,   1.05483,   0.12761,  
    -1.07924,  -1.09302,  -0.60708,   0.06520,   
    0.16983,   0.11377,  -0.63864,  -1.18398,   
    0.81295,   1.20572,   1.08720,   0.57206,   
    0.44007,  -1.03939,  -0.62054,  -1.14267,  
    -0.68141,   0.11229,   0.45668,   0.99748,   
    0.82359,  -1.02074,  -0.82184,  -0.92918,   
    0.27398,  -0.23400,   1.16118,   0.46598, |
  }
}
 * 
*/
    // readWFiles(w, {base+"wq.dat", base+"wk.dat", base+"wv.dat", base+"wo.dat"});
    initBuffer(w->get_output_tensor(), weightSz, 0.0, 0.5);
    io::print_tensor(w->get_output_tensor());


/*
 * ##################### Test ##########################
 * 
*/

    // auto mha = op::mha1(q, k, v, w, true);

    ::magmadnn::math::attnOptions options = ::magmadnn::math::attnOptions(
        1,                      // attnTrain
        CUDNN_DATA_FLOAT,       // attnDataType
        CUDNN_DATA_FLOAT,       // attnCompType
        0,                      // attnQueryMap
        numHead,                // attnNumHeads
        1,                      // attnBeamSize
        1.0,                    // attnSmScaler
        0.0,                    // attnDropoutRate
        qSize,                  // attnQsize
        kSize,                  // attnKsize
        vSize,                  // attnVsize
        projQ,                  // attnProjQSize
        projK,                  // attnProjKSize
        projV,                  // attnProjVSize
        projO,                  // attnProjOSize
        seqLenQ,                // attnSeqLenQ,
        seqLenK,                // attnSeqLenK,  
        batchSz,                // attnBatchSize,
        0,                      // attnDataLayout
        0,                      // attnResLink
        0,                      // attnProjBias
        0,                      // attnSweep
        0,                      // attnRandGeom
        seed,                   // attnRandSeed
        1                       // attnFileDump
    );

    op::Variable<T> *x = op::var<T>("X", {q->get_output_size() + k->get_output_size() + v->get_output_size()}, {CONSTANT, {0.0f}}, MEM); 

    cudaErrchk(cudaMemcpy(x->get_output_tensor()->get_ptr(), q->get_output_tensor()->get_ptr(), sizeof(T) * q->get_output_size(), cudaMemcpyDeviceToDevice));
    cudaErrchk(cudaMemcpy(x->get_output_tensor()->get_ptr() + q->get_output_size(), k->get_output_tensor()->get_ptr(), sizeof(T) * k->get_output_size(), cudaMemcpyDeviceToDevice));
    cudaErrchk(cudaMemcpy(x->get_output_tensor()->get_ptr() + q->get_output_size() + k->get_output_size(), v->get_output_tensor()->get_ptr(), sizeof(T) * v->get_output_size(), cudaMemcpyDeviceToDevice));

    io::print_tensor(x->get_output_tensor());
    auto mha = op::mha1(x, w, options, false);

    mha->print_params();

    // test eval()
    printf("*** test eval() ***\n");
    Tensor<T> *out_tensor = mha->eval();    
    io::print_tensor(out_tensor);

    // test grad()
    printf("*** test grad() ***\n");
    Tensor<T> *grad = new Tensor<T>({beamSz, batchSz, projO, seqLenQ}, {CONSTANT,{(T)0}}, DEVICE);
    // initBuffer(grad, beamSz * batchSz * projO * seqLenQ, 0.0, 0.5);
    grad->set(0, 1.48704201e-01);
    grad->set(1,-7.70772576e-01);
    grad->set(2,-7.48790741e-01);
    grad->set(3,-5.37881076e-01);
    
    // Tensor<float> *out_grad = mha->grad(NULL, x, grad);
    // io::print_tensor(out_grad);

    // test weight grad 
    Tensor<float> *out_grad_w = mha->grad(NULL, w, grad);
    io::print_tensor(out_grad_w);

    magmadnn_finalize();
}