/**
 * @file unit_test3.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */
#include "magmadnn.h"

using namespace magmadnn;
using T = float;
int main( void ) {

	magmadnn_init();
/*
	op::Variable<T> *x = op::var<T>("x", {3,2}, {UNIFORM, {(T)0, (T)10}}, DEVICE);

	op::Variable<T> *y = op::var<T>("y", {3,2}, {UNIFORM, {(T)10, (T)20}}, DEVICE);

	op::Variable<T> *z = op::var<T>("z", {3,2}, {UNIFORM, {(T)20,(T)30}}, DEVICE);
*/

	op::Variable<T> *x = op::var<T>("x", {6,9}, {UNIFORM, {(T)0, (T)10}}, DEVICE);

	op::Variable<T> *y = op::var<T>("y", {6,9}, {UNIFORM, {(T)10, (T)20}}, DEVICE);

	op::Variable<T> *z = op::var<T>("z", {6,9}, {UNIFORM, {(T)20,(T)30}}, DEVICE);

	io::print_tensor(x->get_output_tensor());
	io::print_tensor(y->get_output_tensor());
	io::print_tensor(z->get_output_tensor());
	std::cout << "NOTE: This is the result of concatinating three 6x9" <<
		" matrices into what should be one 6x3x9 matrix\n";
//	std::vector<op::Operation<T> *> vec = {x,y};
	std::vector<op::Operation<T> *> vec = {x,y,z};
	op::Concat3dOp<T> *con = op::concat3d(vec, 1);
	
	con->eval();

	io::print_tensor(con->get_output_tensor());
	
	magmadnn_finalize();
}
