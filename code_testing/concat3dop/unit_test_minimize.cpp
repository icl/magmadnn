/**
 * @file unit_test_minimize.cpp
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */

/*
 * This unit test ensures that the concatination operation works with the
 * minimization feature. Currently not functional, that is the test.
 * */
#include "magmadnn.h"
#define mem_type HOST
using namespace magmadnn;
using T = float;
int main( void ) {

	magmadnn_init();

	op::Variable<T> *x = op::var<T>("x", {3,2}, {CONSTANT, {(T)4}}, mem_type);

	op::Variable<T> *y = op::var<T>("y", {3,2}, {CONSTANT, {(T)2}}, mem_type);

	op::Variable<T> *z = op::var<T>("z", {3,2}, {CONSTANT, {(T)3}}, mem_type);
	
	io::print_tensor(x->get_output_tensor());
	io::print_tensor(y->get_output_tensor());
	io::print_tensor(z->get_output_tensor());

	std::cout << "\nNOTE: This is the result of concatinating three 3x2" <<
		" matrices into what should be one 3x3x2 matrix\n";
	std::vector<op::Operation<T> *> vec = {x,y,z};
	op::Concat3dOp<T> *con = op::concat3d(vec, 1);
	
	con->eval();

	io::print_tensor(con->get_output_tensor());

	std::cout << "\nNOTE: This is a slice of the concatinated operation.\n";	

	op::SliceOp<T> *sliced = op::slice(con, 1, 0);
	sliced->eval();
	
	io::print_tensor(sliced->get_output_tensor());

	std::cout << "\nNOTE: This is the sum of all elements in the slice.\n";
	
	op::ReduceSumOp<T> *sum = op::reducesum(op::reducesum(sliced, 1), 0);
	sum->eval();
	std::cout << "\nreduced sum: " << sum->get_output_tensor()->get({0}) << "\n";
	
	std::cout << "\nNOTE: These are the three tensors, x, y, z, after being modified to"
		<< " minimize the sum. Only x should change and all values should decrease.\n";

	optimizer::GradientDescent<T> optim((T)1, (T)0);
	optim.minimize(sum, {x});


	io::print_tensor(x->get_output_tensor());
	io::print_tensor(y->get_output_tensor());
	io::print_tensor(z->get_output_tensor());

	magmadnn_finalize();
}
