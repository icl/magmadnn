/**
 * @file unit_test.cpp
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */
#include "magmadnn.h"

using namespace magmadnn;
using T = float;
int main( void ) {

	magmadnn_init();

	op::Variable<T> *x = op::var<T>("x", {3,2}, {CONSTANT, {(T)4}}, HOST);

	op::Variable<T> *y = op::var<T>("x", {3,2}, {CONSTANT, {(T)2}}, HOST);

	op::Variable<T> *z = op::var<T>("x", {3,2}, {CONSTANT, {(T)3}}, HOST);
	
	io::print_tensor(x->get_output_tensor());
	io::print_tensor(y->get_output_tensor());
	io::print_tensor(z->get_output_tensor());

	std::cout << "NOTE: This is the result of concatinating three 3x2" <<
		" matrices into what should be one 3x3x2 matrix\n";
//	std::vector<op::Operation<T> *> vec = {x,y};
	std::vector<op::Operation<T> *> vec = {x,y,z};
	op::Concat3dOp<T> *con = op::concat3d(vec, 1);
	
	con->eval();

	io::print_tensor(con->get_output_tensor());
	
//	std::cout << '\n' <<  con->get_output_shape(0) << '\n' <<  con->get_output_shape(1) << '\n' <<  con->get_output_shape(2);


	Tensor<T> *grad = new Tensor<T>({3,3,2}, {UNIFORM, {(T)0, (T)10}}, HOST);

        std::cout << "\n NOTE: This is the upstream gradient for this operation\n";
        io::print_tensor(grad);

        std::cout << "\n NOTE: This should be the gradient of the operation's inputs"
                << " wih respect to the various input tensorsi, given the above"
		<< " upstream gradient.";
	
        Tensor<T> *out_grad = con->grad(x,x,grad);
        io::print_tensor(out_grad);

        Tensor<T> *out_grad2 = con->grad(y,y,grad);
        io::print_tensor(out_grad2);

        Tensor<T> *out_grad3 = con->grad(z,z,grad);
        io::print_tensor(out_grad3);



	magmadnn_finalize();
}
