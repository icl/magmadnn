/**
 * @file resnet_cifar10.cpp
 * @author Florent Lopez
 * @version 1.0
 * @date 2020-07-21
 *
 * @copyright Copyright (c) 2020
 */

#include "magmadnn.h"

#include <iostream>

using namespace magmadnn;
using namespace std;

int main(int argc, char** argv) {

   std::string context = "resnet_cifar10";
   
   // Data type
   using T = float;

   magmadnn_init();

   // Location of the CIFAR-10 dataset
   std::string const cifar10_dir = "./Data";
   // Load CIFAR-10 trainnig dataset
   magmadnn::data::CIFAR10<T> train_set(cifar10_dir, magmadnn::data::All);
   cout << "read data for train" << endl;

   magmadnn::data::CIFAR10<T> all_set(cifar10_dir, magmadnn::data::Train);
   cout << "read data for all" << endl;

   // Training parameters
   magmadnn::model::nn_params_t params;
   params.batch_size = 128;
   params.n_epochs = 1;
   params.learning_rate = 0.001;

   magmadnn::memory_t training_memory_type;
#if defined(MAGMADNN_HAVE_CUDA)
   int devid = 0;
   // cudaSetDevice(1);
   cudaGetDevice(&devid);
   std::cout << "[" << context << "] GPU training (" << devid << ")" << std::endl;
   training_memory_type = DEVICE;
#else
   training_memory_type = HOST;
#endif
   
   std::cout << "[" << context << "] Image dimensions: " << train_set.nrows() << " x " << train_set.ncols() << std::endl;
   std::cout << "[" << context << "] Number of chanels: " << train_set.nchanels() << std::endl;
   std::cout << "[" << context << "] Number of classes: " << train_set.nclasses() << std::endl;
   std::cout << "[" << context << "] Training set size: " << train_set.nimages() << std::endl;
   
   auto x_batch = op::var<T>(
         "x_batch",
         {params.batch_size, train_set.nchanels(),  train_set.nrows(), train_set.ncols()},
         {NONE, {}},
         training_memory_type);

   auto input = layer::input<T>(x_batch);
   auto conv2d1 = layer::conv2d<T>(input->out(), {3, 3}, 16, {1, 1}, {1, 1}, {1, 1});
   auto bn1 = layer::batchnorm(conv2d1->out());      
   auto act1 = layer::activation<T>(bn1->out(), layer::RELU);
   auto flatten = layer::flatten(act1->out());
   auto linear = layer::fullyconnected(flatten ->out(), 10,false);
   auto output = layer::output(linear->out());

   std::vector<layer::Layer<float> *> layers =
       {input,
        conv2d1, bn1, act1, flatten,linear,
        output};


   model::NeuralNetwork<T> model(layers, optimizer::CROSS_ENTROPY, optimizer::SGD, params);

   model::metric_t metrics;

   model.fit(&train_set.images(), &train_set.labels(), metrics, true);
   model.summary();

   delete output;

   magmadnn_finalize();

   return 0;

}
