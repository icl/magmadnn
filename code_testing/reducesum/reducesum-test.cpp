#include <cstdio>
#include <iostream>
#include "magmadnn.h"

using namespace std;
using namespace magmadnn;

int main() {
    magmadnn_init();
    

    // create input data
 
    auto po = op::var<double>("pi",{3,3,3}, {CONSTANT, {1.2}}, (DEVICE));
    
    for(int i = 0; i < 27; i++){
        po->eval()->set(i, i);
    }

    io::print_tensor((po->eval()));

    auto v = op::reducesum(po, 0);


    auto E = v->eval();

    io::print_tensor(E);

    delete E;

    magmadnn_finalize();
    return 0;
}
