/* lossfunctest.cpp
 - test the loss function of mdnn and compare with tensorflow
*/
#include <cstdio>
#include <vector>
#include "magmadnn.h"

/* tell the compiler we're using functions from the magmadnn namespace */
using namespace magmadnn;

int main(int argc, char **argv) {

	using T = float;

	memory_t mem = DEVICE; 

	magmadnn_init();
	
	auto input_data = Tensor<T> ({1, 1, 9}, {CONSTANT, {1.0f}}, mem);
	//sets 3x3 tensor with all 1f

	input_data.set(1, 2.0f);
	input_data.set(2, 3.0f);
	input_data.set(3, 4.0f);
	input_data.set(4, 5.0f);
	input_data.set(5, 6.0f);
	input_data.set(6, 7.0f);
	input_data.set(7, 8.0f);
	input_data.set(8, 9.0f);

    auto target = Tensor<T> ({1, 1, 9}, {CONSTANT, {0.0f}}, mem);
	target.set(6,1.0f);
	target.set(7,1.0f);

	// Initialize our model parameters
	model::nn_params_t params;
	params.batch_size = 1;
	params.n_epochs = 1;
	params.learning_rate = 0.1;
	params.momentum = 0;

	auto x_batch = op::var<T>(
			"x_batch",//name of variable
			{params.batch_size, 1, 1, 9},//tensor size
			{NONE, {}}, mem); //fill type and memory 

	auto input = layer::input(x_batch); 
	auto flatten = layer::flatten(input->out());
	auto act2 = layer::activation<T>(flatten->out(), layer::SOFTMAX);
	auto output = layer::output(act2->out()); //output layer, call at end of the modeldeepp 
	
	std::vector<layer::Layer<T> *> layers =
	{input, flatten, act2, output};

	model::NeuralNetwork<T> model(layers, optimizer::CROSS_ENTROPY, optimizer::SGD, params);
	model.summary();

	model::metric_t metrics;
	// for( int k=0; k<9;k++){
	// std:: cout << model.predict(&input_data)->get(k) << std::endl;
	// }

	model.fit(&input_data, &target, metrics, true, true, true);

	// Clean up memory after training
	delete output;

	// Every magmadnn program should call magmadnn_finalize before
	// exiting
	magmadnn_finalize();

	return 0;
}
