/**
 * @file predict_zeros.cpp
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */

/* The goal of this unit test is to see if the LSTM can
 * learn to always to predict zeros.
 * */
#include "magmadnn.h"
#include "ctime"
using namespace magmadnn;
using T = float;
int main( void )
{
	magmadnn_init();

	model::nn_params_t params;
	params.batch_size = 10;
	params.n_epochs = 200;
	params.learning_rate = 1;

	model::metric_t metric_out;

	op::Variable<T> *x_batch = op::var<T>("x_batch",
		{params.batch_size,2, 1},
		{NONE, {}}, DEVICE);
	
	

	layer::InputLayer<T> *input = layer::input(x_batch);
	layer::LSTMLayer<T> *lstm = layer::lstm(input->out(), 1, false);
	layer::OutputLayer<T> *output = layer::output(lstm->out());

	std::vector<layer::Layer<T> *> layers = {input, lstm, output};

	model::NeuralNetwork<T> lstmModel(layers, optimizer::MSE, optimizer::SGD,
			params);

	op::Variable<T> *X = op::var<T>("X", {100,2,1}, {UNIFORM,
			{(T)0, (T)5}}, DEVICE);
	op::Variable<T> *Y = op::var<T>("Y", {100,1}, {CONSTANT,{(T)0}},
			DEVICE);

	std::cout << "\n vvvvv with return_sequences = false vvvvv \n ";
	lstmModel.summary();
	
	lstmModel.fit(X->get_output_tensor(), Y->get_output_tensor(), metric_out,
		       	false);
	Tensor<T> X_test ({5, 2, 1}, {UNIFORM, {(T)0,(T)5}}, DEVICE);
	Tensor<T> *Y_pred = lstmModel.predict(&X_test);

	io::print_tensor(&X_test);
	io::print_tensor(Y_pred);	



	std::cout << "\n vvvvv with return_sequences = true vvvvv \n";

	layer::InputLayer<T> *input2 = layer::input(x_batch);
	layer::LSTMLayer<T> *lstm2 = layer::lstm(input2->out(), 5, true);
	layer::LSTMLayer<T> *lstm3 = layer::lstm(lstm2->out(), 1, false);

	layer::OutputLayer<T> *output2 = layer::output(lstm3->out());

	std::vector<layer::Layer<T> *> layers2 = {input2, lstm2, lstm3, output2};

	model::NeuralNetwork<T> lstmModel2(layers2, optimizer::MSE, optimizer::SGD,
			params);

	op::Variable<T> *X2 = op::var<T>("X", {100,2,1}, {UNIFORM,
			{(T)0, (T)5}}, DEVICE);
	op::Variable<T> *Y2 = op::var<T>("Y", {100,1}, {CONSTANT,{(T)0}},
			DEVICE);

	lstmModel2.summary();

	time_t t0 = time(NULL);
	lstmModel2.fit(X2->get_output_tensor(), Y2->get_output_tensor(), metric_out,
		       	false);
	std::cout << "\nSeconds taken : " << time(NULL) - t0 << std::endl;
	
	Tensor<float> X_test2 ({5, 2, 1}, {UNIFORM, {(T)0,(T)1}}, DEVICE);
	Tensor<float> *Y_pred2 = lstmModel2.predict(&X_test2);

	io::print_tensor(&X_test2);
	io::print_tensor(Y_pred2);	


	magmadnn_finalize();
}
