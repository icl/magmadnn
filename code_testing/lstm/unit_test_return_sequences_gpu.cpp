/**
 * @unit_test_return_sequences_gpu.cpp
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */
#include "magmadnn.h"
#define MEM DEVICE
using namespace magmadnn;
using T = float;
int main( void ) {
	magmadnn_init();

	unsigned batchsz = 1;
	unsigned seqlen = 2;
	unsigned features = 2;
	unsigned num_nodes = 1;	
	op::Variable<T> *x = op::var<T>("x", {batchsz,seqlen,features}, {NONE, {}},
		       	MEM);
	x->get_output_tensor()->set({0,0,0}, (T)1);
	x->get_output_tensor()->set({0,0,1}, (T)2);
	x->get_output_tensor()->set({0,1,0}, (T)0.5);
	x->get_output_tensor()->set({0,1,1}, (T)3);
	op::Variable<T> *c0 = op::var<T>("c0", {1,1}, {NONE, {}}, MEM);
	c0->get_output_tensor()->set({0,0,0}, (T)0);	
	
	op::Variable<T> *h0 = op::var<T>("h0", {1,1}, {NONE, {}}, MEM);
	h0->get_output_tensor()->set({0,0,0}, (T)0);


	op::Variable<T> *Wf = op::var<T>("Wf",{features, num_nodes},
				{NONE, {}}, MEM);
	Wf->get_output_tensor()->set({0,0}, (T)0.7);
	Wf->get_output_tensor()->set({0,1}, (T)0.45);

	op::Variable<T> *Uf = op::var<T>("Uf",{num_nodes, num_nodes},
				{NONE, {}}, MEM);
	Uf->get_output_tensor()->set({0,0}, (T)0.1);
	op::Variable<T> *Bf = op::var<T>("Bf",{batchsz, num_nodes},
				{NONE, {}}, MEM);
	Bf->get_output_tensor()->set({0,0}, (T)0.15);

	op::Variable<T> *Wi = op::var<T>("Wi",{features, num_nodes},
				{NONE, {}}, MEM);
	Wi->get_output_tensor()->set({0,0}, (T)0.95);
	Wi->get_output_tensor()->set({0,1}, (T)0.8);
	op::Variable<T> *Ui = op::var<T>("Ui",{num_nodes, num_nodes},
				{NONE, {}}, MEM);
	Ui->get_output_tensor()->set({0,0}, (T)0.8);
	op::Variable<T> *Bi = op::var<T>("Bi",{batchsz, num_nodes},
				{NONE, {}}, MEM);
	Bi->get_output_tensor()->set({0,0}, (T)0.65);


	op::Variable<T> *Wo = op::var<T>("Wo",{features, num_nodes},
				{NONE, {}}, MEM);
	Wo->get_output_tensor()->set({0,0}, (T)0.6);
	Wo->get_output_tensor()->set({0,1}, (T)0.4);
	op::Variable<T> *Uo = op::var<T>("Uo",{num_nodes, num_nodes},
				{NONE, {}}, MEM);
	Uo->get_output_tensor()->set({0,0}, (T)0.25);
	op::Variable<T> *Bo = op::var<T>("Bo",{batchsz, num_nodes},
				{NONE, {}}, MEM);
	Bo->get_output_tensor()->set({0,0}, (T)0.1);
		

	op::Variable<T> *Wc = op::var<T>("Wc",{features, num_nodes},
				{NONE, {}}, MEM);
	Wc->get_output_tensor()->set({0,0}, (T)0.45);
	Wc->get_output_tensor()->set({0,1}, (T)0.25);
	op::Variable<T> *Uc = op::var<T>("Uc",{num_nodes, num_nodes},
				{NONE, {}}, MEM);
	Uc->get_output_tensor()->set({0,0}, (T)0.15);
	op::Variable<T> *Bc = op::var<T>("Bc",{batchsz, num_nodes},
				{NONE, {}}, MEM);
	Bc->get_output_tensor()->set({0,0}, (T)0.2);

	std::vector<op::Operation<T> *> weights = {Wf, Uf, Bf, Wi, Ui, Bi, Wo, Uo,
		Bo, Wc, Uc, Bc};	

	op::Variable<T> *y = op::var<T>("y", {batchsz,seqlen,num_nodes},
			{NONE,{}}, MEM);
	y->get_output_tensor()->set({0,0,0}, (T).5);
	y->get_output_tensor()->set({0,1,0}, (T)-1.25);

	
	io::print_tensor((x->get_output_tensor()));

	std::cout << "NOTE: This is the result of a forward pass through the" <<
		 " LSTMOp. It should be [[0.5363134], [0.771982]]\n";
	op::LSTMOp<T> *lstm = op::lstm(x, weights, h0, c0, true);
	
	lstm->eval();

	io::print_tensor((lstm->get_output_tensor()));
	
	
	

	Tensor<T> *grad = op::add(y,
			op::negative(
				lstm))->eval();
	std::cout << "\nNOTE: This is the up-stream gradient.\n";	
	io::print_tensor(grad);	

/*Wf
[[-0.01333808], [-0.08002848]]
Wi
[[-0.01204353], [-0.03352497]]
Wo
[[-0.12823777], [-0.72496299]]
Wc
[[-0.14116326], [-0.44631495]]

Uf
[[-0.01430678]]
Ui
[[-0.00253084]]
Uo
[[-0.12562805]]
Uc
[[-0.0439746]]

bf
[[-0.02667616]]
bi
[[-0.014403]]
bo
[[-0.24535963]]
bc
[[-0.18216037]] */	

	// X	
        Tensor<T> *out_grad = lstm->grad(x,x,grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to the inputs. It should be [[-0.06094415 -0.03723514]"
		<< ", [-0.20059996, -0.12997548]]: \n";
        io::print_tensor((out_grad));

	// W
        out_grad = lstm->grad(weights[0],weights[0],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Wf. It should be [[-0.01333808], [-0.08002848]]"
		<< "\n";
        io::print_tensor((out_grad));

	out_grad = lstm->grad(weights[3],weights[3],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Wi. It should be [[-0.01204353], [-0.03352497]]"
		<< "\n";
        io::print_tensor((out_grad));

	out_grad = lstm->grad(weights[6],weights[6],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Wo. It should be [[-0.12823777], [-0.72496299]]"
		<< "\n";
        io::print_tensor((out_grad));

	out_grad = lstm->grad(weights[9],weights[9],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Wc. It should be [[-0.14116326], [-0.44631495]]"
		<< "\n";
        io::print_tensor((out_grad));
	
	// U
        out_grad = lstm->grad(weights[1],weights[1],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Uf. It should be [[-0.01430678]]"
		<< "\n";
        io::print_tensor((out_grad));

	out_grad = lstm->grad(weights[4],weights[4],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Ui. It should be [[-0.00253084]]"
		<< "\n";
        io::print_tensor((out_grad));

	out_grad = lstm->grad(weights[7],weights[7],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Uo. It should be [[-0.12562805]]"
		<< "\n";
        io::print_tensor((out_grad));

	out_grad = lstm->grad(weights[10],weights[10],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to Uc. It should be [[-0.0439746]]"
		<< "\n";
        io::print_tensor((out_grad));

	// B
	out_grad = lstm->grad(weights[2],weights[2],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to bf. It should be [[-0.02667616]]"
		<< "\n";
        io::print_tensor((out_grad));
	
	out_grad = lstm->grad(weights[5],weights[5],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to bi. It should be [[-0.014403]]"
		<< "\n";
        io::print_tensor((out_grad));
	
	out_grad = lstm->grad(weights[8],weights[8],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to bo. It should be [[-0.24535963]]"
		<< "\n";
        io::print_tensor((out_grad));

	out_grad = lstm->grad(weights[11],weights[11],grad);
	std::cout << "\n NOTE: This is the gradient values calculated with" <<
		" respect to bc. It should be [[-0.18216037]]"
		<< "\n";
        io::print_tensor((out_grad));
	magmadnn_finalize();
}
