/**
 * @file predict_MNIST.cpp
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-07-18
 *
 * A test using an LSTM architecture to run predictions upon the MNIST numeral
 * dataset. NOTE: The model architecture is not meant to be efficient for the task
 * beset it.
 */

#include "magmadnn.h"
#define MEM DEVICE
using namespace magmadnn;
using T = float;
int main( void ) {
	
	magmadnn_init();

	model::nn_params_t params;
	params.batch_size = 10000;
	params.n_epochs = 100;
	params.learning_rate = 0.001;
	
	model::metric_t metric_out;
	
	op::Variable<T> *h_init = op::var<T>("h",
		{params.batch_size,28},
		{CONSTANT, {(T)0}}, MEM);
	
	op::Variable<T> *c_init = op::var<T>("c",
		{params.batch_size,28},
		{CONSTANT, {(T)0}}, MEM);

	op::Variable<T> *h_init2 = op::var<T>("h",
		{params.batch_size,14},
		{CONSTANT, {(T)0}}, MEM);
	
	op::Variable<T> *c_init2 = op::var<T>("c",
		{params.batch_size,14},
		{CONSTANT, {(T)0}}, MEM);

	std::string const mnist_dir = "./Data";

	magmadnn::data::MNIST<T> train_set(mnist_dir, magmadnn::data::Train);
	magmadnn::data::MNIST<T> test_set(mnist_dir, magmadnn::data::Test);
	std::cout << "train_set values: " << train_set.images().get(100) << ' ' << train_set.images().get(1001) << std::endl;
	std::vector<unsigned> s1 = train_set.images().get_shape();
	std::vector<unsigned> s2 = test_set.images().get_shape();
	std::vector<unsigned> s3 = train_set.labels().get_shape();
	std::vector<unsigned> s4 = test_set.labels().get_shape();

	std::cout << "\n train shape: " << s1[0] << "," << s1[1] << "," << s1[2] <<
		"," << s1[3] << "; dim: " << s1.size();
	std::cout << "\n train labels: " << s3[0] << "," << s3[1] << "; dim: " <<
		s3.size() << "\n";
	std::cout << "\n test shape: " << s2[0] << "," << s2[1] << "," << s2[2] <<
		"," << s2[3] << "; dim: " << s2.size();
	std::cout << "\n test labels: " << s4[0] << "," << s4[1] << "; dim: " <<
		s4.size() << "\n";

	op::Variable<T> *x_batch = op::var<T>("x_batch", {params.batch_size, 28, 28},
			{NONE, {}}, MEM);


	layer::InputLayer<T> *input = layer::input(x_batch);
	std::cout << "\npredict_MNIST 0\n";
	layer::LSTMLayer<T> *lstm1 = layer::lstm(input->out(), 28, true);
	std::cout << "\npredict_MNIST 1\n";
	layer::LSTMLayer<T> *lstm2 = layer::lstm(lstm1->out(), 28, true);
	std::cout << "\npredict_MNIST 2\n";
	layer::LSTMLayer<T> *lstm3 = layer::lstm(lstm2->out(), 14, false);
	std::cout << "\npredict_MNIST 3\n";
//	auto flatten = layer::flatten(lstm1->out());
	layer::FullyConnectedLayer<T> *fc1 = layer::fullyconnected(lstm3->out(), 14);
	layer::ActivationLayer<T> *a1 = layer::activation(fc1->out(), layer::RELU);

	layer::FullyConnectedLayer<T> *fc2 = layer::fullyconnected(a1->out(), 14);
	layer::ActivationLayer<T> *a2 = layer::activation(fc2->out(), layer::RELU);

	layer::FullyConnectedLayer<T> *fc3 = layer::fullyconnected(a2->out(), 10);
	layer::ActivationLayer<T> *a3 = layer::activation(fc3->out(), layer::SOFTMAX);

	layer::OutputLayer<T> *output = layer::output(a3->out());

	std::vector<layer::Layer<T> *> layers = {input, lstm1, lstm2, lstm3, /*flatten, */
		fc1, a1, fc2, a2, fc3, a3, output};

	std::cout << "\npredict_MNIST 4\n";
	model::NeuralNetwork<T> lstmModel(layers, optimizer::CROSS_ENTROPY,
			optimizer::ADAM, params);
	std::cout << "\npredict_MNIST 5\n";
	
	lstmModel.summary();

	lstmModel.fit(&train_set.images(), &train_set.labels(), metric_out, true, true);

	
	magmadnn_finalize();
	return 0;
}
