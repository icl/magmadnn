#include <cstdio>
#include "magmadnn.h"

using namespace magmadnn;

int main() {
    magmadnn_init();

    /* first, initialize the variables */

    //TODO find if dimensions are correct
    //TODO see why it doesn't work when there is ts > 1
    unsigned int bs = 28;
    unsigned int features = 27;
    unsigned int ts = 10;
    unsigned int nodes = 12;

    /* the auto keyword is helpful when initializing operations */
    auto x = op::var<float>("X", {bs,ts,features}, {CONSTANT, {1.0f}}, DEVICE);
    auto h = op::var<float>("X", {nodes,bs,features}, {CONSTANT, {1.0f}}, DEVICE);
    auto c = op::var<float>("X", {nodes,bs,features}, {CONSTANT, {1.0f}}, DEVICE);
//    auto b = op::var<float>("B", {10,6}, {UNIFORM, {0.0f, 1.0f}}, DEVICE);

//    io::print_tensor(c->get_output_tensor());
//    io::print_tensor(h->get_output_tensor());
//    io::print_tensor(x->get_output_tensor());
    auto lstm = op::lstm2(x, nodes, h, c, false, false);



    /* this constructs the compute graph */

    Tensor<float> *out_tensor = lstm->eval();

    io::print_tensor(out_tensor);
    auto g = op::var<float>("X", {bs,ts,features}, {CONSTANT, {1.0f}}, DEVICE);

    Tensor<float> *grad_tensor = lstm->grad(NULL, NULL, g->get_output_tensor());
    io::print_tensor(grad_tensor);
    
    magmadnn_finalize();
}

