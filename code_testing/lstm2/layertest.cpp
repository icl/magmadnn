/**
 * @file predict_zeros.cpp
 * @author Joshua Zingale
 * @version 1.0
 * @date 2022-06-27
 */

/* The goal of this unit test is to see if the LSTM can
 * learn to always to predict zeros.
 * */
#include "magmadnn.h"
#include "ctime"
using namespace magmadnn;
using T = float;
int main( void )
{
	//NOTES
	//dweights_t wasn't getting zeroed out, fixed this
	//dweights_t has size 32 but weightspacesize is 128, find out why this is the case
	//also c and h don't converge anymore	
	magmadnn_init();

	model::nn_params_t params;
	params.batch_size = 3;
	params.n_epochs = 1000;
	params.learning_rate = 1;
	unsigned num_nodes = 1;
	unsigned bs = params.batch_size;
	unsigned ts = 2;
	unsigned feat = 5;
	model::metric_t metric_out;

	op::Variable<T> *x_batch = op::var<T>("x_batch",
		{params.batch_size,ts, feat},
		{NONE, {}}, DEVICE);
	
	

	layer::InputLayer<T> *input = layer::input(x_batch);
	printf("here\n");
	layer::LSTM2Layer<T> *lstm2 = layer::lstm2(input->out(), num_nodes, false);
	printf( "after lstm2 layer") ;
	fflush(stdout);

	// layer::FlattenLayer<T> * flatten = layer::flatten(lstm2->out());
	// 	printf("here\n");
	layer::OutputLayer<T> *output = layer::output(/*flatten*/lstm2->out());
printf("here\n");
	std::vector<layer::Layer<T> *> layers = {input, lstm2, /*flatten,*/ output};
	printf("here\n");

	model::NeuralNetwork<T> lstmModel(layers, optimizer::MSE, optimizer::SGD,
			params);
			printf("here\n");

//	op::Variable<T> *X = op::var<T>("X", {bs,ts,feat}, {UNIFORM,
//			{(T)0, (T)5}}, DEVICE);
//	op::Variable<T> *Y = op::var<T>("Y", {bs,ts * feat}, {CONSTANT,{(T)1}},
//			DEVICE);
	
	Tensor<T> *Y = new Tensor<T>({bs, ts}, {CONSTANT,{(T)0.5}},
			DEVICE);
	Tensor<T> *X = new Tensor<T>({bs,ts,feat},
			{CONSTANT, {(T)1}}/*{UNIFORM, {(T)0, (T)1}}*/, DEVICE);

//	X->set(0, 1.0);
//	X->set(1, 2.0);
//	X->set(2, 0.5);
//	X->set(3, 3.0);
//	Y->set(0, 0.5);
//	Y->set(1, -1.25);
	io::print_tensor(X);
	io::print_tensor(Y);	

	std::cout << "\n vvvvv with return_sequences = false vvvvv \n ";
	lstmModel.summary();
	
	lstmModel.fit(X/*->get_output_tensor()*/, Y/*->get_output_tensor()*/, metric_out,
		       	true);
	Tensor<T> *X_test = new Tensor<T>({bs, ts, feat}, {CONSTANT, {(T)1}}, DEVICE);
	Tensor<T> *Y_pred = lstmModel.predict(X_test);

	io::print_tensor(X_test);
	io::print_tensor(Y_pred);	

	magmadnn_finalize();
}
