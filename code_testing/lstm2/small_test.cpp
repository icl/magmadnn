/**
 * @file small_test.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Stephen Qiu
 * @version 1.0
 * @date 2022-07-18
 *
 */

#include <cstdio>
#include "magmadnn.h"

using namespace magmadnn;
using T = float;
#define MEM DEVICE
int main() {
    magmadnn_init();

    unsigned batchsz = 1;
    unsigned seqlen = 2;
    unsigned features = 2;
    unsigned hs = 1;

    op::Variable<T> *x = op::var<T>("x", {batchsz,seqlen,features}, {UNIFORM, {0.0f, 1.0f}}, MEM);
    x->get_output_tensor()->set(0, 1);
    x->get_output_tensor()->set(1, 2);
    x->get_output_tensor()->set(2, 0.5);
    x->get_output_tensor()->set(3, 3);

    op::Variable<T> *c0 = op::var<T>("c0", {batchsz,hs}, {CONSTANT, {0.0f}}, MEM);

    op::Variable<T> *h0 = op::var<T>("h0", {batchsz,hs}, {CONSTANT, {0.0f}}, MEM);

    op::Variable<T> *w = op::var<T>("w", {4*hs*( (features + hs + 1))}, {CONSTANT, {1.0f}}, MEM);

	w->get_output_tensor()->set(0, .95);
	w->get_output_tensor()->set(1, .8);
	w->get_output_tensor()->set(2, .7);
	w->get_output_tensor()->set(3, .45);
	w->get_output_tensor()->set(4, .45);
	w->get_output_tensor()->set(5, .25);
	w->get_output_tensor()->set(6, .6);
	w->get_output_tensor()->set(7, .4);
	w->get_output_tensor()->set(8, .8);
	w->get_output_tensor()->set(9, .1);
	w->get_output_tensor()->set(10, .15);
	w->get_output_tensor()->set(11, .25);
	w->get_output_tensor()->set(12, .65);
	w->get_output_tensor()->set(13, .15);
	w->get_output_tensor()->set(14, .2);
	w->get_output_tensor()->set(15, .1);

    auto lstm = op::lstm2(x, h0, c0, w, true);


    /* this constructs the compute graph */

    Tensor<float> *out_tensor = lstm->eval();

    io::print_tensor(out_tensor);


    Tensor<T> *grad = new Tensor<T>({batchsz, seqlen,hs}, {ZERO}, MEM);
    
    grad->set(0, 0.03631);
    grad->set(1, -0.47803);

    Tensor<float> *grad_tensor = lstm->grad(NULL, x, grad);
    io::print_tensor(grad_tensor);

    magmadnn_finalize();
}

