#include <cstdio>
#include "magmadnn.h"

using namespace magmadnn;
using T = float;
#define MEM DEVICE
int main() {
    magmadnn_init();

    /* first, initialize the variables */

    /* the auto keyword is helpful when initializing operations */
//    auto b = op::var<float>("B", {10,6}, {UNIFORM, {0.0f, 1.0f}}, DEVICE);
    unsigned batchsz = 32;
    unsigned seqlen = 3;
    unsigned features = 11;
    unsigned hs = 10;
    unsigned num_nodes = 1;
    op::Variable<T> *x = op::var<T>("x", {batchsz,seqlen,features}, {UNIFORM, {0.0f, 1.0f}},
                        MEM);
    op::Variable<T> *c0 = op::var<T>("c0", {batchsz,hs}, {CONSTANT, {1.0f}}, MEM);
//    c0->get_output_tensor()->set({0,0,0}, (T)0);

    op::Variable<T> *h0 = op::var<T>("h0", {batchsz,hs}, {CONSTANT, {1.0f}}, MEM);
  //  h0->get_output_tensor()->set({0,0,0}, (T)0);

    op::Variable<T> *w = op::var<T>("w", {4*hs*( (features + hs + 1))}, {CONSTANT, {1.0f}}, MEM);
	

//    io::print_tensor(c->get_output_tensor());
//    io::print_tensor(h->get_output_tensor());
//    io::print_tensor(x->get_output_tensor());
    auto lstm = op::lstm2(x, h0, c0, w, false);



    /* this constructs the compute graph */

    Tensor<float> *out_tensor = lstm->eval();

    io::print_tensor(out_tensor);


    Tensor<T> *grad = new Tensor<T>({batchsz,hs}, {CONSTANT, {1.0f}}, MEM);
    Tensor<float> *grad_tensor = lstm->grad(NULL, x, grad);
    io::print_tensor(grad_tensor);
    
    magmadnn_finalize();
}

