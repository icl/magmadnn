#include <dirent.h>
#include "magmadnn.h"

extern "C" {
#include <jpeglib.h>
}

#include <iostream>

using namespace magmadnn;

int main(int argc, char **argv) {

    const char *input = "./Data/new.jpeg";
    const char *output = "./Data/output.jpeg";
    // const char *input = "./Data/output.jpeg";
    // const char *output = "./Data/output1.jpeg";
    magmadnn_init();

    using T = float;

    unsigned int height = 800;
    unsigned int width = 1200;
    unsigned int n_images = 1;

    unsigned int i = 0;

    Tensor<T> *test = new Tensor<T>({n_images, 3, height, width}, {ZERO, {}}, HOST);
    
    data::add_jpeg_to_tensor(input, false, test, i);

    // io::print_tensor(test);


    Tensor<T> *test2 = new Tensor<T>({n_images, 3, height, width}, {ZERO, {}}, HOST);
    data::add_image_to_tensor(input, height, width, true, test2, 0);

//    io::print_tensor(test2);

    data::tensor_to_jpeg(output, height, width, test2, i, 1);

    magmadnn_finalize();
}
