/* shortcut.cpp
 - tests the shortcut layer to check 
 - forward and backward propagation 
*/
#include <cstdio>
#include <vector>
#include "magmadnn.h"

/* tell the compiler we're using functions from the magmadnn namespace */
using namespace magmadnn;

int main(int argc, char **argv) {

	using T = float;

	memory_t mem = DEVICE; 

	magmadnn_init();
	
	auto input_data = Tensor<T> ({1, 3, 3}, {CONSTANT, {1.0f}}, mem);
	//sets 3x3 tensor with all 1f

	input_data.set(1, 2.0f);
	input_data.set(2, 3.0f);
	input_data.set(3, 4.0f);
	input_data.set(4, 5.0f);
	input_data.set(5, 6.0f);
	input_data.set(6, 7.0f);
	input_data.set(7, 8.0f);
	input_data.set(8, 9.0f);

    auto target = Tensor<T> ({1, 1, 1}, {CONSTANT, {9.0f}}, mem);


	// Initialize our model parameters
	model::nn_params_t params;
	params.batch_size = 1;
	params.n_epochs = 1;
	params.learning_rate = 0.025;
	params.momentum = 0;

	auto x_batch = op::var<T>(
			"x_batch",//name of variable
			{params.batch_size, 1, 3, 3},//tensor size
			{NONE, {}}, mem); //fill type and memory 

	auto input = layer::input(x_batch); 
    auto conv2d = layer::conv2d(input->out(), {3, 3}, 1, {1,1}, {1,1}, {1,1}, false, false);
    conv2d->get_weights()[0]->eval()->set(0, 1.0f);
    conv2d->get_weights()[0]->eval()->set(1, 1.0f);
    conv2d->get_weights()[0]->eval()->set(2, 1.0f);
    conv2d->get_weights()[0]->eval()->set(3, 1.0f);
    conv2d->get_weights()[0]->eval()->set(4, 1.0f);
    conv2d->get_weights()[0]->eval()->set(5, 1.0f);
    conv2d->get_weights()[0]->eval()->set(6, 1.0f);
    conv2d->get_weights()[0]->eval()->set(7, 1.0f);
    conv2d->get_weights()[0]->eval()->set(8, 1.0f);


	printf("w1 = %.10f\n", conv2d->get_weights()[0]->eval()->get(0));
	printf("w2 = %.10f\n", conv2d->get_weights()[0]->eval()->get(1));
	printf("w3 = %.10f\n", conv2d->get_weights()[0]->eval()->get(2));
	printf("w4 = %.10f\n", conv2d->get_weights()[0]->eval()->get(3));
	printf("w5 = %.10f\n", conv2d->get_weights()[0]->eval()->get(4));
	printf("w6 = %.10f\n", conv2d->get_weights()[0]->eval()->get(5));
	printf("w7 = %.10f\n", conv2d->get_weights()[0]->eval()->get(6));
	printf("w8 = %.10f\n", conv2d->get_weights()[0]->eval()->get(7));
	printf("w9 = %.10f\n", conv2d->get_weights()[0]->eval()->get(8));

	auto act = layer::activation(conv2d->out(), layer::SIGMOID);

	auto global_avg = layer::globalavgpooling(act->out());
	auto flatten = layer::flatten(global_avg->out());

	auto output = layer::output(flatten->out()); //output layer, call at end of the model
	
	std::vector<layer::Layer<T> *> layers =
	{	input,
		conv2d, act, global_avg,flatten,output};

	model::NeuralNetwork<T> model(layers, optimizer::MSE, optimizer::SGD, params);
	model.summary();

	model::metric_t metrics;
	// for( int k=0; k<9;k++){
	// std:: cout << model.predict(&input_data)->get(k) << std::endl;
	// }

	model.fit(&input_data, &target, metrics, true);

	// Clean up memory after training
	delete output;

	// Every magmadnn program should call magmadnn_finalize before
	// exiting
	magmadnn_finalize();

	return 0;
}
