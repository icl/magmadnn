/* shortcut.cpp
 - tests the shortcut layer to check
 - forward and backward propagation
*/
#include <cstdio>
#include <vector>
#include "magmadnn.h"

/* tell the compiler we're using functions from the magmadnn namespace */
using namespace magmadnn;

int main(int argc, char **argv) {
    using T = double;

    memory_t mem = DEVICE;

    magmadnn_init();

    auto input_data = Tensor<T>({1, 3, 3}, {CONSTANT, {1.0f}}, mem);

    // input_data.set(0, 0.1f);
    // input_data.set(1, 0.2f);
    // input_data.set(2, 0.3f);
    // input_data.set(3, 0.4f);
    // input_data.set(4, 0.5f);
    // input_data.set(5, 0.6f);
    // input_data.set(6, 0.7f);
    // input_data.set(7, 0.8f);
    // input_data.set(8, 0.9f);

    input_data.set(0, 1.0f);
    input_data.set(1, 2.0f);
    input_data.set(2, 3.0f);
    input_data.set(3, 4.0f);
    input_data.set(4, 5.0f);
    input_data.set(5, 6.0f);
    input_data.set(6, 7.0f);
    input_data.set(7, 8.0f);
    input_data.set(8, 9.0f);

    auto target = Tensor<T>({1, 9},{CONSTANT, {1.0f}}, mem);
    
    target.set(0, 9.0f);
    target.set(1, 10.0f);
    target.set(2, 11.0f);
    target.set(3, 12.0f);
    target.set(4, 13.0f);
    target.set(5, 14.0f);
    target.set(6, 15.0f);
    target.set(7, 16.0f);
    target.set(8, 17.0f);

    // Initialize our model parameters
    model::nn_params_t params;
    params.batch_size = 1;
    params.n_epochs = 2;
    params.learning_rate = 0.01;
    params.momentum = 0;

    auto x_batch = op::var<T>("x_batch",                     // name of variable
                              {params.batch_size, 1, 3, 3},  // tensor size
                              {NONE, {}}, mem);              // fill type and memory

    auto input = layer::input(x_batch);
    auto conv2d = layer::conv2d(input->out(), {3, 3}, 1, {1, 1}, {1, 1}, {1, 1}, true, false, {CONSTANT, {0.1f}});

    auto act = layer::activation(conv2d->out(), layer::SIGMOID);
    auto conv2d1 = layer::conv2d(act->out(), {3, 3}, 1, {1, 1}, {1, 1}, {1, 1}, true, false, {CONSTANT, {0.4f}});

    auto act1 = layer::activation(conv2d1->out(), layer::SIGMOID);

    auto shortcut = layer::shortcut(act->out(), act1->out());
    // auto shortcut = op::add(act->out(), act1->out());
    auto flatten = layer::flatten(shortcut->out());

    auto output = layer::output(flatten->out());
    std::vector<layer::Layer<T> *> layers = {input, conv2d, act, conv2d1, act1, shortcut, flatten, output};

    model::NeuralNetwork<T> model(layers, optimizer::MSE, optimizer::SGD, params);

    model::metric_t metrics;

    model.fit(&input_data, &target, metrics, true);
    model.summary();

    printf("output\n");
    for (int i = 0; i < 9; i++) {
        printf("w%i = %.7f\n", i, output->out()->eval()->get(i));
    }
    printf("\nlayer 1 weights\n");
    for (int i = 0; i < 9; i++) {
        printf("w%i = %.7f\n", i, conv2d->get_weights()[0]->eval()->get(i));
    }
    printf("\nlayer 2 weights\n");
    for (int i = 0; i < 9; i++) {
        printf("w%i = %.7f\n", i, conv2d1->get_weights()[0]->eval()->get(i));
    }

    delete output;

    magmadnn_finalize();

    return 0;
}
