/* shortcut.cpp
 - tests the shortcut layer to check 
 - forward and backward propagation 
*/
#include <cstdio>
#include <vector>
#include "magmadnn.h"

/* tell the compiler we're using functions from the magmadnn namespace */
using namespace magmadnn;

int main(int argc, char **argv) {

	using T = float;

	memory_t mem = DEVICE; 

	magmadnn_init();
	
	auto input_data = Tensor<T> ({1,9}, {CONSTANT, {1.0f}}, mem);
	//sets 3x3 tensor with all 1f

	input_data.set(1, 2.0f);
	input_data.set(2, 3.0f);
	input_data.set(3, 4.0f);
	input_data.set(4, 5.0f);
	input_data.set(5, 6.0f);
	input_data.set(6, 7.0f);
	input_data.set(7, 8.0f);
	input_data.set(8, 9.0f);

    auto target = Tensor<T> ({1, 9}, {CONSTANT, {9.0f}}, mem);

	target.set(1, 10.0f);
	target.set(2, 11.0f);
	target.set(3, 12.0f);
	target.set(4, 13.0f);
	target.set(5, 14.0f);
	target.set(6, 15.0f);
	target.set(7, 16.0f);
	target.set(8, 17.0f);

	// Initialize our model parameters
	model::nn_params_t params;
	params.batch_size = 1;
	params.n_epochs = 1;
	params.learning_rate = 0.0025;
	params.momentum = 0;

	auto x_batch = op::var<T>(
			"x_batch",//name of variable
			{params.batch_size, 1,9},//tensor size
			{NONE, {}}, mem); //fill type and memory 

	auto input = layer::input(x_batch); 
    auto fc1 = layer::fullyconnected(input->out(),9);

	fc1->get_weights()[0]->eval()->set(0, 1.0f); 
	fc1->get_weights()[0]->eval()->set(1, 1.0f);
	fc1->get_weights()[0]->eval()->set(2, 1.0f);
	fc1->get_weights()[0]->eval()->set(3, 1.0f);
    fc1->get_weights()[0]->eval()->set(4, 1.0f);
    fc1->get_weights()[0]->eval()->set(5, 1.0f);
    fc1->get_weights()[0]->eval()->set(6, 1.0f);
    fc1->get_weights()[0]->eval()->set(7, 1.0f);
    fc1->get_weights()[0]->eval()->set(8, 1.0f);


	auto fc2 = layer::fullyconnected(fc1->out(),9);

	fc2->get_weights()[0]->eval()->set(0, 1.0f); 
	fc2->get_weights()[0]->eval()->set(1, 1.0f);
	fc2->get_weights()[0]->eval()->set(2, 1.0f);
	fc2->get_weights()[0]->eval()->set(3, 1.0f);
    fc2->get_weights()[0]->eval()->set(4, 1.0f);
    fc2->get_weights()[0]->eval()->set(5, 1.0f);
    fc2->get_weights()[0]->eval()->set(6, 1.0f);
    fc2->get_weights()[0]->eval()->set(7, 1.0f);
    fc2->get_weights()[0]->eval()->set(8, 1.0f);
    
	auto shortcut = layer::shortcut(fc1->out(), fc2->out());
	// std::cout << shortcut->get_output_shape()[0] << " " << shortcut->get_output_shape()[1] << " " <<  shortcut ->get_output_shape()[2] << " " << shortcut->get_output_shape()[3] << std::endl;

	//auto gap = layer::globalavgpooling(shortcut->out());
	auto output = layer::output(shortcut->out()); //output layer, call at end of the model
	
	std::vector<layer::Layer<T> *> layers =
	{	input,
		fc1,fc2,shortcut, output};

	model::NeuralNetwork<T> model(layers, optimizer::MSE, optimizer::SGD, params);
	model.summary();

	model::metric_t metrics;
	for( int k=0; k<9;k++){
	std:: cout << model.predict(&input_data)->get(k) << std::endl;
	}

	std:: cout << model.predict(&input_data)->get_size();

	model.fit(&input_data, &target, metrics, true,true,true);

	printf("w1 = %.10f\n", fc1->get_weights()[0]->eval()->get(0));
	printf("w2 = %.10f\n", fc1->get_weights()[0]->eval()->get(1));
	printf("w3 = %.10f\n", fc1->get_weights()[0]->eval()->get(2));
	printf("w4 = %.10f\n", fc1->get_weights()[0]->eval()->get(3));
	printf("w5 = %.10f\n", fc1->get_weights()[0]->eval()->get(4));
	printf("w6 = %.10f\n", fc1->get_weights()[0]->eval()->get(5));
	printf("w7 = %.10f\n", fc1->get_weights()[0]->eval()->get(6));
	printf("w8 = %.10f\n", fc1->get_weights()[0]->eval()->get(7));
	printf("w9 = %.10f\n", fc1->get_weights()[0]->eval()->get(8));
	

	printf("w1 = %.10f\n", fc2->get_weights()[0]->eval()->get(0));
	printf("w2 = %.10f\n", fc2->get_weights()[0]->eval()->get(1));
	printf("w3 = %.10f\n", fc2->get_weights()[0]->eval()->get(2));
	printf("w4 = %.10f\n", fc2->get_weights()[0]->eval()->get(3));
	printf("w5 = %.10f\n", fc2->get_weights()[0]->eval()->get(4));
	printf("w6 = %.10f\n", fc2->get_weights()[0]->eval()->get(5));
	printf("w7 = %.10f\n", fc2->get_weights()[0]->eval()->get(6));
	printf("w8 = %.10f\n", fc2->get_weights()[0]->eval()->get(7));
	printf("w9 = %.10f\n", fc2->get_weights()[0]->eval()->get(8));


	// Clean up memory after training
	delete output;

	// Every magmadnn program should call magmadnn_finalize before
	// exiting
	magmadnn_finalize();

	return 0;
}
