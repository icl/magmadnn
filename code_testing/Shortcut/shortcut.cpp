/* shortcut.cpp
 - tests the shortcut layer to check 
 - forward and backward propagation 
*/
#include <cstdio>
#include <vector>
#include "magmadnn.h"

/* tell the compiler we're using functions from the magmadnn namespace */
using namespace magmadnn;

int main(int argc, char **argv) {

	using T = float;

	memory_t mem = DEVICE; 

	magmadnn_init();
	
	auto input_data = Tensor<T> ({1, 3, 3}, {CONSTANT, {1.0f}}, mem);
	//sets 3x3 tensor with all 1f

	input_data.set(1, 2.0f);
	input_data.set(2, 3.0f);
	input_data.set(3, 4.0f);
	input_data.set(4, 5.0f);
	input_data.set(5, 6.0f);
	input_data.set(6, 7.0f);
	input_data.set(7, 8.0f);
	input_data.set(8, 9.0f);

    auto target = Tensor<T> ({1, 3, 3}, {CONSTANT, {1.0f}}, mem);

	target.set(1, 9.0f);
	target.set(2, 10.0f);
	target.set(3, 11.0f);
	target.set(4, 12.0f);
	target.set(5, 13.0f);
	target.set(6, 14.0f);
	target.set(7, 15.0f);
	target.set(8, 16.0f);

	// Initialize our model parameters
	model::nn_params_t params;
	params.batch_size = 1;
	params.n_epochs = 1;
	params.learning_rate = 0.25;
	params.momentum = 0;

	auto x_batch = op::var<T>(
			"x_batch",//name of variable
			{params.batch_size, 1, 3, 3},//tensor size
			{NONE, {}}, mem); //fill type and memory 

	auto input = layer::input(x_batch); 
    auto conv2d = layer::conv2d(input->out(), {3, 3}, 1, {1,1}, {1,1}, {1,1}, true, false);
    
    conv2d->get_weights()[0]->eval()->set(0, 1.0f);
    conv2d->get_weights()[0]->eval()->set(1, 1.0f);
    conv2d->get_weights()[0]->eval()->set(2, 1.0f);
    conv2d->get_weights()[0]->eval()->set(3, 1.0f);
    conv2d->get_weights()[0]->eval()->set(4, 1.0f);
    conv2d->get_weights()[0]->eval()->set(5, 1.0f);
    conv2d->get_weights()[0]->eval()->set(6, 1.0f);
    conv2d->get_weights()[0]->eval()->set(7, 1.0f);
    conv2d->get_weights()[0]->eval()->set(8, 1.0f);

    auto act = layer::activation(conv2d->out(), layer::SIGMOID); //(input layer, activation function)
	auto flatten = layer::flatten(act->out());
	auto fc1 = layer::fullyconnected(flatten->out(), 9); 

	fc1->get_weights()[0]->eval()->set(0, 1.0f); 
	fc1->get_weights()[0]->eval()->set(1, 1.0f);
	fc1->get_weights()[0]->eval()->set(2, 1.0f);
	fc1->get_weights()[0]->eval()->set(3, 1.0f);
    fc1->get_weights()[0]->eval()->set(4, 1.0f);
    fc1->get_weights()[0]->eval()->set(5, 1.0f);
    fc1->get_weights()[0]->eval()->set(6, 1.0f);
    fc1->get_weights()[0]->eval()->set(7, 1.0f);
    fc1->get_weights()[0]->eval()->set(8, 1.0f);
    
	auto act1 = layer::activation(fc1->out(), layer::SIGMOID); //(input layer, activation function)
    
	auto shortcut = layer::shortcut(flatten->out(), act1->out());

	auto output = layer::output(shortcut->out()); //output layer, call at end of the model

	std::vector<layer::Layer<T> *> layers =
	{	input,
		conv2d, act, flatten, fc1, act1, shortcut, output};

	model::NeuralNetwork<T> model(layers, optimizer::MSE, optimizer::SGD, params);


	model::metric_t metrics;

	model.fit(&input_data, &target, metrics, true);
	model.summary();
/*
	printf("w1 = %.10f\n", fc1->get_weights()[0]->eval()->get({0,0}));
        printf("w2 = %.10f\n", fc1->get_weights()[0]->eval()->get({0,1}));
        printf("w3 = %.10f\n", fc1->get_weights()[0]->eval()->get({1,0}));
        printf("w4 = %.10f\n", fc1->get_weights()[0]->eval()->get({1,1}));

	printf("i1 = %.20f\n", fc1->get_input()->eval()->get(0));
        printf("i2 = %.20f\n", fc1->get_input()->eval()->get(1));
	
	printf("i1 = %.20f\n", fc1->get_output()->eval()->get(0));
        printf("i2 = %.20f\n", fc1->get_output()->eval()->get(1));

	printf("h1 = %.8f\n", act1->get_input()->eval()->get(0));
        printf("h2 = %f\n", act1->get_input()->eval()->get(1));

	printf("h1 = %f\n", act1->get_output()->eval()->get(0));
        printf("h2 = %f\n", act1->get_output()->eval()->get(1));

        printf("o1 = %f\n", output->out()->eval()->get(0));
        printf("o2 = %f\n", output->out()->eval()->get(1));
*///ways to print out different values

	printf("w1 = %f\n", fc1->get_weights()[0]->eval()->get({0,0}));
	printf("w2 = %f\n", fc1->get_weights()[0]->eval()->get({1,0}));
	printf("w3 = %f\n", fc1->get_weights()[0]->eval()->get({0,1}));
	printf("w4 = %f\n", fc1->get_weights()[0]->eval()->get({1,1}));
	
	printf("b1 = %f\n", fc1->get_weights()[1]->eval()->get({0}));


	// Clean up memory after training
	delete output;

	// Every magmadnn program should call magmadnn_finalize before
	// exiting
	magmadnn_finalize();

	return 0;
}
