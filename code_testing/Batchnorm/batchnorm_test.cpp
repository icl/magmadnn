/* 10-19-21
 * Program to test if bachnorm works as intended
 */

#include <cstdio>
#include "magmadnn.h"
#include <vector>

using namespace magmadnn;

int main() {
    
	using T = float;
	memory_t mem = DEVICE; 

	magmadnn_init();
	
	auto input_data = Tensor<T> ({2, 2, 2, 2}, {CONSTANT, {1.0f}}, mem);
	//sets 3x3 tensor with all 1f

	input_data.set(0, 1.0f);
	input_data.set(1, 2.0f);
	input_data.set(2, 3.0f);
	input_data.set(3, 4.0f);
	input_data.set(4, 5.0f);
	input_data.set(5, 6.0f);
	input_data.set(6, 7.0f);
	input_data.set(7, 8.0f);
	input_data.set(8, 9.0f);
	input_data.set(9, 10.0f);
	input_data.set(10, 11.0f);
	input_data.set(11, 12.0f);
	input_data.set(12, 13.0f);
	input_data.set(13, 14.0f);
	input_data.set(14, 15.0f);
	input_data.set(15, 16.0f);

    // for (int itr = 0; itr < 16; itr++) {
	// 	printf("cnn input = %f\n", input_data.get(itr));
	// }
	auto target = Tensor<T> ({2, 2, 2, 2}, {CONSTANT, {1.0f}}, mem);

	model::nn_params_t params; 
	//initializes the parameters for the NN
	
	params.batch_size = 2;
	params.n_epochs = 2;
	params.momentum = 0;
	params.learning_rate = 0.001;

	// Create a variable (of type T) with size (batch_size x
    // input size) This will serve as the input to our network.
	auto x_batch = op::var<T>(
			"x_batch", //name of variable
			{params.batch_size, 2,2,2},
			//batch_size, channels, row , column)
			{NONE, {}}, mem); //tensor fill type and memory
			

	auto input = layer::input(x_batch); // input layer, call to start model
	// auto conv = layer::conv2d(input->out(), {3, 3}, 2, {1,1}, {1,1}, {1,1}, false, false);
	auto conv = layer::fullyconnected(input->out(), 8);

	auto bn = layer::batchnorm(conv->out());
	auto flatten = layer::flatten(bn->out());

	auto output = layer::output(flatten->out());

	std::vector<layer::Layer<float> *> layers = 
	{   input,
	conv,
		bn,
		flatten,
		output}; //creates the vector of layers
	
	model::NeuralNetwork<float> model(layers, //vector of layers
		       	optimizer::MSE,//loss function
		       	optimizer::SGD, params);//optimizer function, model parameters set above

	model::metric_t metrics;

	model.fit(&input_data, &target, metrics, true);
	model.summary();

	// for (int itr = 0; itr < 16; itr++) {
	// 	printf("output = %f\n", bn->get_output()->eval()->get(itr));
	// }
/*	for (int itr = 0; itr < 9; itr++) {
		printf("cnn input = %f\n", conv2d->get_input()->eval()->get(itr));
	}
	for (int itr = 0; itr < 4; itr++) {
		printf("cnn out = %f\n", conv2d->get_output()->eval()->get(itr));
		printf("values = %f\n", add_bias->eval()->get(itr));
	}


	printf("final output = %g\n", fc2->get_output()->eval()->get(0));
	printf("final output = %g\n", fc2->get_output()->eval()->get(1));
	printf("final output = %g\n", act3->get_output()->eval()->get(0));
	printf("final output = %g\n", act3->get_output()->eval()->get(1));
*/
	delete output; //deletes the head node
	magmadnn_finalize(); //call at end of code
	return 0;
}
