/**
 * @file get_png_data.cpp
 * @brief Testing to see if the images read in the right way
 * @version 0.1
 * @copyright Copyright (c) 2022
 *
 */

#include "magmadnn.h"
#include <dirent.h>

#include <iostream>

using namespace magmadnn;

int main(int argc, char **argv)
{
    // Data type
    using T = float;

    magmadnn_init();

    unsigned int height = 300;
    unsigned int width = 500;
    unsigned int n_images = 1;

    /* Create the testing and training sets */
    // Tensor<T> *train_x = new Tensor<T>({n_images, 1, height, width}, {ZERO, {}}, HOST);
    Tensor<T> *train_x = new Tensor<T>({1, 3, height, width}, {ZERO, {}}, HOST);


    std::string input = "./Data/test.png";
    std::string output = "./Data/tri_output.png";

    data::add_image_to_tensor(input, height, width, true, train_x, 0);
    // data::add_png_to_tensor(input.c_str(), true, train_x, 0);
    // data::add_trimap_to_tensor(input, height, width, true, train_x, 0);

    // io::print_tensor(train_x);

    // data::tensor_to_image_4d(output, height, width, true, train_x,0);
    data::tensor_to_png(output.c_str(), height, width, train_x, 0, false);
    // data::tensor_to_image_3d(output, height, width, true, train_x, 0);

    magmadnn_finalize();

    return 0;
}