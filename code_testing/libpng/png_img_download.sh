mkdir -p Data

#Download Data
gdown https://drive.google.com/uc?id=1cvmHfcKrcT9K1aOnujKDrSvPby0LzQpI

# extract files
tar -xf new_images_png.tar.gz

# move files to Data and then remove them
cp ./new_images/ ./Data/ -r
rm -r new_images
rm new_images_png.tar.gz