#include <dirent.h>
#include "magmadnn.h"

#include <iostream>

using namespace magmadnn;

int main(int argc, char **argv) {

    const char *input = "./Data/new_images/masks/Abyssinian_232.png";
    const char *output = "./Data/output.png";

    magmadnn_init();

    using T = float;

    unsigned int height = 256;
    unsigned int width = 256;
    unsigned int n_images = 1;

    int i = 0;

    Tensor<T> *test = new Tensor<T>({n_images, 1, height, width}, {ZERO, {}}, HOST);

    data::add_png_to_tensor(input, true, test, i);

    // io::print_tensor(test);
    // Tensor<T> *test_1 = new Tensor<T>({n_images, 3, 2, 2}, {CONSTANT, {0.5f}}, HOST);

    data::tensor_to_png(output, height, width, test, 0, true);

    magmadnn_finalize();
}