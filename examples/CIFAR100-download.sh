# Download CIFAR-10
wget https://www.cs.toronto.edu/~kriz/cifar-100-binary.tar.gz

# extract files
tar -xvf cifar-100-binary.tar.gz

# move files to Data and then remove them
mv ./cifar-100-binary/*.bin ./Data/
rm -r cifar-100-binary/
rm cifar-100-binary.tar.gz
