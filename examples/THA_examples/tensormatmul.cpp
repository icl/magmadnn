#include <cstdio>
#include "magmadnn.h"

using namespace magmadnn;

int main() {
	magmadnn_init();

	auto input = Tensor<float> ({3,3,3}, {NONE, {}}, HOST);
	
	for (int i = 0; i < 27; i++){
		input.set(i,i/100.0f);
	}

	auto second = Tensor<float> ({3,3}, {NONE, {}}, HOST);
	for(int i = 0; i < 9; i++){
		second.set(i,i/5.4f);
	}
	
	auto answer = Tensor<float> ({3,3,3}, {NONE, {}}, HOST);

	auto input_var = op::var("input_var", &input);
	auto answer_var = op::var("answer_var", &answer);
	auto second_var = op::var("second_var", &second);

	auto test = op::tensormatmul(1.0f, op::var("input_var", &input), op::var("second_var", &second), 0.0f, op::var("answer_var", &answer));
//	auto test = op::add(input_var,answer_var);
//	auto test = op::matmul(1.0f, op::var("input_var", &input), op::var("second_var", &second), 0.0f, &answer);
	auto print = test->eval();

	for(int j = 0; j < print->get_size(); j++){
		printf("%g%c",print->get(j), (j == print->get_size() - 1) ? '\n' : ',') ;
	}

	

	magmadnn_finalize();
	return 0;
}
