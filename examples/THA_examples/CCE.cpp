/*
CCE.cpp
 - The CCE example from the LAPENNA lecture notes.
 - Two values, -0.04 and -0.42, are passed through 2 fully connected layers, with sigmoid and softmax activations.
 - The fully connected layers have  weights and  bias each
 	- weights: -2.5, -1.5, 0.6, 0.4, -0.1, 2.4, -2.2, -1.5, -5.2, 3.7
	- biases: -1.6, 0.7 (we average to -0.45) and 0, 0, 1 (we average to 0.33)
 - The target output is (0, 1, 0)
 - We then backpropagate once and adjust the weights and biases.
*/
#include <cstdio>
#include <vector>
#include "magmadnn.h"

/* tell the compiler we're using functions from the magmadnn namespace */
using namespace magmadnn;

int main(int argc, char **argv) {

	using T = float;

	magmadnn_init();

	memory_t mem = DEVICE; //declares the memory we are going to use

	auto input_data = Tensor<T> ({1,2,1}, {NONE, {}}, mem);//creates input tensor
	input_data.set({0,0,0}, -0.04f);//fills the tensors witht he values we want
	input_data.set({0,1,0}, -0.42f);
	
	//creates the target tensor and fills with the target values
	auto target = Tensor<T> ({1,3,1}, {CONSTANT, {0.0f}}, mem);
	target.set({0,0,0}, 0.0f);
	target.set({0,0,1}, 1.0f);
	target.set({0,0,2}, 0.0f);


	// Initialize our model parameters
	model::nn_params_t params;
	params.batch_size = 1;
	params.n_epochs = 1;
	params.learning_rate = 0.5;
	params.momentum = 0;

	// Create a variable (of type T) with size (batch_size x
	// input size) This will serve as the input to our network.
	auto x_batch = op::var<T>(
			"x_batch",//name of variable
			{params.batch_size, 1, 2, 1},//tensor size
			{NONE, {}}, mem); //fill type and memory 

	// Initialize the layers in our network
	auto input = layer::input(x_batch); //input layer, call to start model

	auto flatten = layer::flatten(input->out());
	auto fc1 = layer::fullyconnected(flatten->out(), 2, true); //(input layer, number of neurons, if use bias)
	
	fc1->get_weights()[0]->eval()->set({0,0}, -2.5f); // sets the weights
	fc1->get_weights()[0]->eval()->set({0,1}, -1.5f);
	fc1->get_weights()[0]->eval()->set({1,0}, 0.6f);
	fc1->get_weights()[0]->eval()->set({1,1}, 0.4f);
	fc1->get_weights()[1]->eval()->set(0, -0.45f); // sets the bias


	auto act1 = layer::activation(fc1->out(), layer::SIGMOID); //(input layer, activation function)
	auto fc2 = layer::fullyconnected(act1->out(), 3, true);

	fc2->get_weights()[0]->eval()->set(0, -0.1f); // sets weights through index
	fc2->get_weights()[0]->eval()->set(1, 2.4f);
	fc2->get_weights()[0]->eval()->set(2, -2.2f);
	fc2->get_weights()[0]->eval()->set(3, -1.5f);
	fc2->get_weights()[0]->eval()->set(4, -5.2f);
	fc2->get_weights()[0]->eval()->set(5, 3.7f);
	fc2->get_weights()[1]->eval()->set(0, 0.33f); // sets the bias

	auto act2 = layer::activation(fc2->out(), layer::SOFTMAX);

	auto output = layer::output(act2->out()); //output layer, call at end of the model


	// Wrap each layer in a vector of layers to pass to the model
	std::vector<layer::Layer<T> *> layers =
	{	input,
		flatten,
		fc1, act1,
		fc2, act2,
		output};

	//creates model wirth arguments of 
	//(vector of layers created above, loss funtion, optimizer type, parameters we set above)
	model::NeuralNetwork<T> model(layers, optimizer::CROSS_ENTROPY, optimizer::SGD, params);

	// metric_t records the model metrics such as accuracy, loss, and
	// training time
	model::metric_t metrics;

	model.fit(&input_data, &target, metrics, true);
	//(input data address,ground truth address, metrics, bool if print metrics for each epoch)
	model.summary();
/*
	printf("w1 = %.10f\n", fc1->get_weights()[0]->eval()->get({0,0}));
        printf("w2 = %.10f\n", fc1->get_weights()[0]->eval()->get({0,1}));
        printf("w3 = %.10f\n", fc1->get_weights()[0]->eval()->get({1,0}));
        printf("w4 = %.10f\n", fc1->get_weights()[0]->eval()->get({1,1}));

	printf("i1 = %.20f\n", fc1->get_input()->eval()->get(0));
        printf("i2 = %.20f\n", fc1->get_input()->eval()->get(1));
	
	printf("i1 = %.20f\n", fc1->get_output()->eval()->get(0));
        printf("i2 = %.20f\n", fc1->get_output()->eval()->get(1));

	printf("h1 = %.8f\n", act1->get_input()->eval()->get(0));
        printf("h2 = %f\n", act1->get_input()->eval()->get(1));

	printf("h1 = %f\n", act1->get_output()->eval()->get(0));
        printf("h2 = %f\n", act1->get_output()->eval()->get(1));

        printf("o1 = %f\n", output->out()->eval()->get(0));
        printf("o2 = %f\n", output->out()->eval()->get(1));
*///ways to print out different values
	
        printf("o1 = %f\n", output->out()->eval()->get(0));
        printf("o2 = %f\n", output->out()->eval()->get(1));
        printf("o3 = %f\n", output->out()->eval()->get(2));

	printf("w1 = %f\n", fc1->get_weights()[0]->eval()->get({0,0}));
	printf("w2 = %f\n", fc1->get_weights()[0]->eval()->get({0,1}));
	printf("w3 = %f\n", fc1->get_weights()[0]->eval()->get({1,0}));
	printf("w4 = %f\n", fc1->get_weights()[0]->eval()->get({1,1}));
	
	printf("w5 = %f\n", fc2->get_weights()[0]->eval()->get(0));
	printf("w6 = %f\n", fc2->get_weights()[0]->eval()->get(1));
	printf("w7 = %f\n", fc2->get_weights()[0]->eval()->get(2));
	printf("w8 = %f\n", fc2->get_weights()[0]->eval()->get(3));
	printf("w9 = %f\n", fc2->get_weights()[0]->eval()->get(4));
	printf("w10 = %f\n", fc2->get_weights()[0]->eval()->get(5));
	
	printf("b1 = %f\n", fc1->get_weights()[1]->eval()->get({0}));
	printf("b2 = %f\n", fc2->get_weights()[1]->eval()->get({0}));

	// Clean up memory after training
	delete output;

	// Every magmadnn program should call magmadnn_finalize before
	// exiting
	magmadnn_finalize();

	return 0;
}
