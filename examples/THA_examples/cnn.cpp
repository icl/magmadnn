/*
cnn.cpp
 - The CNN example from the LAPENNA lecture notes.
 - A 3x3 matrix of 0s and 1s is convolved with a 2x2 filter and passed through 2 fully connected layers and then a softmax activation function.
 - We then backpropagate once and adjust the weights and biases.
*/
#include <cstdio>
#include "magmadnn.h"
#include <vector>

using namespace magmadnn;

int main() {
    
	using T = float;
	memory_t mem = DEVICE; 
	// memory is on device since there is a conv2d layer

	magmadnn_init();
	
	auto input_data = Tensor<T> ({1, 3, 3}, {CONSTANT, {1.0f}}, mem);
	//sets 3x3 tensor with all 1f
	
	input_data.set({0,0,1}, 0.0f);
	input_data.set({0,1,0}, 0.0f);
	input_data.set({0,1,2}, 0.0f);
	input_data.set({0,2,1}, 0.0f);
	//sets the tensor values to 0 where needed

	auto target = Tensor<T> ({1, 1, 2}, {CONSTANT, {1.0f}}, mem);
	target.set({0,0,1}, 0.0f);	
    //sets the ground truth tensor

	model::nn_params_t params; 
	//initializes the parameters for the NN
	
	params.batch_size = 1;
	params.n_epochs = 1;
	params.momentum = 0;
	params.learning_rate = 0.5;

	// Create a variable (of type T) with size (batch_size x
    // input size) This will serve as the input to our network.
	auto x_batch = op::var<T>(
			"x_batch", //name of variable
			{params.batch_size, 1, 3, 3},
			//batch_size, channels, row , column)
			{NONE, {}}, mem); //tensor fill type and memory
			
	auto input = layer::input(x_batch); // input layer, call to start model

	auto conv2d = layer::conv2d(input->out(), {2, 2}, 1, {0,0}, {1,1}, {1,1}, true, false);//2d conv layer
	//input arguments(input layer,filter_shape, out_channels, padding, strides, dilation_rates, bool use_cross_correlation, bool use_bias (not implemented yet, keep as false))
	conv2d->get_weights()[0]->eval()->set(0, 0.9); // sets the weights for the layer
	conv2d->get_weights()[0]->eval()->set(1, 0.1);
	conv2d->get_weights()[0]->eval()->set(2, 0.1);
	conv2d->get_weights()[0]->eval()->set(3, 0.9);
	
	auto bias = op::var("bias", std::vector<unsigned int> {1}, tensor_filler_t<T> {CONSTANT, {0.1f}}, mem);
	auto add_bias = op::add(conv2d->out(), bias); 
	//add bias for this example since conv2d does not support it yet, 
	//therefore it is better if no bias is used on conv layer since the bias cant update

	auto act1 = layer::activation(add_bias, layer::RELU); //(input layer, activation function)

	auto flatten = layer::flatten(act1->out()); //flattens to a (1xn) vector
	
	auto fc1 = layer::fullyconnected(flatten->out(), 2, true); //(input layer, number of neurons, if use bias)

	fc1->get_weights()[0]->eval()->set({0,0}, 0.1); //sets the weights, weight matrix is rowmajor
	fc1->get_weights()[0]->eval()->set({1,0}, 0.2); //with the matmul calculation being input x weights
	fc1->get_weights()[0]->eval()->set({2,0}, 0.3);
	fc1->get_weights()[0]->eval()->set({3,0}, 0.4);
	fc1->get_weights()[0]->eval()->set({0,1}, 0.5);
	fc1->get_weights()[0]->eval()->set({1,1}, 0.6);
	fc1->get_weights()[0]->eval()->set({2,1}, 0.7);
	fc1->get_weights()[0]->eval()->set({3,1}, 0.8);
	fc1->get_weights()[1]->eval()->set(0, 0.2); //sets the bias


	auto act2 = layer::activation(fc1->out(), layer::RELU); //(input layer, activation function)

	auto fc2 = layer::fullyconnected(act2->out(), 2, true);
	fc2->get_weights()[0]->eval()->set({0,0}, 0.1);
	fc2->get_weights()[0]->eval()->set({1,0}, 0.2);
	fc2->get_weights()[0]->eval()->set({0,1}, 0.3);
	fc2->get_weights()[0]->eval()->set({1,1}, 0.4);
	fc2->get_weights()[1]->eval()->set(0, 0.3);

	auto act3 = layer::activation(fc2->out(), layer::SOFTMAX);

	auto output = layer::output(act3->out());
	std::vector<layer::Layer<float> *> layers =
	{   input,
		conv2d, act1, flatten,
		fc1, act2,
		fc2, act3,
		output}; //creates the vector of layers
	
	model::NeuralNetwork<float> model(layers, //vector of layers
		       	optimizer::CROSS_ENTROPY,//loss function
		       	optimizer::SGD, params);//optimizer function, model parameters set above

	model::metric_t metrics;

	model.fit(&input_data, &target, metrics, true);//(input data address,ground truth address, metrics,bool if print metrics for each epoch)
	model.summary();
/*	for (int itr = 0; itr < 9; itr++) {
		printf("cnn input = %f\n", conv2d->get_input()->eval()->get(itr));
	}
	for (int itr = 0; itr < 4; itr++) {
		printf("cnn out = %f\n", conv2d->get_output()->eval()->get(itr));
		printf("values = %f\n", add_bias->eval()->get(itr));
	}
*/	printf("\nfilter w1 = %f\n", conv2d->get_weights()[0]->eval()->get(0));
	printf("filter w2 = %f\n", conv2d->get_weights()[0]->eval()->get(1));
	printf("filter w3 = %f\n", conv2d->get_weights()[0]->eval()->get(2));
	printf("filter w4 = %f\n\n", conv2d->get_weights()[0]->eval()->get(3));

	printf("w1 = %f\n", fc1->get_weights()[0]->eval()->get(0));
	printf("w2 = %f\n", fc1->get_weights()[0]->eval()->get(1));
	printf("w3 = %f\n", fc1->get_weights()[0]->eval()->get(2));
	printf("w4 = %f\n", fc1->get_weights()[0]->eval()->get(3));
	printf("w5 = %f\n", fc1->get_weights()[0]->eval()->get(4));
	printf("w6 = %f\n", fc1->get_weights()[0]->eval()->get(5));
	printf("w7 = %f\n", fc1->get_weights()[0]->eval()->get(6));
	printf("w8 = %f\n", fc1->get_weights()[0]->eval()->get(7));
	printf("b1 = %f\n\n", fc1->get_weights()[1]->eval()->get(0));

	printf("w1 = %f\n", fc2->get_weights()[0]->eval()->get(0));
	printf("w2 = %f\n", fc2->get_weights()[0]->eval()->get(1));
	printf("w3 = %f\n", fc2->get_weights()[0]->eval()->get(2));
	printf("w4 = %f\n", fc2->get_weights()[0]->eval()->get(3));
	printf("b2 = %f\n\n", fc2->get_weights()[1]->eval()->get(0));


/*	printf("final output = %g\n", fc2->get_output()->eval()->get(0));
	printf("final output = %g\n", fc2->get_output()->eval()->get(1));
	printf("final output = %g\n", act3->get_output()->eval()->get(0));
	printf("final output = %g\n", act3->get_output()->eval()->get(1));
*/
	delete output; //deletes the head node
	magmadnn_finalize(); //call at end of code
	return 0;
}
