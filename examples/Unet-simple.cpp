// Simple example of U-Net predicting a 4x4 matrix.

#include "magmadnn.h"
#include <dirent.h>

#include <iostream>

using namespace magmadnn;

int main(int argc, char **argv)
{

    std::string context = " Unet ";

    // Data type
    using T = float;

#if defined(MAGMADNN_HAVE_MPI)
    MPI_Init(&argc, &argv);
#endif

    magmadnn_init();
    auto image = Tensor<T>({2048, 1, 4, 4}, {UNIFORM, {0.0f, 1.0f}});
    auto gt = Tensor<T>({2048, 1, 4, 4}, {ZERO, {}});
    auto testing_image = Tensor<T>({1, 1, 4, 4}, {UNIFORM, {0.0f, 1.0f}});
    auto testing_gt = Tensor<T>{{1, 1, 4, 4}, {UNIFORM, {0.0f, 1.0f}}};

    for (unsigned i = 0; i < image.get_size(); i++)
    {
        if (image.get(i) >= 0.5)
            gt.set(i, static_cast<T>(1));
        else
            gt.set(i, static_cast<T>(0));
    }

    for (unsigned i = 0; i < testing_image.get_size(); i++)
    {
        if (testing_image.get(i) >= 0.5)
            testing_gt.set(i, static_cast<T>(1));
        else
            testing_gt.set(i, static_cast<T>(0));
    }
    unsigned int height = 4;
    unsigned int width = 4;

    // Memory
    magmadnn::memory_t training_memory_type;
#if defined(MAGMADNN_HAVE_CUDA)
    training_memory_type = DEVICE;
#else
    training_memory_type = HOST;
#endif

    // Training parameters
    magmadnn::model::nn_params_t params;
    params.batch_size = 1;
    params.n_epochs = 50;
    params.learning_rate = 1e-4;
    params.n_objects = 2; // number of objects in the ground truth images
    unsigned int n_channels = 1;

#if defined(MAGMADNN_HAVE_CUDA)
    int devid = 0;
    // cudaSetDevice(1);
    cudaGetDevice(&devid);
    std::cout << "[" << context << "] GPU training (" << devid << ")" << std::endl;
#else
    printf("Cannot run on CPU.\n");
    exit(EXIT_FAILURE);
#endif

    std::cout << "[" << context << "] Image dimensions: " << height << " x " << width << std::endl;
    std::cout << "[" << context << "] Number of chanels: " << n_channels << std::endl;
    std::cout << "[" << context << "] Number of classes: "
              << "1" << std::endl;

    auto x_batch = op::var<T>(
        "x_batch",
        {params.batch_size, n_channels, height, width},
        {NONE, {}},
        training_memory_type);

    // stores a skip layer from the encoder, initilize it to the input.

    auto input = layer::input<T>(x_batch); // input layer
    auto conv1 = layer::conv2d(input->out(), {3, 3}, 1, layer::SAME);
    auto act1 = layer::activation(conv1->out(), layer::RELU);
    auto conv2 = layer::conv2d(act1->out(), {3, 3}, 1, layer::SAME);
    auto act2 = layer::activation(conv2->out(), layer::RELU);
    auto pooling1 = layer::pooling(act2->out(), {2, 2}, {0, 0}, {2, 2}, MAX_POOL);
    auto conv3 = layer::conv2d(pooling1->out(), {3, 3}, 2, layer::SAME);
    auto act3 = layer::activation(conv3->out(), layer::RELU);
    auto conv6 = layer::conv2d(act3->out(), {3, 3}, 2, layer::SAME);
    auto act6 = layer::activation(conv6->out(), layer::RELU);
    auto up_scale = layer::conv2dtranspose(act6->out(), {3, 3}, 2);
    auto up_scale2 = layer::conv2d(up_scale->out(), {3, 3}, 1, layer::SAME);
    auto shortcut = layer::shortcut(up_scale2->out(), act2->out());
    auto conv4 = layer::conv2d(shortcut->out(), {3, 3}, 1, layer::SAME);
    auto act4 = layer::activation(conv4->out(), layer::RELU);
    auto conv5 = layer::conv2d(act4->out(), {3, 3}, 1, layer::SAME);
    auto act = layer::activation(conv5->out(), layer::SIGMOID);
    auto output = layer::output<T>(act->out());

    std::vector<layer::Layer<T> *> layers = {input, conv1, act1, conv2, act2, pooling1, conv3, act3, conv6, act6, up_scale, up_scale2, shortcut, conv4, act4, conv4, act4, conv5, act, output};

    model::NeuralNetwork<T> model(layers, optimizer::DIST_AWARE_CE, optimizer::ADAM, params);

    model::metric_t metrics;
    model.summary();
    model.fit(&image, &gt, metrics, true);
    auto pred_out = model.predict(&testing_gt);
    for (unsigned i = 0; i < pred_out->get_size(); i++)
    {
        if (pred_out->get(i) > 0.5)
            pred_out->set(i, static_cast<T>(1));
        else
            pred_out->set(i, static_cast<T>(0));
    }
    io::print_tensor(pred_out);
    io::print_tensor(&testing_gt);

    magmadnn_finalize();

#if defined(MAGMADNN_HAVE_MPI)
    MPI_Finalize();
#endif

    return 0;
}