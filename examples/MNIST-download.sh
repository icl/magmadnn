# install gdown
pip3 install gdown

# gdown MNIST tar
gdown https://drive.google.com/uc?id=1_kzlu_vx3N_ia4KR0sXPmITBPPh5dgnE

# extract files
tar -xvf mnist-dataset.tar.gz
cp mnist-dataset/* ./Data
rm -r mnist-dataset
rm mnist-dataset.tar.gz

