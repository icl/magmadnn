# Examples
Several examples for getting familiar with MagmaDNN.

### Datasets
**MNIST DATASET** - to download the dataset, run `sh MNIST-download.sh`
* Data/train-images-idx3-ubyte - 45M
* Data/train-labels-idx1-ubyte - 59K
* Data/t10k-images-idx3-ubyte - 7.5M
* Data/t10k-labels-idx1-ubyte - 9.8K

**CIFAR-10 DATASET** - to download the dataset, run `sh CIFAR10-download.sh`
* Data/data_batch_1.bin - 30M
* Data/data_batch_2.bin - 30M
* Data/data_batch_3.bin - 30M
* Data/data_batch_4.bin - 30M
* Data/data_batch_5.bin - 30M
* Data/test_batch.bin - 30M

**CIFAR-100 DATASET** - to download the dataset, run `sh CIFAR100-download.sh`
* Data/train.bin - 147M
* Data/test.bin - 30M

**OXFORD PETS DATASET** - to download the dataset, run `bash oxford_iiit_pet-download.sh`

### Running the Example
To compile the examples, run `make examples` in `../`, the MagmaDNN directory
To run the examples, run `./bin/program_name`

### MNIST Examples
**simple_network.cpp** - simple neural network for MNIST dataset that uses the 4 files in ./Data/ - trains for 10 epochs

**mnist_interactive.cpp** - simple program where you can view, train, and test the MNIST dataset

	usage: ./bin/mnist_interactive <path-to-mnist-training-data> <path-to-mnist-training-labels> <use_gpu(optional):y or n> 

**cnn_2d.cpp** - MNIST dataset with Convolution layer - trains for 20 epochs

**lenet5.cpp** - MNIST dataset with LeNet-5 - trains for 10 epochs

### CIFAR-10 Examples
**cifar10_interactive.cpp** - program where you can view, train, and predict with the CIFAR-10 dataset and switch batches

**resnet_cifar10.cpp** - resnet with CIFAR-10 - trains for 50 epochs

**resnet.cpp** - resnet with CIFAR-10 - trains for 500 epochs

**vgg16.cpp** - vgg16 with CIFAR-10 - trains for 100 epochs

### CIFAR-100 Examples
**alexnet.cpp** - CIFAR-100 dataset with alexnet - trains for 500 epochs

### Unet Examples

For the Unet codes with image outputs, the images will be located in the `Data/results_30` folder

**Unet-5.cpp** - Full Unet structure with 4 endoder/decoder blocks with SGD (800 images)

**Unet-6.cpp** - Full Unet structure with 4 endoder/decoder blocks with SGD (31 images)

**Unet-7.cpp** - Full Unet structure with 4 endoder/decoder blocks with ADAM (31 images)

**Unet-simple.cpp** - Unet structure with two encoder/decoder blocks predicting a 4x4 tensor

### Other Examples
**tensor_math.cpp** - creates tensors A, x, and b and calculates Ax+b and prints the result

