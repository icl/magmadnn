mkdir -p ./Data/annotation
mkdir -p ./Data/image
mkdir -p ./Data/testing
mkdir -p ./Data/image/images
mkdir -p ./Data/results_30
mkdir -p ./Data/results_800
mkdir -p ./Data/annotation/annotations
mkdir -p ./Data/annotation/annotations_800
mkdir -p ./Data/image/images_800
mkdir -p ./Data/annotation/annotations_30
mkdir -p ./Data/image/images_30
mkdir -p ./Data/testing/images_test_30
mkdir -p ./Data/testing/annotations_test_30
mkdir -p ./Data/testing/annotations_test_800
mkdir -p ./Data/testing/images_test_800

# Download Dataset
wget https://www.robots.ox.ac.uk/~vgg/data/pets/data/images.tar.gz

# extract files
tar -xf images.tar.gz

# move files to Data and then remove them
cp images/* ./Data/image/images
rm -r images/
rm images.tar.gz

#Download Groundtruth
wget https://www.robots.ox.ac.uk/~vgg/data/pets/data/annotations.tar.gz

# extract files
tar -xf annotations.tar.gz

# move files to Data and then remove them
cp annotations/trimaps/* ./Data/annotation/annotations
rm -r annotations/
rm annotations.tar.gz

shopt -s globstar

# Reduced to 800 annotations
cp ./Data/annotation/annotations/Abyssinian_*.png ./Data/annotation/annotations_800
cp ./Data/annotation/annotations/basset_hound_*.png ./Data/annotation/annotations_800
cp ./Data/annotation/annotations/beagle_*.png ./Data/annotation/annotations_800
cp ./Data/annotation/annotations/chihuahua_*.png ./Data/annotation/annotations_800
# Reduced to 30 annotations
cp ./Data/annotation/annotations/Abyssinian_2*.png ./Data/annotation/annotations_30
# Create ground truth for the testing set
cp ./Data/annotation/annotations/Abyssinian_3*.png ./Data/testing/annotations_test_30
cp ./Data/annotation/annotations/Sphynx_*.png ./Data/testing/annotations_test_800

# Reduced to 800 image dataset
cp ./Data/image/images/Abyssinian_*.jpg ./Data/image/images_800
cp ./Data/image/images/basset_hound_*.jpg ./Data/image/images_800
cp ./Data/image/images/beagle_*.jpg ./Data/image/images_800
cp ./Data/image/images/chihuahua_*.jpg ./Data/image/images_800
# Reduced to 30 image dataset
cp ./Data/image/images/Abyssinian_2*.jpg ./Data/image/images_30
# Create testing set
cp ./Data/image/images/Abyssinian_3*.jpg ./Data/testing/images_test_30
cp ./Data/image/images/Sphynx_*.jpg ./Data/testing/images_test_800


rm ./Data/annotation/annotations_800/Abyssinian_34.png
rm ./Data/image/images_800/Abyssinian_34.jpg
rm ./Data/image/images_800/beagle_116.jpg
rm ./Data/annotation/annotations_800/beagle_116.png
rm ./Data/image/images_800/chihuahua_121.jpg
rm ./Data/testing/annotations_test_30/Abyssinian_34.png
rm ./Data/testing/images_test_30/Abyssinian_34.jpg