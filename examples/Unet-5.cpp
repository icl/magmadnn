/**
 * @file Unet-5.cpp
 * @author Spencer Smith
 * @brief Testing the Oxford Pet data set on Unet. Using 4 encoder blocks with max pooling,
 *         one encoder block without max pooling, and then 4 decoder blocks followed
 *          by a 1x1 convolution. This one uses Sigmoid activation and SGD
 * @version 0.1
 * @date 2022-06-27
 * @copyright Copyright (c) 2022
 *
 */

#include "magmadnn.h"
#include "models/unet.h"
#include <dirent.h>

#include <iostream>

using namespace magmadnn;

int main(int argc, char **argv)
{

    std::string context = " Unet ";

    // Data type
    using T = float;

#if defined(MAGMADNN_HAVE_MPI)
    MPI_Init(&argc, &argv);
#endif

    magmadnn_init();

    unsigned int height = 256;
    unsigned int width = 256;
    unsigned int n_images = 797;
    unsigned int n_testing_images = 200;

    /* Create the testing and training sets */
    Tensor<T> *train_x = new Tensor<T>({n_images, 3, height, width}, {ZERO, {}}, HOST);
    Tensor<T> *train_y = new Tensor<T>({n_images, (unsigned)1, height, width}, {ZERO, {}}, HOST);
    Tensor<T> *test_x = new Tensor<T>({n_testing_images, (unsigned)3, height, width}, {ZERO, {}}, HOST);
    Tensor<T> *test_y = new Tensor<T>({n_testing_images, (unsigned)1, height, width}, {ZERO, {}}, HOST);

    std::string train_x_path = "./Data/image/images_800/";
    std::string train_y_path = "./Data/annotation/annotations_800/";
    std::string test_x_path = "./Data/testing/images_test_800/";
    std::string test_y_path = "./Data/testing/annotations_test_800/";
    DIR *dpdf;
    struct dirent *epdf;
    dpdf = opendir("./Data/image/images_800");
    if (dpdf != NULL)
    {
        printf("getting training images...\n");
        int i = 0;
        while (epdf = readdir(dpdf))
        {
            auto temp = std::string(epdf->d_name);
            temp = temp.substr(0, temp.length() - 4);
            if (temp == "." || temp == "..")
                continue;
            std::cout << "Image #" << i << ": " << temp << std::endl;
            std::string image = train_x_path + temp + ".jpg";
            std::string groundtruth = train_y_path + temp + ".png";
            data::add_image_to_tensor(image, height, width, true, train_x, i);
            data::add_trimap_to_tensor(groundtruth, height, width, true, train_y, i);
            i++;
        }
    }
    closedir(dpdf);

    dpdf = opendir("./Data/testing/images_test_800");
    if (dpdf != NULL)
    {
        printf("getting testing images...\n");
        int i = 0;
        while (epdf = readdir(dpdf))
        {
            auto temp = std::string(epdf->d_name);
            temp = temp.substr(0, temp.length() - 4);
            if (temp == "." || temp == "..")
                continue;
            std::cout << "Image #" << i << ": " << temp << std::endl;
            std::string image = test_x_path + temp + ".jpg";
            std::string groundtruth = test_y_path + temp + ".png";
            data::add_image_to_tensor(image, height, width, true, test_x, i);
            data::add_trimap_to_tensor(groundtruth, height, width, true, test_y, i);
            i++;
        }
    }
    closedir(dpdf);

    bool has_one = false;
    Tensor<T> *temp_CPU = new Tensor<T>({n_images, (unsigned)1, height, width}, {ONE, {}}, HOST);
    for (unsigned n = 0; n < n_images; n++)
    {
        for (unsigned int h = 0; h < height; h++)
        {
            for (unsigned int w = 0; w < width; w++)
            {
                auto gt_value = train_y->get({n, (unsigned)0, h, w});
                if (gt_value == static_cast<T>(2) || gt_value == static_cast<T>(3))
                    temp_CPU->set({n, (unsigned)0, h, w}, static_cast<T>(0));

                if (gt_value == static_cast<T>(1))
                    has_one = true;
            }
        }
        if (!has_one)
        {
            printf("Image #%d is all zeros\n", n);
        }
        has_one = false;
    }
    for (unsigned i = 0; i < test_y->get_size(); i++)
    {
        if (test_y->get(i) == static_cast<T>(2) || test_y->get(i) == static_cast<T>(3))
            test_y->set(i, static_cast<T>(0));
    }

    // Memory
    magmadnn::memory_t training_memory_type;
#if defined(MAGMADNN_HAVE_CUDA)
    training_memory_type = DEVICE;
#else
    training_memory_type = HOST;
#endif

    auto train_x_GPU = Tensor<T>({n_images, 3, height, width}, {ZERO, {}}, training_memory_type);
    auto train_y_GPU = Tensor<T>({n_images, 1, height, width}, {ZERO, {}}, training_memory_type);
    train_x_GPU.copy_from(*train_x);
    train_y_GPU.copy_from(*temp_CPU);
    delete temp_CPU;
    delete train_x;
    delete train_y;

    // data::tensor_to_image_4d("./Data/test.jpg", height, width, true, &train_x_GPU, 3);

    /* number of iterations. total epochs = n_iterations * n_epochs. */
    int n_iterations = 10;
    // Training parameters
    magmadnn::model::nn_params_t params;
    params.batch_size = 5;
    params.n_epochs = 10;
    params.learning_rate = 0.01;
    params.n_objects = 2; // number of objects in the ground truth images
    params.momentum = 0.9;
    unsigned int n_channels = train_x_GPU.get_shape(1);

#if defined(MAGMADNN_HAVE_CUDA)
    int devid = 0;
    // cudaSetDevice(1);
    cudaGetDevice(&devid);
    std::cout << "[" << context << "] GPU training (" << devid << ")" << std::endl;
#else
    printf("Cannot run on CPU.\n");
    exit(EXIT_FAILURE);
#endif

    std::cout << "[" << context << "] Image dimensions: " << height << " x " << width << std::endl;
    std::cout << "[" << context << "] Number of chanels: " << n_channels << std::endl;
    std::cout << "[" << context << "] Number of classes: "
              << "1" << std::endl;
    std::cout << "[" << context << "] Training set size: " << train_x_GPU.get_shape(0) << std::endl;

    auto x_batch = op::var<T>(
        "x_batch",
        {params.batch_size, n_channels, height, width},
        {NONE, {}},
        training_memory_type);

    // stores a skip layer from the encoder, initilize it to the input.
    layer::Layer<T> *skip1, *skip2, *skip3, *skip4;
    int n_filters = 32; // number of filters applied to convolutions
    float dropout_prob = 0.0;

    auto input = layer::input<T>(x_batch); // input layer

    /////////////////////////////       ENCODER       //////////////////////////

    printf("Calling EncoderMiniBlock #1...\n");
    auto encoder1 = Unet<T>::EncoderMiniBlock(input->out(), n_filters, dropout_prob, true);
    skip1 = encoder1.back(); encoder1.pop_back(); // get the skip connection, and remove it from the vector
    printf("Calling EncoderMiniBlock #2...\n");
    auto encoder2 = Unet<T>::EncoderMiniBlock(encoder1.back()->out(), n_filters * 2, dropout_prob, true);
    skip2 = encoder2.back(); encoder2.pop_back(); // get the skip connection, and remove it from the vector
    printf("Calling EncoderMiniBlock #3...\n");
    // no max pooling in the last encoder
    auto encoder3 = Unet<T>::EncoderMiniBlock(encoder2.back()->out(), n_filters * 4, dropout_prob, true);
    skip3 = encoder3.back(); encoder3.pop_back();
    printf("Calling EncoderMiniBlock #4...\n");
    auto encoder4 = Unet<T>::EncoderMiniBlock(encoder3.back()->out(), n_filters * 8, dropout_prob, true);
    skip4 = encoder4.back(); encoder4.pop_back();
    printf("Calling EncoderMiniBlock #5...\n");
    auto encoder5 = Unet<T>::EncoderMiniBlock(encoder4.back()->out(), n_filters * 16, dropout_prob, false);
    encoder5.pop_back();

    /////////////////////////////       DECODER         ///////////////////////////////////

    printf("Calling DecoderMiniBlock #1...\n");
    auto decoder1 = Unet<T>::DecoderMiniBlock(encoder5.back()->out(), skip4->out(), n_filters * 8);
    printf("Calling DecoderMiniBlock #2...\n");
    auto decoder2 = Unet<T>::DecoderMiniBlock(decoder1.back()->out(), skip3->out(), n_filters * 4);
    printf("Calling DecoderMiniBlock #3...\n");
    auto decoder3 = Unet<T>::DecoderMiniBlock(decoder2.back()->out(), skip2->out(), n_filters * 2);
    printf("Calling DecoderMiniBlock #4...\n");
    auto decoder4 = Unet<T>::DecoderMiniBlock(decoder3.back()->out(), skip1->out(), n_filters);

    ////////////////////////////        1X1 CONV        ////////////////////////////////////

    // apply a 1x1 convolution to get the upsampled image with the number of output filters
    printf("Applying 1x1 Convolution...\n");
    auto softConv = layer::conv2d(decoder4.back()->out(), {1, 1}, 1, layer::SAME);
    //auto bn = layer::batchnorm(softConv->out());
    auto act = layer::activation(softConv->out(), layer::SIGMOID);
    auto output = layer::output<T>(act->out());

    std::vector<layer::Layer<T> *> layers;
    layers.insert(std::end(layers), input);
    layers.insert(std::end(layers), std::begin(encoder1), std::end(encoder1));
    layers.insert(std::end(layers), std::begin(encoder2), std::end(encoder2));
    layers.insert(std::end(layers), std::begin(encoder3), std::end(encoder3));
    layers.insert(std::end(layers), std::begin(encoder4), std::end(encoder4));
    layers.insert(std::end(layers), std::begin(encoder5), std::end(encoder5));
    layers.insert(std::end(layers), std::begin(decoder1), std::end(decoder1));
    layers.insert(std::end(layers), std::begin(decoder2), std::end(decoder2));
    layers.insert(std::end(layers), std::begin(decoder3), std::end(decoder3));
    layers.insert(std::end(layers), std::begin(decoder4), std::end(decoder4));
    layers.insert(std::end(layers), softConv);
    //layers.push_back(bn);
    layers.insert(std::end(layers), act);
    layers.insert(std::end(layers), output);

    model::NeuralNetwork<T> model(layers, optimizer::DIST_AWARE_CE, optimizer::SGD, params);

    model::metric_t metrics;
    model.summary();

    /* run n number of iterations */
    for (int iter = 0; iter < n_iterations; iter++) {
        printf("\n\t\tIteration (%d/%d)\n", iter + 1, n_iterations);
        model.fit(&train_x_GPU, &train_y_GPU, metrics, true);

        /* run a prediction and test the accuracy */
        for (unsigned n = 0; n < n_testing_images; n++)
        {
            int n_correct = 0;
            Tensor<T> *host_sample = new Tensor<T>({1, 3, height, width}, {NONE, {}}, HOST);
            Tensor<T> *sample = new Tensor<T>({1, 3, height, width}, {NONE, {}}, training_memory_type);
            Tensor<T> *pred_out = new Tensor<T>({1, 1, height, width}, {NONE, {}}, training_memory_type);
            Tensor<T> *y = new Tensor<T>({1, 1, height, width}, {NONE, {}}, HOST);
            for (unsigned c = 0; c < 3; c++)
            {
                for (unsigned h = 0; h < height; h++)
                {
                    for (unsigned w = 0; w < width; w++)
                    {
                        host_sample->set({(unsigned)0, c, h, w}, test_x->get({n, c, h, w}));
                        y->set({(unsigned)0, (unsigned)0, h, w}, test_y->get({n, (unsigned)0, h, w}));
                    }
                }
            }
            sample->copy_from(*host_sample);
            pred_out = model.predict(sample);
            for (unsigned i = 0; i < pred_out->get_size(); i++)
            {
                if (pred_out->get(i) >= 0.5)
                    pred_out->set(i, static_cast<T>(1));
                else
                    pred_out->set(i, static_cast<T>(0));

                if (pred_out->get(i) == y->get(i))
                    n_correct++;
            }
            printf("Accuracy for Sample %d: %0.2f\n", n + 1, ((float)n_correct / (float)pred_out->get_size()));
            std::string pred_path = "./Data/results_800/UNET_SGD800_predicted" + std::to_string((iter * 1000) + n + 1) + ".jpg";
            std::string gt_path = "./Data/results_800/Unet_groundtruth" + std::to_string(n + 1) + ".jpg";
            data::tensor_to_image_3d(pred_path, height, width, true, pred_out, 0);
            data::tensor_to_image_3d(gt_path, height, width, true, test_y, n);
        }
    }

    magmadnn_finalize();

#if defined(MAGMADNN_HAVE_MPI)
    MPI_Finalize();
#endif

    return 0;
}