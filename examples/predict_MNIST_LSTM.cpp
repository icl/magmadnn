/**
 * @file predict_MNIST.cpp
 * @author Pierluigi Cambie-Fabris
 * @author Stephen Qiu
 * @version 1.0
 * @date 2022-07-18
 *
 */

#include "magmadnn.h"
#define MEM DEVICE
using namespace magmadnn;
using T = float;
int main( void ) {
	
	magmadnn_init();

	model::nn_params_t params;
	params.batch_size = 32;
//	params.batch_size = 1;
	params.n_epochs = 100;
	params.learning_rate = .1;
	params.momentum = 0;
	model::metric_t metric_out;

	std::string const mnist_dir = "./Data";

	magmadnn::data::MNIST<T> train_set(mnist_dir, magmadnn::data::Train/*Test*/);
	std::cout << "train_set values: " << train_set.images().get(100) << ' ' << train_set.images().get(1001) << std::endl;
	std::vector<unsigned> s1 = train_set.images().get_shape();
	std::vector<unsigned> s3 = train_set.labels().get_shape();

	std::cout << "\n train shape: " << s1[0] << "," << s1[1] << "," << s1[2] <<
		"," << s1[3] << "; dim: " << s1.size();
	std::cout << "\n train labels: " << s3[0] << "," << s3[1] << "; dim: " <<
		s3.size() << "\n";

	op::Variable<T> *x_batch = op::var<T>("x_batch", {params.batch_size, 28, 28},
			{NONE, {}}, MEM);


	
	layer::InputLayer<T> *input = layer::input(x_batch);

	
	layer::LSTM2Layer<T> *lstm1 = layer::lstm2(input->out(), 10, true);
	layer::FlattenLayer<T> * flatten = layer::flatten(/*input*/lstm1->out());
	layer::FullyConnectedLayer<T> *fc1 = layer::fullyconnected(/*lstm1->out()*//*input->out()*/flatten->out(), 28);
	layer::ActivationLayer<T> *a1 = layer::activation(fc1->out(), layer::RELU);
	layer::FullyConnectedLayer<T> *fc2 = layer::fullyconnected(/*lstm1*/a1->out()/*flatten->out()*/, 14);
	layer::ActivationLayer<T> *a2 = layer::activation(fc2->out(), layer::RELU);
	layer::FullyConnectedLayer<T> *fc3 = layer::fullyconnected(/*lstm1*/a2->out()/*flatten->out()*/, 10);
	layer::ActivationLayer<T> *a3 = layer::activation(fc3->out(), layer::SOFTMAX);

	layer::OutputLayer<T> *output = layer::output(/*a1*//*a2*/a3->out());

	std::vector<layer::Layer<T> *> layers = {input, lstm1, /*lstm2, lstm3,*/ /*flatten,*/ 
		fc1, a1, fc2, a2,  fc3, a3, output};

	model::NeuralNetwork<T> lstmModel(layers, optimizer::CROSS_ENTROPY,
			optimizer::SGD, params);
	
	lstmModel.summary();

	lstmModel.fit(&train_set.images(), &train_set.labels(), metric_out, true, true);

	
	magmadnn_finalize();
	return 0;
}
